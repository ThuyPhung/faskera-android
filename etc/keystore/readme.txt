View keystore details: 
	keytool -list -v -keystore debug.keystore
================================================================
Password:
	Debug: 	 android
==================
SHA1:
	Debug:	 77:13:6E:FE:57:A4:1D:AC:66:F5:9A:7C:DD:62:F9:AD:10:D6:2D:7F
FB Hash:
	Debug:   dxNu/lekHaxm9Zp83WL5rRDWLX8=
==================
keytool -genkey -v -keystore debug.keystore -alias androiddebugkey -storepass android -keypass android -keyalg RSA -keysize 2048 -validity 14000
==================
CN=Faskera, Inc., OU=Software Development, O=Faskera, Inc., L=Nam Tu Liem, ST=Ha Noi, C=VN