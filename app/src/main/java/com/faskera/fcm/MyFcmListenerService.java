/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.fcm;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;

import com.faskera.common.AppSharePreference;
import com.faskera.common.UserDefine;
import com.faskera.era.R;
import com.faskera.view.activity.mypage.ProfileActivity;
import com.faskera.view.activity.notification.NotificationContentUtils;
import com.faskera.view.activity.polldetail.PollDetailActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import static com.faskera.view.activity.mypage.ProfileActivity.BUNDLE_GO_THROUGH_NOTIFICATION;
import static com.faskera.view.activity.mypage.ProfileActivity.BUNDLE_TYPE_USER;
import static com.faskera.view.activity.mypage.ProfileActivity.BUNDLE_USER_ID;
import static com.faskera.view.activity.polldetail.PollDetailActivity.BUNDLE_POLL_ID;

/**
 * @author framgia
 * @since 13/02/2017
 */
public class MyFcmListenerService extends FirebaseMessagingService {
    public static final int MESSAGE_NOTIFICATION_ID = 435345;
    private static final String YOUR_FOLLOWING_CREATE_POLL = "your-following-create-poll";
    private static final String YOUR_FOLLOWING_SHARE_POLL = "your-following-share-poll";
    private static final String SOMEONE_ANSWER_YOUR_POLL = "someone-answer-your-poll";
    private static final String POLL_YOU_ANSWERED_WAS_CLOSED = "poll-you-answered-was-closed";
    private static final String SOMEONE_FOLLOW_YOU = "someone-follow-you";
    private static final String YOUR_FACEBOOK_FRIEND_START_USING = "your-facebook-friend-start-using";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();
        createNotification(data);
    }

    // Creates notification based on title and body received
    private void createNotification(Map<String, String> data) {
        Context context = getBaseContext();

        String action = data.get("action");
        String pollId = data.get("pollId");
        String user = data.get("user");
        String userId = data.get("userId");
        String pollQuote = data.get("pollQuote");

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setSmallIcon(R.drawable.ic_launcher);
        mBuilder.setContentTitle("Faskera");

        if (action != null) {
            CharSequence content = "";
            switch (action) {
                case YOUR_FOLLOWING_CREATE_POLL:
                    content = NotificationContentUtils.createPoll(context, user, pollQuote);
                    break;
                case YOUR_FOLLOWING_SHARE_POLL:
                    content = NotificationContentUtils.oneSharePoll(context, user, pollQuote);
                    break;
                case SOMEONE_ANSWER_YOUR_POLL:
                    content = NotificationContentUtils.oneVotePoll(context, user, pollQuote);
                    break;
                case SOMEONE_FOLLOW_YOU:
                    content = NotificationContentUtils.following(context, user);
                    break;
                case POLL_YOU_ANSWERED_WAS_CLOSED:
                    break;
                case YOUR_FACEBOOK_FRIEND_START_USING:
                    content = NotificationContentUtils.newFbFriendUsing(context, user);
                    break;
            }
            mBuilder.setContentText(content);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            Intent moveIntent = null;
            Bundle bundle = new Bundle();

            //Message about poll
            if (pollId != null) {
                moveIntent = new Intent(this, PollDetailActivity.class);
                bundle.putString(BUNDLE_POLL_ID, pollId);
                bundle.putBoolean(BUNDLE_GO_THROUGH_NOTIFICATION, true);
                stackBuilder.addParentStack(PollDetailActivity.class);
            }
            //Message about user
            if (userId != null) {
                moveIntent = new Intent(this, ProfileActivity.class);
                bundle.putInt(BUNDLE_TYPE_USER, UserDefine.USER_TYPE_FRIENDS);
                bundle.putString(BUNDLE_USER_ID, userId);
                bundle.putBoolean(BUNDLE_GO_THROUGH_NOTIFICATION, true);
                stackBuilder.addParentStack(ProfileActivity.class);
            }
            if (moveIntent != null) {
                moveIntent.putExtras(bundle);
                moveIntent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            }
            stackBuilder.addNextIntent(moveIntent);
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(
                            0,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            mBuilder.setContentIntent(resultPendingIntent);
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder.setSound(uri);
            mBuilder.setAutoCancel(true);
            mBuilder.setSmallIcon(R.drawable.ic_faskera_notification_icon);
            mBuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher));
            mBuilder.setColor(ContextCompat.getColor(context, R.color.color_app_icon));

            NotificationManager mNotificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(MESSAGE_NOTIFICATION_ID, mBuilder.build());

            AppSharePreference.getInstance(context).setNotificationCounter(1);

        }
    }
}
