/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.fcm;

import android.app.IntentService;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.faskera.common.AppSharePreference;
import com.faskera.common.model.BaseResponse;
import com.faskera.network.ApiManager;
import com.faskera.view.activity.notification.network.RegisterToken;
import com.google.firebase.iid.FirebaseInstanceId;

import rx.Observer;

import static java.net.HttpURLConnection.HTTP_OK;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class RegistrationIntentService extends IntentService {
    private static final String TAG = "RegIntentService";

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Make a call to Instance API
        FirebaseInstanceId instanceID = FirebaseInstanceId.getInstance();
        // Request token that will be used by the server to send push notifications
        String token = instanceID.getToken();
        sendRegistrationToServer(token);
    }

    private void sendRegistrationToServer(String token) {
        if (TextUtils.isEmpty(token)) {
            Log.e(TAG, "Fail to get push token");
            return;
        }
        RegisterToken registerToken = new RegisterToken(0, token);
        ApiManager.getInstance()
            .registerDevice(getBaseContext(), registerToken, new Observer<BaseResponse>() {
                @Override
                public void onCompleted() {
                    Log.d(TAG, "Register push token success");
                }

                @Override
                public void onError(Throwable e) {
                    Log.d(TAG, "Register push token error", e);
                }

                @Override
                public void onNext(BaseResponse response) {
                    if (response.code == HTTP_OK) {
                        AppSharePreference
                            .getInstance(getBaseContext())
                            .storeTokenNotification(token);
                    }
                }
            });
    }
}
