/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.model.option;

/**
 * @author theanh
 * @since 1/24/2017
 */

public class LanguageModel {
    private String code;
    private String name;
    private boolean isUsed;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public void setUsed(boolean used) {
        isUsed = used;
    }

    public LanguageModel(String code, String name, boolean isUsed) {
        this.code = code;
        this.name = name;
        this.isUsed = isUsed;
    }
}
