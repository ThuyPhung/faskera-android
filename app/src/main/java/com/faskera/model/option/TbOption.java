/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.model.option;

/**
 * @author theanh
 * @since 1/16/2017
 */

public class TbOption {

    public String mTitle;
    public boolean isEditMode;

    public TbOption(String mTitle, boolean isEditMode) {
        this.mTitle = mTitle;
        this.isEditMode = isEditMode;
    }
}
