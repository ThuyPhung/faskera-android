/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.model.createpoll;

public class TbCreatePoll {

    public String mTitle;
    public String mDone;

    public TbCreatePoll(String mTitle, String mDone) {
        this.mTitle = mTitle;
        this.mDone = mDone;
    }
}