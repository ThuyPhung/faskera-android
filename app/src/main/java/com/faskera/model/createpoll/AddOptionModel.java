/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.model.createpoll;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class AddOptionModel implements Serializable {

    @Expose
    public String text;

    public AddOptionModel(String text) {
        this.text = text;
    }
}
