/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.model.createpoll;

import com.google.gson.annotations.Expose;

import java.io.Serializable;


public class SettingPollModel implements Serializable {

    public SettingPollModel(){
        multiChoice = false;
        anonymous = false;
        shareFacebook = false;
        peopleToAdd = false;
    }

    @Expose
    private boolean multiChoice;

    @Expose
    private boolean anonymous;

    private boolean shareFacebook;

    private boolean peopleToAdd;

    public boolean isMultiChoice() {
        return multiChoice;
    }

    public void setMultiChoice(boolean multiChoice) {
        this.multiChoice = multiChoice;
    }

    public boolean isAnonymous() {
        return anonymous;
    }

    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

    public boolean isShareFacebook() {
        return shareFacebook;
    }

    public void setShareFacebook(boolean shareFacebook) {
        this.shareFacebook = shareFacebook;
    }

    public boolean isPeopleToAdd() {
        return peopleToAdd;
    }

    public void setPeopleToAdd(boolean peopleToAdd) {
        this.peopleToAdd = peopleToAdd;
    }
}
