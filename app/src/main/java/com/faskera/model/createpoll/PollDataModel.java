/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.model.createpoll;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.List;

public class PollDataModel implements Serializable {
    @Expose
    private List<AddOptionModel> options;

    @Expose
    private SettingPollModel settings;

    @Expose
    private QuestionPollModel question;

    @Expose
    private List<String> topicIds;

    public List<AddOptionModel> getOptions() {
        return options;
    }

    public void setOptions(List<AddOptionModel> options) {
        this.options = options;
    }

    public SettingPollModel getSettings() {
        return settings;
    }

    public void setSettings(SettingPollModel settings) {
        this.settings = settings;
    }

    public QuestionPollModel getQuestion() {
        return question;
    }

    public void setQuestion(QuestionPollModel question) {
        this.question = question;
    }

    public List<String> getTopicIds() {
        return topicIds;
    }

    public void setTopicIds(List<String> topicIds) {
        this.topicIds = topicIds;
    }
}
