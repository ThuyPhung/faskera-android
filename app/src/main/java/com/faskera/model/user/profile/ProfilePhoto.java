/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.model.user.profile;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author theanh
 * @since 1/3/2017
 */

public class ProfilePhoto implements Serializable {
    @SerializedName("original")
    public String original;
    @SerializedName("originalWidth")
    public int originalWidth;
    @SerializedName("originalHeight")
    public int originalHeight;
    @SerializedName("medium")
    public String medium;
    @SerializedName("mediumWidth")
    public int mediumWidth;
    @SerializedName("mediumHeight")
    public int mediumHeight;
    @SerializedName("small")
    public String small;
    @SerializedName("smallWidth")
    public int smallWidth;
    @SerializedName("smallHeight")
    public int smallHeight;
}
