/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.model.user.token;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * @author theanh
 * @since 2/4/2017
 */

public class OAuthToken {

    @SerializedName("access_token")
    public String accessToken;

    @SerializedName("token_type")
    public String tokenType;

    @SerializedName("expires_in")
    public String expiresIn;

    @SerializedName("scope")
    public String scope;

    @SerializedName("refresh_token")
    public String mRefreshToken;

    public String getAccessToken() {
        return accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public String getExpiresIn() {
        return expiresIn;
    }

    public String getScope() {
        return scope;
    }

    public static OAuthToken getOAuthToken(String accessToken) {
        return new Gson().fromJson(accessToken, OAuthToken.class);
    }

    public static String getOAuthTokenString(OAuthToken OAuthToken) {
        return new Gson().toJson(OAuthToken);
    }
}

