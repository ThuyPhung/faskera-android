/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.model.user.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author theanh
 * @since 1/12/2017
 */

public class UserStats {
    @SerializedName("nbPollCreated")
    @Expose
    public int nbPollCreated;
    @SerializedName("nbPollAnswered")
    @Expose
    public int nbPollAnswered;
    @SerializedName("nbFollower")
    @Expose
    public int nbFollower;
    @SerializedName("nbFollowing")
    @Expose
    public int nbFollowing;
}
