/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.model.user;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.faskera.common.AppSharePreference;
import com.faskera.common.UserDefine;
import com.faskera.common.adapter.AbsUserState;
import com.faskera.database.table.UserInfoTable;
import com.faskera.database.table.UserTable;
import com.faskera.era.R;
import com.faskera.view.activity.mypage.ProfileActivity;
import com.faskera.view.activity.mypage.abs.AbsInfoExtra;
import com.faskera.view.activity.mypage.abs.AbsProfile;
import com.faskera.view.activity.mypage.model.InfoExtra;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by DANGNGOCDUC on 1/13/2017.
 * <p>
 * Class nay dinh nghia thong tin Profile cua User phuc vu cho tran MyProfile va Friends
 * {@link ProfileActivity}
 * <p>
 * </p>
 */
public class Profile extends AbsProfile implements Serializable, AbsUserState{

    @SerializedName("id")
    public String mUid;

    @SerializedName("fullname")
    public String mFullName;

    @SerializedName("profilePhoto")
    public Photo mAvatar;

    @SerializedName("following")
    public boolean mIsFollowing;

    // // TODO: 1/13/2017  gender , status , birthday dang tra null chua check dc

    @SerializedName("about")
    public String mAbout;

    @SerializedName("organizationWork")
    public InfoExtra mOrganizationWork;

    @SerializedName("organizationStudy")
    public InfoExtra mOrganizationStudy;

    @SerializedName("placeLive")
    public InfoExtra mPlaceLive;

    @SerializedName("hometown")
    public InfoExtra mHometown;

    //// TODO: 1/13/2017 Country chưa rõ rạng trả về


    @SerializedName("userStats")
    public UserStat mUserStat;

    @Override
    public int getTypeProfile(Context context) {
        if (AppSharePreference.getUid(context).equals(mUid)) {
            return UserDefine.USER_TYPE_PROFILE;
        } else {
            return UserDefine.USER_TYPE_FRIENDS;
        }
    }

    @Override
    public int getStateFollow() {
        if (mIsFollowing) {
            return UserDefine.STATE_FOLLWING;
        } else {
            return UserDefine.STATE_FOLLOW;
        }
    }

    @Override
    public String getName() {
        return mFullName;
    }

    @Override
    public int getSate() {
        if (mIsFollowing) {
            return UserDefine.STATE_FOLLWING;
        } else {
            return UserDefine.STATE_FOLLOW;
        }
    }

    @Override
    public void setSate(@UserDefine.StateFollow int state) {
        mIsFollowing = state == UserDefine.STATE_FOLLWING;

    }

    @Override
    public String getAvatar() {
        if (mAvatar != null) {
            return mAvatar.original;
        } else {
            return null;
        }
    }

    @Override
    public String getUid() {
        return mUid;
    }

    @Override
    public int getNumberPoll() {
        return mUserStat.mNumberPollCreated;
    }

    @Override
    public int getNumberFollower() {
        return mUserStat.mNumberFollower;
    }

    @Override
    public int getNumberFollowing() {
        return mUserStat.mNumberFollowing;
    }

    @Override
    public String getIntroduction() {
        return mAbout;
    }

    @Override
    public String getUserId() {
        return mUid;
    }

    public ContentValues toContentValue() {
        Gson gson = new Gson();
        ContentValues contentValues = new ContentValues();
        contentValues.put(UserInfoTable._ID, mUid);
        contentValues.put(UserInfoTable.FOLLOWING, mIsFollowing);
        contentValues.put(UserInfoTable.DATA, gson.toJson(this));

        return contentValues;
    }

    public ArrayList<AbsInfoExtra> genListInfoExtra() {
        ArrayList<AbsInfoExtra> list = new ArrayList<>();

        if (mOrganizationWork != null) {
            mOrganizationWork.setIconSource(R.drawable.ic_work_png);
            list.add(mOrganizationWork);
        }

        if (mOrganizationStudy != null) {
            mOrganizationStudy.setIconSource(R.drawable.ic_school_png);
            list.add(mOrganizationStudy);
        }

        if (mHometown != null) {
            mHometown.setIconSource(R.drawable.ic_home_png);
            list.add(mHometown);
        }

        if (mPlaceLive != null) {
            mPlaceLive.setIconSource(R.drawable.ic_room_png);
            list.add(mPlaceLive);
        }



        return list;
    }

    public static Profile parseProfile(Cursor cursor) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        return gson.fromJson(cursor.getString(cursor.getColumnIndex(UserTable.DATA)), Profile.class);
    }


    public static class UserStat {

        @SerializedName("nbPollCreated")
        public int mNumberPollCreated;

        @SerializedName("nbPollAnswered")
        public int mNumberPollAnswered;

        @SerializedName("nbFollower")
        public int mNumberFollower;

        @SerializedName("nbFollowing")
        public int mNumberFollowing;


    }
}
