/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.model.user;

import com.faskera.common.UserDefine;
import com.faskera.common.adapter.AbsUserState;
import com.google.gson.annotations.SerializedName;

/**
 * @author theanh
 * @since 1/3/2017
 */

public class UserInfo implements AbsUserState {

    @SerializedName("id")
    public String id;
    @SerializedName("fullname")
    public String fullName;
    @SerializedName("profilePhoto")
    public Photo photo;
    @SerializedName("following")
    public boolean following;
    @SerializedName("about")
    public Object about;
    @SerializedName("language")
    public Object language;

    @Override
    public String getAvatar() {
        if (photo != null) {
            return photo.original;
        }
        return null;
    }

    @Override
    public String getUid() {
        return id;
    }

    @Override
    public String getName() {
        return fullName;
    }

    @Override
    public int getSate() {
        if (following) {
            return UserDefine.STATE_FOLLWING;
        } else {
            return UserDefine.STATE_FOLLOW;
        }

    }

    @Override
    public void setSate(@UserDefine.StateFollow int state) {
        following = state == UserDefine.STATE_FOLLWING;
    }

    @Override
    public int getType() {
        return TYPE_USER;
    }
}
