/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.model.user;

import android.content.ContentValues;

import com.faskera.database.table.UserTable;
import com.faskera.model.user.profile.HomeTown;
import com.faskera.model.user.profile.OrganizationStudy;
import com.faskera.model.user.profile.OrganizationWork;
import com.faskera.model.user.profile.PlaceLive;
import com.faskera.model.user.profile.ProfilePhoto;
import com.faskera.model.user.profile.UserStats;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author theanh
 * @since 1/12/2017
 */

public class MyProfile {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("fullname")
    @Expose
    public String fullname;
    @SerializedName("profilePhoto")
    @Expose
    public ProfilePhoto profilePhoto;
    @SerializedName("following")
    @Expose
    public boolean following;
    @SerializedName("gender")
    @Expose
    public Object gender;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("birthday")
    @Expose
    public Object birthday;
    @SerializedName("about")
    @Expose
    public String about;
    @SerializedName("organizationWork")
    @Expose
    public OrganizationWork organizationWork;
    @SerializedName("organizationStudy")
    @Expose
    public OrganizationStudy organizationStudy;
    @SerializedName("placeLive")
    @Expose
    public PlaceLive placeLive;
    @SerializedName("hometown")
    @Expose
    public HomeTown hometown;
    @SerializedName("country")
    @Expose
    public Object country;
    @SerializedName("userStats")
    @Expose
    public UserStats userStats;


    public ContentValues getContentValue() {
        Gson gson = new Gson();
        ContentValues contentValues = new ContentValues();
        contentValues.put(UserTable.USER_ID, id);
        contentValues.put(UserTable.FULL_NAME, fullname);
        contentValues.put(UserTable.PROFILE_PHOTO, gson.toJson(this.profilePhoto));
        contentValues.put(UserTable.DATA, gson.toJson(this));
        return contentValues;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public void setOrganizationStudy(OrganizationStudy organizationStudy) {
        this.organizationStudy = organizationStudy;
    }

    public void setOrganizationWork(OrganizationWork organizationWork) {
        this.organizationWork = organizationWork;
    }

    public void setHometown(HomeTown hometown) {
        this.hometown = hometown;
    }

    public void setPlaceLive(PlaceLive placeLive) {
        this.placeLive = placeLive;
    }

    public String getFullname() {
        return fullname;
    }

    public String getAbout() {
        return about;
    }

    public OrganizationWork getOrganizationWork() {
        return organizationWork;
    }

    public OrganizationStudy getOrganizationStudy() {
        return organizationStudy;
    }

    public PlaceLive getPlaceLive() {
        return placeLive;
    }

    public HomeTown getHometown() {
        return hometown;
    }
}
