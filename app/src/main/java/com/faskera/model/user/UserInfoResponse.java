/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.model.user;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author theanh
 * @since 1/3/2017
 */

public class UserInfoResponse implements Serializable {

    @SerializedName("id")
    public String id;

    @SerializedName("fullname")
    public String fullName;

    @SerializedName("profilePhoto")
    public Photo photo;

    @SerializedName("following")
    public boolean following;

    @SerializedName("about")
    public String about;

    @SerializedName("language")
    public String language;

    public static UserInfoResponse getUserInfo(String userInfo) {
        return new Gson().fromJson(userInfo, UserInfoResponse.class);
    }

    public static String getUserInfoString(UserInfoResponse userInfoResponse) {
        return new Gson().toJson(userInfoResponse);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public boolean isFollowing() {
        return following;
    }

    public void setFollowing(boolean following) {
        this.following = following;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
