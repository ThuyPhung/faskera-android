/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.network;

import com.faskera.common.model.BaseResponse;
import com.faskera.model.user.token.OAuthToken;
import com.faskera.network.poll.ReportPoll;
import com.faskera.network.user.MyProfileBody;
import com.faskera.view.activity.home.network.ListFeedReponse;
import com.faskera.view.activity.notification.network.RegisterToken;
import com.faskera.view.activity.option.view.fbfriend.model.ListFriendBody;
import com.faskera.view.activity.topic.model.UpdateRequestBody;
import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * @author DANGNGOCDUC
 * @since 1/3/2017
 */

public interface ImlServiceWrapper {

    //<editor-fold desc="Login">
    @FormUrlEncoded
    @POST("oauth/token")
    Observable<JsonObject> oauthFace(
            @Field("client_id") String clientId,
            @Field("client_secret") String clientSecret,
            @Field("grant_type") String grantType,
            @Field("access_token") String token,
            @Field("provider_id") String providerId
    );

    @GET("user/me/info")
    Observable<JsonObject> getUserInfo();

    //</editor-fold>

    //<editor-fold desc="Topic">
    @GET("user/me/interested/topic")
    Observable<JsonObject> getInterestedTopic(@Query("dl") String location);

    @GET("topic")
    Observable<JsonObject> getListTopic(@Query("dl") String location);

    @PUT("user/me/interested/topic")
    Observable<JsonObject> UpdateInterestTopic(@Header("Content-Type") String contentType, @Body UpdateRequestBody body);
    //</editor-fold>

    //<editor-fold desc="User">
    @GET("user/me/profile")
    Observable<JsonObject> getMyProfile();

    @PUT("user/me/profile")
    Observable<JsonObject> updateMyProfile(@Header("Content-Type") String contentType, @Body MyProfileBody myProfile);

    @Multipart
    @POST("user/me/profile-photo")
    Call<JsonObject> updateMyProfilePhoto(@Part MultipartBody.Part file);

    @GET("user/{uid}/profile")
    Observable<JsonObject> getInfoUser(@Path("uid") String uid);

    @DELETE("user/me/profile-photo")
    Observable<BaseResponse> removePhoto();

    //</editor-fold>

    //<editor-fold desc="Feed">

    @GET("feed/me?&count=20")
    Observable<Result<ListFeedReponse>> getHomeFeeds(@Query("max_id") String nextMaxId, @Query("dl") String location);

    @GET("user/{uid}/poll?count=20")
    Observable<JsonObject> getUserFeed(@Path("uid") String uid, @Query("max_id") String nextMaxId, @Query("dl") String location);

    //</editor-fold>

    //<editor-fold desc="Discovery People">
    @GET("user/me/friend/discovery")
    Observable<JsonObject> discoveryPeople();

    @PUT("user/me/friend/follow")
    Observable<JsonObject> followUser(@Query("follow_user_id") String userId);

    @PUT("user/me/friend/unfollow")
    Observable<JsonObject> unfollowUser(@Query("unfollow_user_id") String userId);

    @PUT("user/me/friend/follow-all")
    Observable<JsonObject> followAllUser(@Body ListFriendBody listUserId);
    //</editor-fold>

    @GET("poll/{pollId}/users-answered?count=20")
    Observable<JsonObject> getUserVoted(@Path("pollId") String pollId, @Query("max_id") String nextMaxId);

    @GET("user/{userId}/friend/{typeList}?count=20")
    Observable<JsonObject> getListUser(@Path("userId") String userId, @Path("typeList") String typeList, @Query("max_id") String nextMaxId);

    //<editor-fold desc="Notification>
    @GET("notification?&count=20")
    Observable<JsonObject> getNotifications(@Query("max_id") String nextMaxId, @Query("dl") String location);

    @POST("notification/device/register")
    Observable<BaseResponse> registerDevice(@Body RegisterToken token);

    @HTTP(method = "DELETE", path = "notification/device/unregister", hasBody = true)
    Observable<BaseResponse> unregisterDevice(@Body RegisterToken token);
    //</editor-fold>

    @GET("poll/{pollId}")
    Observable<JsonObject> getPollDetail(@Path("pollId") String pollId, @Query("dl") String location);

    @PUT("user/me/friend/unfollow")
    Observable<JsonObject> unFollowFriend(@Query("unfollow_user_id") String userId);

    @POST("poll-report")
    Observable<JsonObject> reportPoll(@Body ReportPoll reportPoll);

    @PUT("poll/{pollId}/answer")
    Observable<JsonObject> votePoll(@Path("pollId") String pollId, @Query("option_id") String query, @Query("select") boolean select);

    @GET("feed?count=20")
    Observable<Result<ListFeedReponse>> getHomeFeedsAnonymous(@Query("max_id") String nextMaxId, @Query("dl") String location);

    @GET("poll/users-chose?count=20")
    Observable<JsonObject> getUserVotedOption(@Query("poll_option_id") String pollOptionID, @Query("max_id") String nextMaxId);

    @PUT("poll/{pollID}/{statehide}")
    Observable<JsonObject> hideAndUnhidePoll(@Path("pollID") String pollID, @Path("statehide") String stateHide);

    @DELETE("poll/{pollID}")
    Observable<JsonObject> deletePoll(@Path("pollID") String pollID);

    @FormUrlEncoded
    @POST("oauth/token")
    Observable<Result<JsonObject>> refreshToken(
            @Field("client_id") String clientId,
            @Field("client_secret") String clientSecret,
            @Field("grant_type") String grantType,
            @Field("refresh_token") String token
    );

    @FormUrlEncoded
    @POST("oauth/token")
    Observable<Result<OAuthToken>> oauthFaceRefresh(
            @Field("client_id") String clientId,
            @Field("client_secret") String clientSecret,
            @Field("grant_type") String grantType,
            @Field("access_token") String token,
            @Field("provider_id") String providerId
    );

    @FormUrlEncoded
    @POST("oauth/token")
    Call<OAuthToken> refreshTokenCall(
            @Field("client_id") String clientId,
            @Field("client_secret") String clientSecret,
            @Field("grant_type") String grantType,
            @Field("refresh_token") String token
    );

    @FormUrlEncoded
    @POST("oauth/token")
    Call<OAuthToken> oauthFaceRefreshCall(
            @Field("client_id") String clientId,
            @Field("client_secret") String clientSecret,
            @Field("grant_type") String grantType,
            @Field("access_token") String token,
            @Field("provider_id") String providerId
    );
}
