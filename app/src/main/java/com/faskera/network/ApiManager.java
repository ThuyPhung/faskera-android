/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.network;

import android.content.Context;
import android.support.annotation.NonNull;

import com.faskera.common.AppSharePreference;
import com.faskera.common.LocaleHelper;
import com.faskera.common.adapter.ListPeople;
import com.faskera.common.apihandle.RetryNetworkManager;
import com.faskera.common.base.ObserverAdvance;
import com.faskera.common.model.BaseResponse;
import com.faskera.era.BuildConfig;
import com.faskera.model.user.MyProfile;
import com.faskera.model.user.Profile;
import com.faskera.model.user.UserInfoResponse;
import com.faskera.model.user.token.OAuthToken;
import com.faskera.network.poll.ReportPoll;
import com.faskera.network.user.MyProfileBody;
import com.faskera.network.user.UpdateProfileRespond;
import com.faskera.view.activity.home.model.Feed;
import com.faskera.view.activity.home.network.ListFeedReponse;
import com.faskera.view.activity.login.LoginPresenter;
import com.faskera.view.activity.mypage.network.ListPollsResponse;
import com.faskera.view.activity.notification.model.Notification;
import com.faskera.view.activity.notification.network.RegisterToken;
import com.faskera.view.activity.option.view.fbfriend.model.ListFriendBody;
import com.faskera.view.activity.option.view.fbfriend.network.FriendsResponse;
import com.faskera.view.activity.topic.model.UpdateRequestBody;
import com.faskera.view.activity.topic.network.InterestedTopicResponse;
import com.faskera.view.activity.topic.network.UpdateTopicInterestResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.Authenticator;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.Result;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author theanh
 * @since 1/3/2017
 */

public class ApiManager {
    public ArrayList<RetryNetworkManager> mListSubcribe = new ArrayList<>();

    public void register(RetryNetworkManager event) {
        if (!mListSubcribe.contains(event)) {
            mListSubcribe.add(event);
        }
    }

    public void unRegister(RetryNetworkManager event) {
        if (mListSubcribe.contains(event)) {
            mListSubcribe.remove(event);
        }
    }

    //<editor-fold desc="init">
    private static ApiManager sInstance;

    public Object lock = new Object();

    public static ApiManager getInstance() {
        if (sInstance == null)
            sInstance = new ApiManager();
        return sInstance;
    }

    private static OkHttpClient mOkHttpClientLogin;

    private static OkHttpClient mOkHttpClientWithTokent;

    private ApiManager() {

    }

    public ImlServiceWrapper createServiceWrapperHomeAnonymous() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor().setLevel(BuildConfig.OKHTTP_LOG_LEVEL);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        OkHttpClient mOkHttpClientLogin = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .readTimeout(APIConfig.SERVER_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(APIConfig.SERVER_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(APIConfig.SERVER_TIMEOUT, TimeUnit.SECONDS)
                .build();


        return new Retrofit.Builder().baseUrl(APIConfig.BASE_API_URL)
                .client(mOkHttpClientLogin)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build().create(ImlServiceWrapper.class);

    }

    public ImlServiceWrapper createServiceWrapper(boolean isLogin, Context context) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor().setLevel(BuildConfig.OKHTTP_LOG_LEVEL);

        if (isLogin) {
            if (mOkHttpClientLogin == null) {
                mOkHttpClientLogin = new OkHttpClient.Builder()
                        .readTimeout(APIConfig.SERVER_TIMEOUT, TimeUnit.SECONDS)
                        .writeTimeout(APIConfig.SERVER_TIMEOUT, TimeUnit.SECONDS)
                        .connectTimeout(APIConfig.SERVER_TIMEOUT, TimeUnit.SECONDS)
                        .addInterceptor(logging)
                        .build();
            }

            return new Retrofit.Builder().baseUrl(APIConfig.BASE_URL)
                    .client(mOkHttpClientLogin)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build().create(ImlServiceWrapper.class);
        } else {
            if (mOkHttpClientWithTokent == null) {
                mOkHttpClientWithTokent = new OkHttpClient.Builder()
                        .addInterceptor(new TokentHeaderRequestInterceptor(context))
                        .addInterceptor(logging)
                        .readTimeout(APIConfig.SERVER_TIMEOUT, TimeUnit.SECONDS)
                        .writeTimeout(APIConfig.SERVER_TIMEOUT, TimeUnit.SECONDS)
                        .connectTimeout(APIConfig.SERVER_TIMEOUT, TimeUnit.SECONDS)
                        .authenticator(new Authenticator() {
                            @Override
                            public Request authenticate(Route route, Response response) throws IOException {

                                if (response.code() == 401) {

                                    retrofit2.Response<OAuthToken> response1 = ApiManager.getInstance().refreshTokentCall(LoginPresenter.CLIENT_ID, LoginPresenter.CLIENT_SECRET, "refresh_token"
                                            , AppSharePreference.getRefreshToken(context)).execute();
                                    if (response1.isSuccessful()) {
                                        AppSharePreference.getInstance(context).storeOAuthToken(response1.body());
                                    } else {
                                        retrofit2.Response<OAuthToken> response2 = ApiManager.getInstance().oauthFaceRefreshCall(LoginPresenter.CLIENT_ID, LoginPresenter.CLIENT_SECRET, LoginPresenter.GRANT_TYPE
                                                , AppSharePreference.getInstance(context).getFaceToken(), LoginPresenter.PROVIDER_ID).execute();
                                        if (response2.isSuccessful()) {
                                            AppSharePreference.getInstance(context).storeOAuthToken(response2.body());
                                        }
                                    }
                                    String AuthorizationBuilder = AppSharePreference.getTokenType(context) +
                                            " " +
                                            AppSharePreference.getToken(context);
                                    Request.Builder builder = response.request().newBuilder().removeHeader("Authorization");
                                    builder.addHeader("Authorization", AuthorizationBuilder);
                                    return builder.build();
                                }


                                return null;
                            }
                        })
                        .build();
            }


            return new Retrofit.Builder().baseUrl(APIConfig.BASE_API_URL)
                    .client(mOkHttpClientWithTokent)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build().create(ImlServiceWrapper.class);
        }

    }
    //</editor-fold>

    public ImlServiceWrapper createService(@NonNull Context context) {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        if (mOkHttpClientWithTokent == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor().setLevel(BuildConfig.OKHTTP_LOG_LEVEL);

            mOkHttpClientWithTokent = new OkHttpClient.Builder()
                    .addInterceptor(new TokentHeaderRequestInterceptor(context))
                    .addInterceptor(logging)
                    .addInterceptor(chain -> {
                        Request.Builder requestBuilder = chain.request().newBuilder();
                        requestBuilder.header("Content-Type", "multipart/form-data");
                        return chain.proceed(requestBuilder.build());
                    })
                    .readTimeout(APIConfig.SERVER_TIMEOUT, TimeUnit.SECONDS)
                    .writeTimeout(APIConfig.SERVER_TIMEOUT, TimeUnit.SECONDS)
                    .connectTimeout(APIConfig.SERVER_TIMEOUT, TimeUnit.SECONDS)
                    .build();
        }


        return new Retrofit.Builder().baseUrl(APIConfig.BASE_API_URL)
                .client(mOkHttpClientWithTokent)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build().create(ImlServiceWrapper.class);
    }

    //<editor-fold desc="Login">
    public Subscription oauthFace(String clientId, String clientSecret, String grantType, String token, String providerId, ObserverAdvance<OAuthToken> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(true, null);
        return apiService.oauthFace(clientId, clientSecret, grantType, token, providerId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription getUserInfo(@NonNull Context context, ObserverAdvance<UserInfoResponse> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.getUserInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }
    //</editor-fold>

    //<editor-fold desc="Topic">
    public Subscription getInterestedTopic(@NonNull Context context, ObserverAdvance<InterestedTopicResponse> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.getInterestedTopic(LocaleHelper.getLanguage(context))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription getListTopic(@NonNull Context context, ObserverAdvance<InterestedTopicResponse> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.getListTopic(LocaleHelper.getLanguage(context))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription updateTopicInterest(@NonNull Context context, UpdateRequestBody body, ObserverAdvance<UpdateTopicInterestResponse> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.UpdateInterestTopic("application/json", body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }
    //</editor-fold>

    //<editor-fold desc="User">
    public Subscription getInfoUser(@NonNull Context context, String uid, ObserverAdvance<Profile> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.getInfoUser(uid)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription updateMyProfile(@NonNull Context context, MyProfileBody body, ObserverAdvance<UpdateProfileRespond> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.updateMyProfile("application/json", body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription getMyProfile(@NonNull Context context, ObserverAdvance<MyProfile> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.getMyProfile()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription removePhoto(@NonNull Context context, Observer<BaseResponse> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.removePhoto()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }
    //</editor-fold>

    //<editor-fold desc="Feed">
    public Subscription getHomeFeeds(@NonNull Context context, String nextMaxId, Observer<Result<ListFeedReponse>> dataOutput) {
        if (nextMaxId!= null && nextMaxId.trim().isEmpty()) {
            nextMaxId = null;
        }
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.getHomeFeeds(nextMaxId, LocaleHelper.getLanguage(context))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription getUserFeed(@NonNull Context context, String nextMaxId, String uid, ObserverAdvance<ListPollsResponse> dataOutput) {
        if (nextMaxId!= null && nextMaxId.trim().isEmpty()) {
            nextMaxId = null;
        }
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.getUserFeed(uid, nextMaxId, LocaleHelper.getLanguage(context))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }
    //</editor-fold>

    //<editor-fold desc="Discovery People">

    public Subscription discoveryPeople(@NonNull Context context, ObserverAdvance<FriendsResponse> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.discoveryPeople()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription getUserVoted(@NonNull Context context, String pollId, String maxID, ObserverAdvance<ListPeople> dataOutput) {
        String maxIDF;
        if (maxID.trim().isEmpty()) {
            maxIDF = null;
        } else {
            maxIDF = maxID;
        }
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.getUserVoted(pollId, maxIDF)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription followUser(@NonNull Context context, String userId, ObserverAdvance<BaseResponse> response) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.followUser(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response);
    }

    public Subscription unfollowUser(@NonNull Context context, String userId, ObserverAdvance<BaseResponse> response) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.unfollowUser(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response);
    }

    public Subscription unFollowFriend(@NonNull Context context, String userId, ObserverAdvance<BaseResponse> response) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.unFollowFriend(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response);
    }

    public Subscription followAllFriend(@NonNull Context context, ListFriendBody listId, ObserverAdvance<BaseResponse> response) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.followAllUser(listId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response);
    }

    //</editor-fold>

    //<editor-fold desc="Notification">
    public Subscription getNotifications(@NonNull Context context, String maxId, ObserverAdvance<Notification> dataOutput) {
        if (maxId!= null && maxId.trim().isEmpty()) {
            maxId = null;
        }
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.getNotifications(maxId, LocaleHelper.getLanguage(context))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription registerDevice(@NonNull Context context, RegisterToken token, Observer<BaseResponse> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.registerDevice(token)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription unregisterDevice(@NonNull Context context, RegisterToken token, Observer<BaseResponse> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.unregisterDevice(token)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }
    //</editor-fold>

    public Subscription getListUser(@NonNull Context context, String userId, String typeList, String nmaxId, ObserverAdvance<ListPeople> dataOutput) {
        if (nmaxId!= null && nmaxId.trim().isEmpty()) {
            nmaxId = null;
        }
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.getListUser(userId, typeList, nmaxId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription getPollDetail(@NonNull Context context, String pollID, ObserverAdvance<Feed.Poll> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.getPollDetail(pollID, LocaleHelper.getLanguage(context))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription reportPoll(@NonNull Context context, String pollID, ObserverAdvance<UpdateTopicInterestResponse> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.reportPoll(new ReportPoll(pollID))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription votePoll(@NonNull Context context, String pollID, String optionID, boolean select, ObserverAdvance<UpdateTopicInterestResponse> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.votePoll(pollID, optionID, select)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription getHomeFeedsAnonymous(@NonNull Context context, String nextMaxId, Observer<Result<ListFeedReponse>> dataOutput) {
        if (nextMaxId!= null && nextMaxId.trim().isEmpty()) {
            nextMaxId = null;
        }
        ImlServiceWrapper apiService = createServiceWrapperHomeAnonymous();
        return apiService.getHomeFeedsAnonymous(nextMaxId, LocaleHelper.getLanguage(context))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription getUserVotedOption(@NonNull Context context, String pollOptionId, String nextMaxID, ObserverAdvance<ListPeople> dataOutput) {
        if (nextMaxID!= null && nextMaxID.trim().isEmpty()) {
            nextMaxID = null;
        }
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.getUserVotedOption(pollOptionId, nextMaxID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    /**
     * @param statehide "hide"  if send action hide polls, "unhide"  if send action show poll
     */
    public Subscription hideAndUnhidePoll(@NonNull Context context, String pollID, String statehide, ObserverAdvance<PutResponse> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.hideAndUnhidePoll(pollID, statehide)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription delPoll(@NonNull Context context, String pollID, ObserverAdvance<PutResponse> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(false, context);
        return apiService.deletePoll(pollID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription refreshTokent(String clientId, String clientSecret, String grantType, String tokenRefresh, Observer<Result<JsonObject>> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(true, null);
        return apiService.refreshToken(clientId, clientSecret, grantType, tokenRefresh)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Subscription oauthFaceRefresh(String clientId, String clientSecret, String grantType, String token, String providerId, Observer<Result<OAuthToken>> dataOutput) {
        ImlServiceWrapper apiService = createServiceWrapper(true, null);
        return apiService.oauthFaceRefresh(clientId, clientSecret, grantType, token, providerId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataOutput);
    }

    public Call<OAuthToken> refreshTokentCall(String clientId, String clientSecret, String grantType, String tokenRefresh) {
        ImlServiceWrapper apiService = createServiceWrapper(true, null);
        return apiService.refreshTokenCall(clientId, clientSecret, grantType, tokenRefresh);

    }

    public Call<OAuthToken> oauthFaceRefreshCall(String clientId, String clientSecret, String grantType, String token, String providerId) {
        ImlServiceWrapper apiService = createServiceWrapper(true, null);
        return apiService.oauthFaceRefreshCall(clientId, clientSecret, grantType, token, providerId);

    }
}
