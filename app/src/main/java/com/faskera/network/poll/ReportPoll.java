/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.network.poll;

import com.google.gson.annotations.SerializedName;

/**
 * @author DANGNGOCDUC
 * @since 2/16/2017
 */

public class ReportPoll {

    @SerializedName("pollId")
    public String mPollID;

    public ReportPoll(String pollID) {
        mPollID = pollID;
    }
}
