/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.network;

import android.content.Context;

import com.faskera.common.AppSharePreference;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author DANGNGOCDUC
 * @since 1/3/2017
 */

public class TokentHeaderRequestInterceptor implements Interceptor {

    private Context mContext;

    public TokentHeaderRequestInterceptor(Context context) {
        mContext = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request newRequest;
        String AuthorizationBuilder = AppSharePreference.getTokenType(mContext) +
                " " +
                AppSharePreference.getToken(mContext);


        newRequest = request.newBuilder()
                .addHeader("Authorization", AuthorizationBuilder)
                .build();

        return chain.proceed(newRequest);
    }
}
