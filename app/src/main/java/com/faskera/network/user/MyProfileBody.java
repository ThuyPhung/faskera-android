/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.network.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author theanh
 * @since 1/12/2017
 */

public class MyProfileBody {

    @SerializedName("fullname")
    @Expose
    public String fullname;
    @SerializedName("about")
    @Expose
    public String about;
    @SerializedName("organizationWork")
    @Expose
    public String organizationWork;
    @SerializedName("organizationStudy")
    @Expose
    public String organizationStudy;
    @SerializedName("placeLive")
    @Expose
    public String placeLive;
    @SerializedName("hometown")
    @Expose
    public String hometown;
}
