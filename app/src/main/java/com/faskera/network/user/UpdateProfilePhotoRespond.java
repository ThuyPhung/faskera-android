/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.network.user;

import com.google.gson.annotations.SerializedName;

/**
 * @author theanh
 * @since 1/12/2017
 */

public class UpdateProfilePhotoRespond {

    @SerializedName("code")
    public String code;
}
