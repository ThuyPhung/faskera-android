/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.network;

import com.google.gson.annotations.SerializedName;

/**
 * @author DANGNGOCDUC
 * @since 3/1/2017
 */

public class PutResponse {
    @SerializedName("code")
    public int mStatus;
}
