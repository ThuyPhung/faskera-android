/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.network;

/**
 * @author DANGNGOCDUC
 * @since 1/3/2017
 */

public class APIConfig {

    public static int SERVER_TIMEOUT = 30; // 30 s
    public static String BASE_URL = "http://test.faskera.com/";
    public static String BASE_API_URL = "http://test.faskera.com/a/";
}
