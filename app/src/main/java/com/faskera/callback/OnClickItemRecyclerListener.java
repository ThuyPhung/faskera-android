/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.callback;

/**
 * @author jacky
 * @since 1/13/17
 */

public interface OnClickItemRecyclerListener<T> {
    void onClick(T t);
}
