/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.util.Log;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.faskera.Constant;
import com.faskera.callback.OnClickItemToolbarListener;
import com.faskera.common.AppSharePreference;
import com.faskera.common.base.BaseFragment;
import com.faskera.common.model.BaseResponse;
import com.faskera.era.R;
import com.faskera.model.option.TbOption;
import com.faskera.network.ApiManager;
import com.faskera.utils.ActivityUtils;
import com.faskera.utils.NetworkUtils;
import com.faskera.view.activity.home.HomeActivityOptimize;
import com.faskera.view.activity.notification.network.RegisterToken;
import com.faskera.view.activity.option.view.fbfriend.FbFriendFragment;
import com.faskera.view.activity.option.view.language.SwitchLanguageDialog;
import com.faskera.view.activity.option.view.profile.EditProfileFragment;
import com.faskera.view.activity.option.view.topic.EditTopicFragment;
import com.faskera.view.widget.CustomAlertDialog;

import butterknife.OnClick;
import rx.Observer;

import static java.net.HttpURLConnection.HTTP_OK;

/**
 * @author theanh
 * @since 1/9/2017
 */
public class OptionFragment extends BaseFragment {
    public static final String TAG = OptionFragment.class.getSimpleName();
//    @BindView(R.id.text_title_invite_friends)
//    AnyTextview mTextTitleInviteFriends;
//    @BindView(R.id.text_title_follow_people)
//    AnyTextview mTextTitleFollowPeople;
//    @BindView(R.id.text_title_account)
//    AnyTextview mTextTitleAccount;
//    @BindView(R.id.text_invite_fb_friends)
//    AnyTextview mTextInviteFbFriends;
//    @BindView(R.id.invite_friends)
//    AnyTextview mTextInviteFriends;
//    @BindView(R.id.text_find_fb_friends)
//    AnyTextview mTextFindFbFriends;
//    @BindView(R.id.text_edit_profile)
//    AnyTextview mTextEditProfile;
//    @BindView(R.id.text_edit_topics)
//    AnyTextview mTextEditTopics;
//    @BindView(R.id.text_edit_language)
//    AnyTextview mTextEditLanguage;
//    @BindView(R.id.text_log_out)
//    AnyTextview mTextLogOut;
    private CallbackManager mFacebookCallbackManager;

    @Override
    protected int idRoot() {
        return R.layout.fragment_options;
    }

    @Override
    protected void initView(View root) {
    }

    @Override
    protected void initData() {
    }

    @Override
    public void onRefresh(Object o) {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFacebookCallbackManager = CallbackManager.Factory.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        Resources resource = getContext().getResources();
        getBaseActivity().update(new TbOption(resource.getString(R.string.tb_options), false));
        getBaseActivity().update(new OnClickItemToolbarListener() {
            @Override
            public void onBack() {
                getBaseActivity().onBack(null);
            }

            @Override
            public void onDone() {
            }
        });
        initView(null);
    }

    @OnClick({R.id.text_invite_fb_friends, R.id.invite_friends, R.id.text_find_fb_friends,
        R.id.text_edit_profile, R.id.text_edit_topics, R.id.text_edit_language, R.id.text_log_out,
        R.id.text_privacy_policy, R.id.text_term_of_use, R.id.text_community_guidelines})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_invite_fb_friends:
                String appLinkUrl, previewImageUrl;
                appLinkUrl = "https://www.faskera.com/";
                previewImageUrl = "https://www.faskera.com/images/fb-app-invite-preview.jpg";
                if (AppInviteDialog.canShow() && NetworkUtils.isOnline(getContext())) {
                    AppInviteContent content = new AppInviteContent.Builder()
                        .setApplinkUrl(appLinkUrl)
                        .setPreviewImageUrl(previewImageUrl)
                        .build();
                    AppInviteDialog appInviteDialog = new AppInviteDialog(this);
                    appInviteDialog.show(content);
                    appInviteDialog.registerCallback(mFacebookCallbackManager,
                        new FacebookCallback<AppInviteDialog.Result>() {
                            @Override
                            public void onSuccess(AppInviteDialog.Result result) {
                                Log.d(TAG, "Invite Success");
                            }

                            @Override
                            public void onCancel() {
                                Log.d(TAG, "Cancel Invite");
                            }

                            @Override
                            public void onError(FacebookException error) {
                                Log.d(TAG, "Invite Error");
                            }
                        });
                } else {
                    ActivityUtils
                        .showToast(getContext(), getString(R.string.error_connection_not_found));
                }
                break;
            case R.id.invite_friends:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBodyText = getString(R.string.share_intro);
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject here");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBodyText);
                startActivity(Intent.createChooser(sharingIntent, "Sharing Option"));
                break;
            case R.id.text_find_fb_friends:
                getBaseActivity().goToFragment(new FbFriendFragment(), null, false);
                break;
            case R.id.text_edit_profile:
                getBaseActivity().goToFragment(new EditProfileFragment(), null, false);
                break;
            case R.id.text_edit_topics:
                getBaseActivity().goToFragment(new EditTopicFragment(), null, false);
                break;
            case R.id.text_edit_language:
                SwitchLanguageDialog switchLanguageDialog = new SwitchLanguageDialog();
                switchLanguageDialog.show(getBaseActivity().getSupportFragmentManager(),
                    switchLanguageDialog.getTag());
                break;
            case R.id.text_privacy_policy:
                startActivity(
                    new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.URL_PRIVACY_POLICY)));
                break;
            case R.id.text_term_of_use:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.URL_TERM_OF_USE)));
                break;
            case R.id.text_community_guidelines:
                startActivity(
                    new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.URL_COMMUNITY_GUIDELINES)));
                break;
            case R.id.text_log_out:
                CustomAlertDialog alertDialog = new CustomAlertDialog(getContext());
                alertDialog.setMessage(getString(R.string.msg_log_out_confirm));
                alertDialog.setYesButton(getString(R.string.action_ok), view1 -> {
                    String tokenNotification =
                        AppSharePreference.getInstance(getContext()).getTokenNotification();
                    if (!tokenNotification.isEmpty()) {
                        RegisterToken registerToken = new RegisterToken(0, tokenNotification);
                        ApiManager.getInstance().unregisterDevice(getContext(), registerToken,
                            new Observer<BaseResponse>() {
                                @Override
                                public void onCompleted() {
                                }

                                @Override
                                public void onError(Throwable e) {
                                    alertDialog.dismiss();
                                }

                                @Override
                                public void onNext(BaseResponse response) {
                                    if (response.code != HTTP_OK) return;
                                    alertDialog.dismiss();
                                    LoginManager.getInstance().logOut();
                                    AppSharePreference.getInstance(getContext()).clearData();
                                    Intent intent =
                                        new Intent(getContext(), HomeActivityOptimize.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                        IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                            });
                    } else {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.setNoButton(getString(R.string.action_cancel),
                    view12 -> alertDialog.dismiss());
                alertDialog.show();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
