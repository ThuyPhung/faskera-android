/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.filter;

import java.util.ArrayList;
import java.util.List;

public class FilterList {

    public List<String> names = new ArrayList<>();
    public List<ApplyFilterController.FilterType> types = new ArrayList<>();

    public void addFilter(String name, ApplyFilterController.FilterType type) {
        names.add(name);
        types.add(type);
    }

}