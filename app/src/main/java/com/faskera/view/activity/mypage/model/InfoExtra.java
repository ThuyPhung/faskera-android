/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.mypage.model;

import android.support.annotation.DrawableRes;

import com.faskera.view.activity.mypage.abs.AbsInfoExtra;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author DANGNGOCDUC
 * @since 1/4/2017
 */

public class InfoExtra extends AbsInfoExtra implements Serializable {
    private int mResIcon;

    @SerializedName("id")
    public String mId;


    @SerializedName("name")
    public String mTitle;


    @Override
    public int getIconSource() {
        return mResIcon;
    }

    @Override
    public void setIconSource(@DrawableRes int res) {
        mResIcon = res;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }
}
