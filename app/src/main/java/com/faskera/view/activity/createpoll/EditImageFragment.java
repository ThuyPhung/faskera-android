/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.faskera.common.base.BaseFragment;
import com.faskera.era.R;
import com.faskera.model.createpoll.TbCreatePoll;
import com.faskera.utils.AppUtils;
import com.faskera.view.activity.createpoll.frame.model.FrameModel;
import com.faskera.view.activity.createpoll.frame.utils.PhotoType;
import com.faskera.view.activity.createpoll.frame.utils.TemplateEntity;
import com.faskera.view.activity.createpoll.frame.utils.TemplateUtils;
import com.faskera.view.activity.createpoll.frame.widget.PhotoModelLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class EditImageFragment extends BaseFragment {

    private PhotoType mPhotoType;
    private int mPositionFrame;
    private int mPositionFilter;
    private int mModelAreaParentWidth;
    private int mModelAreaParentHeight;
    private ArrayList<String> mListPath;
    private static final float PHOTO_MODEL_RATIO = 1.0f;
    private int mBorder = 0;
    private ArrayList<Bitmap> mListBitmapFilter;
    private FrameModel mFrameModel;
    private boolean isFilter;

    @BindView(R.id.single_model_area)
    PhotoModelLayout mModelArea;

    @BindView(R.id.model_area_parent)
    RelativeLayout mModelAreaParent;

    @BindView(R.id.iv_frame)
    ImageView mIvFrame;

    @BindView(R.id.iv_filter)
    ImageView mIvFilter;

    @OnClick(R.id.iv_frame)
    public void goToListFrame() {
        goToFragmentFrame();
    }

    private void goToFragmentFrame() {
        setTabBackgroundSelect(true);
        Bundle mBundle = new Bundle();
        mBundle.putStringArrayList(AppUtils.SELECTED_PATHS, this.mListPath);
        mBundle.putSerializable(AppUtils.TYPE_OF_PHOTO, mPhotoType);
        mBundle.putInt(AppUtils.ID_OF_TEMPLATE, mPositionFrame);
        getBaseActivity().goToFragment(FrameFragment.getInstance(position -> {
            isFilter = false;
            mPositionFrame = position;
            setFrameType(mListPath, position);
        }), mBundle);
    }

    @OnClick(R.id.iv_filter)
    public void goToListFilter() {
        setTabBackgroundSelect(false);
        getBaseActivity().goToFragment(FilterFragment.getInstance((isFinish, mList, position) -> {
            isFilter = true;
            mListBitmapFilter = mList;
            mPositionFilter = position;
            mModelArea.saveImage();
            mModelArea.renderSelectedListBitmap(mList);
            mModelArea.invalidate();
        }, mModelArea.getListBitmap(), mPositionFilter), null);
    }

    private void setTabBackgroundSelect(boolean isSelectFrame) {
        mIvFrame.setImageResource(isSelectFrame ? R.drawable.ic_frame_select : R.drawable.ic_frame);
        mIvFilter.setImageResource(isSelectFrame ? R.drawable.ic_camera : R.drawable.ic_filter_select);
    }

    @Override
    protected int idRoot() {
        return R.layout.fragment_edit_image;
    }

    @Override
    protected void initView(View root) {
        mFrgManager = getChildFragmentManager();
        mModelArea.setStrokeWidth(true);
        mModelArea.setEdit(true);
    }

    @Override
    protected void initData() {
        if (getArguments() != null) {
            Bundle mBundle = getArguments();
            mListPath = mBundle.getStringArrayList(AppUtils.SELECTED_PATHS);
            mPhotoType = (PhotoType) mBundle.getSerializable(AppUtils.TYPE_OF_PHOTO);
            mPositionFrame = mBundle.getInt(AppUtils.ID_OF_TEMPLATE);
            mFrameModel = (FrameModel) mBundle.getSerializable(FrameModel.class.getName());
            setFrameType(mListPath, mPositionFrame);
            goToFragmentFrame();
        }
    }

    @Override
    public void onRefresh(Object o) {

    }

    @Override
    public void onResume() {
        super.onResume();
        getBaseActivity().update(new TbCreatePoll(getString(R.string.msg_title_edit_frame), getString(R.string.msg_save_frame)));
    }

    private void setFrameType(ArrayList<String> mListPath, int mPosition) {
        if (mListPath == null) {
            return;
        }
        this.mListPath = mListPath;
        mPhotoType = TemplateUtils.getJigsawType(mListPath.size());
        TemplateEntity entity = TemplateUtils.getEntity(getActivity(), mPhotoType, mPosition);
        mModelArea.setImagePathList(this.mListPath);
        mModelArea.setTemplateEntity(entity);
        if (mFrameModel != null) {
            mModelArea.setBundleList(mFrameModel.mListBundles);
            mListBitmapFilter = mFrameModel.mListBitmapFilter;
            mPositionFilter = mFrameModel.mPositionFilter;
        }
        mModelArea.invalidate();

        final ViewTreeObserver observer = mModelAreaParent.getViewTreeObserver();
        observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                mModelAreaParent.getViewTreeObserver().removeOnPreDrawListener(this);
                mModelAreaParentWidth = mModelAreaParent.getMeasuredWidth();
                mModelAreaParentHeight = mModelAreaParent.getMeasuredHeight();
                // redraw model area
                reDrawModelArea();
                return true;
            }
        });
    }

    private void reDrawModelArea() {
        if (0 != mModelAreaParentWidth && 0 != mModelAreaParentHeight && this.mListPath != null) {
            float ratio = ((float) mModelAreaParentWidth) / mModelAreaParentHeight;

            int modelAreaWidth;
            int modelAreaHeight;

            if (PHOTO_MODEL_RATIO > ratio) {
                modelAreaWidth = mModelAreaParentWidth;
                modelAreaHeight = (int) (mModelAreaParentWidth / PHOTO_MODEL_RATIO);
            } else {
                modelAreaHeight = mModelAreaParentHeight;
                modelAreaWidth = (int) (modelAreaHeight * PHOTO_MODEL_RATIO);
            }

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(modelAreaWidth, modelAreaHeight);
            params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            mModelArea.setLayoutParams(params);

            mModelArea.reDraw(mFrameModel, modelAreaWidth, modelAreaWidth, mListPath.size(), mBorder);

            mModelArea.invalidate();
        }
    }


    @Override
    public void goToDone() {
        super.goToDone();

        if (getActivity() instanceof CreatePollActivity && mModelArea != null) {
            mModelArea.saveImage();
            FrameModel mFrameModel = new FrameModel();
            if (mListBitmapFilter != null) {
                mFrameModel.mListBitmapFilter = mListBitmapFilter;
            }
            mFrameModel.mListBundles = mModelArea.getBundleList();
            mFrameModel.mPositionFrame = mPositionFrame;
            mFrameModel.mPositionFilter = mPositionFilter;
            mFrameModel.isFilter = isFilter;
            getBaseActivity().onBack(mFrameModel);
        }
    }

    @Override
    public void onDestroyView() {
        mModelArea.removeAllViews();
        mModelAreaParent.removeAllViews();
        super.onDestroyView();
        getBaseActivity().clearBackStackInclusive();
        new Handler().postAtTime(() -> {
            //removeBitmap();
            System.gc();
            Runtime.getRuntime().gc();
        }, 100L);
    }

    public void removeBitmap() {
        if (mListBitmapFilter != null && mListBitmapFilter.size() > 0) {
            for (Bitmap mBitmap : mListBitmapFilter) {
                mBitmap.recycle();
            }
        }
    }
}