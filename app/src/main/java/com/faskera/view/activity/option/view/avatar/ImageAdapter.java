/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.avatar;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.faskera.era.R;
import com.faskera.model.createpoll.ImageModel;
import com.faskera.utils.DisplayUtility;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author theanh
 * @since 1/16/2017
 */
public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageHolder> {
    private LayoutInflater mInflater;
    private List<ImageModel> mImages;
    private Context mContext;
    private onImageClickListener mCallback;
    private int mGridItemWidth;

    public interface onImageClickListener {
        void onImageClick(ImageModel image);
    }

    public ImageAdapter(Context context, List<ImageModel> images) {
        mContext = context;
        mImages = images;
        mInflater = LayoutInflater.from(context);
        int screenWidth = DisplayUtility.getScreenWidth(context);
        int numOfColumns = DisplayUtility.isInLandscapeMode(context) ? 4 : 3;
        mGridItemWidth = screenWidth / numOfColumns;
    }

    public void setOnImageClickListener(onImageClickListener callback) {
        mCallback = callback;
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.item_view_image, parent, false);
        v.setLayoutParams(getGridItemLayoutParams(v));
        return new ImageHolder(v);
    }

    private ViewGroup.LayoutParams getGridItemLayoutParams(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = mGridItemWidth;
        layoutParams.height = mGridItemWidth;
        return layoutParams;
    }

    @Override
    public void onBindViewHolder(ImageHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mImages.size();
    }

    public class ImageHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView mImage;

        public ImageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(int position) {
            ImageModel image = mImages.get(position);
            Glide.with(mContext).load(image.getThumb()).override(200, 200)
                .crossFade()
                .centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mImage);
            mImage.setOnClickListener(view -> mCallback.onImageClick(image));
        }
    }
}
