/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.topic.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author DANGNGOCDUC
 * @since 12/27/2016
 */

public class Topic extends ChooseCateUI implements Serializable {

    @SerializedName("id")
    public String mId;

    @SerializedName("name")
    private String mName;

    @SerializedName("interestedIn")
    private boolean isSelected = false;


    @Override
    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public boolean isSelected() {
        return isSelected;
    }

    @Override
    public String getId() {
        return mId;
    }


}
