/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.faskera.database.table.FeedTable;
import com.faskera.database.table.PollTable;
import com.faskera.model.user.Photo;
import com.faskera.model.user.UserInfoResponse;
import com.faskera.view.activity.home.model.abst.AbsFeedAns;
import com.faskera.view.activity.home.model.abst.AbsFeedContentMedia;
import com.faskera.view.activity.home.model.abst.AbsFeedContentText;
import com.faskera.view.activity.home.model.abst.AbsFeedFooter;
import com.faskera.view.activity.home.model.abst.AbsFeedHeader;
import com.faskera.view.activity.home.model.abst.AbsFeedSuperHeader;
import com.faskera.view.activity.home.model.abst.FeedAbs;
import com.faskera.view.activity.topic.model.Topic;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/*
  Created by DANGNGOCDUC on 1/11/2017.
  <p>
  Class Feed chua thong tin  ve Feed  bao gom c�c thong tin server tra ve kem theo 1 so phuong thuc de
  gen ra <code>{@link ContentValues}</code>  hoac tao Object tu <code> {@link Cursor}</code>
  </p>
 */
public class Feed extends FeedAbs {

    @SerializedName("feedType")
    public String mFeedType;

    @SerializedName("objectId")
    public String mObjectId;

    @SerializedName("objectType")
    public String mObjectType;

    @SerializedName("lastActivity")
    public LastActivity mLastActivity;

    @SerializedName("poll")
    public Poll mPoll;


    @Override
    public ArrayList<AbsFeedAns> getFeedAns() {

        if (mListAsw == null) {

            mListAsw = new ArrayList<>();

            for (Options op : mPoll.mListOptions) {
                mListAsw.add(new FeedAns(op, this));
            }
        }

        return mListAsw;

    }

    @Override
    public AbsFeedContentMedia getFeedContentMedia() {

        if (mAbsFeedContentMedia == null) {
            mAbsFeedContentMedia = new FeedContentMedia(mPoll);
        }
        return mAbsFeedContentMedia;
    }

    @Override
    public AbsFeedContentText getFeedContentText() {
        if (mAbsFeedContentText == null) {
            mAbsFeedContentText = new FeedContentText(mPoll);
        }
        return mAbsFeedContentText;
    }

    @Override
    public AbsFeedHeader getFeedHeader() {
        if (mAbsFeedHeader == null) {
            mAbsFeedHeader = new FeedHeader(mPoll);
        }
        return mAbsFeedHeader;
    }

    @Override
    public AbsFeedSuperHeader getFeedSuperHeader() {

        if (mAbsFeedSuperHeader == null) {
            mAbsFeedSuperHeader = new FeedSuperHeader(mLastActivity);
        }
        return mAbsFeedSuperHeader;
    }

    @Override
    public FeedViewDetail getFeedViewDetail() {
        if (mFeedViewDetail == null) {
            mFeedViewDetail = new FeedViewDetail(mPoll);
        }
        return mFeedViewDetail;
    }

    @Override
    public AbsFeedFooter getFeedFooter() {
        if (mAbsFeedFooter == null) {
            mAbsFeedFooter = new FeedFooter(mPoll);
        }
        return mAbsFeedFooter;
    }

    @Override
    public String getPollId() {
        return mPoll.mPollID;
    }

    @Override
    public String getPollLink() {
        return mPoll.mPermalink;
    }

    @Override
    public Poll getPoll() {
        return mPoll;
    }

    public static class Poll extends FeedAbs implements Serializable {
        @SerializedName("id")
        public String mPollID;

        @SerializedName("createdTime")
        public long mCreatedTime;

        @SerializedName("createdUser")
        public UserInfoResponse mCreatedUser;

        @SerializedName("settings")
        public Setting mSetting;

        @SerializedName("question")
        public Question mQuestion;

        @SerializedName("options")
        public ArrayList<Options> mListOptions;

        @SerializedName("nbUsersAnswered")
        public int mNbUsersAnswered;

        @SerializedName("votedByMe")
        public boolean votedByMe;

        @SerializedName("topic")
        public Topic mCate;

        //// TODO: 1/11/2017 bo sung them  tags countries languages sau

        @SerializedName("hidden")
        public boolean mHidden;

        @SerializedName("closed")
        public boolean mClosed;

        @SerializedName("permalink")
        public String mPermalink;

        @SerializedName("mine")
        public boolean mMine;


        public ContentValues toContentValue() {

            ContentValues contentValues = new ContentValues();
            Gson gson = new Gson();
            contentValues.put(PollTable._ID, mPollID);
            contentValues.put(PollTable.TIME_CREATE, mCreatedTime);
            contentValues.put(PollTable.NUMBER_ITEM, genCountType());
            contentValues.put(PollTable.OFFLINE, false);
            if (mCreatedUser != null)
            contentValues.put(PollTable.USER_ID, mCreatedUser.getId());

            mListAsw = null;
            mAbsFeedContentMedia = null;
            mAbsFeedContentText = null;
            mAbsFeedHeader = null;
            mAbsFeedFooter = null;
            mFeedViewDetail = null;
            mAbsFeedSuperHeader = null;
            contentValues.put(PollTable.DATA, gson.toJson(this));

            return contentValues;
        }

        public int genCountType() {
            int count = 0;

            count++;
            count++;
            if (mQuestion.mPhoto != null) {
                count++;
            }

            int countx;
            if (mListOptions.size() >= 2) {
                countx = 2;
            } else {
                countx = mListOptions.size();
            }

            for (int i = 0; i < countx; i++) {
                count++;
            }
            count++;
            count++;
            return count;
        }

        public ArrayList<ImlHomeRow> genListItem() {
            ArrayList<ImlHomeRow> list = new ArrayList<>();
            if (mListOptions == null || mQuestion == null) {
                return list;
            }

            list.add(getFeedHeader());
            list.add(getFeedContentText());
            if (mQuestion.mPhoto != null) {
                list.add(getFeedContentMedia());
            }

            int count;
            if (mListOptions.size() >= 2) {
                count = mListOptions.size();
            } else {
                count = mListOptions.size();
            }
            for (int i = 0; i < count; i++) {
                list.add(new FeedAns(mListOptions.get(i), this));
            }

            if (mListOptions.size() > count) {
                list.add(new FeedViewDetail(this));
            } else {
                list.add(new FeedViewDetail(this));
            }
            list.add(getFeedFooter());

            return list;
        }

        public ArrayList<ImlHomeRow> genListItemDetail() {
            ArrayList<ImlHomeRow> list = new ArrayList<>();

            list.add(getFeedHeader());
            list.add(getFeedContentText());
            if (mQuestion.mPhoto != null) {
                list.add(getFeedContentMedia());
            }


            for (int i = 0; i < mListOptions.size(); i++) {
                list.add(new FeedAns(mListOptions.get(i), this));
            }


            list.add(getFeedFooter());

            return list;
        }

        @Override
        public ArrayList<AbsFeedAns> getFeedAns() {
            if (mListAsw == null) {

                mListAsw = new ArrayList<>();

                for (Options op : mListOptions) {
                    mListAsw.add(new FeedAns(op, this));
                }
            }

            return mListAsw;

        }

        @Override
        public AbsFeedContentMedia getFeedContentMedia() {
            if (mAbsFeedContentMedia == null) {
                mAbsFeedContentMedia = new FeedContentMedia(this);
            }
            return mAbsFeedContentMedia;
        }

        @Override
        public AbsFeedContentText getFeedContentText() {
            if (mAbsFeedContentText == null) {
                mAbsFeedContentText = new FeedContentText(this);
            }
            return mAbsFeedContentText;
        }

        @Override
        public AbsFeedHeader getFeedHeader() {
            if (mAbsFeedHeader == null) {
                mAbsFeedHeader = new FeedHeader(this);
            }
            return mAbsFeedHeader;
        }

        @Override
        public AbsFeedSuperHeader getFeedSuperHeader() {
            return null;
        }

        @Override
        public FeedViewDetail getFeedViewDetail() {
            if (mFeedViewDetail == null) {
                mFeedViewDetail = new FeedViewDetail(this);
            }
            return mFeedViewDetail;
        }

        @Override
        public AbsFeedFooter getFeedFooter() {
            if (mAbsFeedFooter == null) {
                mAbsFeedFooter = new FeedFooter(this);
            }
            return mAbsFeedFooter;
        }

        @Override
        public String getPollId() {
            return mPollID;
        }

        @Override
        public String getPollLink() {
            return mPermalink;
        }

        @Override
        public Poll getPoll() {
            return this;
        }

        @Override
        public ContentValues toContentValue(boolean ishome) {
            return toContentValue();
        }

        @Override
        public void cloneSateObject(FeedAbs target) {

                if (mPollID.equals(target.getPollId())) { // Check The Target poll is a Instance of poll.
                    mHidden = target.getPoll().mHidden;
                    mNbUsersAnswered = target.getPoll().mNbUsersAnswered;
                    votedByMe = target.getPoll().votedByMe;
                    for (Options option : mListOptions) {
                        for (Options optionTarget : target.getPoll().mListOptions) {
                            if (option.getAnsID().equals(optionTarget.getAnsID())) {
                                option.mChossen = optionTarget.mChossen;
                                option.mResult = optionTarget.mResult;
                            }
                        }
                    }
                }

        }

        public static Poll parsePoll(Cursor cursor) {
            Gson gson = new Gson();
            return gson.fromJson(cursor.getString(cursor.getColumnIndex(PollTable.DATA)), Poll.class);
        }

        public int[] getAllType() {
            int[] type = new int[genCountType()];
            int count = 0;
            type[count] = ImlHomeRow.TYPE_HEADER_FEED;
            count++;
            type[count] = ImlHomeRow.TYPE_CONTENT_FEED;
            count++;


            if (mQuestion.mPhoto != null) {
                type[count] = ImlHomeRow.TYPE_CONTENT_FEED_IMAGE;
                count++;


            }

            int countx;
            if (mListOptions.size() >= 2) {
                countx = 2;
            } else {
                countx = mListOptions.size();
            }

            for (int i = 0; i < countx; i++) {

                type[count] = ImlHomeRow.TYPE_ANS;
                count++;

            }
            type[count] = ImlHomeRow.TYPE_VIEW_DETAIL;
            count++;
            type[count] = ImlHomeRow.TYPE_FOOTER;
            return type;
        }


    }

    public static class Options extends AbsFeedAns implements Serializable {
        @SerializedName("id")
        public String mID;

        @SerializedName("seqNo")
        public int mSeqNo;

        @SerializedName("text")
        public String mText;

        @SerializedName("result")
        public int mResult;

        @SerializedName("chosen")
        public boolean mChossen;

        @Override
        public int getNbSeq() {
            return mSeqNo;
        }

        @Override
        public boolean isSelected() {
            return mChossen;
        }

        @Override
        public void setSelected(boolean selected) {

            mChossen = selected;
        }

        @Override
        public String getContent() {
            return mText;
        }

        @Override
        public String getVotes() {
            return String.valueOf(mResult);
        }

        @Override
        public int getVoteInt() {
            return mResult;
        }

        @Override
        public FeedAbs getPoll() {
            return null;
        }

        @Override
        public String getAnsID() {
            return mID;
        }
    }

    public static class Setting implements Serializable {

        @SerializedName("anonymous")
        public boolean mAnonymous;

        @SerializedName("multiChoice")
        public boolean mMultiChoice;

    }

    public static class Question implements Serializable {
        @SerializedName("text")
        public String mText;

        @SerializedName("photo")
        public Photo mPhoto;

    }

    public static class LastActivity implements Serializable {

        @SerializedName("user")
        public UserInfoResponse mUser;

        @SerializedName("actionType")
        public String mActionType;

        @SerializedName("time")
        public long mTime;
    }

    public static Feed parseFeed(Cursor cursor) {
        Gson gson = new Gson();
        return gson.fromJson(cursor.getString(cursor.getColumnIndex(FeedTable.DATA)), Feed.class);
    }

    /**
     * @param isHome xem object can parse de insert co phai dang o trang Home khong
     * @return <code>{@link ContentValues}</code>
     */
    public ContentValues toContentValue(boolean isHome) {
        Gson gson = new Gson();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FeedTable._ID, mObjectId);
        contentValues.put(FeedTable.ISHOME, isHome);
        contentValues.put(FeedTable.TIME_CREATE, mPoll.mCreatedTime);
        if (mLastActivity != null) {
            contentValues.put(FeedTable.TIME_MODIFIED, mLastActivity.mTime);
        } else {
            contentValues.put(FeedTable.TIME_MODIFIED, mPoll.mCreatedTime);
        }
        if (mPoll.mCreatedUser != null) {
            contentValues.put(FeedTable.USER_ID, mPoll.mCreatedUser.id);
        } else {
            contentValues.put(FeedTable.USER_ID, "");
        }
        contentValues.put(FeedTable.POLL_ID, mPoll.mPollID);
        contentValues.put(FeedTable.NUMBER_ITEM, genCountType());
        contentValues.put(FeedTable.HIDEN, mPoll.mHidden);
        contentValues.put(FeedTable.CLOSED, mPoll.mClosed);
        mListAsw = null;
        mAbsFeedContentMedia = null;
        mAbsFeedContentText = null;
        mAbsFeedHeader = null;
        mAbsFeedFooter = null;
        mFeedViewDetail = null;
        mAbsFeedSuperHeader = null;
        contentValues.put(FeedTable.DATA, gson.toJson(this));

        return contentValues;
    }

    @Override
    public void cloneSateObject(FeedAbs target) {
        if (mPoll.getPollId().equals(target.getPollId())) { // Check The Target poll is a Instance of poll.
            mPoll.mHidden = target.getPoll().mHidden;
            mPoll.mNbUsersAnswered = target.getPoll().mNbUsersAnswered;
            mPoll.votedByMe = target.getPoll().votedByMe;
            for (Options option : mPoll.mListOptions) {
                for (Options optionTarget : target.getPoll().mListOptions) {
                    if (option.getAnsID().equals(optionTarget.getAnsID())) {
                        option.mChossen = optionTarget.mChossen;
                        option.mResult = optionTarget.mResult;
                    }
                }
            }
        }
    }

    /**
     * <p>
     * Phuong thuc nay gen ra list object ma Adapter can su dung de
     * hien thi len RecycleView
     * </p>
     *
     * @return list Item of Adapter NewFeeds
     */
    public ArrayList<ImlHomeRow> genListItem(boolean isLogin) {
        ArrayList<ImlHomeRow> list = new ArrayList<>();
        if (isLogin) {
            if (mFeedType.equals("trending")) {
                list.add(new FeedSuperHeader());
            } else {
                if (mLastActivity != null && mLastActivity.mUser != null) {
                    list.add(new FeedSuperHeader(mLastActivity));
                }
            }
        }
        list.add(new FeedHeader(mPoll));
        list.add(new FeedContentText(mPoll));
        if (mPoll.mQuestion.mPhoto != null) {
            list.add(new FeedContentMedia(mPoll));
        }
        int count;
        if (mPoll.mListOptions.size() >= 2) {
            count = mPoll.mListOptions.size();
        } else {
            count = mPoll.mListOptions.size();
        }
        for (int i = 0; i < count; i++) {
            list.add(new FeedAns(mPoll.mListOptions.get(i), this));
        }

        if (mPoll.mListOptions.size() > count) {
            list.add(new FeedViewDetail(mPoll));
        } else {
            list.add(new FeedViewDetail(mPoll));
        }
        list.add(new FeedFooter(mPoll));

        return list;
    }

    public int genCountType() {
        int count = 0;
        if (mLastActivity != null && mLastActivity.mUser != null) {
            count++;
        }
        count++;
        count++;
        if (mPoll.mQuestion.mPhoto != null) {
            count++;
        }
        int countx;
        if (mPoll.mListOptions.size() >= 2) {
            countx = 2;
        } else {
            countx = mPoll.mListOptions.size();
        }

        for (int i = 0; i < countx; i++) {
            count++;
        }
        count++;
        count++;

        return count;
    }


}
