/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.login;

import com.faskera.model.user.UserInfoResponse;
import com.faskera.model.user.token.OAuthToken;
import com.faskera.mvp.BasePresenter;
import com.faskera.mvp.BaseView;

/**
 * @author theanh
 * @since 1/3/2017
 */
public interface LoginContract {
    interface Presenter extends BasePresenter {
        void loginWithFacebook(String accessToken);
        void getUserInfo();
        void subscribe();
        void unsubscribe();
    }

    interface View extends BaseView<Presenter> {
        void onAuthTokenSuccess(OAuthToken token);
        void onGetUserInfoSuccess(UserInfoResponse userInfo);
        void onUpdateInterestedTopic();
        void onErrorConnection(int code);
        void onErrorLoginFacebook();
        void showLoading();
        void hideLoading();
    }
}
