/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home.model;

import com.faskera.view.activity.home.model.abst.AbsFeedAns;
import com.faskera.view.activity.home.model.abst.FeedAbs;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public class FeedAns extends AbsFeedAns {

    private Feed.Options mOption;
    private FeedAbs mFeedAbs;

    public FeedAns(Feed.Options option, FeedAbs feedAbs) {
        mOption = option;
        mFeedAbs = feedAbs;
    }

    @Override
    public int getNbSeq() {
        return mOption.mSeqNo;
    }

    @Override
    public boolean isSelected() {
        return mOption.mChossen;
    }

    @Override
    public void setSelected(boolean selected) {

        mOption.mChossen = selected;
    }

    @Override
    public String getContent() {
        return mOption.mText;
    }

    @Override
    public String getVotes() {
        return String.valueOf(mOption.mResult);
    }

    @Override
    public int getVoteInt() {
        return mOption.getVoteInt();
    }

    @Override
    public FeedAbs getPoll() {
        return mFeedAbs;
    }

    @Override
    public String getAnsID() {
        return mOption.mID;
    }
}
