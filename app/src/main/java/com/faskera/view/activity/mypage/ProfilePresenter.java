/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.mypage;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;

import com.faskera.common.AppSharePreference;
import com.faskera.common.UserDefine;
import com.faskera.common.base.ObserverAdvance;
import com.faskera.common.debug.DebugUtil;
import com.faskera.database.FaskeraProvider;
import com.faskera.database.table.PollTable;
import com.faskera.database.table.UserInfoTable;
import com.faskera.model.user.Profile;
import com.faskera.model.user.UserInfoResponse;
import com.faskera.network.ApiManager;
import com.faskera.view.activity.home.model.Feed;
import com.faskera.view.activity.mypage.abs.AbsProfile;
import com.faskera.view.activity.mypage.network.ListPollsResponse;

import java.util.ArrayList;

import rx.subscriptions.CompositeSubscription;

import static com.faskera.view.activity.mypage.ProfileActivity.BUNDLE_GO_THROUGH_NOTIFICATION;
import static com.faskera.view.activity.mypage.ProfileActivity.BUNDLE_TYPE_USER;

/**
 * @author DANGNGOCDUC
 * @since 1/4/2017
 */

public class ProfilePresenter implements ProfileContract.Presenter {

    private ProfileContract.View mView;
    private Context mContext;
    @UserDefine.TypeUser
    public int mTypeUser;
    public String mUid;

    String mNextMaxId = "";

    CompositeSubscription mCompositeSubscription = new CompositeSubscription();

    boolean stopLoadMore = false;

    public ProfilePresenter(Context context, ProfileContract.View view, Bundle bundle) {
        mContext = context;
        mView = view;
        if (bundle.getInt(BUNDLE_TYPE_USER) == UserDefine.USER_TYPE_PROFILE) {
            mTypeUser = UserDefine.USER_TYPE_PROFILE;
            mUid = AppSharePreference.getUid(mContext);
        } else {
            mTypeUser = UserDefine.USER_TYPE_FRIENDS;
            mUid = bundle.getString(ProfileActivity.BUNDLE_USER_ID);
        }

        //If user click notification , decrease counter notification
        if (bundle.getBoolean(BUNDLE_GO_THROUGH_NOTIFICATION, false))
            AppSharePreference.getInstance(context).setNotificationCounter(-1);

        mView.loadViewForUserFriends(mTypeUser == UserDefine.USER_TYPE_FRIENDS);
        mView.setPresenter(this);
    }

    public void clearIndex() {
        mNextMaxId = "";
    }

    @Override
    public AbsProfile getAbsProfile() {
        ContentResolver contentResolver = mContext.getContentResolver();
        Cursor cursor = contentResolver.query(FaskeraProvider.CONTENT_URI_USER, null, UserInfoTable._ID + "='" + mUid  + "'", null, null);
        Profile profile;
        if (cursor != null && cursor.moveToFirst()) {
            profile = Profile.parseProfile(cursor);

        } else {
            profile = null;
        }
        return profile;
    }

    @Override
    public ArrayList<Feed.Poll> getPollOfUserDB() {
        ArrayList<Feed.Poll> polls = new ArrayList<Feed.Poll>();
        ContentResolver contentResolver = mContext.getContentResolver();

        Cursor cursor = contentResolver.query(FaskeraProvider.CONTENT_URI_POLL, null, PollTable.USER_ID + "='" + mUid + "'", null, PollTable.TIME_CREATE + " ASC");

        if (cursor != null && cursor.moveToFirst()) {
            do {
                polls.add(Feed.Poll.parsePoll(cursor));

            } while (cursor.moveToNext());
        }
        return polls;
    }

    @Override
    public void getUserInfo() {
        if (mView != null) {
            mView.onLoadInfo();
        }
        stopLoadMore = true;
        mNextMaxId = "";

        mCompositeSubscription.add(ApiManager.getInstance().getInfoUser(mContext, mUid, new ObserverAdvance<Profile>() {


            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(int code) {
                mView.onLoadInfoError(code);
            }

            @Override
            public void onSuccess(Profile data) {
                stopLoadMore = false;
                if (mView != null) {
                    ContentResolver contentResolver = mContext.getContentResolver();

                    if (data != null) {

                        Cursor cursor = contentResolver.query(FaskeraProvider.CONTENT_URI_USER, null, UserInfoTable._ID + "='" + data.mUid + "'", null, null);
                        if (cursor != null) {
                            if (cursor.moveToFirst()) {
                                contentResolver.update(FaskeraProvider.CONTENT_URI_USER, data.toContentValue(), UserInfoTable._ID + "='" + data.mUid + "'", null);
                            } else {
                                contentResolver.insert(FaskeraProvider.CONTENT_URI_USER, data.toContentValue());
                            }
                            contentResolver.notifyChange(FaskeraProvider.CONTENT_URI_USER, null);
                            cursor.close();
                        }
                    }
                    if (mView != null) {
                        mView.onLoadInfoSuccess(data, true);
                    }

                }
            }
        }));
    }

    @Override
    public void getPollOfUser() {

        if (stopLoadMore) return;

        if (mView != null) {
            mView.onLoadPoll();
        }
        String uid = mTypeUser == UserDefine.USER_TYPE_FRIENDS ? mUid : "me";

        mCompositeSubscription.add(ApiManager.getInstance().getUserFeed(mContext, mNextMaxId, uid, new ObserverAdvance<ListPollsResponse>() {
            @Override
            public void onError(int code) {
                mView.onLoadPollError(code);
            }

            @Override
            public void onSuccess(ListPollsResponse data) {
                if (mView != null) {

                    mNextMaxId = data.nmNextMaxId;
                    if (mNextMaxId == null || mNextMaxId.isEmpty()) {
                        stopLoadMore = true;
                    }


                    if (data.mListFeed != null) {
                      mView.onLoadPollSuccess(data.mListFeed, true);


                        ContentResolver contentResolver =  mContext.getContentResolver();
                        if (mNextMaxId == null || mNextMaxId.trim().isEmpty()) {
                            contentResolver.delete(FaskeraProvider.CONTENT_URI_POLL, null, null);
                        }
                        long mLastTime;
                        for (int i = 0;  i < data.mListFeed.size(); i++ ) {
                            mLastTime = System.currentTimeMillis() + i;
                            ContentValues contentValues =  data.mListFeed.get(i).toContentValue(true);
                            contentValues.put(PollTable.TIME_CREATE, mLastTime);
                            String id =  data.mListFeed.get(i).mPollID;
                            Cursor cursor = contentResolver.query(FaskeraProvider.CONTENT_URI_POLL, null, PollTable._ID + "= '" + id + "'", null, null, null);
                            if (cursor != null && cursor.moveToFirst()) {
                                contentResolver.update(FaskeraProvider.CONTENT_URI_POLL, contentValues, PollTable._ID + "= '" + id + "'", null);
                            } else {
                                contentResolver.insert(FaskeraProvider.CONTENT_URI_POLL, contentValues);

                            }
                            if (cursor != null && !cursor.isClosed())
                                cursor.close();
                        }
                    }
                }
            }

            @Override
            public void onCompleted() {

            }


        }));

    }

    @Override
    public String getUid() {
        return mUid;
    }

    @Override
    public void onUpdateInfo() {

        mCompositeSubscription.add(ApiManager.getInstance().getInfoUser(mContext, mUid, new ObserverAdvance<Profile>() {


            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                DebugUtil.d("===========================ProfilePresenter-onErrorConnection", e.toString());


            }

            @Override
            public void onError(int code) {

            }

            @Override
            public void onSuccess(Profile data) {

                    if (mView != null) {
                        mView.onUpdateInfoSuccess(data);
                    }


            }
        }));


        mCompositeSubscription.add(ApiManager.getInstance().getUserInfo(mContext, new ObserverAdvance<UserInfoResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(int code) {

            }

            @Override
            public void onSuccess(UserInfoResponse data) {
                AppSharePreference.getInstance(mContext).storeUserInfo(data);
            }
        }));

    }

}
