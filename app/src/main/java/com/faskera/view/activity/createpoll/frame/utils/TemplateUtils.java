/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.utils;

import android.content.Context;

import com.faskera.view.activity.createpoll.frame.widget.PhotoModelLayout;

import java.util.ArrayList;
import java.util.List;

public class TemplateUtils {
    /**
     * type
     *
     * @param context
     * @param type
     * @param paths
     * @return
     */
    public static List<PhotoModelLayout> getSlotLayoutList(Context context, PhotoType type, List<String> paths) {
        if (null == type || null == paths) {
            return null;
        }

        List<TemplateEntity> entityList = ParserHelper.getInstance(context).getEntityList(type);
        List<PhotoModelLayout> photoModelLayoutList = new ArrayList<>();

        int width = DisplayUtils.dp2px(context, 110);
        int height = DisplayUtils.dp2px(context, 110);

        for (TemplateEntity entity : entityList) {
            PhotoModelLayout photoModelLayout = new PhotoModelLayout(context, false);
            photoModelLayout.setImagePathList(paths);
            photoModelLayout.setTemplateEntity(entity);

            photoModelLayout.reDraw(null, width, height, paths.size(), 0);

            photoModelLayoutList.add(photoModelLayout);
        }

        return photoModelLayoutList;
    }

    /**
     * SlotView Entity
     *
     * @param context
     * @param type
     * @param position
     * @return
     */
    public static TemplateEntity getEntity(Context context, PhotoType type, int position) {
        if (null == type) {
            return null;
        }

        List<TemplateEntity> entities = ParserHelper.getInstance(context).getEntityList(type);
        if (position < 0 || position >= entities.size()) {
            return null;
        }

        return entities.get(position);
    }

    /**
     * size PhotoType
     *
     * @param size
     * @return
     */
    public static PhotoType getJigsawType(int size) {
        PhotoType photoType = null;
        switch (size) {
            case 1:
                photoType = PhotoType.ONE_PHOTO;
                break;
            case 2:
                photoType = PhotoType.TWO_PHOTO;
                break;
            case 3:
                photoType = PhotoType.THREE_PHOTO;
                break;
            case 4:
                photoType = PhotoType.FOUR_PHOTO;
                break;
            case 5:
                photoType = PhotoType.FIVE_PHOTO;
                break;
            case 6:
                photoType = PhotoType.SEX_PHOTO;
                break;
            default:
                break;
        }

        return photoType;
    }
}
