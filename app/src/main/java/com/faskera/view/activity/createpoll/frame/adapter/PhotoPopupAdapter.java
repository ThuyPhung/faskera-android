/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.faskera.era.R;
import com.faskera.view.activity.createpoll.frame.event.OnItemClickListener;
import com.faskera.view.activity.createpoll.frame.model.PopupFilterItem;

import java.util.List;

public class PhotoPopupAdapter extends RecyclerView.Adapter<PhotoPopupAdapter.ViewHolder> {

    private int curSelection = 0;
    private LayoutInflater inflater;
    private List<PopupFilterItem> list;
    private OnItemClickListener onItemClickListener;

    public PhotoPopupAdapter(Context context, List<PopupFilterItem> list) {
        this.list = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.item_photo_filter, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        PopupFilterItem filterItem = list.get(position);
        holder.frameLayout.setBackgroundResource(R.color.black54);
        holder.imageView.setImageBitmap(filterItem.getBitmap());
        holder.mTitleFilter.setText(filterItem.getNameTitle());

        holder.imageView.setOnClickListener(v -> {
            curSelection = position;
            notifyDataSetChanged();

            if (null != onItemClickListener) {
                onItemClickListener.onItemClick(v, position);
            }
        });

        if (curSelection == position) {
            holder.frameLayout.setVisibility(View.VISIBLE);
        } else {
            holder.frameLayout.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return null == list ? 0 : list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private FrameLayout frameLayout;
        private TextView mTitleFilter;

        public ViewHolder(View itemView) {
            super(itemView);

            frameLayout = (FrameLayout) itemView.findViewById(R.id.item_photo_filter_layout);
            imageView = (ImageView) itemView.findViewById(R.id.item_photo_filter_image);
            mTitleFilter= (TextView) itemView.findViewById(R.id.tv_title_filter);
        }
    }

    public void setSelection(int position) {
        curSelection = position;
    }

    public void setList(List<PopupFilterItem> list) {
        this.list = list;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
