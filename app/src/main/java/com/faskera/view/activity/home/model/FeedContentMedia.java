/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home.model;

import com.faskera.model.user.Photo;
import com.faskera.view.activity.home.model.abst.AbsFeedContentMedia;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public class FeedContentMedia extends AbsFeedContentMedia {
    public Feed.Poll mPolls;

    public FeedContentMedia(Feed.Poll poll) {
        mPolls = poll;
    }

    @Override
    public Photo getPhoto() {
        return mPolls.mQuestion.mPhoto;
    }
}
