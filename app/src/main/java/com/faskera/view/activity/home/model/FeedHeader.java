/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home.model;

import com.faskera.utils.HumanTime;
import com.faskera.view.activity.home.model.abst.AbsFeedHeader;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public class FeedHeader extends AbsFeedHeader {

    Feed.Poll mPoll;
    public String mSubContent = null;

    public FeedHeader(Feed.Poll poll) {
        mPoll = poll;
    }

    @Override
    public String getAvatar() {
        if (mPoll.mSetting.mAnonymous) {
            return null;
        } else {

            if (mPoll.mCreatedUser.photo != null) {
                return mPoll.mCreatedUser.photo.original;
            } else {
                return null;
            }

        }
    }

    @Override
    public String getName() {
        if (mPoll.mSetting.mAnonymous) {
            return null;
        } else {
            return mPoll.mCreatedUser.fullName;
        }
    }

    @Override
    public String getSubName() {
        mSubContent = mPoll.mCate.getName() + " \u2022 " + new HumanTime().timeAgo(mPoll.mCreatedTime);
        return mSubContent;
    }

    @Override
    public boolean isAnonymous() {
        return mPoll.mSetting.mAnonymous;
    }

    @Override
    public Feed.Poll getPoll() {
        return mPoll;
    }
}
