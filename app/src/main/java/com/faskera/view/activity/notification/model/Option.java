/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.notification.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author framgia
 * @since 14/02/2017
 */

public class Option {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("seqNo")
    @Expose
    public int seqNo;
    @SerializedName("text")
    @Expose
    public String text;
    @SerializedName("result")
    @Expose
    public int result;
    @SerializedName("chosen")
    @Expose
    public boolean chosen;

}