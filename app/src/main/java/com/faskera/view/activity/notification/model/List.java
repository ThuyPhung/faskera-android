/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.notification.model;

import com.faskera.model.user.UserInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author framgia
 * @since 14/02/2017
 */

public class List{

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("action")
    @Expose
    public String action;
    @SerializedName("targetId")
    @Expose
    public String targetId;
    @SerializedName("targetType")
    @Expose
    public String targetType;
    @SerializedName("targetPoll")
    @Expose
    public TargetPoll targetPoll;
    @SerializedName("targetUser")
    @Expose
    public UserInfo targetUser;
    @SerializedName("time")
    @Expose
    public long time;
    @SerializedName("activities")
    @Expose
    public java.util.List<Activity> activities = null;

}
