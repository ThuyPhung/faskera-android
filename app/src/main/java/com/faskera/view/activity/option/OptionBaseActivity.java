/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option;

import com.faskera.common.base.BaseActivity;

import java.util.Stack;

/**
 * @author theanh
 * @since 1/16/2017
 */
public class OptionBaseActivity extends BaseActivity {
    @Override
    protected int getIdLayout() {
        return 0;
    }

    @Override
    protected void initView() {
        mFrgManager = getSupportFragmentManager();
        mFragStack = new Stack<>();
//      mProgressDialog = new ProgressDialog(this, R.style.MyTheme);
//      mProgressDialog.setCancelable(false);
//      mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
    }

    @Override
    protected void initData() {
    }
}
