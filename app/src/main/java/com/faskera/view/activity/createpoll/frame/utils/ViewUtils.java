/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

public class ViewUtils
{
    /**
     * The animation displays the view
     */
    public static void viewFadeIn(Context c, View view, int duration)
    {
        if(View.VISIBLE == view.getVisibility())
        {
            return;
        }

        if (duration != 0)
        {
            Animation fadeAnimation = AnimationUtils.loadAnimation(c, android.R.anim.fade_in);
            fadeAnimation.setDuration(duration);
            view.startAnimation(fadeAnimation);
        }
        view.setVisibility(View.VISIBLE);
    }

    /**
     * Used to hide view
     * C
     * View Hides the view
     * Duration The duration of the animation
     */
    public static void viewFadeOut(Context c, View view, int duration)
    {
        if (duration != 0 && view.getVisibility() == View.VISIBLE)
        {
            Animation fadeAnimation = AnimationUtils.loadAnimation(c, android.R.anim.fade_out);
            fadeAnimation.setDuration(duration);
            view.startAnimation(fadeAnimation);
        }

        view.setVisibility(View.INVISIBLE);
    }
}
