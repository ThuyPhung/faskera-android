/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.fbfriend;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.faskera.common.ViewHolder;
import com.faskera.common.view.AnyTextview;
import com.faskera.era.R;
import com.faskera.utils.ImageLoader;
import com.faskera.view.activity.option.view.fbfriend.itemview.FriendHeader;
import com.faskera.view.activity.option.view.fbfriend.model.Friend;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author theanh
 * @since 1/9/2017
 */
public class FbFriendAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_CONTENT = 1;
    private Context mContext;
    private ArrayList<Object> mFriends;
    private LayoutInflater mInflater;
    private FbFriendCallback mCallback;

    public interface FbFriendCallback {
        void onButtonFollowClick(String userId, int position);
        void onButtonUnFollowClick(Friend friend, int position);
        void onButtonFollowAllClick(ArrayList<String> listUserId);
        void onUserPageClick(String userId);
    }

    public FbFriendAdapter(Context context, ArrayList<Object> listItems) {
        mContext = context;
        mFriends = listItems;
    }

    public void setOnButtonFollowClickListener(FbFriendCallback callback) {
        mCallback = callback;
    }

    public void updateListFriends(List<Objects> listFriend) {
        mFriends.clear();
        mFriends.addAll(listFriend);
        notifyDataSetChanged();
    }

    public void updateListFollowAll() {
        for (int i = 1; i < mFriends.size(); i++) {
            if (mFriends.get(i) instanceof Friend) {
                Friend friend = (Friend) mFriends.get(i);
                if (!friend.following) friend.following = true;
                mFriends.set(i, friend);
            }
        }
        notifyDataSetChanged();
    }

    public void updateUnfollowUser(int position) {
        ((Friend) mFriends.get(position)).following = false;
        notifyItemChanged(position);
    }

    public void updateFollowUser(int position) {
        ((Friend) mFriends.get(position)).following = true;
        notifyItemChanged(position);
    }

    public ArrayList<String> getListUserId() {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < mFriends.size(); i++) {
            if (mFriends.get(i) instanceof Friend) {
                Friend friend = (Friend) mFriends.get(i);
                list.add(friend.id);
            }
        }
        return list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mInflater == null) mInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                return new HeaderViewHolder(
                    mInflater.inflate(R.layout.row_header_find_fb_friends, parent, false));
            case VIEW_TYPE_CONTENT:
                return new FriendViewHolder(
                    mInflater.inflate(R.layout.row_find_fb_friends, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case VIEW_TYPE_HEADER:
                ((HeaderViewHolder) holder).bind(position);
                break;
            case VIEW_TYPE_CONTENT:
                ((FriendViewHolder) holder).bind(position);
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mFriends != null ? mFriends.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? VIEW_TYPE_HEADER : VIEW_TYPE_CONTENT;
    }

    public class HeaderViewHolder extends ViewHolder {
        @BindView(R.id.text_total_friend)
        AnyTextview mTextTotalFriend;
        @BindView(R.id.btn_follow_all)
        AnyTextview mBtnFollowAll;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(int position) {
            if (mFriends.get(position) instanceof FriendHeader) {
                FriendHeader header = (FriendHeader) mFriends.get(position);
                mTextTotalFriend.setText(
                    mContext.getString(R.string.title_follow_all, header.getTotalFriends()));
                mBtnFollowAll
                    .setOnClickListener(view -> mCallback.onButtonFollowAllClick(getListUserId()));
            }
        }
    }

    public class FriendViewHolder extends ViewHolder {
        @BindView(R.id.btn_follow)
        AnyTextview mBtnFollow;
        @BindView(R.id.image_avatar)
        ImageView mImageAvatar;
        @BindView(R.id.text_name)
        AnyTextview mTextName;

        public FriendViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(int position) {
            if (mFriends.get(position) instanceof Friend) {
                Friend friend = (Friend) mFriends.get(position);
                if (friend.profilePhoto != null) {
                    ImageLoader
                        .loadImageViewWith(mContext, friend.profilePhoto.original, mImageAvatar);
                } else {
                    ImageLoader
                        .loadImageViewWith(mContext, R.drawable.ic_avatar_default, mImageAvatar);
                }
                mTextName.setText(friend.fullName);
                mBtnFollow.setText(friend.following ? mContext.getString(R.string.following_caps) :
                    mContext.getResources().getString(R.string.action_follow));
                mBtnFollow.setSelected(friend.following);
                mBtnFollow.setOnClickListener(view -> {
                    if (mBtnFollow.isSelected()) mCallback.onButtonUnFollowClick(friend, position);
                    else mCallback.onButtonFollowClick(friend.id, position);
                });
                itemView.setOnClickListener(view -> mCallback.onUserPageClick(friend.id));
            }
        }
    }
}
