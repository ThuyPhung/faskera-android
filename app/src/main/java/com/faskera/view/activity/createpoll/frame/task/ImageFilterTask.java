/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.task;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import java.util.ArrayList;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;

public class ImageFilterTask extends AsyncTask<Void, Void, ArrayList<Bitmap>> {

    private GPUImage mGpuImage;
    private ArrayList<Bitmap> mListBitmap;
    private GPUImageFilter mFilter;
    private Context mContext;
    private OnCallBackAsyncTaskListener mOnCallBackAsyncTaskListener;

    public ImageFilterTask(Context mContext, GPUImageFilter filter, ArrayList<Bitmap> mListBitmap) {
        this.mListBitmap = new ArrayList<>();
        this.mFilter = filter;
        this.mContext = mContext;
        this.mListBitmap = mListBitmap;
    }

    public void setOnCallBackAsyncTaskListener(OnCallBackAsyncTaskListener mOnCallBackAsyncTaskListener) {
        this.mOnCallBackAsyncTaskListener = mOnCallBackAsyncTaskListener;
    }

    @Override
    protected void onPreExecute() {
        mGpuImage = new GPUImage(mContext);
        if (mOnCallBackAsyncTaskListener != null) {
            mOnCallBackAsyncTaskListener.onPreExecute();
        }
    }

    @Override
    protected ArrayList<Bitmap> doInBackground(Void... params) {
        ArrayList<Bitmap> mList = new ArrayList<>();
        if (null == mFilter) {
            return mListBitmap;
        }
        mGpuImage.setFilter(mFilter);
        for (Bitmap mDrawBitmap : mListBitmap) {
            if (isLiveAsyncTask(this, mContext)) {
                break;
            }
            if (mDrawBitmap != null) {
                mList.add(mGpuImage.getBitmapWithFilterApplied(mDrawBitmap));
            }
        }
        if (isLiveAsyncTask(this, mContext)) {
            return null;
        }
        return mList;
    }

    @Override
    protected void onPostExecute(ArrayList<Bitmap> mList) {
        if (isLiveAsyncTask(this, mContext)) {
            return;
        }
        if (mOnCallBackAsyncTaskListener != null && mList != null) {
            mOnCallBackAsyncTaskListener.onPostExecute(mList);
        }
    }

    private boolean isLiveAsyncTask(AsyncTask asyncTask, Context mContext) {
        return ((asyncTask != null && asyncTask.isCancelled()) || asyncTask == null || mContext == null);
    }

    public void removeBitmap() {
//        if (mListBitmap != null && mListBitmap.size() > 0) {
//            for (Bitmap mBitmap : mListBitmap) {
//                mBitmap.recycle();
//            }
//        }
    }
}