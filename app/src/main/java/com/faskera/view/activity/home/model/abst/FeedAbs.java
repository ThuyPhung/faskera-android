/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home.model.abst;

import android.content.ContentValues;

import com.faskera.view.activity.home.model.Feed;
import com.faskera.view.activity.home.model.FeedViewDetail;

import java.util.ArrayList;

/**
 * @author DANGNGOCDUC
 * @since 1/21/2017
 */

public abstract class FeedAbs  {

    //<editor-fold desc="variable">
    protected ArrayList<AbsFeedAns> mListAsw;

    protected AbsFeedContentMedia mAbsFeedContentMedia;

    protected AbsFeedContentText mAbsFeedContentText;

    protected AbsFeedHeader mAbsFeedHeader;

    protected AbsFeedFooter mAbsFeedFooter;

    protected  FeedViewDetail mFeedViewDetail;

    protected AbsFeedSuperHeader mAbsFeedSuperHeader;
    //</editor-fold>

    public abstract ArrayList<AbsFeedAns> getFeedAns();

    public abstract AbsFeedContentMedia getFeedContentMedia();

    public abstract AbsFeedContentText getFeedContentText();

    public abstract AbsFeedHeader getFeedHeader();

    public abstract AbsFeedSuperHeader getFeedSuperHeader();

    public abstract FeedViewDetail getFeedViewDetail();

    public abstract AbsFeedFooter getFeedFooter();

    public abstract String getPollId();

    public abstract String getPollLink();

    public abstract Feed.Poll getPoll();

    public abstract ContentValues toContentValue(boolean ishome);

    public abstract void cloneSateObject(FeedAbs target);


}
