/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.utils;

public enum PhotoType {
    ONE_PHOTO(0), TWO_PHOTO(0), THREE_PHOTO(2), FOUR_PHOTO(0), FIVE_PHOTO(0) ,SEX_PHOTO(0),;

    int mValue;

    PhotoType(int value) {
        mValue = value;
    }

    public int value() {
        return mValue;
    }
}
