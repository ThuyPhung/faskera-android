/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.profile;

import android.widget.ProgressBar;

import com.faskera.callback.OnClickItemToolbarListener;
import com.faskera.era.R;
import com.faskera.model.option.TbOption;
import com.faskera.view.activity.option.OptionBaseActivity;
import com.faskera.view.activity.option.view.profile.EditPhotoDialog;
import com.faskera.view.activity.option.view.profile.EditProfileFragment;
import com.faskera.view.widget.ToolbarOptions;

import butterknife.BindView;

public class EditProfileActivity extends OptionBaseActivity implements EditPhotoDialog.EditPhotoBottomSheetCallback {

    @BindView(R.id.toolbar)
    ToolbarOptions mToolbar;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    public void goToBack() {
        onBack(null);
    }

    @Override
    protected int getIdLayout() {
        return R.layout.activity_edit_profile;
    }

    @Override
    protected void initView() {
        super.initView();
        mToolbar.setOnClickItemToolbarListener(new OnClickItemToolbarListener() {
            @Override
            public void onBack() {
                goToBack();
            }

            @Override
            public void onDone() {}
        });
        goToFragment(new EditProfileFragment(), null, false);
    }

    @Override
    protected void initData() {

    }

    @Override
    public void update(Object o) {
        super.update(o);
        if (o instanceof TbOption) {
            mToolbar.setToolbarModel((TbOption) o);
        } else if (o instanceof OnClickItemToolbarListener) {
            mToolbar.setOnClickItemToolbarListener((OnClickItemToolbarListener) o);
        }
    }

    @Override
    public void onBackPressed() {
        onBack(null);
    }

    /**
     * Change avatar callback
     */

    @Override
    public void onChangePhotoClick() {
        EditProfileFragment fragment = (EditProfileFragment)
                getSupportFragmentManager().findFragmentByTag(EditProfileFragment.class.getSimpleName());
        if (fragment != null)
            fragment.showGallery();
    }

    @Override
    public void onRemovePhotoClick() {
        EditProfileFragment fragment = (EditProfileFragment)
                getSupportFragmentManager().findFragmentByTag(EditProfileFragment.class.getSimpleName());
        if (fragment != null)
            fragment.removePhoto();
    }

}
