/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.faskera.callback.OnClickItemRecyclerListener;
import com.faskera.common.ViewHolder;
import com.faskera.era.R;
import com.faskera.model.createpoll.AddOptionModel;

import java.util.List;

import butterknife.BindView;

public class AddOptionAdapter extends GenericRecycleAdapter<AddOptionModel,
        AddOptionAdapter.AddOptionViewHolder> {

    private OnClickItemRecyclerListener mOnClickItemRecyclerListener;

    public AddOptionAdapter(List<AddOptionModel> mList, Context context) {
        super(mList, context);
    }

    @Override
    public int getLayout() {
        return R.layout.row_add_option;
    }

    @Override
    protected RecyclerView.ViewHolder getHolderViewHolder(View v) {
        return new AddOptionViewHolder(v);
    }

    @Override
    protected void onItem(AddOptionModel item, int position) {

    }

    @Override
    protected void onSet(AddOptionModel item, AddOptionViewHolder holder, int position) {
        if (item != null) {
            holder.mTitle.setText(item.text);
            holder.mIvDelete.setOnClickListener(v -> {
                if (mOnClickItemRecyclerListener!=null) {
                    mOnClickItemRecyclerListener.onClick(item);
                }
            });
        }
    }

    public class AddOptionViewHolder extends ViewHolder {
        @BindView(R.id.tv_title)
        TextView mTitle;

        @BindView(R.id.iv_delete)
        ImageView mIvDelete;

        public AddOptionViewHolder(View itemView) {
            super(itemView);
        }
    }

    public void setOnClickItemRecyclerListener(OnClickItemRecyclerListener mOnClickItemRecyclerListener) {
        this.mOnClickItemRecyclerListener = mOnClickItemRecyclerListener;
    }
}
