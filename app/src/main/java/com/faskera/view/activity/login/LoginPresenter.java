/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.login;

import android.content.Context;

import com.faskera.common.AppSharePreference;
import com.faskera.common.base.ObserverAdvance;
import com.faskera.model.user.UserInfoResponse;
import com.faskera.model.user.token.OAuthToken;
import com.faskera.network.ApiManager;

import rx.subscriptions.CompositeSubscription;

/**
 * @author theanh
 * @since 1/3/2017
 */
public class LoginPresenter implements LoginContract.Presenter {
    public static final String CLIENT_ID = "faskera";
    public static final String CLIENT_SECRET = "secret";
    public static final String GRANT_TYPE = "connect";
    public static final String PROVIDER_ID = "facebook";
    private Context mContext;
    private LoginContract.View mView;
    private CompositeSubscription mCompositeSubscription;

    public LoginPresenter(Context context, LoginContract.View view) {
        mContext = context;
        mView = view;
        mView.setPresenter(this);
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void loginWithFacebook(String accessToken) {
        AppSharePreference.getInstance(mContext).storeFaceToken(accessToken);
        mView.showLoading();
        // accessToken = "EAAPNdp7u354BAGU4EZBZCVTvpFhng5EbFBYxMTqrPIfEZClu9OMQjeQ2MW7qCkYZCC3i4SS1LYodtMVI6TacXEEvzoOVxvXAfDGzypBAVw6Jn2h1xFu8alS4L7R8jV3duBO6uA2leIA62ZB2ZAscIcfP6UXjeF06Us82RCcjCCQxMTBL9t14p74BS2Vt2ZC6A772fKcZBtcHBKaz4Kp9If1ywdZBKexScOT0ZD";
        mCompositeSubscription.add(ApiManager.getInstance()
            .oauthFace(CLIENT_ID, CLIENT_SECRET, GRANT_TYPE, accessToken, PROVIDER_ID,
                new ObserverAdvance<OAuthToken>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(int code) {
                        if (mView == null) return;
                        mView.hideLoading();
                        mView.onErrorLoginFacebook();
                    }

                    @Override
                    public void onSuccess(OAuthToken data) {
                        if (mView == null) return;
                        mView.hideLoading();
                        mView.onAuthTokenSuccess(data);
                    }
                }));
    }

    @Override
    public void getUserInfo() {
        mView.showLoading();
        mCompositeSubscription.add(
            ApiManager.getInstance().getUserInfo(mContext, new ObserverAdvance<UserInfoResponse>() {
                @Override
                public void onCompleted() {
                }

                @Override
                public void onError(int code) {
                    if (mView == null) return;
                    mView.hideLoading();
                    mView.onErrorConnection(code);
                }

                @Override
                public void onSuccess(UserInfoResponse data) {
                    if (mView == null) return;
                    mView.hideLoading();
                    mView.onGetUserInfoSuccess(data);
                }
            }));
    }

    @Override
    public void subscribe() {
    }

    @Override
    public void unsubscribe() {
        if (mCompositeSubscription != null && mCompositeSubscription.isUnsubscribed()) {
            mCompositeSubscription.unsubscribe();
            mView = null;
        }
    }
}
