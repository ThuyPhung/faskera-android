/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home.model;

import com.faskera.view.activity.home.model.abst.AbsFeedFooter;
import com.faskera.view.activity.home.model.abst.FeedAbs;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public class FeedFooter extends AbsFeedFooter {


    private Feed.Poll mPoll;

    public FeedFooter(Feed.Poll poll) {
        mPoll = poll;
    }


    @Override
    public FeedAbs getPoll() {
        return mPoll;
    }

    @Override
    public String getPollID() {
        return mPoll.getPollId();
    }

    @Override
    public String getVote() {
        return String.valueOf(mPoll.mNbUsersAnswered);
    }

    @Override
    public int getVoteInt() {
        return mPoll.mNbUsersAnswered;
    }

    @Override
    public String getLink() {
        return mPoll.getPollLink();
    }
}
