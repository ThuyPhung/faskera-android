/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.topic.network;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author DANGNGOCDUC
 * @since 1/11/2017
 */

public class UpdateTopicInterestResponse implements Serializable {
    @SerializedName("error_type")
    public int mErrorType;
    @SerializedName("code")
    public int mCode;
    @SerializedName("error_message")
    public int mErrorMessage;
}
