/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.polldetail;

import android.content.Context;

import com.faskera.common.base.ObserverAdvance;
import com.faskera.network.ApiManager;
import com.faskera.view.activity.home.model.Feed;
import com.faskera.view.activity.polldetail.PollDetailContract.Presenter;

import rx.subscriptions.CompositeSubscription;

/**
 * @author DANGNGOCDUC
 * @since 1/4/2017
 */
public class PollDetailPresenter implements Presenter {
    private PollDetailContract.View mView;
    private Context mContext;
    private CompositeSubscription mSubscription = new CompositeSubscription();

    public PollDetailPresenter(Context context, PollDetailContract.View view) {
        mContext = context;
        mView = view;
        mView.setPresenter(this);
    }

    @Override
    public void getPollDetail(String pollID) {
        if (mView != null) mView.onLoading();
        mSubscription.add(ApiManager.getInstance()
            .getPollDetail(mContext, pollID, new ObserverAdvance<Feed.Poll>() {
                @Override
                public void onError(int code) {
                    mView.onLoadError(code);
                }

                @Override
                public void onSuccess(Feed.Poll data) {
                    if (mView != null) mView.onSuccess(data.genListItemDetail());
                }

                @Override
                public void onCompleted() {
                }
            }));
    }

    @Override
    public void onDestroy() {
        if (mSubscription != null && mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
            mView = null;
        }
    }
}
