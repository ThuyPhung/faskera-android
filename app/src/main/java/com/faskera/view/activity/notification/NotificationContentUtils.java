/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.notification;

import android.content.Context;
import android.os.Build;
import android.text.Html;

import com.faskera.era.R;

/**
 * @author theanh
 * @since 2/17/2017
 */
public class NotificationContentUtils {
    private static CharSequence HtmlForm(String form) {
        if (Build.VERSION.SDK_INT >= 24)
            return Html.fromHtml(form, Html.FROM_HTML_MODE_LEGACY);
        else
            return Html.fromHtml(form);
    }

    public static CharSequence newFbFriendUsing(Context context, String friendName) {
        return HtmlForm(context.getString(R.string.notification_friend_on_faskera, friendName));
    }

    public static CharSequence oneSharePoll(Context context, String nameFollowing,
                                            String question) {
        return HtmlForm(
            context.getString(R.string.notification_one_share_poll, nameFollowing, question));
    }

    public static CharSequence twoSharePoll(Context context, String first, String second,
                                            String question) {
        return HtmlForm(
            context.getString(R.string.notification_two_share_poll, first, second, question));
    }

    public static CharSequence manySharePoll(Context context, String first, int totalReactor,
                                             String question) {
        return HtmlForm(context.getString(R.string.notification_many_share_poll, first,
            String.valueOf(totalReactor - 1), question));
    }

    public static CharSequence oneVotePoll(Context context, String first, String question) {
        return HtmlForm(context.getString(R.string.notification_one_answer_poll, first, question));
    }

    public static CharSequence twoVotePoll(Context context, String first, String second,
                                           String question) {
        return HtmlForm(
            context.getString(R.string.notification_two_answer_poll, first, second, question));
    }

    public static CharSequence manyVotePoll(Context context, String first, int totalReactor,
                                            String question) {
        return HtmlForm(context.getString(R.string.notification_many_answer_poll, first,
            String.valueOf(totalReactor - 1), question));
    }

    public static CharSequence following(Context context, String nameFollower) {
        return HtmlForm(context.getString(R.string.notification_following, nameFollower));
    }

    public static CharSequence createPoll(Context context, String actorUser, String question) {
        return HtmlForm(context.getString(R.string.notification_create_poll, actorUser, question));
    }
}
