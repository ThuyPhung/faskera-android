/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.profile;

import android.content.Context;

import com.faskera.common.AppSharePreference;
import com.faskera.common.base.ObserverAdvance;
import com.faskera.common.model.BaseResponse;
import com.faskera.model.user.MyProfile;
import com.faskera.network.ApiManager;
import com.faskera.network.user.MyProfileBody;
import com.faskera.network.user.UpdateProfileRespond;

import rx.Observer;
import rx.subscriptions.CompositeSubscription;

import static java.net.HttpURLConnection.HTTP_OK;

/**
 * @author theanh
 * @since 1/12/2017
 */
public class EditProfilePresenter implements EditProfileContract.Presenter {
    private Context mContext;
    private EditProfileContract.View mView;
    private CompositeSubscription mCompositeSubscription;

    public EditProfilePresenter(Context context, EditProfileContract.View view) {
        this.mContext = context;
        this.mView = view;
        mView.setPresenter(this);
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getMyProfile() {
        mView.showLoading();
        mCompositeSubscription
            .add(ApiManager.getInstance().getMyProfile(mContext, new ObserverAdvance<MyProfile>() {
                @Override
                public void onError(int code) {
                    if (mView != null) {
                        mView.hideLoading();
                        mView.onErrorConnection(code);
                    }
                }

                @Override
                public void onSuccess(MyProfile data) {
                    AppSharePreference.getInstance(mContext).storeMyProfile(data);
                    if (mView != null) {
                        mView.hideLoading();
                        mView.onGetProfileSuccess();
                    }
                }

                @Override
                public void onCompleted() {
                }
            }));
    }

    @Override
    public void updateMyProfile(MyProfileBody body) {
        mView.showLoading();
        mCompositeSubscription.add(ApiManager.getInstance()
            .updateMyProfile(mContext, body, new ObserverAdvance<UpdateProfileRespond>() {
                @Override
                public void onError(int code) {
                    if (mView != null) {
                        mView.hideLoading();
                        mView.onErrorConnection(code);
                    }
                }

                @Override
                public void onSuccess(UpdateProfileRespond data) {
                    if (mView != null) {
                        mView.hideLoading();
                        mView.onUpdateProfileSuccess();
//                    //Update profile in shared preference
//                    MyProfile profile = AppSharePreference.getInstance(mContext).getMyProfileObj();
//                    profile.setFullname(body.fullname);
//                    profile.about =  (body.about != null ? body.about : "");
//                    profile.getOrganizationWork().setName(body.organizationWork != null ? body.organizationWork : "");
//                    profile.getOrganizationStudy().setName(body.organizationStudy != null ? body.organizationStudy : "");
//                    profile.getHometown().setName(body.hometown);
//                    profile.getPlaceLive().setName(body.placeLive != null ? body.placeLive : "");
//                    AppSharePreference.getInstance(mContext).storeMyProfile(profile);
//
//                    //Update user info
//                    UserInfoResponse userInfo = AppSharePreference.getInstance(mContext).getUserInfo();
//                    userInfo.setAbout(body.about);
//                    userInfo.setFullName(body.fullname);
//                    AppSharePreference.getInstance(mContext).storeUserInfo(userInfo);
                    }
                }

                @Override
                public void onCompleted() {
                }
            }));
    }

    @Override
    public void removePhoto() {
        mCompositeSubscription
            .add(ApiManager.getInstance().removePhoto(mContext, new Observer<BaseResponse>() {
                @Override
                public void onCompleted() {
                }

                @Override
                public void onError(Throwable e) {
                }

                @Override
                public void onNext(BaseResponse response) {
                    if (mView != null && response.code == HTTP_OK) mView.onRemovePhotoSuccess();
                }
            }));
    }

    @Override
    public void subscribe() {
        getMyProfile();
    }

    @Override
    public void unsubscribe() {
        if (mCompositeSubscription != null && mCompositeSubscription.isUnsubscribed()) {
            mCompositeSubscription.unsubscribe();
            mView = null;
        }
    }
}
