/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;

public class PhotoLinearLayout extends LinearLayout
{
    private OnSelectedStateChangedListener onSelectedStateChangedListener;

    public PhotoLinearLayout(Context context)
    {
        super(context);
    }

    public PhotoLinearLayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public PhotoLinearLayout(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev)
    {
        if(MotionEvent.ACTION_DOWN == ev.getAction() && null != onSelectedStateChangedListener)
        {
            onSelectedStateChangedListener.onSelectedStateChanged();
        }

        return super.onInterceptTouchEvent(ev);
    }

    public void setOnSelectedStateChangedListener(OnSelectedStateChangedListener onSelectedStateChangedListener)
    {
        this.onSelectedStateChangedListener = onSelectedStateChangedListener;
    }

    public interface OnSelectedStateChangedListener
    {
        void onSelectedStateChanged();
    }
}
