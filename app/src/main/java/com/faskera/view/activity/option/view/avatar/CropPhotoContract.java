/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.avatar;

import com.faskera.model.user.UserInfoResponse;
import com.faskera.mvp.BasePresenter;
import com.faskera.mvp.BaseView;

/**
 * @author theanh
 * @since 2/28/2017
 */
public interface CropPhotoContract {
    interface Presenter extends BasePresenter {
        void getUserInfo();
        void unsubscribe();
    }

    interface View extends BaseView<CropPhotoContract.Presenter> {
        void onGetUserInfoSuccess(UserInfoResponse userInfo);
        void onError(int code);
        void showLoading();
        void hideLoading();
    }
}
