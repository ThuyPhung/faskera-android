/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.task;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.faskera.view.activity.createpoll.frame.model.FilterItem;
import com.faskera.view.activity.createpoll.frame.model.PopupFilterItem;

import java.util.ArrayList;
import java.util.List;

import jp.co.cyberagent.android.gpuimage.GPUImage;

public class ImageRenderTask extends AsyncTask<Void, Void, List<PopupFilterItem>> {

    private GPUImage gpuImage;
    private List<FilterItem> filterList;
    private Context mContext;
    private OnCallBackAsyncTaskListener mOnCallBackAsyncTaskListener;
    private Bitmap mOriginBitmap;

    public ImageRenderTask(Context mContext, List<FilterItem> filterList,Bitmap mOriginBitmap) {
        this.filterList = filterList;
        this.mContext = mContext;
        this.mOriginBitmap = mOriginBitmap;
    }

    public void setOnCallBackAsyncTaskListener(OnCallBackAsyncTaskListener mOnCallBackAsyncTaskListener) {
        this.mOnCallBackAsyncTaskListener = mOnCallBackAsyncTaskListener;
    }

    @Override
    protected void onPreExecute() {
        gpuImage = new GPUImage(mContext);
        if (mOnCallBackAsyncTaskListener != null) {
            mOnCallBackAsyncTaskListener.onPreExecute();
        }
    }

    @Override
    protected List<PopupFilterItem> doInBackground(Void... params) {
        List<PopupFilterItem> popupFilterList = new ArrayList<>();
        int size = filterList.size();

        for (int i = 0; i < size; i++) {
            FilterItem filterItem = filterList.get(i);
            gpuImage.setFilter(filterItem.getFilter());
            Bitmap pvBitmap = gpuImage.getBitmapWithFilterApplied(mOriginBitmap);

            PopupFilterItem item = new PopupFilterItem();
            item.setBitmap(pvBitmap);
            item.setFilterItem(filterList.get(i));
            item.setTitle(mContext.getString(filterList.get(i).getName()));

            popupFilterList.add(item);
        }
        if (isLiveAsyncTask(this, mContext)) {
            return null;
        }
        return popupFilterList;
    }

    @Override
    protected void onPostExecute(List<PopupFilterItem> result) {
        if (isLiveAsyncTask(this, mContext)) {
            return;
        }
        if (mOnCallBackAsyncTaskListener != null) {
            mOnCallBackAsyncTaskListener.onPostExecute(result);
        }
    }

    private boolean isLiveAsyncTask(AsyncTask asyncTask, Context mContext) {
        return ((asyncTask != null && asyncTask.isCancelled()) || asyncTask == null || mContext == null);
    }
}