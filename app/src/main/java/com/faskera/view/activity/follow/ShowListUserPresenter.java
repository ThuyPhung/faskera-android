/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.follow;

import android.content.Context;

import com.faskera.common.AppSharePreference;
import com.faskera.common.UserDefine;
import com.faskera.common.adapter.ListPeople;
import com.faskera.common.base.ObserverAdvance;
import com.faskera.network.ApiManager;

import java.util.ArrayList;

import rx.Subscription;

/**
 * @author DANGNGOCDUC
 * @since 2/2/2017
 */

public class ShowListUserPresenter implements ShowListUserContract.Presenter {
    private Context mContext;
    private ShowListUserContract.View mView;
    private String mUserId;
    private String mNextMaxId = "";
    private boolean allowLoadMore = true;

    @UserDefine.StateFollow
    int mTypeListUser;


    public ShowListUserPresenter(Context context, ShowListUserContract.View view, String userId, int typeList) {
        mContext =  context;
        mView = view;
        mUserId = userId;
        mTypeListUser = typeList;
        mView.setPresenter(this);
    }

    @Override
    public Subscription getUserList(boolean refresh) {
        if ( !allowLoadMore)  return null;

        if(refresh) {
            mNextMaxId = "";
            allowLoadMore = true;
        } else {
            if (mNextMaxId == null || mNextMaxId.trim().isEmpty()) {
                mView.onLoading();
            } else {
                mView.onLoadMore();
            }
        }
        //<editor-fold desc="path API">
        String pathUserId, pathTypeList;

        if (mUserId.equals(AppSharePreference.getUid(mContext))) {
            pathUserId = "me";
        } else {
            pathUserId = mUserId;
        }

        if (mTypeListUser == UserDefine.STATE_FOLLOW) {
            pathTypeList = "follower";
        } else {
            pathTypeList = "following";
        }
        //</editor-fold>
        allowLoadMore = false;

        return ApiManager.getInstance().getListUser(mContext, pathUserId, pathTypeList, mNextMaxId, new ObserverAdvance<ListPeople>() {
            @Override
            public void onError(int code) {
                 mView.onLoadError(code);
            }

            @Override
            public void onSuccess(ListPeople data) {
                if (data.mNextMaxId != null && !data.mNextMaxId.isEmpty()) {
                    mNextMaxId = data.mNextMaxId;
                    allowLoadMore = true;
                }
                if (mNextMaxId == null || mNextMaxId.trim().isEmpty()) {
                    allowLoadMore = false;
                }
                mView.onLoadSuccess(new ArrayList<>(data.mlistUser));
            }

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                mView.onLoadError(e);
            }
        });

    }
}
