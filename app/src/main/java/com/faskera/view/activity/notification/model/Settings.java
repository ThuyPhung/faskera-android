/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.notification.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author framgia
 * @since 14/02/2017
 */
public class Settings {

    @SerializedName("anonymous")
    @Expose
    public boolean anonymous;
    @SerializedName("multiChoice")
    @Expose
    public boolean multiChoice;

}