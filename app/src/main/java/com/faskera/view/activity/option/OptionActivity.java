/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option;

import android.content.Intent;
import android.support.v4.content.IntentCompat;
import android.widget.ProgressBar;

import com.faskera.Constant;
import com.faskera.MainApplication;
import com.faskera.callback.OnClickItemToolbarListener;
import com.faskera.common.AppSharePreference;
import com.faskera.common.LocaleHelper;
import com.faskera.era.R;
import com.faskera.model.option.TbOption;
import com.faskera.view.activity.home.HomeActivityOptimize;
import com.faskera.view.activity.option.view.language.SwitchLanguageDialog;
import com.faskera.view.activity.option.view.profile.EditPhotoDialog;
import com.faskera.view.activity.option.view.profile.EditProfileFragment;
import com.faskera.view.widget.CustomAlertDialog;
import com.faskera.view.widget.ToolbarOptions;

import butterknife.BindView;

public class OptionActivity extends OptionBaseActivity
    implements EditPhotoDialog.EditPhotoBottomSheetCallback,
    SwitchLanguageDialog.ChangeLanguageBottomSheetCallback {
    @BindView(R.id.toolbar)
    ToolbarOptions mToolbar;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    public void goToBack() {
        onBack(null);
    }

    @Override
    protected int getIdLayout() {
        return R.layout.activity_option;
    }

    @Override
    protected void initView() {
        super.initView();
        mToolbar.setOnClickItemToolbarListener(new OnClickItemToolbarListener() {
            @Override
            public void onBack() {
                goToBack();
            }

            @Override
            public void onDone() {
            }
        });
        goToFragment(new OptionFragment(), null, false);
    }

    @Override
    protected void initData() {
    }

    @Override
    public void update(Object o) {
        super.update(o);
        if (o instanceof TbOption) {
            mToolbar.setToolbarModel((TbOption) o);
        } else if (o instanceof OnClickItemToolbarListener) {
            mToolbar.setOnClickItemToolbarListener((OnClickItemToolbarListener) o);
        } else if (o == null) {
            mToolbar.disableAction();
        }
    }

    @Override
    public void onBackPressed() {
        onBack(null);
    }

    /**
     * Change avatar callback
     */
    @Override
    public void onChangePhotoClick() {
        EditProfileFragment fragment = (EditProfileFragment)
            getSupportFragmentManager()
                .findFragmentByTag(EditProfileFragment.class.getSimpleName());
        if (fragment != null) fragment.showGallery();
    }

    @Override
    public void onRemovePhotoClick() {
        EditProfileFragment fragment = (EditProfileFragment)
            getSupportFragmentManager()
                .findFragmentByTag(EditProfileFragment.class.getSimpleName());
        if (fragment != null)
            fragment.removePhoto();
    }

    /**
     * Choose language callback
     */
    @Override
    public void onChooseEnglish() {
        showChooseLanguageDialog(Constant.Language.EN_US_CODE);
    }

    @Override
    public void onChooseVietnamese() {
        showChooseLanguageDialog(Constant.Language.VN_CODE);
    }

    private void showChooseLanguageDialog(String localeCode) {
        if (AppSharePreference.getInstance(this).getMyLanguage().equals(localeCode)) return;
        CustomAlertDialog alertDialog = new CustomAlertDialog(this);
        alertDialog.setMessage(getString(R.string.option_language_confirm));
        alertDialog.setYesButton(getString(R.string.action_ok), view -> {
            AppSharePreference.getInstance(this).setMyLanguage(localeCode);
            LocaleHelper.setLocale(this, localeCode);
            ((MainApplication) getApplication()).initLanguage();
            Intent intent = new Intent(this, HomeActivityOptimize.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        });
        alertDialog.setNoButton(getString(R.string.action_cancel), view -> alertDialog.dismiss());
        alertDialog.show();
    }
}