/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.view;


import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import com.faskera.era.R;
import com.faskera.view.widget.BaseRelativeLayout;

import butterknife.BindView;

public class RowSettingView extends BaseRelativeLayout {

    @BindView(R.id.switch_btn)
    Switch mSwitch;

    @BindView(R.id.tv_title)
    TextView mTextView;

    private int mColor;
    private String mTextTitle;
    private int mTextStyle;
    private OnSwitchChangeListener mOnSwitchChangeListener;

    public RowSettingView(Context context) {
        super(context);
    }

    public RowSettingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RowSettingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void setStyle(AttributeSet attrs) {
        TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs,
                R.styleable.MyCustomElement, 0, 0);
        try {
            mTextTitle = a.getString(R.styleable.MyCustomElement_text);
            mColor = a.getColor(R.styleable.MyCustomElement_textColor, 0xff000000);
            mTextStyle = a.getInt(R.styleable.MyCustomElement_textColor, 0);
        } finally {
            a.recycle();
        }
    }

    @Override
    protected int getIdLayoutRes() {
        return R.layout.row_setting_create_poll;
    }

    @Override
    protected void initView() {
        setTitle();
        mSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (mOnSwitchChangeListener != null) {
                mOnSwitchChangeListener.onChange();
            }
        });
    }

    public void setTitle() {
        if (mTextView != null) {
            mTextView.setText(mTextTitle);
            mTextView.setTextColor(mColor);
        }
    }

    @Override
    public void onClick(View v) {

    }

    public boolean isCheck() {
        return mSwitch.isChecked();
    }

    public void setCheck(boolean isCheck) {
        this.mSwitch.setChecked(isCheck);
    }

    public void setOnSwitchChangeListener(OnSwitchChangeListener mOnSwitchChangeListener) {
        this.mOnSwitchChangeListener = mOnSwitchChangeListener;
    }

    public interface OnSwitchChangeListener {
        void onChange();
    }
}
