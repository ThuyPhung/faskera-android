/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;

import com.faskera.callback.OnClickItemToolbarListener;
import com.faskera.era.R;
import com.faskera.model.createpoll.TbCreatePoll;

public class CreatePollActivity extends CreatePollBaseActivity implements CreatePollContract.View {

    private Bitmap mBitmapView;

    @Override
    protected int getIdLayout() {
        return R.layout.activity_create_poll;
    }

    @Override
    protected void initView() {
        super.initView();
        mToolbarCreatePoll.setOnClickItemToolbarListener(new OnClickItemToolbarListener() {
            @Override
            public void onBack() {
                goToBack();
            }

            @Override
            public void onDone() {
                gotoDone();
            }
        });
    }


    private void goToBack() {
        onBack(null);
    }

    @Override
    protected void initData() {
        super.initData();
        goToFragment(new CreatePollFragment(), getIntent().getExtras(), false);
    }

    @Override
    public void update(Object o) {
        super.update(o);
        if (o instanceof TbCreatePoll) {
            TbCreatePoll mTbCreatePoll = (TbCreatePoll) o;
            mToolbarCreatePoll.setToolBarModel(mTbCreatePoll);
        }
    }

    @Override
    public void setPresenter(CreatePollContract.Presenter presenter) {

    }

    public void setBitmapView(Bitmap mBitmapView) {
        this.mBitmapView = mBitmapView;
    }

    public Bitmap getBitmapView() {
        return mBitmapView;
    }


    Intent mData;

    void onSuccess() {

        mToolbarCreatePoll.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mData != null) {
                    setResult(Activity.RESULT_OK, mData);
                }
                finish();
            }
        }, 500);
    }


}
