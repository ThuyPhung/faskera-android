/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home.model;

import com.faskera.view.activity.home.model.abst.AbsFeedContentText;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public class FeedContentText extends AbsFeedContentText {
    private Feed.Poll mPoll;



    public FeedContentText(Feed.Poll poll) {
        mPoll = poll;
    }
    @Override
    public String getContent() {
        return mPoll.mQuestion.mText;
    }
}
