/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.fbfriend.model;

import com.faskera.common.UserDefine;
import com.faskera.common.adapter.AbsUserState;
import com.faskera.model.user.profile.ProfilePhoto;
import com.google.gson.annotations.SerializedName;

/**
 * @author theanh
 * @since 1/28/2017
 */
public class Friend implements AbsUserState {
    @SerializedName("id")
    public String id;
    @SerializedName("fullname")
    public String fullName;
    @SerializedName("profilePhoto")
    public ProfilePhoto profilePhoto;
    @SerializedName("following")
    public boolean following;
    @SerializedName("relationship")
    public String relationship;

    @Override
    public String getAvatar() {
        return profilePhoto != null ? profilePhoto.original : null;
    }

    @Override
    public String getUid() {
        return id;
    }

    @Override
    public String getName() {
        return fullName;
    }

    @Override
    public int getSate() {
        return following ? UserDefine.STATE_FOLLWING : UserDefine.STATE_FOLLOW;
    }

    @Override
    public void setSate(@UserDefine.StateFollow int state) {
        following = (state == UserDefine.STATE_FOLLWING);
    }

    @Override
    public int getType() {
        return TYPE_USER;
    }
}
