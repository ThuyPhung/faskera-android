/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.avatar;

import android.Manifest;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.faskera.callback.OnClickItemToolbarListener;
import com.faskera.common.base.BaseFragment;
import com.faskera.era.R;
import com.faskera.model.createpoll.ImageModel;
import com.faskera.model.option.TbOption;
import com.faskera.utils.ActivityUtils;
import com.faskera.utils.DisplayUtility;
import com.faskera.utils.MediaUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static com.faskera.Constant.REQUEST_EDIT_PROFILE;

/**
 * @author theanh
 * @since 1/16/2017
 */
public class ChoosePhotoFragment extends BaseFragment
    implements LoaderManager.LoaderCallbacks<Cursor>, ImageAdapter.onImageClickListener,
    EasyPermissions.PermissionCallbacks {
    private static final int RC_READ_EXTERNAL_STORAGE_PERM = 934;
    @BindView(R.id.recycle_view)
    RecyclerView mRecycleView;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    ImageAdapter mAdapter;

    @Override
    protected int idRoot() {
        return R.layout.fragment_pick_photo;
    }

    @Override
    protected void initView(View root) {
    }

    private void setUpRecyclerView(List<ImageModel> images) {
        int numOfColumns = DisplayUtility.isInLandscapeMode(getActivity()) ? 4 : 3;
        mAdapter = new ImageAdapter(getContext(), images);
        mAdapter.setOnImageClickListener(this);
        mRecycleView.setLayoutManager(new GridLayoutManager(getContext(), numOfColumns));
        mRecycleView.setAdapter(mAdapter);
    }

    @Override
    protected void initData() {
    }

    @Override
    public void onRefresh(Object o) {
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (EasyPermissions
            .hasPermissions(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
            getLoaderManager().initLoader(0, null, this);
        } else {
            EasyPermissions.requestPermissions(this,
                getString(R.string.permission_storage),
                RC_READ_EXTERNAL_STORAGE_PERM,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        mProgressBar.setVisibility(View.VISIBLE);
        String[] projects = MediaUtils.PROJECT_PHOTO;
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String order = MediaStore.MediaColumns.DATE_ADDED + " DESC";
        return new CursorLoader(getActivity(), uri, projects, null, null, order);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mProgressBar.setVisibility(View.GONE);
        if (cursor != null && cursor.getCount() > 0) {
            List<ImageModel> items = new ArrayList<>();
            long backupSize = -1L;
            Uri backupUri = null;
            int index = 0;
            while (cursor.moveToNext()) {
                Uri uri = MediaUtils.getPhotoUri(cursor);
                long size = MediaUtils.getSize(cursor);
                if (uri != null && size > 0) {
                    if (size == backupSize && index <= MediaUtils.FILE_CHECKING_RANGE) {
                        String path = MediaUtils.getPathFromUri(getActivity(), uri);
                        if (path != null) {
                            File duplicateFile = new File(path);
                            if (duplicateFile.exists() && duplicateFile.delete()) {
                                MediaUtils.delete(getActivity(), uri);
                            } else if (duplicateFile.exists()) {
                                // Log.i("Do not delete file: " + duplicateFile.getAbsolutePath());
                                String otherPath =
                                    MediaUtils.getPathFromUri(getActivity(), backupUri);
                                if (otherPath != null && new File(otherPath).delete()) {
                                    items.remove(items.size());
                                    items.add(
                                        new ImageModel(MediaUtils.getId(cursor), uri.toString(),
                                            null));
                                    MediaUtils.delete(getActivity(), backupUri);
                                    backupSize = size;
                                    backupUri = uri;
                                }
                            }
                        }
                    } else {
                        items.add(new ImageModel(MediaUtils.getId(cursor), uri.toString(), null));
                        backupSize = size;
                        backupUri = uri;
                    }
                }
                index++;
            }
            setUpRecyclerView(items);
        } else {
            mProgressBar.setVisibility(View.GONE);
            ActivityUtils.showToast(getContext(), getString(R.string.msg_no_images_in_device));
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public void onResume() {
        super.onResume();
        Resources resource = getContext().getResources();
        getBaseActivity().update(new TbOption(resource.getString(R.string.tb_gallery), false));
        getBaseActivity().update(new OnClickItemToolbarListener() {
            @Override
            public void onBack() {
                getBaseActivity().onBack(null);
            }

            @Override
            public void onDone() {
            }
        });
        // getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public void onImageClick(ImageModel image) {
        Intent intent = CropPhotoActivity.getUriIntent(getContext(), image.getThumb());
        startActivityForResult(intent, REQUEST_EDIT_PROFILE);
        getBaseActivity().onBack(null);
    }

    /**
     * Return permission request storage result
     *
     * @param requestCode
     * @param perms
     */
    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this, getString(R.string.msg_permissions_ask_again))
                .build().show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            // Do something after user returned from app settings screen, like showing a Toast.
            getBaseActivity().onBack(null);
        }
    }
}
