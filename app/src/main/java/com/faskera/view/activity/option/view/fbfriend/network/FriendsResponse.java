/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.fbfriend.network;

import com.faskera.view.activity.option.view.fbfriend.model.Friend;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * @author theanh
 * @since 1/28/2017
 */

public class FriendsResponse {
    @SerializedName("list")
    public ArrayList<Friend> listFriends;
}
