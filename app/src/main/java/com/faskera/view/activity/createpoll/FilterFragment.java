/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.faskera.common.base.BaseFragment;
import com.faskera.era.R;
import com.faskera.view.activity.createpoll.frame.adapter.PhotoPopupAdapter;
import com.faskera.view.activity.createpoll.frame.filter.GPUImageFilterTools;
import com.faskera.view.activity.createpoll.frame.model.FilterItem;
import com.faskera.view.activity.createpoll.frame.model.PopupFilterItem;
import com.faskera.view.activity.createpoll.frame.task.ImageFilterTask;
import com.faskera.view.activity.createpoll.frame.task.ImageRenderTask;
import com.faskera.view.activity.createpoll.frame.task.OnCallBackAsyncTaskListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;

public class FilterFragment extends BaseFragment {

    private boolean isChangeLayout;
    private Bitmap mOriginBitmap;
    private List<FilterItem> mFilterList;
    private List<PopupFilterItem> mPopupFilterItems;
    private PhotoPopupAdapter mPhotoPopupAdapter;
    private OnListenerLoadFilter mOnListenerLoadFilter;
    private ArrayList<Bitmap> mListBitmap;
    private int mPosition;
    private boolean isDestroy;
    private ImageFilterTask mImageFilterTask;

    @BindView(R.id.rl_filter)
    RecyclerView mRecyclerFilter;

    public void setOnListenerLoadFilter(OnListenerLoadFilter mOnListenerLoadFilter) {
        this.mOnListenerLoadFilter = mOnListenerLoadFilter;
    }

    public static FilterFragment getInstance(OnListenerLoadFilter mOnListenerLoadFilter,
                                             ArrayList<Bitmap> mListBitmap,
                                             int position) {
        FilterFragment mFilterFragment = new FilterFragment();
        mFilterFragment.setOnListenerLoadFilter(mOnListenerLoadFilter);
        mFilterFragment.mListBitmap = mListBitmap;
        mFilterFragment.mPosition = position;
        return mFilterFragment;
    }

    @Override
    protected int idRoot() {
        return R.layout.fragment_filter;
    }

    private void initRecycler(RecyclerView mRecycler) {
        mRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mRecycler.setHasFixedSize(true);
    }

    @Override
    protected void initView(View root) {
        initRecycler(mRecyclerFilter);
    }

    @Override
    protected void initData() {
        setAdapterFilter();
    }

    @Override
    public void onRefresh(Object o) {

    }

    private List<FilterItem> filterLists = new ArrayList<>();
    private GPUImageFilterTools.FilterList mList = new GPUImageFilterTools.FilterList();

    public List<FilterItem> getFilterLists() {
        int index = 0;
        initDataFilter();
        for (GPUImageFilterTools.FilterType mFilterType : mList.filters) {
            filterLists.add(new FilterItem(GPUImageFilterTools.createFilterForType(getBaseActivity(), mFilterType), R.color.filter_color0, mList.names.get(index)));
            index++;
        }
        return filterLists;
    }

    private void initDataFilter() {
        mList.addFilter("Default", GPUImageFilterTools.FilterType.NORMAL);
        mList.addFilter("1977", GPUImageFilterTools.FilterType.I_1977);
        mList.addFilter("Amaro", GPUImageFilterTools.FilterType.I_AMARO);
        mList.addFilter("Brannan", GPUImageFilterTools.FilterType.I_BRANNAN);
        mList.addFilter("Earlybird", GPUImageFilterTools.FilterType.I_EARLYBIRD);
        mList.addFilter("Hefe", GPUImageFilterTools.FilterType.I_HEFE);
        mList.addFilter("Hudson", GPUImageFilterTools.FilterType.I_HUDSON);
        mList.addFilter("Inkwell", GPUImageFilterTools.FilterType.I_INKWELL);
        mList.addFilter("Lomo", GPUImageFilterTools.FilterType.I_LOMO);
        mList.addFilter("LordKelvin", GPUImageFilterTools.FilterType.I_LORDKELVIN);
        mList.addFilter("Nashville", GPUImageFilterTools.FilterType.I_NASHVILLE);
        mList.addFilter("Rise", GPUImageFilterTools.FilterType.I_NASHVILLE);
        mList.addFilter("Sierra", GPUImageFilterTools.FilterType.I_SIERRA);
        mList.addFilter("Sutro", GPUImageFilterTools.FilterType.I_SUTRO);
        mList.addFilter("Toaster", GPUImageFilterTools.FilterType.I_TOASTER);
        mList.addFilter("Valencia", GPUImageFilterTools.FilterType.I_VALENCIA);
        mList.addFilter("Walden", GPUImageFilterTools.FilterType.I_WALDEN);
        mList.addFilter("Xproll", GPUImageFilterTools.FilterType.I_XPROII);
        mList.addFilter("Contrast", GPUImageFilterTools.FilterType.NORMAL);
        mList.addFilter("Brightness", GPUImageFilterTools.FilterType.ACV_AIMEI);
        mList.addFilter("Sepia", GPUImageFilterTools.FilterType.ACV_DANLAN);
        mList.addFilter("Vignette", GPUImageFilterTools.FilterType.ACV_DANHUANG);
        mList.addFilter("ToneCurve", GPUImageFilterTools.FilterType.ACV_FUGU);
        mList.addFilter("Lookup (Amatorka)", GPUImageFilterTools.FilterType.ACV_GAOLENG);
        mList.addFilter("Primitive", GPUImageFilterTools.FilterType.PIXELATION);
        mList.addFilter("Ambiguous", GPUImageFilterTools.FilterType.ACV_AIMEI);
        mList.addFilter("Light blue", GPUImageFilterTools.FilterType.ACV_DANLAN);
        mList.addFilter("Egg yolk", GPUImageFilterTools.FilterType.ACV_DANHUANG);
        mList.addFilter("Retro", GPUImageFilterTools.FilterType.ACV_FUGU);
        mList.addFilter("Chill", GPUImageFilterTools.FilterType.ACV_GAOLENG);
        mList.addFilter("Nostalgia", GPUImageFilterTools.FilterType.ACV_HUAIJIU);
        mList.addFilter("Filmed", GPUImageFilterTools.FilterType.ACV_JIAOPIAN);
        mList.addFilter("Cute", GPUImageFilterTools.FilterType.ACV_KEAI);
        mList.addFilter("Lonely", GPUImageFilterTools.FilterType.ACV_LOMO);
        mList.addFilter("Boost", GPUImageFilterTools.FilterType.ACV_MORENJIAQIANG);
        mList.addFilter("Heartbeat", GPUImageFilterTools.FilterType.ACV_NUANXIN);
        mList.addFilter("Fresh", GPUImageFilterTools.FilterType.ACV_QINGXIN);
        mList.addFilter("Japanese", GPUImageFilterTools.FilterType.ACV_RIXI);
        mList.addFilter("Warm", GPUImageFilterTools.FilterType.ACV_WENNUAN);
    }


    private void setAdapterFilter() {
        if (!isChangeLayout) {
            getBaseActivity().showLoading(R.string.msg_loading);
            isChangeLayout = true;
            mFilterList = getFilterLists();
            mPopupFilterItems = new ArrayList<>();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = false;
            options.inSampleSize = 2;
            mOriginBitmap = ((CreatePollActivity) getActivity()).getBitmapView();

            for (FilterItem filterItem : mFilterList) {
                mPopupFilterItems.add(new PopupFilterItem(filterItem, mOriginBitmap,
                        filterItem.getName() == 0 ? filterItem.getTitleStr() : getString(filterItem.getName())
                        , filterItem.getColorId()));
            }

            mPhotoPopupAdapter = new PhotoPopupAdapter(getActivity(), mPopupFilterItems);
            mRecyclerFilter.setAdapter(mPhotoPopupAdapter);

            ImageRenderTask imageRenderTask = new ImageRenderTask(getActivity(), mFilterList, mOriginBitmap);
            imageRenderTask.setOnCallBackAsyncTaskListener(new OnCallBackAsyncTaskListener<List<PopupFilterItem>>() {
                @Override
                public void onPreExecute() {

                }

                @Override
                public void onPostExecute(List<PopupFilterItem> mList) {
                    if (mList != null) {
                        mPopupFilterItems.clear();
                        mPopupFilterItems.addAll(mList);
                        mPhotoPopupAdapter.notifyDataSetChanged();
                        getBaseActivity().visibleLoading(false);
                    }
                }
            });
            //imageRenderTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            new FilterLoader().execute();
            mPhotoPopupAdapter.setOnItemClickListener((view, position) -> {
                goToFilterFrame(position);
                //scrollToCenter(position);
            });
            mPhotoPopupAdapter.setSelection(mPosition);
            mRecyclerFilter.scrollToPosition(mPosition);
            mPhotoPopupAdapter.notifyDataSetChanged();
        }
    }

    public void goToFilterFrame(int position) {
        if (mFilterList != null && mFilterList.size() > 0) {
            getBaseActivity().showLoading(R.string.com_facebook_loading);
            FilterItem filterItem = mFilterList.get(position);
            GPUImageFilter filter = filterItem.getFilter();
            if (mImageFilterTask != null) {
                mImageFilterTask.removeBitmap();
                mImageFilterTask.isCancelled();
            }
            mImageFilterTask = new ImageFilterTask(getActivity(), filter, mListBitmap);
            mImageFilterTask.setOnCallBackAsyncTaskListener(new OnCallBackAsyncTaskListener<ArrayList<Bitmap>>() {
                @Override
                public void onPreExecute() {
                    getBaseActivity().showLoading(R.string.msg_loading);
                }

                @Override
                public void onPostExecute(ArrayList<Bitmap> mList) {
                    if (mList != null && mOnListenerLoadFilter != null) {
                        mOnListenerLoadFilter.onFinish(true, mList, position);
                    } else if (mOnListenerLoadFilter != null) {
                        mOnListenerLoadFilter.onFinish(false, null, -1);
                    }
                    getBaseActivity().visibleLoading(false);
                }
            });
            mImageFilterTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private class FilterLoader extends AsyncTask<Integer, Void, List<PopupFilterItem>> {

        @Override
        protected List<PopupFilterItem> doInBackground(Integer... params) {
            List<PopupFilterItem> popupFilterList = new ArrayList<>();
            for (int i = 0; i < mList.filters.size(); i++) {
                if (isLiveAsyncTask(this, getActivity())) {
                    break;
                }
                GPUImage gpuImage = new GPUImage(getActivity());
                gpuImage.setImage(mOriginBitmap);
                GPUImageFilter filter = GPUImageFilterTools.createFilterForType(getBaseActivity(), mList.filters.get(i));
                gpuImage.setFilter(filter);
                Bitmap bmp = gpuImage.getBitmapWithFilterApplied(mOriginBitmap);
                //Bitmap bmp = Bitmap.createScaledBitmap(gpuImage.getBitmapWithFilterApplied(), 120, 120, false);
                PopupFilterItem item = new PopupFilterItem();
                item.setBitmap(bmp);
                // item.setFilterItem(filterList.get(i));
                item.setTitle(mList.names.get(i));

                popupFilterList.add(item);
            }
            return popupFilterList;
        }


        @Override
        protected void onPostExecute(List<PopupFilterItem> mList) {
            super.onPostExecute(mList);
            if (isLiveAsyncTask(this, getActivity())) {
                return;
            }
            if (mList != null) {
                mPopupFilterItems.clear();
                mPopupFilterItems.addAll(mList);
                mPhotoPopupAdapter.notifyDataSetChanged();
            }
            getBaseActivity().visibleLoading(false);
        }
    }

    public interface OnListenerLoadFilter {
        void onFinish(boolean isFinish, ArrayList<Bitmap> mList, int postion);
    }

    private boolean isLiveAsyncTask(AsyncTask asyncTask, Context mContext) {
        return ((asyncTask != null && asyncTask.isCancelled()) || asyncTask == null
                || mContext == null || mListBitmap == null || isDestroy);
    }

    public void setPosition(int mPosition) {
        this.mPosition = mPosition;
    }

    @Override
    public void onDestroyView() {
        isDestroy = true;
        if (mImageFilterTask != null) {
            mImageFilterTask.isCancelled();
        }
        super.onDestroyView();
        removeBitmap();
    }

    public void removeBitmap() {
        if (mListBitmap != null && mListBitmap.size() > 0) {
            for (Bitmap mBitmap : mListBitmap) {
                mBitmap.recycle();
            }
        }
        mListBitmap = null;
    }
}
