/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll;

import com.faskera.mvp.BasePresenter;
import com.faskera.mvp.BaseView;

/**
 * @author DANGNGOCDUC
 * @since 12/30/2016
 */

public interface CreatePollContract {

    interface Presenter extends BasePresenter {

    }

    interface View extends BaseView<Presenter> {

    }

}
