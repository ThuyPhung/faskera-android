/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;

import com.faskera.era.R;
import com.faskera.model.createpoll.SettingPollModel;
import com.faskera.view.activity.createpoll.view.RowSettingView;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SettingCreatePollDialog {

    private CustomDialog customDialog;

    public SettingCreatePollDialog(Context context) {
        customDialog = new SettingCreatePollDialog.CustomDialog(context);
    }

    public SettingCreatePollDialog show() {
        customDialog.show();
        return this;
    }

    public SettingCreatePollDialog dismiss() {
        customDialog.dismiss();
        return this;
    }

    public SettingCreatePollDialog setSettingPollModel(SettingPollModel settingPollModel) {
        customDialog.setSettingPollModel(settingPollModel);
        return this;
    }

    public CustomDialog getCustomDialog() {
        return customDialog;
    }

    public SettingCreatePollDialog setOnDataChangeListener(OnDataChangeListener onDataChangeListener) {
        customDialog.setOnDataChangeListener(onDataChangeListener);
        return this;
    }

    public class CustomDialog extends Dialog implements View.OnClickListener, RowSettingView.OnSwitchChangeListener {

        @BindView(R.id.setting_share_face)
        RowSettingView mShareFace;
        @BindView(R.id.setting_ask_anonymously)
        RowSettingView mAskAnonymously;
        @BindView(R.id.setting_people_to_add_option)
        RowSettingView mPeopleToAddOption;
        @BindView(R.id.setting_people_to_choose)
        RowSettingView mPeopleToChoose;

        private SettingPollModel mSettingPollModel;
        private OnDataChangeListener mOnDataChangeListener;

        public CustomDialog(Context context) {
            super(context);
            setContentView(R.layout.setting_create_poll_dialog);
            ButterKnife.bind(this);
            setCancelable(true);
            setCanceledOnTouchOutside(true);
            getWindow().setGravity(Gravity.BOTTOM);
            getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.WRAP_CONTENT);
            mShareFace.setOnSwitchChangeListener(this);
            mAskAnonymously.setOnSwitchChangeListener(this);
            mPeopleToAddOption.setOnSwitchChangeListener(this);
            mPeopleToChoose.setOnSwitchChangeListener(this);
            getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        }

        public void title(int title) {
            title(getContext().getString(title));
        }

        public void title(String title) {
        }

        @Override
        public void onClick(View v) {

        }

        public boolean getShareFaceSwitch() {
            return mShareFace.isCheck();
        }

        public boolean getAnonymouslySwitch() {
            return mAskAnonymously.isCheck();
        }

        public boolean getPeopleToAddOptionSwitch() {
            return mPeopleToAddOption.isCheck();
        }

        public boolean getPeopleToChooseSwitch() {
            return mPeopleToChoose.isCheck();
        }

        public void setShareFaceSwitch(boolean isCheck) {
            mShareFace.setCheck(isCheck);
        }

        public void setAnonymouslySwitch(boolean isCheck) {
            mAskAnonymously.setCheck(isCheck);
        }

        public void setPeopleToAddOptionSwitch(boolean isCheck) {
            mPeopleToAddOption.setCheck(isCheck);
        }

        public void setPeopleToChooseSwitch(boolean isCheck) {
            mPeopleToChoose.setCheck(isCheck);
        }

        private void initCheck() {
            setAnonymouslySwitch(mSettingPollModel.isAnonymous());
            setPeopleToChooseSwitch(mSettingPollModel.isMultiChoice());
            setPeopleToAddOptionSwitch(mSettingPollModel.isPeopleToAdd());
            setShareFaceSwitch(mSettingPollModel.isShareFacebook());
        }

        @Override
        public void onBackPressed() {
            super.onBackPressed();
        }

        public void setSettingPollModel(SettingPollModel mSettingPollModel) {
            this.mSettingPollModel = mSettingPollModel;
            initCheck();
        }

        public void setOnDataChangeListener(OnDataChangeListener onDataChangeListener) {
            mOnDataChangeListener = onDataChangeListener;
        }

        @Override
        public void onChange() {
            if (mOnDataChangeListener != null) {
                mSettingPollModel.setAnonymous(getAnonymouslySwitch());
                mSettingPollModel.setMultiChoice(getPeopleToChooseSwitch());
                mSettingPollModel.setShareFacebook(getShareFaceSwitch());
                mSettingPollModel.setPeopleToAdd(getPeopleToAddOptionSwitch());
                mOnDataChangeListener.onChange(mSettingPollModel);
            }
        }
    }

    public interface OnDataChangeListener {
        void onChange(SettingPollModel mSettingPollModel);
    }

}
