/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.event;

import android.view.View;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;

public interface OnFilterItemClickListener
{
    void onItemClick(View view, int position, GPUImageFilter filter);
}
