/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.model;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;

public class FilterItem {
    private GPUImageFilter filter;
    private int colorId;
    private int mTitle;
    private String mTitleStr;

    public FilterItem(GPUImageFilter filter, int colorId, int mTitle) {
        this.filter = filter;
        this.colorId = colorId;
        this.mTitle = mTitle;
    }

    public FilterItem(GPUImageFilter filter, int colorId, String mTitleStr) {
        this.filter = filter;
        this.colorId = colorId;
        this.mTitleStr = mTitleStr;
    }

    public GPUImageFilter getFilter() {
        return filter;
    }

    public void setFilter(GPUImageFilter filter) {
        this.filter = filter;
    }

    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    public int getName() {
        return mTitle;
    }

    public String getTitleStr() {
        return mTitleStr;
    }
}
