/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.faskera.era.R;
import com.faskera.model.createpoll.ImageModel;
import com.faskera.utils.AppUtils;
import com.faskera.utils.DisplayUtility;

import java.util.ArrayList;
import java.util.List;

public class ImageGalleryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int TYPE_CAMERA = 1;
    public static final int TYPE_PICTURE = 2;
    private boolean showCamera = true;
    private final List<ImageModel> images;
    private int gridItemWidth;
    private OnImageClickListener onImageClickListener;
    private ImageThumbnailLoader imageThumbnailLoader;
    private List<ImageGalleryAdapter.Picked> mListPick;
    private int numberRequired;
    private PickerAdapterCallback mPickerAdapterCallback;

    public interface OnImageClickListener {
        void onImageClick(int position);

        void onTakePhoto();
    }

    public interface ImageThumbnailLoader {
        void loadImageThumbnail(ImageView iv, String imageUrl, int dimension);
    }

    public ImageGalleryAdapter(Context context, List<ImageModel> images) {
        this.images = images;

        int screenWidth = DisplayUtility.getScreenWidth(context);
        int numOfColumns;
        if (DisplayUtility.isInLandscapeMode(context)) {
            numOfColumns = 4;
        } else {
            numOfColumns = 3;
        }

        gridItemWidth = screenWidth / numOfColumns;
        mListPick = new ArrayList<>();
        numberRequired = AppUtils.MAX_NUMBER_IMAGE_PICKER;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == TYPE_CAMERA) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.picture_item_camera, viewGroup, false);
            return new HeaderViewHolder(view);
        } else {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_thumbnail, viewGroup, false);
            v.setLayoutParams(getGridItemLayoutParams(v));

            return new ImageViewHolder(v);
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        View headerView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            headerView = itemView;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (getItemViewType(position) == TYPE_CAMERA) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) viewHolder;
            headerHolder.headerView.setOnClickListener(v -> {
                if (onImageClickListener != null) {
                    onImageClickListener.onTakePhoto();
                }
            });
        } else {
            if (images != null) {
                final ImageViewHolder holder = (ImageViewHolder) viewHolder;
                String image = null;
                if (position < images.size()) {
                    image = images.get(position).getThumb();
                }
                if (!TextUtils.isEmpty(image)) {
                    imageThumbnailLoader.loadImageThumbnail(holder.mImage, image, gridItemWidth);
                    holder.mNumber.setVisibility(View.GONE);
                    holder.frameLayout.setOnClickListener(v -> {
                        int adapterPos = holder.getAdapterPosition();
                        if (adapterPos != RecyclerView.NO_POSITION) {
                            if (onImageClickListener != null) {
                                onImageClickListener.onImageClick(adapterPos);
                            }
                        }
                    });

                    for (Picked picked : mListPick) {
                        if (position == picked.position) {
                            holder.mNumber.setVisibility(View.VISIBLE);
                            holder.mNumber.setText(String.valueOf(picked.value));
                        }
                    }
                }
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (showCamera && position == 0 && images.get(0).getThumb() == null) {
            return TYPE_CAMERA;
        } else {
            return TYPE_PICTURE;
        }
    }

    @Override
    public int getItemCount() {
        return images != null ? images.size() : 0;
    }

    public void setOnImageClickListener(OnImageClickListener listener) {
        this.onImageClickListener = listener;
    }

    public void setImageThumbnailLoader(ImageThumbnailLoader loader) {
        this.imageThumbnailLoader = loader;
    }

    private ViewGroup.LayoutParams getGridItemLayoutParams(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = gridItemWidth;
        layoutParams.height = gridItemWidth;

        return layoutParams;
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder {

        private final ImageView mImage;
        private final FrameLayout frameLayout;
        private final TextView mNumber;

        public ImageViewHolder(final View view) {
            super(view);
            mNumber = (TextView) view.findViewById(R.id.number);
            mImage = (ImageView) view.findViewById(R.id.iv);
            frameLayout = (FrameLayout) view.findViewById(R.id.fl);
        }
    }

    public interface PickerAdapterCallback {
        void onMaxItemSelected();
    }

    private class Picked {
        public Picked(int position, int value) {
            this.position = position;
            this.value = value;
        }

        public int position;
        public int value;
    }

    public boolean nextEnable() {
        return mListPick.size() <= numberRequired && mListPick.size() > 0;
    }

    public ArrayList<String> getPaths() {
        ArrayList<String> result = new ArrayList<>();
        for (ImageGalleryAdapter.Picked picked : mListPick) {
            result.add(images.get(picked.position).getThumb());
        }
        return result;
    }


    public void setPickerAdapterCallback(PickerAdapterCallback mPickerAdapterCallback) {
        this.mPickerAdapterCallback = mPickerAdapterCallback;
    }

    public void setCurrentSelected(int position) {
        boolean add = true;
        int currentValue = -1;

        for (ImageGalleryAdapter.Picked picked : mListPick) {
            if (picked.position == position) {
                add = false;
                currentValue = picked.value;
                mListPick.remove(picked);
                break;
            }
        }

        if (!add) {
            for (ImageGalleryAdapter.Picked picked : mListPick) {
                if (picked.value > currentValue) {
                    picked.value--;
                }
            }
        } else {
            if (mListPick.size() >= numberRequired) {
                if (mPickerAdapterCallback != null) {
                    mPickerAdapterCallback.onMaxItemSelected();
                }
                return;
            }

            mListPick.add(new ImageGalleryAdapter.Picked(position, getMaxPicked(mListPick)));
        }

        notifyDataSetChanged();
    }

    private int getMaxPicked(List<ImageGalleryAdapter.Picked> pickers) {
        if (pickers == null || pickers.size() == 0) {
            return 1;
        } else {
            int max = pickers.get(0).value;
            for (ImageGalleryAdapter.Picked picked : pickers) {
                if (picked.value > max) {
                    max = picked.value;
                }
            }

            return max + 1;
        }
    }
}