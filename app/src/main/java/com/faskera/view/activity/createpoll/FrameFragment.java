/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.faskera.common.base.BaseFragment;
import com.faskera.era.R;
import com.faskera.utils.AppUtils;
import com.faskera.view.activity.createpoll.adapter.ModelLinearAdapter;
import com.faskera.view.activity.createpoll.frame.utils.FileUtils;
import com.faskera.view.activity.createpoll.frame.utils.PhotoType;
import com.faskera.view.activity.createpoll.frame.utils.TemplateUtils;
import com.faskera.view.activity.createpoll.frame.widget.PhotoModelLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class FrameFragment extends BaseFragment {

    private ModelLinearAdapter mModelLinearAdapter;
    private int mPosition = -1;
    private PhotoType mPhotoType;
    private ArrayList<String> mListPath;
    private OnClickListenerAdapterFrame mOnClickListenerAdapterFrame;

    @BindView(R.id.rl_frame)
    RecyclerView mRecyclerFrame;

    public static FrameFragment getInstance(OnClickListenerAdapterFrame mOnClickListenerAdapterFrame) {
        FrameFragment mFrameFragment = new FrameFragment();
        mFrameFragment.mOnClickListenerAdapterFrame = mOnClickListenerAdapterFrame;
        return mFrameFragment;
    }

    @Override
    protected int idRoot() {
        return R.layout.fragment_frame;
    }

    @Override
    protected void initView(View root) {
        initRecycler(mRecyclerFrame);
        mModelLinearAdapter = new ModelLinearAdapter(getActivity());
        mRecyclerFrame.setAdapter(mModelLinearAdapter);
        mModelLinearAdapter.setOnModelItemClickListener((view, position) -> {
            if (mOnClickListenerAdapterFrame != null && mPosition != position) {
                if(getActivity() instanceof CreatePollActivity){
                    ((CreatePollActivity) getActivity()).setBitmapView(FileUtils.createBitmapFromView(view));
                }
                mPosition = position;
                mOnClickListenerAdapterFrame.onClickAdapterPosition(position);
            }
        });
    }

    private void initRecycler(RecyclerView mRecycler) {
        mRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mRecycler.setHasFixedSize(true);
    }

    @Override
    protected void initData() {
        if (getArguments() != null) {
            Bundle mBundle = getArguments();
            mListPath = mBundle.getStringArrayList(AppUtils.SELECTED_PATHS);
            mPhotoType = (PhotoType) mBundle.getSerializable(AppUtils.TYPE_OF_PHOTO);
            mPosition = mBundle.getInt(AppUtils.ID_OF_TEMPLATE);
            List<PhotoModelLayout> models = TemplateUtils.getSlotLayoutList(getActivity(), mPhotoType, mListPath);
            mModelLinearAdapter.setModels(models);
            mRecyclerFrame.scrollToPosition(mPosition);
            mModelLinearAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onRefresh(Object o) {

    }

    public interface OnClickListenerAdapterFrame {
        void onClickAdapterPosition(int position);
    }
}
