/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.notification.model;

import com.faskera.model.user.profile.ProfilePhoto;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author framgia
 * @since 14/02/2017
 */

public class CreatedUser {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("fullname")
    @Expose
    public String fullname;
    @SerializedName("profilePhoto")
    @Expose
    public ProfilePhoto profilePhoto;
    @SerializedName("following")
    @Expose
    public boolean following;
}
