/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.viewvote;

import android.content.Context;

import com.faskera.common.adapter.ListPeople;
import com.faskera.common.base.ObserverAdvance;
import com.faskera.network.ApiManager;

import java.util.ArrayList;

/**
 * @author DANGNGOCDUC
 * @since 1/3/2017
 */

public class ViewVotePresenter implements ViewVoteContract.Presenter {

    private ViewVoteContract.View mView;
    private Context mContext;
    private String mNextId = "";
    boolean isLoadMore;

    public ViewVotePresenter(Context context, ViewVoteContract.View view) {
        mView = view;
        mContext = context;
        mView.setPresenter(this);
    }

    @Override
    public void getListVote(String pollId) {
        if (isLoadMore || mNextId == null) return;
        if (mNextId.trim().isEmpty()) {
            mView.onLoading();
        } else {
            mView.onLoadMore();
        }
        isLoadMore = true;

        ApiManager.getInstance().getUserVoted(mContext, pollId, mNextId, new ObserverAdvance<ListPeople>() {
            @Override
            public void onError(int code) {
                 mView.onError(code);
                isLoadMore = false;
            }

            @Override
            public void onSuccess(ListPeople data) {
                isLoadMore = false;
                if (data.mNextMaxId == null || data.mNextMaxId.trim().isEmpty()) {
                    mNextId = null;
                } else {
                    mNextId = data.mNextMaxId;
                }
                mView.onSuccess(new ArrayList<>(data.mlistUser));
            }

            @Override
            public void onCompleted() {}

        });

    }
}
