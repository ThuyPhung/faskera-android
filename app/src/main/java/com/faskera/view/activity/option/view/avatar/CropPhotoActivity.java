/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.avatar;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import com.faskera.callback.OnClickItemToolbarListener;
import com.faskera.common.AppSharePreference;
import com.faskera.common.base.BaseActivity;
import com.faskera.era.R;
import com.faskera.model.option.TbOption;
import com.faskera.model.user.UserInfoResponse;
import com.faskera.net.gotev.uploadservice.MultipartUploadRequest;
import com.faskera.net.gotev.uploadservice.ServerResponse;
import com.faskera.net.gotev.uploadservice.UploadInfo;
import com.faskera.net.gotev.uploadservice.UploadNotificationConfig;
import com.faskera.net.gotev.uploadservice.UploadStatusDelegate;
import com.faskera.network.APIConfig;
import com.faskera.utils.ActivityUtils;
import com.faskera.view.activity.createpoll.frame.utils.FileUtils;
import com.faskera.view.widget.ToolbarOptions;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;

public class CropPhotoActivity extends BaseActivity implements CropPhotoContract.View {
    public static final String EXTRA_URI = "EXTRA_URI";
    @BindView(R.id.cropImageView)
    CropImageView mCropImageView;
    @BindView(R.id.toolbar)
    ToolbarOptions mToolbar;
    private CropPhotoContract.Presenter mPresenter;
    private String mImageUri;

    public static Intent getUriIntent(Context context, String imageUri) {
        Intent intent = new Intent(context, CropPhotoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_URI, imageUri);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected int getIdLayout() {
        return R.layout.activity_choose_photo;
    }

    @Override
    protected void initView() {
        mPresenter = new CropPhotoPresenter(this, this);
        mImageUri = getIntent().getExtras().getString(EXTRA_URI);
        mToolbar.setToolbarModel(new TbOption("", true));
        mToolbar.setOnClickItemToolbarListener(new OnClickItemToolbarListener() {
            @Override
            public void onBack() {
                onBackPressed();
            }

            @Override
            public void onDone() {
                Bitmap cropped = mCropImageView.getCroppedImage();
                SimpleDateFormat dateFormat =
                    new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
                Date curTime = new Date();
                String fileName = "Faskera_" + dateFormat.format(curTime) + ".jpg";
                String filePath = saveImage(FileUtils.FASKERA_DIR, fileName, cropped);
                uploadImage(filePath);
            }
        });
        mCropImageView.setCropShape(CropImageView.CropShape.RECTANGLE);
        mCropImageView.setMinCropResultSize(256, 256);
        mCropImageView.setMaxCropResultSize(1024, 1024);
        mCropImageView.setAspectRatio(1, 1);
        mCropImageView.setImageUriAsync(Uri.parse(mImageUri));
    }

    private void uploadImage(String filePath) {
        MultipartUploadRequest req = null;
        try {
            String AuthorizationBuilder =
                AppSharePreference.getTokenType(this) + " " + AppSharePreference.getToken(this);
            req = new MultipartUploadRequest(this, APIConfig.BASE_API_URL + "user/me/profile-photo")
                .addFileToUpload(filePath, "photo")
                .addHeader("Authorization", AuthorizationBuilder)
                .setAutoDeleteFilesAfterSuccessfulUpload(true)
                .setNotificationConfig(getNotificationConfig("Upload"))
                .setUsesFixedLengthStreamingMode(true)
                .setMaxRetries(3);
            req.setUtf8Charset();


        showLoading(null);

            req.setDelegate(new UploadStatusDelegate() {
                @Override
                public void onProgress(Context context, UploadInfo uploadInfo) {
                }

                @Override
                public void onError(Context context, UploadInfo uploadInfo, Exception exception) {
                    ActivityUtils.showToast(context,
                        context.getResources().getString(R.string.error_connection));
                    finish();
                }

                @Override
                public void onCompleted(Context context, UploadInfo uploadInfo,
                                        ServerResponse serverResponse) {
                    visibleLoading(false);
                    setResult(android.app.Activity.RESULT_OK);
                    finish();
                    mPresenter.getUserInfo();
                }

                @Override
                public void onCancelled(Context context, UploadInfo uploadInfo) {
                }
            });
            req.startUpload();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
        e.printStackTrace();
    }
    }

    private String saveImage(String folderName, String fileName, Bitmap cropped) {
        if (null == cropped) {
            return null;
        }
        File dir = new File(folderName);
        if (!dir.exists() && !dir.mkdirs()) {
            return null;
        }
        File file = new File(dir, fileName);
        OutputStream out = null;
        try {
            out = new BufferedOutputStream(new FileOutputStream(file));
            if (!cropped.compress(Bitmap.CompressFormat.JPEG, 100, out)) {
                return null;
            }
            if (!cropped.isRecycled()) {
                cropped.recycle();
            }
            Intent mediaScannerIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            final Uri fileContentUri = Uri.fromFile(file);
            mediaScannerIntent.setData(fileContentUri);
            //   sendBroadcast(mediaScannerIntent);
            return file.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != out) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void initData() {
    }

    private UploadNotificationConfig getNotificationConfig(String filename) {
        return new UploadNotificationConfig()
            .setIcon(R.drawable.ic_faskera_notification_icon)
            .setCompletedIcon(R.drawable.ic_faskera_notification_icon)
            .setErrorIcon(R.drawable.ic_faskera_notification_icon)
            .setCancelledIcon(R.drawable.ic_faskera_notification_icon)
            .setIconColor(Color.BLUE)
            .setCompletedIconColor(Color.GREEN)
            .setErrorIconColor(Color.RED)
            .setCancelledIconColor(Color.YELLOW)
            .setTitle(filename)
            .setInProgressMessage(getString(R.string.edit_profile))
            .setCompletedMessage(getString(R.string.edit_profile))
            .setErrorMessage(getString(R.string.edit_profile))
            .setCancelledMessage(getString(R.string.edit_profile))
            .setAutoClearOnCancel(true)
            .setAutoClearOnSuccess(true)
            .setClearOnAction(true)
            .setRingToneEnabled(true);
    }

    @Override
    public void onGetUserInfoSuccess(UserInfoResponse userInfo) {
        //Update user info
        UserInfoResponse updatePhotoInfo = AppSharePreference.getInstance(this).getUserInfo();
        updatePhotoInfo.setPhoto(updatePhotoInfo.photo);
        AppSharePreference.getInstance(this).storeUserInfo(updatePhotoInfo);
    }

    @Override
    public void onError(int code) {
        handleErrorCode(code);
    }

    @Override
    public void showLoading() {
        showLoading(null);
    }

    @Override
    public void hideLoading() {
        visibleLoading(false);
    }

    @Override
    public void setPresenter(CropPhotoContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.unsubscribe();
    }
}
