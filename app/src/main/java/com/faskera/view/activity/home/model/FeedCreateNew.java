/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home.model;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public class FeedCreateNew implements ImlHomeRow {
    @Override
    public int getType() {
        return TYPE_CREATE_FEED;
    }
}
