/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.notification.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author framgia
 * @since 14/02/2017
 */

public class TargetPoll {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("createdTime")
    @Expose
    public long createdTime;
    @SerializedName("createdUser")
    @Expose
    public CreatedUser createdUser;
    @SerializedName("settings")
    @Expose
    public Settings settings;
    @SerializedName("question")
    @Expose
    public Question question;
    @SerializedName("options")
    @Expose
    public java.util.List<Option> options = null;
    @SerializedName("nbUsersAnswered")
    @Expose
    public int nbUsersAnswered;
    @SerializedName("votedByMe")
    @Expose
    public boolean votedByMe;
    @SerializedName("topic")
    @Expose
    public Topic topic;
    @SerializedName("tags")
    @Expose
    public java.util.List<Object> tags = null;
    @SerializedName("countries")
    @Expose
    public java.util.List<Object> countries = null;
    @SerializedName("languages")
    @Expose
    public java.util.List<Language> languages = null;
    @SerializedName("hidden")
    @Expose
    public boolean hidden;
    @SerializedName("closed")
    @Expose
    public boolean closed;
    @SerializedName("quote")
    @Expose
    public String quote;
    @SerializedName("permalink")
    @Expose
    public String permalink;
    @SerializedName("mine")
    @Expose
    public boolean mine;

}