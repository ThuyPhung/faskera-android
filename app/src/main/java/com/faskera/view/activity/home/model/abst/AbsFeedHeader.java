/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home.model.abst;

import com.faskera.view.activity.home.model.Feed;
import com.faskera.view.activity.home.model.ImlHomeRow;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public abstract class AbsFeedHeader implements ImlHomeRow {
    @Override
    public int getType() {
        return TYPE_HEADER_FEED;
    }

    public abstract String getAvatar() ;

    public abstract String getName() ;

    public abstract String getSubName() ;

    public abstract boolean isAnonymous() ;

    public abstract Feed.Poll getPoll();


}
