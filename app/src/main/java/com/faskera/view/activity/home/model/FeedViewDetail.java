/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home.model;

/**
 * @author DANGNGOCDUC
 * @since 2/8/2017
 */

public class FeedViewDetail implements ImlHomeRow {
    public int mContent;
    public Feed.Poll mPoll;

    public Feed.Poll getPoll() {
        return mPoll;
    }


    public FeedViewDetail(Feed.Poll poll) {
        mPoll = poll;
        int count;
        if (mPoll.mListOptions.size() >= 2)
        {
            count = 2;
        } else {
            count = mPoll.mListOptions.size();
        }


        if (mPoll.mListOptions.size() > count) {
           mContent = 0;
        } else {
            mContent = 0;
        }
    }

    @Override
    public int getType() {
        return TYPE_VIEW_DETAIL;
    }
}
