/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.profile;

import com.faskera.mvp.BasePresenter;
import com.faskera.mvp.BaseView;
import com.faskera.network.user.MyProfileBody;

/**
 * @author theanh
 * @since 1/12/2017
 */
public interface EditProfileContract {
    interface Presenter extends BasePresenter {
        void getMyProfile();
        void updateMyProfile(MyProfileBody body);
        void removePhoto();
        void subscribe();
        void unsubscribe();
    }

    interface View extends BaseView<EditProfileContract.Presenter> {
        void onGetProfileSuccess();
        void onUpdateProfileSuccess();
        void onRemovePhotoSuccess();
        void onErrorConnection(int code);
        void onUpdatePhotoSuccess();
        void showLoading();
        void hideLoading();
    }
}
