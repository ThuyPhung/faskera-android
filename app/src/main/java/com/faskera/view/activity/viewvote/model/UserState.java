/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.viewvote.model;

import com.faskera.common.UserDefine;
import com.faskera.common.adapter.AbsUserState;

/**
 * @author DANGNGOCDUC
 * @since 1/3/2017
 */

public class UserState implements AbsUserState {

    public String mAvatar;

    public int mSate;

    public String mName;

    private String mId;

    public UserState(String avatar, String name, int sate, String  id) {
        mAvatar = avatar;
        mName = name;
        mSate =  sate;
        mId = id;
    }

    @Override
    public String getAvatar() {
        return mAvatar;
    }

    @Override
    public String getUid() {
        return mId;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public int getSate() {
        if (mSate == 1) {
            return UserDefine.STATE_FOLLOW;
        } else {
            return UserDefine.STATE_FOLLWING;
        }
    }

    @Override
    public void setSate(@UserDefine.StateFollow int state) {

    }

    @Override
    public int getType() {
        return TYPE_USER;
    }
}
