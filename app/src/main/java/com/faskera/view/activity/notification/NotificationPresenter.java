/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.notification;

import android.content.Context;

import com.faskera.common.base.ObserverAdvance;
import com.faskera.network.ApiManager;
import com.faskera.view.activity.notification.model.Notification;

import rx.subscriptions.CompositeSubscription;

/**
 * @author framgia
 * @since 14/02/2017
 */
public class NotificationPresenter implements NotificationContract.Presenter {
    private static final String TYPE_LOAD = "TYPE_LOAD";
    private static final String TYPE_LOAD_MORE = "TYPE_LOAD_MORE";
    private Context mContext;
    private NotificationContract.View mView;
    private CompositeSubscription mCompositeSubscription;
    private String mNextMaxId = "";
    private String mType;

    public NotificationPresenter(Context context, NotificationContract.View view) {
        mContext = context;
        mView = view;
        mView.setPresenter(this);
        mCompositeSubscription = new CompositeSubscription();
        mType = TYPE_LOAD;
    }

    @Override
    public void getNotifications() {
        switch (mType) {
            case TYPE_LOAD:
                mView.showLoading();
                break;
            case TYPE_LOAD_MORE:
                break;
        }
        mCompositeSubscription.add(ApiManager.getInstance()
            .getNotifications(mContext, mNextMaxId, new ObserverAdvance<Notification>() {
                @Override
                public void onError(int code) {
                    if (mView == null) return;
                    mView.hideLoading();
                    mView.onError(code);
                }

                @Override
                public void onSuccess(Notification data) {
                    if (mView == null) return;
                    mView.hideLoading();
                    if (data.size > 0) {
                        switch (mType) {
                            case TYPE_LOAD:
                                mView.onGetNotificationsSuccess(data);
                                mType = TYPE_LOAD_MORE;
                                break;
                            case TYPE_LOAD_MORE:
                                mView.onGetMoreNotification(data);
                        }
                        mNextMaxId = data.nextMaxId;
                    } else {
                        switch (mType) {
                            case TYPE_LOAD:
                                mView.onGetNotificationEmpty();
                                break;
                            case TYPE_LOAD_MORE:
                                mView.onGetAllNotification();
                                break;
                        }
                    }
                }

                @Override
                public void onCompleted() {
                }
            }));
    }

    @Override
    public void goToNotificationDetail() {
    }

    //refresh notification when swipe
    @Override
    public void refresh() {
        mNextMaxId = "";
        mType = TYPE_LOAD;
        subscribe();
    }

    @Override
    public void subscribe() {
        getNotifications();
    }

    @Override
    public void unsubscibe() {
        if (mCompositeSubscription != null && mCompositeSubscription.isUnsubscribed()) {
            mCompositeSubscription.unsubscribe();
            mView = null;
        }
    }
}
