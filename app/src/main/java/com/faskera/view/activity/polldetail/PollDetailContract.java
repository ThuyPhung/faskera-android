/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.polldetail;

import com.faskera.mvp.BasePresenter;
import com.faskera.mvp.BaseView;
import com.faskera.view.activity.home.model.ImlHomeRow;

import java.util.ArrayList;

/**
 * @author DANGNGOCDUC
 * @since 1/4/2017
 */
public interface PollDetailContract {
    interface Presenter extends BasePresenter {
        void getPollDetail(String pollID);
        void onDestroy();
    }

    interface View extends BaseView<Presenter> {
        void onLoading();
        void onLoadError(int code);
        void onSuccess(ArrayList<ImlHomeRow> list);
    }
}
