/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.profile;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.faskera.callback.OnClickItemToolbarListener;
import com.faskera.common.AppSharePreference;
import com.faskera.common.base.BaseFragment;
import com.faskera.common.view.AnyTextview;
import com.faskera.era.R;
import com.faskera.model.option.TbOption;
import com.faskera.model.user.MyProfile;
import com.faskera.network.user.MyProfileBody;
import com.faskera.utils.EditTextUtil;
import com.faskera.utils.ImageLoader;
import com.faskera.view.activity.option.view.avatar.ChoosePhotoFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author theanh
 * @since 1/9/2017
 */
public class EditProfileFragment extends BaseFragment implements EditProfileContract.View {
    private static final String TAG = EditProfileFragment.class.getSimpleName();
    @BindView(R.id.image_profile)
    ImageView mImageProfile;
    @BindView(R.id.button_change_photo)
    AnyTextview mButtonChangePhoto;
    @BindView(R.id.edit_name)
    EditText mEditName;
    @BindView(R.id.edit_about)
    EditText mEditAbout;
    @BindView(R.id.edit_work)
    EditText mEditWork;
    @BindView(R.id.edit_study)
    EditText mEditStudy;
    @BindView(R.id.edit_home)
    EditText mEditHome;
    @BindView(R.id.edit_place)
    EditText mEditPlace;
    @BindView(R.id.edit_layout)
    LinearLayout mEditLayout;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.text_error_connection)
    AnyTextview mTextErrorConnection;
    private EditProfileContract.Presenter mPresenter;

    @Override
    protected int idRoot() {
        return R.layout.fragment_edit_profile;
    }

    @Override
    protected void initData() {
    }

    @Override
    protected void initView(View root) {
        //hide EditLayout when call API
        mEditLayout.setVisibility(View.GONE);
        mEditAbout.setOnEditorActionListener(editorActionListener);
        mEditHome.setOnEditorActionListener(editorActionListener);
        mEditName.setOnEditorActionListener(editorActionListener);
        mEditPlace.setOnEditorActionListener(editorActionListener);
        mEditStudy.setOnEditorActionListener(editorActionListener);
        mEditWork.setOnEditorActionListener(editorActionListener);
        new EditProfilePresenter(getContext(), this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
        getBaseActivity()
                .update(new TbOption(getContext().getString(R.string.tb_edit_profile), true));
        getBaseActivity().update(new OnClickItemToolbarListener() {
            @Override
            public void onBack() {
                getBaseActivity().setResult(Activity.RESULT_OK);
                getBaseActivity().onBack(null);
            }

            @Override
            public void onDone() {
                if (mEditName.getText().toString().trim().isEmpty()) {
                    mEditName.setText("");
                    mEditName.setHint(getContext().getString(R.string.msg_full_name_required));
                    mEditName.setHintTextColor(getContext().getResources().getColor(R.color.red));
                    mEditName.requestFocus();
                    return;
                }
                MyProfileBody newProfile = new MyProfileBody();
                newProfile.fullname = mEditName.getText().toString().trim().replaceAll("\\s+", " ");
                newProfile.about = mEditAbout.getText().toString().trim().replaceAll("\\s+", " ");
                newProfile.organizationStudy = mEditStudy.getText().toString().trim().replaceAll("\\s+", " ");
                newProfile.organizationWork = mEditWork.getText().toString().trim().replaceAll("\\s+", " ");
                newProfile.hometown = mEditHome.getText().toString().trim().replaceAll("\\s+", " ");
                newProfile.placeLive = mEditPlace.getText().toString().trim().replaceAll("\\s+", " ");
                mPresenter.updateMyProfile(newProfile);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(EditProfileContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onRefresh(Object o) {
    }

    public void showGallery() {
        getBaseActivity().goToFragment(new ChoosePhotoFragment(), null, false);
    }

    public void removePhoto() {
        mPresenter.removePhoto();
    }

    /**
     * View
     */
    @Override
    public void onGetProfileSuccess() {
        mEditLayout.setVisibility(View.VISIBLE);
        MyProfile mMyProfile = AppSharePreference.getInstance(getContext()).getMyProfileObj();
        if (mMyProfile == null) return;
        if (mMyProfile.profilePhoto != null) {
            ImageLoader.loadImageViewWith(getContext(), mMyProfile.profilePhoto.original,
                    mImageProfile);
        }
        mEditName.setText(mMyProfile.fullname);
        mEditAbout.setText(mMyProfile.about != null ? mMyProfile.about : "");
        mEditPlace.setText(mMyProfile.placeLive != null ? mMyProfile.placeLive.name : "");
        mEditStudy.setText(
                mMyProfile.organizationStudy != null ? mMyProfile.organizationStudy.name : "");
        mEditWork.setText(
                mMyProfile.organizationWork != null ? mMyProfile.organizationWork.name : "");
        mEditHome.setText(mMyProfile.hometown != null ? mMyProfile.hometown.name : "");
    }

    @Override
    public void onUpdateProfileSuccess() {
        getBaseActivity().setResult(Activity.RESULT_OK);
        getBaseActivity().onBack(null);
    }

    @Override
    public void onRemovePhotoSuccess() {
        mImageProfile.setImageResource(R.drawable.ic_avatar_default);
    }

    @Override
    public void onUpdatePhotoSuccess() {
        getActivity().finish();
    }

    @Override
    public void onErrorConnection(int code) {
        getBaseActivity().handleErrorCode(code);
        mTextErrorConnection.setVisibility(View.VISIBLE);
        mEditLayout.setVisibility(View.GONE);
        //Disable action
        getBaseActivity().update(null);
    }

    @Override
    public void showLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mProgressBar.setVisibility(View.GONE);
    }

    /**
     * OnClick
     */
    @OnClick({R.id.image_profile, R.id.button_change_photo})
    public void onClick(View view) {
        //Initializing a bottom sheet
        EditPhotoDialog editPhotoDialog = new EditPhotoDialog();
        editPhotoDialog
                .show(getBaseActivity().getSupportFragmentManager(), editPhotoDialog.getTag());
    }

    /**
     * Inner Listener
     */
    public TextView.OnEditorActionListener editorActionListener =
            (textView, actionId, keyEvent) -> {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    EditTextUtil.hideSoftKeyboard(getBaseActivity());
                    return true;
                }
                return false;
            };
}
