/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.fbfriend;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.faskera.callback.OnClickItemToolbarListener;
import com.faskera.common.AppSharePreference;
import com.faskera.common.UserDefine;
import com.faskera.common.base.BaseFragment;
import com.faskera.era.R;
import com.faskera.model.option.TbOption;
import com.faskera.utils.IntentUtils;
import com.faskera.view.activity.mypage.ProfileActivity;
import com.faskera.view.activity.option.view.fbfriend.itemview.FriendHeader;
import com.faskera.view.activity.option.view.fbfriend.model.Friend;
import com.faskera.view.widget.ConfirmUnfollowDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author theanh
 * @since 1/9/2017
 */
public class FbFriendFragment extends BaseFragment
    implements FbFriendContract.View, FbFriendAdapter.FbFriendCallback {
    private static final String LOG_TAG = FbFriendFragment.class.getSimpleName();
    @BindView(R.id.recycle_view)
    RecyclerView mRecycleView;
    @BindView(R.id.text_no_user)
    TextView mTextNoUser;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.text_error_connection)
    TextView mTextErrorConnection;
    private FbFriendAdapter mAdapter;
    private FbFriendContract.Presenter mPresenter;

    @Override
    protected int idRoot() {
        return R.layout.fragment_find_fb_friend;
    }

    @Override
    protected void initView(View root) {
        mAdapter = new FbFriendAdapter(getContext(), new ArrayList<>());
        mAdapter.setOnButtonFollowClickListener(this);
        mRecycleView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecycleView.setAdapter(mAdapter);
        new FbFriendPresenter(getContext(), this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.subscribe();
    }

    @Override
    public void onResume() {
        super.onResume();
        Resources resource = getContext().getResources();
        getBaseActivity()
            .update(new TbOption(resource.getString(R.string.tb_facebook_friend), false));
        getBaseActivity().update(new OnClickItemToolbarListener() {
            @Override
            public void onBack() {
                getBaseActivity().onBack(null);
            }

            @Override
            public void onDone() {
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.unsubscribe();
    }

    @Override
    protected void initData() {
    }

    @Override
    public void onRefresh(Object o) {
    }

    /**
     * VIEW
     */
    @Override
    public void onGetFriendsSuccess(int totalFbFriends, List<Friend> listFriends) {
        mRecycleView.setVisibility(View.VISIBLE);
        mTextNoUser.setVisibility(View.GONE);
        List list = new ArrayList<>();
        list.add(new FriendHeader(totalFbFriends));
        list.addAll(listFriends);
        mAdapter.updateListFriends(list);
    }

    @Override
    public void onGetFriendsEmpty() {
        mRecycleView.setVisibility(View.GONE);
        mTextNoUser.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFollowSuccess() {
        Log.d(LOG_TAG, "Follow Success");
    }

    @Override
    public void onFollowAllSuccess() {
        Log.d(LOG_TAG, "Follow All Success");
    }

    @Override
    public void onUnfollowSuccess() {
        Log.d(LOG_TAG, "UnFollowed");
    }

    @Override
    public void showLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onErrorConnection(int code) {
        getBaseActivity().handleErrorCode(code);
        mTextErrorConnection.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFollowFailed() {
        Toast.makeText(getContext(), getString(R.string.error_connection_not_found),
            Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setPresenter(FbFriendContract.Presenter presenter) {
        mPresenter = presenter;
    }

    /**
     * Adapter Callback
     *
     * @param userId
     */
    @Override
    public void onButtonFollowClick(String userId, int position) {
        mPresenter.followFriend(userId);
        mAdapter.updateFollowUser(position);
    }

    @Override
    public void onButtonUnFollowClick(Friend friend, int position) {
        ConfirmUnfollowDialog dialog = new ConfirmUnfollowDialog(getContext());
        dialog.setMessage(getString(R.string.unfollow_user, friend.fullName));
        if (friend.profilePhoto != null) {
            dialog.setImageAvatar(friend.profilePhoto.original);
        } else {
            dialog.setImageAvatar(null);

        }
        dialog.setNoButton(getContext().getResources().getString(R.string.action_cancel),
            view -> dialog.dismiss());
        dialog
            .setYesButton(getContext().getResources().getString(R.string.action_unfollow), view -> {
                mPresenter.unfollowFriend(friend.id);
                mAdapter.updateUnfollowUser(position);
                dialog.dismiss();
            });
        dialog.show();
    }

    @Override
    public void onButtonFollowAllClick(ArrayList<String> listUserId) {
        mPresenter.followAllFriend(listUserId);
        mAdapter.updateListFollowAll();
    }

    @Override
    public void onUserPageClick(String userId) {
        Bundle bundle = new Bundle();
        if (userId.equals(AppSharePreference.getUid(getContext()))) {
            bundle.putInt(ProfileActivity.BUNDLE_TYPE_USER, UserDefine.USER_TYPE_PROFILE);
        } else {
            bundle.putInt(ProfileActivity.BUNDLE_TYPE_USER, UserDefine.USER_TYPE_FRIENDS);
            bundle.putString(ProfileActivity.BUNDLE_USER_ID, userId);
        }
        IntentUtils.startActivityMyPage(bundle, getContext());
    }
}
