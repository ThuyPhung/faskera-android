/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.topic;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.faskera.callback.OnClickItemToolbarListener;
import com.faskera.common.base.BaseFragment;
import com.faskera.common.view.AnyTextview;
import com.faskera.era.R;
import com.faskera.model.option.TbOption;
import com.faskera.utils.ActivityUtils;
import com.faskera.view.activity.topic.TopicAdapter;
import com.faskera.view.activity.topic.model.Topic;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * @author theanh
 * @since 1/28/2017
 */
public class EditTopicFragment extends BaseFragment implements EditTopicContract.View {
    @BindView(R.id.text_suggest)
    AnyTextview mTextSuggest;
    @BindView(R.id.recycle_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.text_error)
    AnyTextview mTextError;
    private EditTopicContract.Presenter mPresenter;
    private TopicAdapter mTopicAdapter;

    @Override
    protected int idRoot() {
        return R.layout.fragment_topic;
    }

    @Override
    protected void initView(View root) {
        mTextSuggest.setVisibility(View.GONE);
        mTextError.setVisibility(View.GONE);
        mTextSuggest.setText(getString(R.string.edit_topic_suggest));
        mTopicAdapter = new TopicAdapter();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setAdapter(mTopicAdapter);
        new EditTopicPresenter(getContext(), this);
    }

    @Override
    protected void initData() {
    }

    @Override
    public void onRefresh(Object o) {
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.subscribe();
    }

    @Override
    public void onResume() {
        super.onResume();
        Resources resource = getContext().getResources();
        getBaseActivity().update(new TbOption(resource.getString(R.string.tb_edit_topics), true));
        getBaseActivity().update(new OnClickItemToolbarListener() {
            @Override
            public void onBack() {
                getBaseActivity().onBack(null);
            }

            @Override
            public void onDone() {
                if (mTopicAdapter.getSelectedTopics().size() >= 5) {
                    mPresenter.updateInterestedTopics(mTopicAdapter.getSelectedTopics());
                } else {
                    onErrorUpdate();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.unsubscribe();
    }

    /**
     * View
     */
    @Override
    public void setPresenter(EditTopicContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onGetTopicsSuccess(ArrayList<Topic> topics) {
        mTextSuggest.setVisibility(View.VISIBLE);
        mTopicAdapter.setTopicList(topics);
    }

    @Override
    public void onUpdateTopicSuccess() {
        getBaseActivity().onBack(null);
    }

    @Override
    public void showLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onErrorConnection(int code) {
        getBaseActivity().handleErrorCode(code);
        mRecyclerView.setVisibility(View.GONE);
        mTextSuggest.setVisibility(View.GONE);
        mTextError.setVisibility(View.VISIBLE);
        //Disable action
        getBaseActivity().update(null);
    }

    @Override
    public void onErrorUpdate() {
        ActivityUtils.showToast(getContext(), getString(R.string.msg_edit_topic_error));
    }
}
