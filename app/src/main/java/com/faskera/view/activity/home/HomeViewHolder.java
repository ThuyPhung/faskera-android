/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.faskera.common.UserDefine;
import com.faskera.common.ViewHolder;
import com.faskera.common.view.DynamicHeightImageView;
import com.faskera.common.view.FollowerCompound;
import com.faskera.era.R;
import com.faskera.utils.HumanTime;
import com.faskera.utils.ImageLoader;
import com.faskera.view.activity.home.model.FeedViewDetail;
import com.faskera.view.activity.home.model.abst.AbsFeedAns;
import com.faskera.view.activity.home.model.abst.AbsFeedContentMedia;
import com.faskera.view.activity.home.model.abst.AbsFeedContentText;
import com.faskera.view.activity.home.model.abst.AbsFeedFooter;
import com.faskera.view.activity.home.model.abst.AbsFeedHeader;
import com.faskera.view.activity.home.model.abst.AbsFeedSuperHeader;
import com.faskera.view.activity.mypage.abs.AbsInfoExtra;
import com.faskera.view.activity.mypage.abs.AbsProfile;

import butterknife.BindView;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public class HomeViewHolder {

    public static class FeedViewHolderCreate extends ViewHolder {

        public FeedViewHolderCreate(View itemView) {
            super(itemView);
        }
    }

    public static class FeedViewHolderHeader extends ViewHolder {

        @BindView(R.id.avatar)
        public ImageView mAvatar;

        @BindView(R.id.txt_name)
        public TextView mTextViewName;

        @BindView(R.id.txt_content)
        public TextView mTextViewContent;

        @BindView(R.id.image_more)
        public ImageView mImageMore;


        public FeedViewHolderHeader(View itemView) {
            super(itemView);
        }

        public void bindData(Context context, AbsFeedHeader data, AbsProfile profile) {

            if (data.isAnonymous()) {
                ImageLoader.loadImageViewWith(context, R.drawable.ic_anonymous, mAvatar);
            } else {
                if (profile == null) {
                    if (data.getAvatar() == null || data.getAvatar().trim().isEmpty()) {
                        ImageLoader.loadImageViewWith(context, R.drawable.ic_avatar_default, mAvatar);

                    } else {
                        ImageLoader.loadImageViewWith(context, data.getAvatar(), mAvatar);
                    }
                } else {
                    if (profile.getAvatar() == null || profile.getAvatar().trim().isEmpty()) {
                        ImageLoader.loadImageViewWith(context, R.drawable.ic_avatar_default, mAvatar);

                    } else {
                        ImageLoader.loadImageViewWith(context, profile.getAvatar(), mAvatar);
                    }
                }
            }

            if (profile == null) {
                mTextViewName.setText(
                        data.getName() != null ? data.getName() : context.getResources().getString(R.string.anonymous));
            } else {
                mTextViewName.setText(
                        data.getName() != null ? profile.getName() : context.getResources().getString(R.string.anonymous));
            }
            // mTextViewContent.setText(data.getSubName());
            StringBuilder content = new StringBuilder();
            content.append(data.getPoll().mCate.getName());
            content.append(" \u2022 ");
            content.append(new HumanTime().timeAgo(data.getPoll().mCreatedTime));
            mTextViewContent.setText(content);

        }
    }

    public static class FeedViewHolderContent extends ViewHolder {
        @BindView(R.id.content)
        public TextView mContent;

        public FeedViewHolderContent(View itemView) {
            super(itemView);
        }

        public void bindData(Context context, AbsFeedContentText data) {
            mContent.setText(data.getContent());

        }
    }

    public static class FeedViewHolderContentImage extends ViewHolder {
        @BindView(R.id.imageview)
        public ImageView mImage;

        public FeedViewHolderContentImage(View itemView) {
            super(itemView);
        }

        public void bindData(Context context, AbsFeedContentMedia data) {
            if (mImage instanceof DynamicHeightImageView) {
                ((DynamicHeightImageView) (mImage)).setHeightRatio(data.getPhoto().originalHeight * 1.0f / data.getPhoto().originalWidth);
            }
            ImageLoader.loadImageViewWith(context, data.getPhoto().original, mImage);
        }
    }

    public static class FeedViewHolderAns extends ViewHolder {
        @BindView(R.id.image_selected)
        public ImageView mImage;

        @BindView(R.id.content)
        public TextView mTxtContent;

        @BindView(R.id.txt_votes)
        public TextView mTxtVotes;


        public FeedViewHolderAns(View itemView) {
            super(itemView);
        }

        public void bindData(Context context, AbsFeedAns data) {
            if (data.isSelected()) {
                ImageLoader.loadImageViewWith(context, R.drawable.ic_voter, mImage);
            } else {
                ImageLoader.loadImageViewWith(context, R.drawable.ic_radio_button_unchecked, mImage);
            }

            mTxtContent.setText(data.getContent());

            mTxtVotes.setText(data.getVotes());

        }
    }

    public static class FeedViewHolderFooter extends ViewHolder {

        @BindView(R.id.icon_vote)
        public ImageView mImageIcon;

        @BindView(R.id.txt_votes)
        public TextView mTextViewVotes;

        @BindView(R.id.text_share)
        public TextView mTextShare;


        public FeedViewHolderFooter(View itemView) {
            super(itemView);
        }

        public void bindData(Context context, AbsFeedFooter data) {

            StringBuilder stringBuilder = new StringBuilder();
            if (data.getVoteInt() > 0 ) {
                stringBuilder.append(data.getVote()).append(" ");

                if (Integer.parseInt(data.getVote()) <= 1)
                    stringBuilder.append(context.getResources().getString(R.string.vote));
                else
                    stringBuilder.append(context.getResources().getString(R.string.votes));
            } else {
                stringBuilder.append(context.getResources().getString(R.string.votes_first));
            }

//            if (data.getPoll().getPoll().votedByMe) {
//                ImageLoader.loadImageViewWith(context, R.drawable.ic_voter, mImageIcon);
//            } else {
//                ImageLoader.loadImageViewWith(context, R.drawable.ic_vote, mImageIcon);
//            }
            ImageLoader.loadImageViewWith(context, R.drawable.ic_radio_button_checked, mImageIcon);

            mTextViewVotes.setText(stringBuilder);
        }
    }

    public static class FeedViewHolderSuperHeader extends ViewHolder {

        @BindView(R.id.content)
        public TextView mTextContent;


        public FeedViewHolderSuperHeader(View itemView) {
            super(itemView);
        }

        public void bindData(Context context, AbsFeedSuperHeader data) {
            mTextContent.setText(data.getContent(context));
        }
    }

    public static class ProfileUser extends ViewHolder {

        @BindView(R.id.avatar)
        public ImageView mAvatar;

        @BindView(R.id.polls)
        public FollowerCompound mPollView;

        @BindView(R.id.follow)
        public FollowerCompound mFollowView;

        @BindView(R.id.following)
        public FollowerCompound mFollowingView;

        @BindView(R.id.txt_name)
        public TextView mName;

        @BindView(R.id.text_edit_profile)
        public TextView mEditProfile;

        @BindView(R.id.introduction)
        public TextView mIntroduction;


        public ProfileUser(View itemView) {
            super(itemView);
        }

        public void bindData(Context context, AbsProfile data) {
            if (data.getAvatar() == null) {
                    ImageLoader.loadImageViewWith(context, R.drawable.ic_avatar_default, mAvatar);
            } else {
                if (data.getAvatar().trim().isEmpty()) {
                    ImageLoader.loadImageViewWith(context, R.drawable.ic_avatar_default, mAvatar);

                } else {
                    ImageLoader.loadImageViewWith(context, data.getAvatar(), mAvatar);
                }
            }

            mPollView.setTitle(data.getNumberPoll() + "");
            mPollView.setSubContent(context.getResources().getString(R.string.poll));

            mFollowView.setTitle(data.getNumberFollower() + "");
            mFollowView.setSubContent(context.getResources().getString(R.string.followers));

            mFollowingView.setTitle(data.getNumberFollowing() + "");
            mFollowingView.setSubContent(context.getResources().getString(R.string.following));

            mName.setText(data.getName());

            if (data.getTypeProfile(context) == UserDefine.USER_TYPE_FRIENDS) {

                if (data.getStateFollow() == UserDefine.STATE_FOLLOW) {
                    mEditProfile.setText(context.getString(R.string.action_follow));
                    mEditProfile.setTextAppearance(context, R.style.Follow);
                    mEditProfile.setBackgroundResource(R.drawable.bg_follow);
                } else {
                    mEditProfile.setTextAppearance(context, R.style.Following);
                    mEditProfile.setText(context.getString(R.string.following_caps));
                    mEditProfile.setBackgroundResource(R.drawable.bg_following);
                }

            } else {
                mEditProfile.setText(context.getString(R.string.edit_profile));
                mEditProfile.setBackgroundResource(R.drawable.bg_edit_profile);
            }


            if (data.getIntroduction() == null || data.getIntroduction().isEmpty()) {
                mIntroduction.setVisibility(View.GONE);
            } else {
                mIntroduction.setVisibility(View.VISIBLE);
            }
            mIntroduction.setText(data.getIntroduction());
        }
    }

    public static class ViewMoreViewHolder extends ViewHolder {

        @BindView(R.id.root_layout)
        public View mRoot;

        @BindView(R.id.title)
        public TextView mTitle;

        @BindView(R.id.right_icon)
        public ImageView mIcon;

        public ViewMoreViewHolder(View itemView) {
            super(itemView);
        }

        public void bindData(Context context, boolean isViewMore) {
            if (isViewMore) {
                mTitle.setText(context.getResources().getString(R.string.action_collapse));
                mIcon.setImageResource(R.drawable.ic_keyboard_arrow_up);
            } else {
                mTitle.setText(context.getResources().getString(R.string.view_more));
                mIcon.setImageResource(R.drawable.ic_keyboard_arrow_down);
            }
        }
    }

    public static class InfoExtraViewHolder extends ViewHolder {
        @BindView(R.id.imageview)
        public ImageView mIcon;

        @BindView(R.id.content)
        public TextView mContent;


        public InfoExtraViewHolder(View itemView) {
            super(itemView);
        }

        public void bindData(Context context, AbsInfoExtra data) {
            ImageLoader.loadImageViewWith(context, data.getIconSource(), mIcon);
            mContent.setText(data.getTitle());
        }
    }

    public static class ItemLoadingViewHolder extends ViewHolder {


        public ItemLoadingViewHolder(View itemView) {
            super(itemView);
        }


    }

    public static class ItemViewDetailViewHolder extends ViewHolder {
        @BindView(R.id.image_selected)
        public ImageView mImage;

        @BindView(R.id.content)
        public TextView mTxtContent;

        @BindView(R.id.txt_votes)
        public TextView mTxtVotes;

        public ItemViewDetailViewHolder(View itemView) {
            super(itemView);
        }

        public void bindData(Context context, FeedViewDetail text) {
            if (text.mContent > 0) {
                mTxtContent.setText(String.format(context.getResources().getString(R.string.text_more), text.mContent));
            } else {
                mTxtContent.setText(context.getResources().getString(R.string.text_detail));
            }
            mTxtVotes.setText(String.valueOf(text.mPoll.mListOptions.size()));
            mTxtVotes.setVisibility(View.INVISIBLE);
            mImage.setVisibility(View.INVISIBLE);
            mTxtContent.setBackgroundResource(R.drawable.bg_poll_detail);
            mTxtContent.setGravity(Gravity.CENTER);
        }


    }


}
