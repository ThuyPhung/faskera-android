/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.follow;

import com.faskera.common.adapter.AbsUserState;
import com.faskera.mvp.BasePresenter;
import com.faskera.mvp.BaseView;

import java.util.ArrayList;

import rx.Subscription;

/**
 * @author DANGNGOCDUC
 * @since 2/2/2017
 */

public interface ShowListUserContract {

    interface Presenter extends BasePresenter {

        Subscription getUserList(boolean refresh);

    }

    interface View extends BaseView<Presenter> {

        void onLoadMore();

        void onLoading();

        void onLoadError(int code);

        void onLoadError(Throwable e);

        void onLoadSuccess(ArrayList<AbsUserState> list);
    }

}
