/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.discoverpeople;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.faskera.common.view.AnyTextview;
import com.faskera.era.R;
import com.faskera.utils.ImageLoader;
import com.faskera.view.activity.option.view.fbfriend.model.Friend;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.faskera.Constant.FriendRelation.FACEBOOK_FRIEND;

/**
 * @author theanh
 * @since 2/12/2017
 */
public class SearchFriendsAdapter
    extends RecyclerView.Adapter<SearchFriendsAdapter.FriendsViewHolder> {
    private ArrayList<Friend> mFriends;
    private Context mContext;
    private SearchFriendsAdapterListener mListener;
    private LayoutInflater mInflater;

    public interface SearchFriendsAdapterListener {
        void onClickBtnFollow(String userId, int position);
        void onClickBtnUnfollow(Friend friend, int position);
        void onClickProfile(String userId);
    }

    public void setSearchFriendsAdapterListener(SearchFriendsAdapterListener listener) {
        mListener = listener;
    }

    public SearchFriendsAdapter(Context context, ArrayList<Friend> friends) {
        mFriends = friends;
        mContext = context;
    }

    public void updateListFriends(ArrayList<Friend> friends) {
        if (friends == null) return;
        mFriends.clear();
        mFriends.addAll(friends);
        notifyDataSetChanged();
    }

    public void updateUnfollowUser(int position) {
        if (mFriends.get(position) == null) return;
        mFriends.get(position).following = false;
        notifyItemChanged(position);
    }

    public void updateFollowUser(int position) {
        if (mFriends.get(position) == null) return;
        mFriends.get(position).following = true;
        notifyItemChanged(position);
    }

    @Override
    public FriendsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mInflater == null) mInflater = LayoutInflater.from(parent.getContext());
        return new FriendsViewHolder(mInflater.inflate(R.layout.row_friends, parent, false));
    }

    @Override
    public void onBindViewHolder(FriendsViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mFriends.size();
    }

    public class FriendsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_avatar)
        ImageView mIvAvatar;
        @BindView(R.id.txt_name)
        AnyTextview mTxtName;
        @BindView(R.id.txt_relationship)
        AnyTextview mTxtRelationship;
        @BindView(R.id.btn_follow)
        AnyTextview mBtnFollow;
        @BindView(R.id.item_fb_friend)
        LinearLayout itemFbFriend;

        public FriendsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(int position) {
            if (mFriends.get(position) == null) return;
            Friend friend = mFriends.get(position);
            if (friend.profilePhoto != null) {
                ImageLoader.loadImageViewWith(mContext, friend.profilePhoto.original, mIvAvatar);
            } else {
                ImageLoader.loadImageViewWith(mContext, R.drawable.ic_avatar_default, mIvAvatar);
            }
            mTxtName.setText(friend.fullName);
            if (friend.relationship == null) {
                mTxtRelationship.setText("");
            } else if (friend.relationship.equals(FACEBOOK_FRIEND)) {
                mTxtRelationship.setText(mContext.getString(R.string.facebook_friends));
            } else {
                mTxtRelationship.setText(mContext.getString(R.string.text_following_you));
            }
            mBtnFollow.setText(friend.following ? mContext.getString(R.string.following_caps) :
                mContext.getString(R.string.action_follow));
            mBtnFollow.setSelected(friend.following);
            mBtnFollow.setOnClickListener(view -> {
                if (mBtnFollow.isSelected()) mListener.onClickBtnUnfollow(friend, position);
                else mListener.onClickBtnFollow(friend.id, position);
            });
            itemFbFriend.setOnClickListener(view -> mListener.onClickProfile(friend.id));
        }
    }
}
