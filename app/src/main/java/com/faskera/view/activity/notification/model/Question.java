/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.notification.model;

import com.faskera.model.user.Photo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author framgia
 * @since 14/02/2017
 */


public class Question {

    @SerializedName("text")
    @Expose
    public String text;
    @SerializedName("photo")
    @Expose
    public Photo photo;

}
