/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.faskera.era.R;
import com.faskera.view.activity.createpoll.frame.adapter.PhotoPopupAdapter;
import com.faskera.view.activity.createpoll.frame.event.OnFilterItemClickListener;
import com.faskera.view.activity.createpoll.frame.event.OnItemClickListener;
import com.faskera.view.activity.createpoll.frame.model.FilterItem;
import com.faskera.view.activity.createpoll.frame.model.PopupFilterItem;
import com.faskera.view.activity.createpoll.frame.utils.DisplayUtils;
import com.faskera.view.activity.createpoll.frame.utils.FilterUtils;

import java.util.ArrayList;
import java.util.List;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;

public class PhotoPopupWindow extends PopupWindow {
    private static final int ITEM_PS_FILTER_WIDTH_IN_DP = 60;

    private Context context;

    private Bitmap originBitmap;

    private ImageView selectBtn;
    private RecyclerView recyclerView;

    private List<FilterItem> filterList;
    private List<PopupFilterItem> popupFilterList;

    private PhotoPopupAdapter popupAdapter;

    public PhotoPopupWindow(Context context) {
        super(context);

        this.context = context;

        View contentView = LayoutInflater.from(context).inflate(R.layout.photo_popup_window_layout, null);

        setContentView(contentView);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setFocusable(false);

        init(contentView);
    }

    private void init(View contentView) {
        selectBtn = (ImageView) contentView.findViewById(R.id.jigsaw_select_btn);

        recyclerView = (RecyclerView) contentView.findViewById(R.id.jigsaw_filter_horizontal_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setHasFixedSize(true);


        filterList = FilterUtils.getFilterLists();
        popupFilterList = new ArrayList<>();


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inSampleSize = 2;
        originBitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_popup_origin, options);

        for (FilterItem filterItem : filterList) {
            popupFilterList.add(new PopupFilterItem(filterItem, originBitmap,context.getString(filterItem.getName())));
        }

        popupAdapter = new PhotoPopupAdapter(context, popupFilterList);

        recyclerView.setAdapter(popupAdapter);

        new ImageRenderTask(filterList).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void setSelectedFilterPosition(int position) {
        popupAdapter.setSelection(position);
        popupAdapter.notifyDataSetChanged();
        scrollToCenter(position);
    }

    public void scrollToCenter(int position) {
        int itemWidth = DisplayUtils.dp2px(context, ITEM_PS_FILTER_WIDTH_IN_DP);
        int middleX = (DisplayUtils.getScreenWidth(context) - itemWidth) / 2;
        int offset = recyclerView.computeHorizontalScrollOffset();
        int selectedOffset = itemWidth * position + itemWidth / 2;
        int x = selectedOffset - offset - middleX;

        recyclerView.smoothScrollBy(x, 0);
    }

    public void setSelectListener(final View.OnClickListener onClickListener) {
        selectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onClickListener) {
                    onClickListener.onClick(v);
                }
            }
        });
    }

    public void setOnFilterItemClickListener(final OnFilterItemClickListener onFilterItemClickListener) {
        popupAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                FilterItem filterItem = filterList.get(position);
                GPUImageFilter filter = filterItem.getFilter();

                if (null != onFilterItemClickListener) {
                    onFilterItemClickListener.onItemClick(view, position, filter);
                }

                scrollToCenter(position);
            }
        });
    }

    private class ImageRenderTask extends AsyncTask<Void, Void, List<PopupFilterItem>> {
        private GPUImage gpuImage;
        private List<FilterItem> filterList;

        public ImageRenderTask(List<FilterItem> filterList) {
            this.filterList = filterList;
        }

        @Override
        protected void onPreExecute() {
            gpuImage = new GPUImage(context);
        }

        @Override
        protected List<PopupFilterItem> doInBackground(Void... params) {
            List<PopupFilterItem> popupFilterList = new ArrayList<>();
            int size = filterList.size();

            for (int i = 0; i < size; i++) {
                FilterItem filterItem = filterList.get(i);
                gpuImage.setFilter(filterItem.getFilter());
                Bitmap pvBitmap = gpuImage.getBitmapWithFilterApplied(originBitmap);


                PopupFilterItem item = new PopupFilterItem();
                item.setBitmap(pvBitmap);
                item.setFilterItem(filterList.get(i));

                popupFilterList.add(item);
            }

            return popupFilterList;
        }

        @Override
        protected void onPostExecute(List<PopupFilterItem> result) {
            popupFilterList.clear();
            popupFilterList.addAll(result);
            popupAdapter.notifyDataSetChanged();
        }
    }
}
