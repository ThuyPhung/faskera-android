/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll;

import android.content.Context;

import com.faskera.common.base.ObserverAdvance;
import com.faskera.network.ApiManager;
import com.faskera.view.activity.topic.TopicContract;
import com.faskera.view.activity.topic.network.InterestedTopicResponse;

import java.util.ArrayList;

import rx.subscriptions.CompositeSubscription;

public class ListTopicPresenter implements TopicContract.Presenter {

    private TopicContract.View mView;
    private Context mContext;
    private CompositeSubscription mCompositeSubscription;

    public ListTopicPresenter(Context context, TopicContract.View view) {
        mContext = context;
        mView = view;
        mView.setPresenter(this);
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getTopics() {
        mView.showLoading();
        mCompositeSubscription.add(ApiManager.getInstance()
                .getListTopic(mContext, new ObserverAdvance<InterestedTopicResponse>() {
                    @Override
                    public void onError(int code) {
                        if (mView != null) {
                            mView.hideLoading();
                            mView.onError(code);
                        }

                    }

                    @Override
                    public void onSuccess(InterestedTopicResponse data) {
                        if (mView != null) {
                            mView.hideLoading();
                            mView.onGetTopicsSuccess(data.mListTopic);
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }));


    }

    @Override
    public void updateInterestedTopics(ArrayList<String> listId) {

    }

    @Override
    public void subscribe() {
        getTopics();
    }

    @Override
    public void unsubscribe() {
        if (mCompositeSubscription != null && mCompositeSubscription.isUnsubscribed()) {
            mCompositeSubscription.unsubscribe();
            mView = null;
        }
    }
}
