/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.viewvote;

import com.faskera.common.adapter.AbsUserState;
import com.faskera.mvp.BasePresenter;
import com.faskera.mvp.BaseView;

import java.util.ArrayList;

/**
 * @author DANGNGOCDUC
 * @since 1/3/2017
 */

public interface ViewVoteContract {

    interface Presenter extends BasePresenter {

        void getListVote(String pollId);

    }

    interface View extends BaseView<Presenter> {

        void onLoadMore();

        void onLoading();

        void onSuccess(ArrayList<AbsUserState> listVoter);

        void onError(int code);

    }
}
