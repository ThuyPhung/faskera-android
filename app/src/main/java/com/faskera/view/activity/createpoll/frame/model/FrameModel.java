/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.model;

import android.graphics.Bitmap;
import android.os.Bundle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jacky
 * @since 2/9/17
 */

public class FrameModel implements Serializable {

    public int mPositionFrame;
    public List<Bundle> mListBundles;
    public ArrayList<Bitmap> mListBitmapFilter;
    public int mPositionFilter;
    public boolean isFilter;

    public void removeBitmap() {
        if (mListBitmapFilter != null && mListBitmapFilter.size() > 0) {
            for (Bitmap mBitmap : mListBitmapFilter) {
                mBitmap.recycle();
            }
        }
    }
}
