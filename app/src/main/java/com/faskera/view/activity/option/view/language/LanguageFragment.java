/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.language;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.content.IntentCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.faskera.callback.OnClickItemToolbarListener;
import com.faskera.common.AppSharePreference;
import com.faskera.common.LocaleHelper;
import com.faskera.common.base.BaseFragment;
import com.faskera.era.R;
import com.faskera.model.option.LanguageModel;
import com.faskera.model.option.TbOption;
import com.faskera.utils.LanguageUtils;
import com.faskera.view.activity.login.LoginActivity;
import com.faskera.view.widget.CustomAlertDialog;

import butterknife.BindView;

/**
 * @author theanh
 * @since 1/24/2017
 */
public class LanguageFragment extends BaseFragment
    implements LanguageAdapter.onLanguageClickListener {
    @BindView(R.id.recycle_view)
    RecyclerView mRecycleView;
    private LanguageAdapter mAdapter;

    @Override
    protected int idRoot() {
        return R.layout.fragment_change_language;
    }

    @Override
    protected void initView(View root) {
        mAdapter = new LanguageAdapter(LanguageUtils.initLanguageList(getContext()));
        mAdapter.setOnItemClickListener(this);
        mRecycleView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecycleView.addItemDecoration(
            new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        mRecycleView.setAdapter(mAdapter);
    }

    @Override
    protected void initData() {
    }

    @Override
    public void onRefresh(Object o) {
    }

    @Override
    public void onItemClick(LanguageModel languageModel) {
        CustomAlertDialog alertDialog = new CustomAlertDialog(getContext());
        alertDialog.setMessage(getString(R.string.option_language_confirm));
        alertDialog.setYesButton(getString(R.string.action_ok), view -> {
            AppSharePreference.getInstance(getContext()).setMyLanguage(languageModel.getCode());
            LocaleHelper.setLocale(getContext(), languageModel.getCode());
            Intent intent = new Intent(getContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        });
        alertDialog.setNoButton(getString(R.string.action_cancel), view -> alertDialog.dismiss());
        alertDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        Resources resource = getContext().getResources();
        getBaseActivity().update(new TbOption(resource.getString(R.string.tb_language), false));
        getBaseActivity().update(new OnClickItemToolbarListener() {
            @Override
            public void onBack() {
                getBaseActivity().onBack(null);
            }

            @Override
            public void onDone() {
            }
        });
    }
}
