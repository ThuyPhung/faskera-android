/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.notification.network;

import com.google.gson.annotations.SerializedName;

/**
 * @author framgia
 * @since 21/02/2017
 */
public class RegisterToken {
    @SerializedName("type")
    public int type;
    @SerializedName("token")
    public String token;

    public RegisterToken(int type, String token) {
        this.type = type;
        this.token = token;
    }
}
