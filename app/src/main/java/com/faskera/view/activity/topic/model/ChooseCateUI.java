/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.topic.model;

/**
 * @author DANGNGOCDUC
 * @since 12/27/2016
 */

public abstract class ChooseCateUI {

    public abstract void setSelected(boolean selected);

    public abstract String getName();

    public abstract boolean isSelected();

    public abstract String getId();

}
