/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.faskera.common.debug.DebugUtil;
import com.faskera.era.R;
import com.faskera.view.activity.createpoll.frame.event.OnModelItemClickListener;
import com.faskera.view.activity.createpoll.frame.widget.PhotoFrameLayout;
import com.faskera.view.activity.createpoll.frame.widget.PhotoModelLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ModelLinearAdapter extends RecyclerView.Adapter<ModelLinearAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private List<PhotoModelLayout> models;
    private int curSelection = 0;

    private OnModelItemClickListener onModelItemClickListener;

    public ModelLinearAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.item_model, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        try {
            holder.mFrameLayout.removeAllViews();
            //holder.mFrame.setBackgroundResource(R.color.black54);
            if (holder.mFrameLayout.indexOfChild(models.get(position)) == -1) {
                holder.mFrameLayout.addView(models.get(position));
            }

            holder.mFrameLayout.setOnClickListener(v -> {
                if (null != onModelItemClickListener) {
                    curSelection = position;
                    onModelItemClickListener.onClick(v, position);
                    //notifyDataSetChanged();
                }
            });
            /*
            if (curSelection == position) {
                holder.mFrame.setVisibility(View.VISIBLE);
            } else {
                holder.mFrame.setVisibility(View.INVISIBLE);
            }*/
        } catch (Exception e) {

            DebugUtil.d("===========================ModelLinearAdapter-onBindViewHolder", e.toString());
            
        }
    }

    @Override
    public int getItemCount() {
        return null == models ? 0 : models.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_model_layout)
        PhotoFrameLayout mFrameLayout;
        @BindView(R.id.item_photo_filter_layout)
        FrameLayout mFrame;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setModels(List<PhotoModelLayout> models) {
        this.models = models;
    }

    public void setOnModelItemClickListener(OnModelItemClickListener onModelItemClickListener) {
        this.onModelItemClickListener = onModelItemClickListener;
    }
}
