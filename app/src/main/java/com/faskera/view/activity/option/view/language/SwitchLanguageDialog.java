/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.language;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.faskera.common.view.AnyTextview;
import com.faskera.era.R;
import com.faskera.view.activity.option.OptionActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author framgia
 * @since 10/02/2017
 */
public class SwitchLanguageDialog extends BottomSheetDialogFragment {
    @BindView(R.id.text_english)
    AnyTextview mTextEnglish;
    @BindView(R.id.text_vietnamese)
    AnyTextview mTextVietnamese;
    private LayoutInflater mInflater;
    private ChangeLanguageBottomSheetCallback mCallback;

    public interface ChangeLanguageBottomSheetCallback {
        void onChooseEnglish();
        void onChooseVietnamese();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OptionActivity) {
            mCallback = (ChangeLanguageBottomSheetCallback) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mInflater == null) mInflater = LayoutInflater.from(getContext());
        View rootView = mInflater.inflate(R.layout.dialog_switch_language, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @OnClick({R.id.text_english, R.id.text_vietnamese, R.id.text_cancel})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_english:
                mCallback.onChooseEnglish();
                break;
            case R.id.text_vietnamese:
                mCallback.onChooseVietnamese();
                break;
            case R.id.text_cancel:
                break;
        }
        dismiss();
    }
}
