/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.faskera.common.AppSharePreference;
import com.faskera.common.DividerViewHolder;
import com.faskera.common.UserDefine;
import com.faskera.era.R;
import com.faskera.utils.IntentUtils;
import com.faskera.view.activity.createpoll.CreatePollActivity;
import com.faskera.view.activity.createpoll.GalleryActivity;
import com.faskera.view.activity.home.model.Feed;
import com.faskera.view.activity.home.model.ImlHomeRow;
import com.faskera.view.activity.home.model.abst.AbsFeedAns;
import com.faskera.view.activity.home.model.abst.AbsFeedContentMedia;
import com.faskera.view.activity.home.model.abst.AbsFeedContentText;
import com.faskera.view.activity.home.model.abst.AbsFeedFooter;
import com.faskera.view.activity.home.model.abst.AbsFeedHeader;
import com.faskera.view.activity.home.model.abst.AbsFeedSuperHeader;
import com.faskera.view.activity.login.LoginActivity;
import com.faskera.view.activity.mypage.ProfileActivity;
import com.faskera.view.activity.mypage.abs.AbsInfoExtra;
import com.faskera.view.activity.mypage.abs.AbsProfile;

import java.util.ArrayList;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public class HomeAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<ImlHomeRow> mListItemHome = new ArrayList<>();

    public ArrayList<ImlHomeRow> getListItemHome() {
        return mListItemHome;
    }

    private OnClickCallback mCallbackClickMenuFeed;

    private ArrayList<AbsInfoExtra> mListInfoExtra = new ArrayList<>();

    public boolean mViewMore = false;

    private View.OnClickListener mOnGotoProfile = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Feed.Poll poll = (Feed.Poll) v.getTag();

            if (AppSharePreference.getUid(mContext).equals(poll.mCreatedUser.id)) {
                Bundle bundle = new Bundle();
                bundle.putInt(ProfileActivity.BUNDLE_TYPE_USER, UserDefine.USER_TYPE_PROFILE);
                IntentUtils.startActivityMyPage(bundle, mContext);
            } else {
                Bundle bundle = new Bundle();
                bundle.putInt(ProfileActivity.BUNDLE_TYPE_USER, UserDefine.USER_TYPE_FRIENDS);
                IntentUtils.startActivityMyPage(bundle, mContext);
            }
        }
    };

    private View.OnClickListener onClickCreatePoll = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!AppSharePreference.getInstance(mContext).isUserLoggedIn()) {
                mContext.startActivity(new Intent(mContext, LoginActivity.class));
            } else {
                if (v instanceof TextView) {
                    Intent intent = new Intent(mContext, CreatePollActivity.class);
                    mContext.startActivity(intent);
                } else {
                    Intent intent = new Intent(mContext, GalleryActivity.class);
                    mContext.startActivity(intent);
                }
            }
        }
    };

    private View.OnClickListener mOnClickAns = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int pos = (int) v.getTag();
            int pos2 = -1;
            boolean isStop = false;
            for (int i = (pos - 1); i >= 0; i--) {
                if (mListItemHome.get(i) instanceof AbsFeedAns) {
                    if (((AbsFeedAns) mListItemHome.get(i)).isSelected()) {
                        ((AbsFeedAns) mListItemHome.get(i)).setSelected(false);
                        isStop = true;
                        pos2 = i;
                        break;
                    }
                } else {
                    break;
                }
            }

            if (!isStop) {
                for (int i = (pos + 1); i < mListItemHome.size(); i++) {
                    if (mListItemHome.get(i) instanceof AbsFeedAns) {
                        if (((AbsFeedAns) mListItemHome.get(i)).isSelected()) {
                            ((AbsFeedAns) mListItemHome.get(i)).setSelected(false);
                            pos2 = i;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }


            ((AbsFeedAns) mListItemHome.get(pos)).setSelected(true);

            notifyItemChanged(pos);
            if (pos2 != -1) {
                notifyItemChanged(pos2);
            }


        }
    };

    public void setOnClickMenuFeed(OnClickCallback callback) {

        mCallbackClickMenuFeed = callback;

    }

    private View.OnClickListener onClickViewMore = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (!mViewMore) {
                mViewMore = true;
                mListItemHome.addAll(1, mListInfoExtra);
                notifyItemRangeInserted(1, mListInfoExtra.size());
                notifyItemChanged(1 + mListInfoExtra.size());

            } else {
                mViewMore = false;
                mListItemHome.removeAll(mListInfoExtra);
                notifyItemChanged(1 + mListInfoExtra.size());
                notifyItemRangeRemoved(1, mListInfoExtra.size());
            }
        }
    };

    private View.OnClickListener mOnClickViewMoreFeed = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mCallbackClickMenuFeed != null) {
                mCallbackClickMenuFeed.onClickMenuFeed();
            }

        }
    };

    private View.OnClickListener mOnClickDetail = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Bundle bundle = new Bundle();
            IntentUtils.startActivityDetail(mContext, bundle);


        }
    };

    private View.OnClickListener mOnViewVotePeople = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            IntentUtils.startActivityViewVote(null, mContext);
        }
    };

    public HomeAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    public void addAll(ArrayList<ImlHomeRow> list) {
        mListItemHome.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return mListItemHome.get(position).getType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case ImlHomeRow.TYPE_CREATE_FEED:
                view = mLayoutInflater.inflate(R.layout.item_feed_create, parent, false);
                view.findViewById(R.id.tv_ask_something).setOnClickListener(onClickCreatePoll);
                view.findViewById(R.id.iv_camera).setOnClickListener(onClickCreatePoll);
                return new HomeViewHolder.FeedViewHolderCreate(view);
            case ImlHomeRow.TYPE_HEADER_FEED:
                view = mLayoutInflater.inflate(R.layout.item_feed_header, parent, false);
                HomeViewHolder.FeedViewHolderHeader headerHolder = new HomeViewHolder.FeedViewHolderHeader(view);
                headerHolder.mAvatar.setOnClickListener(mOnGotoProfile);
                headerHolder.mTextViewName.setOnClickListener(mOnGotoProfile);
                headerHolder.mImageMore.setOnClickListener(mOnClickViewMoreFeed);
                return headerHolder;
            case ImlHomeRow.TYPE_CONTENT_FEED:
                view = mLayoutInflater.inflate(R.layout.item_feed_content, parent, false);
                return new HomeViewHolder.FeedViewHolderContent(view);
            case ImlHomeRow.TYPE_CONTENT_FEED_IMAGE:
                view = mLayoutInflater.inflate(R.layout.item_feed_content_image, parent, false);
                return new HomeViewHolder.FeedViewHolderContentImage(view);
            case ImlHomeRow.TYPE_ANS:
                view = mLayoutInflater.inflate(R.layout.item_feed_asw, parent, false);
                view.setOnClickListener(mOnClickAns);
                return new HomeViewHolder.FeedViewHolderAns(view);
            case ImlHomeRow.TYPE_FOOTER:
                view = mLayoutInflater.inflate(R.layout.item_feed_footer, parent, false);
                return new HomeViewHolder.FeedViewHolderFooter(view);
            case ImlHomeRow.TYPE_SUPER_HEADER:
                view = mLayoutInflater.inflate(R.layout.item_super_header, parent, false);
                return new HomeViewHolder.FeedViewHolderSuperHeader(view);
            case ImlHomeRow.TYPE_DIVIDER:
                view = mLayoutInflater.inflate(R.layout.item_big_divider, parent, false);
                return new DividerViewHolder(view);
            case ImlHomeRow.TYPE_VIEW_MY_PROFILE:
                view = mLayoutInflater.inflate(R.layout.item_myprofile, parent, false);
                return new HomeViewHolder.ProfileUser(view);
            case ImlHomeRow.TYPE_VIEW_MORE_INFO:
                view = mLayoutInflater.inflate(R.layout.item_view_more_profile, parent, false);
                view.setOnClickListener(onClickViewMore);
                return new HomeViewHolder.ViewMoreViewHolder(view);
            case ImlHomeRow.TYPE_VIEW_INFO_USER:
                view = mLayoutInflater.inflate(R.layout.item_user_info, parent, false);
                return new HomeViewHolder.InfoExtraViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        int viewType = getItemViewType(position);

        switch (viewType) {
            case ImlHomeRow.TYPE_CREATE_FEED:

                break;
            case ImlHomeRow.TYPE_HEADER_FEED:
                ((HomeViewHolder.FeedViewHolderHeader) holder).bindData(mContext, (AbsFeedHeader) mListItemHome.get(position), null);
                holder.itemView.setOnClickListener(mOnClickDetail);
//                ((HomeViewHolder.FeedViewHolderHeader) holder).mAvatar.setTag(((AbsFeedHeader) mListItemHome.get(position)).getPoll());
//                ((HomeViewHolder.FeedViewHolderHeader) holder).mTextViewName.setTag(((AbsFeedHeader) mListItemHome.get(position)).getPoll());

                break;
            case ImlHomeRow.TYPE_CONTENT_FEED:
                ((HomeViewHolder.FeedViewHolderContent) holder).bindData(mContext, (AbsFeedContentText) mListItemHome.get(position));
                holder.itemView.setOnClickListener(mOnClickDetail);
                break;
            case ImlHomeRow.TYPE_CONTENT_FEED_IMAGE:
                ((HomeViewHolder.FeedViewHolderContentImage) holder).bindData(mContext, (AbsFeedContentMedia) mListItemHome.get(position));
                holder.itemView.setOnClickListener(mOnClickDetail);
                break;
            case ImlHomeRow.TYPE_ANS:
                ((HomeViewHolder.FeedViewHolderAns) holder).bindData(mContext, (AbsFeedAns) mListItemHome.get(position));
                ((HomeViewHolder.FeedViewHolderAns) holder).itemView.setTag(position);
                break;
            case ImlHomeRow.TYPE_FOOTER:
                ((HomeViewHolder.FeedViewHolderFooter) holder).bindData(mContext, (AbsFeedFooter) mListItemHome.get(position));
                ((HomeViewHolder.FeedViewHolderFooter) holder).itemView.setOnClickListener(mOnViewVotePeople);
                break;
            case ImlHomeRow.TYPE_SUPER_HEADER:
                ((HomeViewHolder.FeedViewHolderSuperHeader) holder).bindData(mContext, (AbsFeedSuperHeader) mListItemHome.get(position));
                break;
            case ImlHomeRow.TYPE_VIEW_MY_PROFILE:
                ((HomeViewHolder.ProfileUser) holder).bindData(mContext, (AbsProfile) mListItemHome.get(position));
                break;

            case ImlHomeRow.TYPE_VIEW_INFO_USER:
                ((HomeViewHolder.InfoExtraViewHolder) holder).bindData(mContext, (AbsInfoExtra) mListItemHome.get(position));
                break;
            case ImlHomeRow.TYPE_VIEW_MORE_INFO:
                ((HomeViewHolder.ViewMoreViewHolder) holder).bindData(mContext, mViewMore);
                break;

        }

    }

    @Override
    public int getItemCount() {
        return mListItemHome.size();
    }

    public void setListInfoExtra(ArrayList<AbsInfoExtra> listInfoExtra) {
        mListInfoExtra = new ArrayList<>(listInfoExtra);
    }

    public interface OnClickCallback {
        void onClickMenuFeed();
    }
}
