/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.task;

/**
 * @author jacky
 * @since 1/17/17
 */

public interface OnCallBackAsyncTaskListener<T> {
    void onPreExecute();
    void onPostExecute(T t);
}
