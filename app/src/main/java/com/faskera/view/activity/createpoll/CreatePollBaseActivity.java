/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll;

import android.content.Intent;

import com.faskera.common.base.BaseActivity;
import com.faskera.common.base.BaseFragment;
import com.faskera.era.R;
import com.faskera.view.widget.ToolbarCreatePoll;

import java.util.Stack;

import butterknife.BindView;

/**
 * @author jacky
 * @since 1/9/17
 */

public abstract class CreatePollBaseActivity extends BaseActivity implements BaseFragment.OnCallBackData {

    @BindView(R.id.tool_bar)
    ToolbarCreatePoll mToolbarCreatePoll;

    @Override
    protected int getIdLayout() {
        return 0;
    }

    @Override
    protected void initView() {
        mFrgManager = getSupportFragmentManager();
        mFragStack = new Stack<>();
    }

    @Override
    protected void initData() {

    }

    public void gotoDone() {
        if (mFragStack != null && mFragStack.size() > 0) {
            ((BaseFragment) mFragStack.lastElement()).goToDone();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mFragStack != null && mFragStack.size() > 1) {
            ((BaseFragment) mFragStack.lastElement()).onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onCallBack(Object o) {
        if (mFragStack != null && mFragStack.size() >= 1) {
            ((BaseFragment) mFragStack.lastElement()).onRefresh(o);
        }
    }

}
