/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home.network;

import com.faskera.view.activity.home.model.Feed;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author DANGNGOCDUC
 * @since 1/11/2017
 */

public class ListFeedReponse implements Serializable {

    @SerializedName("nextMaxId")
    public String nmNextMaxId;

    @SerializedName("size")
    public int mSize;

    @SerializedName("list")
    public ArrayList<Feed> mListFeed;


}
