/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.mypage;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.faskera.common.AppSharePreference;
import com.faskera.common.BasePollActivity;
import com.faskera.common.Divider;
import com.faskera.common.asyncdata.CursorAdapterOptimize;
import com.faskera.common.asyncdata.FooterType;
import com.faskera.common.feedflow.ActionCreator;
import com.faskera.common.feedflow.Dispatcher;
import com.faskera.common.feedflow.FeedAction;
import com.faskera.common.feedflow.FeedListener;
import com.faskera.common.view.FrameRecycleView;
import com.faskera.era.R;
import com.faskera.model.user.Profile;
import com.faskera.view.activity.home.model.Feed;
import com.faskera.view.activity.home.model.FeedCreateNew;
import com.faskera.view.activity.home.model.ImlHomeRow;
import com.faskera.view.activity.home.model.abst.AbsFeedAns;
import com.faskera.view.activity.home.model.abst.FeedAbs;
import com.faskera.view.activity.mypage.abs.AbsProfile;
import com.faskera.view.activity.mypage.model.ViewMoreInfo;
import com.faskera.view.activity.option.OptionActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

import static com.faskera.Constant.REQUEST_EDIT_PROFILE;


/**
 * Created by DANGNGOCDUC on 1/4/2017.
 *
 * @author DANGNGOCDUC
 *         Activity hien thi tran ca nhan cua user . O day co 2 phan can hien thi la {@link Profile}
 *         va danh sach cac Poll cua user da tao {@link Feed.Poll}
 */

public class ProfileActivity extends BasePollActivity implements ProfileContract.View, FeedListener {

    public static String BUNDLE_GO_THROUGH_NOTIFICATION = "bundle_go_throuh_notify";

    private ProfileContract.Presenter mPresenter;

    private SwipeRefreshLayout.OnRefreshListener mOnRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            mAdapterProfile.setTypeLoadMore(FooterType.TYPE_LOADMORE_NONE);
            mRecycleView.setRefreshing(true);
            mPresenter.getClass();
            mPresenter.getUserInfo();
        }
    };

    @Override
    protected void onChangeUserInfo() {
        super.onChangeUserInfo();
        mPresenter.onUpdateInfo();
    }

    @BindView(R.id.framerecycleview)
    public FrameRecycleView mRecycleView;

    @OnClick(R.id.image_back)
    public void onClickBack() {
        finish();
    }

    @BindView(R.id.toolbar_title)
    public TextView mTextViewTitle;

    private CursorAdapterOptimize mAdapterProfile;

    @BindView(R.id.btn_discover)
    public ImageView mBtnDiscover;

    @BindView(R.id.btn_menu)
    public ImageView mBtnSetting;

    @OnClick(R.id.btn_menu)
    public void onClickMenu() {
        // startActivity(new Intent(this, OptionActivity.class));
        startActivityForResult(new Intent(this, OptionActivity.class), REQUEST_EDIT_PROFILE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected int getIdLayout() {
        return R.layout.activity_mypage;
    }

    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Dispatcher.register(this);
    }

    @Override
    protected void initView() {
        mTextViewTitle.setText("");
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecycleView.getRecycleView().setLayoutManager(linearLayoutManager);
        mAdapterProfile = new CursorAdapterOptimize(this);
        mAdapterProfile.setCallbackImageFeed(this);
        mRecycleView.getRecycleView().setAdapter(mAdapterProfile);
        new ProfilePresenter(this, this, getIntent().getExtras());
        mBtnDiscover.setVisibility(View.GONE);
        mRecycleView.setOnSwipeRefreshLayout(mOnRefreshListener);
        AbsProfile profile = mPresenter.getAbsProfile();
        if (profile != null) {
            mRecycleView.setRefreshing(false);
            mAdapterProfile.mViewMore = false;
            mRecycleView.showRecycleView();
            mProfile = profile;
            mAdapterProfile.setProfile(profile);
            mAdapterProfile.clearData();
            mTextViewTitle.setText(profile.getName());
            ArrayList<ImlHomeRow> list = new ArrayList<>();
            list.add(mProfile);
            list.add(new ViewMoreInfo());
            if (mPresenter.getUid().equals(AppSharePreference.getUid(this))) {
                list.add(new Divider());
                list.add(new FeedCreateNew());
            }
            list.add(new Divider());
            mAdapterProfile.insertListPoll(list);

            ArrayList<Feed.Poll> listPoll = mPresenter.getPollOfUserDB();
            ArrayList<ImlHomeRow> listRow = new ArrayList<>();
            for (Feed.Poll poll : listPoll) {
                listRow.addAll(poll.genListItem());

            }
            mAdapterProfile.insertListPoll(listRow);
            mPresenter.getUserInfo();
        } else {
            mPresenter.getUserInfo();
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Dispatcher.unRegister(this);
    }

    /**
     * @param isFriends {true}
     */
    @Override
    public void loadViewForUserFriends(boolean isFriends) {
        if (isFriends) {
            mBtnSetting.setVisibility(View.GONE);
        } else {
            mBtnSetting.setVisibility(View.VISIBLE);
        }
    }

    /**
     * <p>
     * Tra ve khi dang load thong tin user
     * </p>
     */
    @Override
    public void onLoadInfo() {
        if (!mRecycleView.isRefreshing() && mAdapterProfile.getItemCount() == 0) {
            mRecycleView.showLoading();
        }
    }

    /**
     * @param profile thong tin cua user tra ve trong trương hop thannh cong
     */
    @Override
    public void onLoadInfoSuccess(AbsProfile profile, boolean isOnline) {
        mRecycleView.setRefreshing(false);
        mAdapterProfile.mViewMore = false;
        mRecycleView.showRecycleView();
        mProfile = profile;
        mAdapterProfile.setProfile(profile);
        mAdapterProfile.clearData();
        mTextViewTitle.setText(profile.getName());
        ArrayList<ImlHomeRow> list = new ArrayList<>();
        list.add(mProfile);
        list.add(new ViewMoreInfo());
        if (mPresenter.getUid().equals(AppSharePreference.getUid(this))) {
            list.add(new Divider());
            list.add(new FeedCreateNew());
        }
        list.add(new Divider());
        mAdapterProfile.insertListPoll(list);
        mPresenter.getPollOfUser();
    }

    /**
     * @param e handle nhung loi khong the xu ly dc do chua ro nguyen nhan khi load thong tin user
     */
    @Override
    public void onLoadInfoError(int e) {

        mRecycleView.showContent(R.drawable.ic_cloud_off, R.string.error_connection);

    }

    /**
     * Khi dang load polls cua user sẽ tra ve
     */
    @Override
    public void onLoadPoll() {
        mAdapterProfile.setTypeLoadMore(FooterType.TYPE_LOADMORE);
    }

    /**
     * @param e handle nhung loi khong the xu ly dc do chua ro nguyen nhan khi load polls cua user
     */
    @Override
    public void onLoadPollError(int e) {
        mAdapterProfile.setTypeLoadMore(FooterType.TYPE_LOADMORE_ERROR);
    }

    /**
     * @param list danh sach poll cua user
     */
    @Override
    public void onLoadPollSuccess(ArrayList<Feed.Poll> list, boolean isOnline) {
        if (isOnline) {
            mRecycleView.getRecycleView().addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                }

                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        int totalItemCount = linearLayoutManager.getItemCount();
                        int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();


                        if (mAdapterProfile.getTypeLoadMore() == FooterType.TYPE_LOADMORE_NONE && totalItemCount <= (lastVisibleItem + 3)) {
                            mPresenter.getPollOfUser();
                        }

                    }
                }
            });
        }
        mAdapterProfile.setTypeLoadMore(FooterType.TYPE_LOADMORE_NONE);
        ArrayList<ImlHomeRow> listRow = new ArrayList<>();
        for (Feed.Poll poll : list) {
            listRow.addAll(poll.genListItem());

        }
        mAdapterProfile.insertListPoll(listRow);
    }

    @Override
    public void onUpdateInfoSuccess(AbsProfile profile) {
        mProfile = profile;
        mAdapterProfile.updateUserInfo(profile);
        mTextViewTitle.setText(mProfile.getName());
    }

    @Override
    public void setPresenter(ProfileContract.Presenter presenter) {
        mPresenter = presenter;


    }

    @Override
    public void onCreatePollSuccess(Feed.Poll poll) {
        mAdapterProfile.insertListPoll(poll.genListItem(), mAdapterProfile.findOffsetInsertNewFeedProfile());
    }

    @Override
    public void onFollowClick() {
        super.onFollowClick();
        mAdapterProfile.notifyItemChanged(0);
    }

    @Override
    public void onAgreeUnfollow() {
        super.onAgreeUnfollow();
        mAdapterProfile.notifyItemChanged(0);
    }

    @Override
    public void onFollowingClick() {
        super.onFollowingClick();
        mAdapterProfile.notifyItemChanged(0);

    }

    @Override
    public void onClickAsw(AbsFeedAns absFeedAns) {
        super.onClickAsw(absFeedAns);
        if (AppSharePreference.getInstance(this).isUserLoggedIn()) {

            if (mAdapterProfile.getIndex(absFeedAns) > 0) {
                if (absFeedAns.getNbSeq() == 0) {
                    mAdapterProfile.notifyItemRangeChanged(mAdapterProfile.getIndex(absFeedAns), absFeedAns.getPoll().getFeedAns().size() + 2);
                } else {
                    mAdapterProfile.notifyItemRangeChanged(mAdapterProfile.getIndex(absFeedAns) - absFeedAns.getNbSeq(), absFeedAns.getPoll().getFeedAns().size() + 2);
                }
            } else {
                mAdapterProfile.notifyDataSetChanged();
            }
            Dispatcher.sendEvent(this, ActionCreator.createActionFeed(ActionCreator.CHANGE, absFeedAns.getPoll()));
        }

    }

    @Override
    protected void onDelPollResult(String pollID, boolean result) {
        super.onDelPollResult(pollID, result);
        if (result) {
            mAdapterProfile.removePoll(pollID);
        }
    }

    @Override
    public void onHeaderClick(String id) {

    }

    @Override
    public void onSateChange(FeedAction action) {
        switch (action.getType()) {
            case ActionCreator.ADD:
                if (action.getData() != null && action.getData().getPoll() != null) {
                    onCreatePollSuccess(action.getData().getPoll());
                }
                break;
            case ActionCreator.REMOVE:
                break;
            case ActionCreator.CHANGE:
                AbsFeedAns item = mAdapterProfile.findFeedAns(action.getData().getPoll().getFeedAns().get(0).getAnsID());
               if (item != null) {
                   FeedAbs abs = item.getPoll();
                   abs.getPoll().cloneSateObject(action.getData());
                   mAdapterProfile.notifyDataSetChanged();
                   break;
               }

        }
    }

    @Override
    protected void onAccessTokenChange() {
        super.onAccessTokenChange();
        mPresenter.getUserInfo();
    }

    //</editor-fold>
}
