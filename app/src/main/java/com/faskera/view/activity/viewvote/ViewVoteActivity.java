/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.viewvote;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.faskera.common.AppSharePreference;
import com.faskera.common.UserDefine;
import com.faskera.common.adapter.AbsUserState;
import com.faskera.common.adapter.ListUserAdapter;
import com.faskera.common.base.BaseActivity;
import com.faskera.common.debug.DebugUtil;
import com.faskera.common.view.FrameRecycleView;
import com.faskera.era.R;
import com.faskera.utils.ImageLoader;
import com.faskera.utils.IntentUtils;
import com.faskera.view.activity.mypage.ProfileActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author DANGNGOCDUC
 * @since 1/3/2017
 */

public class ViewVoteActivity extends BaseActivity implements ViewVoteContract.View, ListUserAdapter.CallbackFollow {

    public static final String BUNDLE_POLL_ID = "poll_id";

    protected ViewVoteContract.Presenter mPresenter;

    private int mPossitionUnfollow;

    @BindView(R.id.framerecycleview)
    public FrameRecycleView mRecycleView;

    @OnClick(R.id.image_back)
    public void onClickBack() {
        finish();
    }

    @BindView(R.id.toolbar_text_done)
    public TextView mTextDone;

    @BindView(R.id.toolbar_title)
    public TextView mTextViewTitle;

    private ListUserAdapter mAdapter;

    @Inject
    ArrayList<AbsUserState> mVoterList;

    @Override
    protected int getIdLayout() {
        return R.layout.activity_viewvote;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        mTextViewTitle.setText(getResources().getString(R.string.title_people_vote));
        mTextDone.setVisibility(View.GONE);
        new ViewVotePresenter(this, this);
        mRecycleView.showLoading();
        LinearLayoutManager mLinearLayoutManager =  new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecycleView.getRecycleView().setLayoutManager(mLinearLayoutManager);
        mAdapter = new ListUserAdapter(this);
        mAdapter.setCallbackClick(this);
        mRecycleView.getRecycleView().setAdapter(mAdapter);
        mRecycleView.getRecycleView().addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int totalItemCount = mLinearLayoutManager.getItemCount();
                int lastVisibleItem = mLinearLayoutManager.findLastVisibleItemPosition();
                if (!mAdapter.isLoadMore() && totalItemCount <= (lastVisibleItem + 3)) {
                    mPresenter.getListVote(getIntent().getExtras().getString(BUNDLE_POLL_ID));
                }
            }
        });
        mPresenter.getListVote(getIntent().getExtras().getString(BUNDLE_POLL_ID));
    }

    @Override
    public void onLoadMore() {
        if (mAdapter.isLoadMore()) {
            mAdapter.setLoadMore(true);
        } else {
            DebugUtil.d("===========================ViewVoteActivity-onLoadMore", "Calling action Loadmore while View is loadmore.");
        }
    }

    @Override
    public void onLoading() {
        mRecycleView.showLoading();
    }

    @Override
    public void onSuccess(ArrayList<AbsUserState> Collection) {
        if (mAdapter.getItemCount() == 0) {
            mAdapter.setListVoter(Collection);
            mRecycleView.showRecycleView();
        } else {
            if (mAdapter.isLoadMore()) mAdapter.setLoadMore(false);
            mAdapter.addListVoter(Collection);
        }
        if (Collection.size() == 0) {
            mRecycleView.getRecycleView().clearOnScrollListeners();
        }
    }

    @Override
    public void onError(int code) {
        mRecycleView.showContent(R.drawable.ic_cloud_off, R.string.error_connection);
    }

    @Override
    public void setPresenter(ViewVoteContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onClickState(int pos) {
        if (mAdapter.getUserAtPosition(pos).getSate() == UserDefine.STATE_FOLLOW) {
            onFollowUser(mAdapter.getUserAtPosition(pos).getUid());
            mAdapter.getUserAtPosition(pos).setSate( UserDefine.STATE_FOLLWING);
            mAdapter.notifyItemChanged(pos);
        } else if (mAdapter.getUserAtPosition(pos).getSate() == UserDefine.STATE_FOLLWING) {
            mUserUnfollow = mAdapter.getUserAtPosition(pos);
            mDialogConfirmUnfollow.show();
            ImageLoader.loadImageViewWith(this, mUserUnfollow.getAvatar(), mAvatarUnfollow);
            mTextViewUserNameUnfollow.setText(String.format(getResources().getString(R.string.unfollow_user), mUserUnfollow.getName()));
            mPossitionUnfollow = pos;
        }
    }

    @Override
    public void onClickUser(String id) {
        Bundle bundle = new Bundle();
        if (id.equals(AppSharePreference.getUid(this))) {
            bundle.putInt(ProfileActivity.BUNDLE_TYPE_USER, UserDefine.USER_TYPE_PROFILE);
        } else {
            bundle.putInt(ProfileActivity.BUNDLE_TYPE_USER, UserDefine.USER_TYPE_FRIENDS);
            bundle.putString(ProfileActivity.BUNDLE_USER_ID, id  );
        }
        IntentUtils.startActivityMyPage(bundle, this);
    }

    @Override
    public void onAgreeUnfollow() {
        super.onAgreeUnfollow();
        mAdapter.getUserAtPosition(mPossitionUnfollow).setSate( UserDefine.STATE_FOLLOW);
        mAdapter.notifyItemChanged(mPossitionUnfollow);
    }
}
