/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.fbfriend;

import com.faskera.mvp.BasePresenter;
import com.faskera.mvp.BaseView;
import com.faskera.view.activity.option.view.fbfriend.model.Friend;

import java.util.ArrayList;
import java.util.List;

/**
 * @author theanh
 * @since 1/28/2017
 */
public interface FbFriendContract {
    interface Presenter extends BasePresenter {
        void getFbFriends();
        void followFriend(String userId);
        void unfollowFriend(String userId);
        void followAllFriend(ArrayList<String> listUserId);
        void subscribe();
        void unsubscribe();
    }

    interface View extends BaseView<FbFriendContract.Presenter> {
        void onGetFriendsSuccess(int totalFbFriends, List<Friend> listFriends);
        void onGetFriendsEmpty();
        void onFollowSuccess();
        void onFollowAllSuccess();
        void onUnfollowSuccess();
        void showLoading();
        void hideLoading();
        void onErrorConnection(int code);
        void onFollowFailed();
    }
}
