/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.topic;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.faskera.common.view.AnyTextview;
import com.faskera.era.R;
import com.faskera.view.activity.topic.model.Topic;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author theanh
 * @since 1/27/2017
 */

public class TopicAdapter extends RecyclerView.Adapter<TopicAdapter.TopicViewHolder> {

    private ArrayList<Topic> mTopics;
    private Context mContext;

    public void setTopicList(ArrayList<Topic> categories) {
        mTopics.addAll(categories);
        notifyItemRangeInserted(0, categories.size());
    }

    public ArrayList<String> getSelectedTopics() {
        ArrayList<String> selectedTopics = new ArrayList<>();
        for (Topic topic : mTopics) {
            if (topic.isSelected())
                selectedTopics.add(topic.getId());
        }
        return selectedTopics;
    }

    public ArrayList<Topic> getTopicList() {
        return mTopics;
    }

    public TopicAdapter() {
        mTopics = new ArrayList<>();
    }

    @Override
    public TopicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        return new TopicViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row_topic, parent, false));
    }

    @Override
    public void onBindViewHolder(TopicViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mTopics.size();
    }

    public class TopicViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_title)
        AnyTextview mTextTitle;
        @BindView(R.id.image_choose)
        ImageView mImageChoose;

        private Topic mTopic;

        public TopicViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(int position) {
            mTopic = mTopics.get(position);

            mTextTitle.setText(mTopic.getName());
            mImageChoose.setSelected(mTopic.isSelected());

            itemView.setOnClickListener(view -> {
                boolean isTopicSelected = mTopic.isSelected();
                if (isTopicSelected) {
                    mImageChoose.setSelected(false);
                    mTopic.setSelected(false);
                } else {
                    mImageChoose.setSelected(true);
                    mTopic.setSelected(true);
                }
                notifyItemChanged(position);
            });
        }
    }
}
