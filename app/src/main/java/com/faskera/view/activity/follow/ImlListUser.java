/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.follow;

import com.faskera.common.ImlBaseRow;

/**
 * @author DANGNGOCDUC
 * @since 3/14/2017
 */

public interface ImlListUser extends ImlBaseRow {
    int TYPE_USER = 3;
}
