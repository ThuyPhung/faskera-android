/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.viewoption;

import android.content.Context;

import com.faskera.common.adapter.ListPeople;
import com.faskera.common.base.ObserverAdvance;
import com.faskera.network.ApiManager;

import java.util.ArrayList;

/**
 * @author DANGNGOCDUC
 * @since 3/1/2017
 */

public class ViewOptionPresenter implements ViewOptionContract.Presenter {

    private ViewOptionContract.View mView;
    private Context mContext;
    private String mNextMaxID  = "";
    private boolean isStopLoadMore = false;
    private boolean isLoadmore = false;

    public ViewOptionPresenter(Context context, ViewOptionContract.View view) {
        mView = view;
        mContext = context;
        mView.setPresenter(this);
    }

    @Override
    public void getListVote(String mOptionID) {
        if (isStopLoadMore || isLoadmore) return;

        if (mNextMaxID == null || mNextMaxID.trim().isEmpty()) {
            mView.onLoading();
        } else {
            mView.onLoadMore();
        }

        isLoadmore = true;

        ApiManager.getInstance().getUserVotedOption(mContext, mOptionID, mNextMaxID, new ObserverAdvance<ListPeople>() {
            @Override
            public void onError(int code) {
                mView.onError(code);
                isLoadmore =false;
            }
            @Override
            public void onSuccess(ListPeople data) {
                    mNextMaxID = data.mNextMaxId;
                    mView.onSuccess(new ArrayList<>(data.mlistUser));
                    if (mNextMaxID == null || mNextMaxID.trim().isEmpty()) {
                        isStopLoadMore = true;
                    }
                isLoadmore =false;
            }

            @Override
            public void onCompleted() {}

        });

    }
}
