/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.faskera.common.base.BaseFragment;
import com.faskera.era.R;
import com.faskera.model.createpoll.ImageModel;
import com.faskera.model.createpoll.TbCreatePoll;
import com.faskera.permission.AppSettingsDialog;
import com.faskera.permission.PermissionsUtils;
import com.faskera.utils.AppDialog;
import com.faskera.utils.AppUtils;
import com.faskera.utils.CameraUtils;
import com.faskera.utils.DisplayUtility;
import com.faskera.utils.FunctionConfig;
import com.faskera.utils.MediaUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static android.app.Activity.RESULT_OK;

public class GalleryFragment extends BaseFragment implements ImageGalleryAdapter.OnImageClickListener
        , ImageGalleryAdapter.ImageThumbnailLoader, ImageGalleryAdapter.PickerAdapterCallback
        , LoaderManager.LoaderCallbacks<Cursor>, PermissionsUtils.PermissionCallbacks {

    private static final String LOADER_EXTRA_URI = "loader_extra_uri";
    private static final String LOADER_EXTRA_PROJECT = "loader_extra_project";
    private String mCamImageName = null;
    private boolean isCameraFormCreatePoll;

    @BindView(R.id.recycleview)
    RecyclerView mRecyclerView;
    private ImageGalleryAdapter mImageGalleryAdapter;

    @Override
    protected int idRoot() {
        return R.layout.fragment_gallery;
    }

    @Override
    protected void initView(View root) {
        mLoading = (ProgressBar) root.findViewById(R.id.loading);
    }

    @Override
    protected void initData() {
        if (getArguments() != null && getArguments().containsKey(CreatePollFragment.class.getName())) {
            isCameraFormCreatePoll = getArguments().getBoolean(CreatePollFragment.class.getName());
        }
        mLoading.setVisibility(View.GONE);
        if (PermissionsUtils.hasPermissions(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            requestPhotos(false);
        } else {
            PermissionsUtils.requestPermissions(this, getString(R.string.msg_read_external_storage),
                    FunctionConfig.REQUEST_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void onRefresh(Object o) {
    }

    @Override
    public void onBack() {
        super.onBack();
        requestPhotos(false);
    }

    private void requestPhotos(boolean reload) {
        Bundle bundle = new Bundle();
        bundle.putStringArray(LOADER_EXTRA_PROJECT, MediaUtils.PROJECT_PHOTO);
        bundle.putString(LOADER_EXTRA_URI, MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString());
        if (!reload) {
            getLoaderManager().initLoader(0, bundle, this);
        } else {
            getLoaderManager().restartLoader(0, bundle, this);
        }
    }

    private void setUpRecyclerView(List<ImageModel> images) {
        int numOfColumns;
        if (DisplayUtility.isInLandscapeMode(getActivity())) {
            numOfColumns = 4;
        } else {
            numOfColumns = 3;
        }
        //set camera
        ImageModel mImageModel = new ImageModel(-1, null, null);
        images.add(0, mImageModel);

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), numOfColumns);
        mRecyclerView.setLayoutManager(layoutManager);
        mImageGalleryAdapter = new ImageGalleryAdapter(getContext(), images);
        mImageGalleryAdapter.setOnImageClickListener(this);
        mImageGalleryAdapter.setImageThumbnailLoader(this);
        mImageGalleryAdapter.setPickerAdapterCallback(this);
        mLoading.setVisibility(View.GONE);
        mRecyclerView.setAdapter(mImageGalleryAdapter);
    }

    @Override
    public void loadImageThumbnail(ImageView iv, String imageUrl, int dimension) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Glide.with(this).load(imageUrl)
                    .override(200, 200)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(iv);
        } else {
            iv.setBackgroundResource(R.drawable.ic_camera_alt);
        }
    }

    @Override
    public void onImageClick(int position) {
        if (mImageGalleryAdapter != null && position != -1) {
            mImageGalleryAdapter.setCurrentSelected(position);
        }
    }

    @Override
    public void onTakePhoto() {
        if (PermissionsUtils.hasPermissions(getContext(), Manifest.permission.CAMERA)) {
            toOpenCamera();
        } else {
            PermissionsUtils.requestPermissions(this, getString(R.string.msg_read_external_storage)
                    , FunctionConfig.REQUEST_CAMERA,
                    Manifest.permission.CAMERA);
        }
    }

    @Override
    public void onMaxItemSelected() {
        //show dialog when choose max image
        new AppDialog.Builder(this, getString(R.string.msg_content_select_image))
                .setTitle(getString(R.string.msg_title_select_image))
                .setPositiveButton(getString(R.string.msg_ok))
                .setOnClickButtonDialogApp(new AppDialog.OnClickButtonDialogApp() {
                    @Override
                    public void onOk() {

                    }

                    @Override
                    public void onCancel() {

                    }
                })
                .build()
                .show();
    }

    @Override
    public void onResume() {
        super.onResume();
        getBaseActivity().update(new TbCreatePoll(getString(R.string.msg_title_gallery), getString(R.string.msg_done)));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isDestroyView = true;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        if (mLoading != null) {
            mLoading.setVisibility(View.VISIBLE);
        }
        Uri uri = Uri.parse(bundle.getString(LOADER_EXTRA_URI));
        String[] projects = bundle.getStringArray(LOADER_EXTRA_PROJECT);
        String order = MediaStore.MediaColumns.DATE_ADDED + " DESC";
        return new CursorLoader(getActivity(), uri, projects, null, null, order);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor != null && cursor.getCount() > 0) {
            List<ImageModel> items = new ArrayList<>();
            long backupSize = -1L;
            Uri backupUri = null;

            int index = 0;
            while (cursor.moveToNext()) {
                Uri uri = MediaUtils.getPhotoUri(cursor);
                long size = MediaUtils.getSize(cursor);


                if (uri != null && size > 0) {
                    if (size == backupSize && index <= MediaUtils.FILE_CHECKING_RANGE) {
                        String path = MediaUtils.getPathFromUri(getActivity(), uri);

                        if (path != null) {
                            File duplicateFile = new File(path);

                            if (duplicateFile.exists() && duplicateFile.delete()) {
                                MediaUtils.delete(getActivity(), uri);
                            } else if (duplicateFile.exists()) {
                                // Log.i("Do not delete file: " + duplicateFile.getAbsolutePath());

                                String otherPath = MediaUtils.getPathFromUri(getActivity(), backupUri);
                                if (otherPath != null && new File(otherPath).delete()) {
                                    items.remove(items.size());
                                    items.add(new ImageModel(MediaUtils.getId(cursor), uri.toString(), null));

                                    MediaUtils.delete(getActivity(), backupUri);

                                    backupSize = size;
                                    backupUri = uri;
                                }
                            }
                        }

                    } else {
                        items.add(new ImageModel(MediaUtils.getId(cursor), uri.toString(), null));

                        backupSize = size;
                        backupUri = uri;
                    }
                }

                index++;
            }
            setUpRecyclerView(items);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void goToDone() {
        super.goToDone();
        actionDone();
    }

    private void actionDone() {
        if (mImageGalleryAdapter != null && mImageGalleryAdapter.nextEnable()) {
            if (mImageGalleryAdapter.getPaths().size() > 0) {
                Bundle mBundle = new Bundle();
                mBundle.putStringArrayList(AppUtils.SELECTED_PATHS, mImageGalleryAdapter.getPaths());
                if (!isCameraFormCreatePoll) {
                    getBaseActivity().openActivity(CreatePollActivity.class, mBundle);
                } else {
                    Intent intent = new Intent();
                    intent.putExtras(mBundle);
                    getActivity().setResult(RESULT_OK, intent);
                }
                gotoFinish();
            }
        } else {
            gotoFinish();
        }
    }

    private void gotoFinish() {
        getActivity().finish();
        Runtime.getRuntime().gc();
        System.gc();
    }

    private void showResult(ArrayList<String> paths) {
        if (mOnCallBackData != null && paths != null && paths.size() > 0) {
            mOnCallBackData.onCallBack(paths);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mOnCallBackData = (OnCallBackData) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnCallBackData");
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        switch (requestCode) {
            case FunctionConfig.REQUEST_STORAGE:
                requestPhotos(false);
                break;
            case FunctionConfig.REQUEST_CAMERA:
                onTakePhoto();
                break;
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (PermissionsUtils.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this, getString(R.string.msg_permissions_ask_again))
                    .setTitle(getString(R.string.msg_permissions_title_settings_dialog))
                    .setPositiveButton(getString(R.string.setting))

                    .setNegativeButton(getString(R.string.cancel), null /* click listener */)
                    .setRequestCode(FunctionConfig.REQUEST_STORAGE)
                    .setRequestCode(FunctionConfig.REQUEST_CAMERA)
                    .build()
                    .show();
        }else {
             getActivity().finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionsUtils.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 0x03 && !TextUtils.isEmpty(mCamImageName)) {
            Uri localUri = Uri.fromFile(new File(CameraUtils.getCameraPath() + mCamImageName));
            Intent localIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, localUri);
            getActivity().sendBroadcast(localIntent);
        }
    }

    private void toOpenCamera() {
        String savePath = "";
        if (CameraUtils.hasSDCard()) {
            savePath = CameraUtils.getCameraPath();
            File saveDir = new File(savePath);
            if (!saveDir.exists()) {
                saveDir.mkdirs();
            }
        }

        if (TextUtils.isEmpty(savePath)) {
            Toast.makeText(getActivity(), "Can not save photo, please check whether the SD card is mounted", Toast.LENGTH_LONG).show();
            return;
        }

        mCamImageName = CameraUtils.getSaveImageFullName();
        File out = new File(savePath, mCamImageName);
        Uri uri = Uri.fromFile(out);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent,
                0x03);
    }
}
