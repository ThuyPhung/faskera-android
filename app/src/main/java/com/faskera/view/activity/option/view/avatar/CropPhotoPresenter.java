/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.avatar;

import android.content.Context;

import com.faskera.common.base.ObserverAdvance;
import com.faskera.model.user.UserInfoResponse;
import com.faskera.network.ApiManager;

import rx.subscriptions.CompositeSubscription;

/**
 * @author theanh
 * @since 2/28/2017
 */
public class CropPhotoPresenter implements CropPhotoContract.Presenter {
    private Context mContext;
    private CropPhotoContract.View mView;
    private CompositeSubscription mCompositeSubscription;

    public CropPhotoPresenter(Context context, CropPhotoContract.View view) {
        mContext = context;
        mView = view;
        mView.setPresenter(this);
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getUserInfo() {
        mView.showLoading();
        mCompositeSubscription.add(
            ApiManager.getInstance().getUserInfo(mContext, new ObserverAdvance<UserInfoResponse>() {
                @Override
                public void onCompleted() {
                }

                @Override
                public void onError(int code) {
                    if (mView == null) return;
                    mView.hideLoading();
                    mView.onError(code);
                }

                @Override
                public void onSuccess(UserInfoResponse data) {
                    if (mView == null) return;
                    mView.hideLoading();
                    mView.onGetUserInfoSuccess(data);
                }
            }));
    }

    @Override
    public void unsubscribe() {
        if (mCompositeSubscription != null && mCompositeSubscription.isUnsubscribed()) {
            mCompositeSubscription.unsubscribe();
            mView = null;
        }
    }
}
