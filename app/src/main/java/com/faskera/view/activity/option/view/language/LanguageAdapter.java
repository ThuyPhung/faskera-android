/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.language;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.faskera.common.view.AnyTextview;
import com.faskera.era.R;
import com.faskera.model.option.LanguageModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author theanh
 * @since 1/24/2017
 */
public class LanguageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<LanguageModel> mLanguages;
    private LayoutInflater mInflater;
    private onLanguageClickListener mCallback;

    public interface onLanguageClickListener {
        void onItemClick(LanguageModel languageModel);
    }

    public LanguageAdapter(List<LanguageModel> languages) {
        if (languages != null) mLanguages = languages;
    }

    public void setOnItemClickListener(onLanguageClickListener callback) {
        this.mCallback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mInflater == null) mInflater = LayoutInflater.from(parent.getContext());
        return new LanguageHolder(mInflater.inflate(R.layout.row_language, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((LanguageHolder) holder).bind(mLanguages.get(position));
    }

    @Override
    public int getItemCount() {
        return mLanguages.size();
    }

    public class LanguageHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_language)
        AnyTextview mTextLanguage;
        @BindView(R.id.image_check)
        ImageView mImageCheck;
        @BindView(R.id.row_language)
        LinearLayout mItem;

        public LanguageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(LanguageModel languageModel) {
            if (languageModel == null) return;
            mTextLanguage.setText(languageModel.getName());
            if (languageModel.isUsed()) mImageCheck.setVisibility(View.VISIBLE);
            else mImageCheck.setVisibility(View.INVISIBLE);
            mItem.setOnClickListener(view -> mCallback.onItemClick(languageModel));
        }
    }
}
