/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.topic;

import com.faskera.mvp.BasePresenter;
import com.faskera.mvp.BaseView;
import com.faskera.view.activity.topic.model.Topic;

import java.util.ArrayList;

/**
 * @author theanh
 * @since 2/4/2017
 */
public interface EditTopicContract {
    interface Presenter extends BasePresenter {
        void getTopics();
        void updateInterestedTopics(ArrayList<String> listId);
        void subscribe();
        void unsubscribe();
    }

    interface View extends BaseView<EditTopicContract.Presenter> {
        void onGetTopicsSuccess(ArrayList<Topic> listCate);
        void onUpdateTopicSuccess();
        void showLoading();
        void hideLoading();
        void onErrorConnection(int code);
        void onErrorUpdate();
    }
}
