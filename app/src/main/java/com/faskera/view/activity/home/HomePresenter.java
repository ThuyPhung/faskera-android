/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;

import com.faskera.common.AppSharePreference;
import com.faskera.common.HandleErrorCodeAPIService;
import com.faskera.common.debug.DebugUtil;
import com.faskera.database.FaskeraProvider;
import com.faskera.database.table.FeedTable;
import com.faskera.network.ApiManager;
import com.faskera.view.activity.home.model.Feed;
import com.faskera.view.activity.home.network.ListFeedReponse;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.adapter.rxjava.Result;
import rx.Observer;
import rx.Subscription;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public class HomePresenter implements HomeContract.Presenter  {
    private HomeContract.View mView;
    private Context mContext;
    public Subscription mSubscription;
    public String mNextMaxId = "";
    private boolean mIsLoadingFeed = false; // true : loading data.
    private boolean mAllowLoadMore = true; // check exist feed to Load.

    public HomePresenter(Context context, HomeContract.View view) {
        mView = view;
        mContext = context;
        mView.setPresenter(this);
    }

    @Override
    public ArrayList<Feed> getFeedFromDB() {
        ContentResolver contentResolver = mContext.getContentResolver();
        Cursor conCursor = contentResolver.query(FaskeraProvider.CONTENT_URI_FEED, null, null, null, FeedTable.TIME_MODIFIED + " ASC");
        ArrayList<Feed> list = new ArrayList<Feed>();
        if ( conCursor!= null && conCursor.moveToFirst()) {
            do {
                list.add(Feed.parseFeed(conCursor));
            } while (conCursor.moveToNext());
        }
        return list;
    }

    @Override
    public void getNewFeed(boolean isRefresh) {
        if (isRefresh) {
            mAllowLoadMore = true;
            resetNextMaxId();
            // if app is requesting , unsubcribe this response and  call new request
            if (mSubscription != null && !mSubscription.isUnsubscribed()) {
                mSubscription.unsubscribe();
                mIsLoadingFeed = false;
            }
        } else  if (mIsLoadingFeed || !mAllowLoadMore) {
                return; // if app is loading. Return .
        }

        if (mNextMaxId.isEmpty()) {
            mView.onLoading();
        } else {
            mView.onLoadMore();
        }

        if (AppSharePreference.getToken(mContext).trim().isEmpty()) {
            mSubscription = ApiManager.getInstance().getHomeFeedsAnonymous(mContext, mNextMaxId, new Observer<Result<ListFeedReponse>>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    mView.onLoadError(e);
                    mIsLoadingFeed = false;
                }

                @Override
                public void onNext(Result<ListFeedReponse> listFeedResponseResult) {
                    if (listFeedResponseResult.isError()) {
                        mIsLoadingFeed = false;
                        mView.onLoadError(HandleErrorCodeAPIService.ERROR_NETWORK);
                    } else {
                        if (listFeedResponseResult.response().body() != null) {
                            mView.onGetFeedSuccess(listFeedResponseResult.response().body().mListFeed, mNextMaxId);
                            if (listFeedResponseResult.response().body().nmNextMaxId == null
                                    || listFeedResponseResult.response().body().nmNextMaxId.trim().isEmpty()) {
                                mNextMaxId = null;
                            } else {
                                mNextMaxId = listFeedResponseResult.response().body().nmNextMaxId;
                            }
                            if (mNextMaxId == null || mNextMaxId.isEmpty()) {
                                mAllowLoadMore = false;
                            }
                            mIsLoadingFeed = false;
                        } else if (listFeedResponseResult.response().errorBody() != null) {
                            try {
                                int code = HandleErrorCodeAPIService.parseErrorFromErrorBody(listFeedResponseResult.response().errorBody().string());
                                mView.onLoadError(code);
                            } catch (IOException e) {
                                mView.onLoadError(e);
                            } catch (UnknownError e) {
                                mView.onLoadError(e);
                            }
                        } else {
                            mView.onLoadError(new UnknownError("Unknown Exception"));
                        }
                    }

                }
            });
        } else {
           {

                mSubscription = ApiManager.getInstance().getHomeFeeds(mContext, mNextMaxId, new Observer<Result<ListFeedReponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {

                        mView.onLoadError(e);
                        DebugUtil.d("===========================HomePresenter-onError", e.toString());
                    }

                    @Override
                    public void onNext(Result<ListFeedReponse> listFeedResponseResult) {
                        if (listFeedResponseResult.isError()) {
                            mView.onLoadError(HandleErrorCodeAPIService.ERROR_NETWORK);
                            mView.onLoadError(listFeedResponseResult.error());

                        } else {
                            if (mView != null && listFeedResponseResult.response().body() != null) {
                                mView.onGetFeedSuccess(listFeedResponseResult.response().body().mListFeed, mNextMaxId);
                                mNextMaxId = listFeedResponseResult.response().body().nmNextMaxId;
                                if (mNextMaxId == null || mNextMaxId.isEmpty()) {
                                    mAllowLoadMore = false;
                                }
                            } else if (listFeedResponseResult.response().errorBody() != null) {
                                try {
                                    int code = HandleErrorCodeAPIService.parseErrorFromErrorBody(listFeedResponseResult.response().errorBody().string());
                                    mView.onLoadError(code);
                                } catch (IOException e) {
                                    mView.onLoadError(e);
                                } catch (UnknownError e) {
                                    mView.onLoadError(e);
                                }
                            } else {
                                mView.onLoadError(new UnknownError("Unknown Exception"));
                            }
                        }

                    }
                });
            }
        }
    }

    @Override
    public String getNextMaxId() {
        return mNextMaxId;
    }

    @Override
    public void resetNextMaxId() {
        mNextMaxId = "";
    }

    @Override
    public void onDestroy() {
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }





}
