/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.notification;

import com.faskera.mvp.BasePresenter;
import com.faskera.mvp.BaseView;
import com.faskera.view.activity.notification.model.Notification;

/**
 * @author framgia
 * @since 14/02/2017
 */
public interface NotificationContract {
    interface Presenter extends BasePresenter {
        void getNotifications();
        void goToNotificationDetail();
        void refresh();
        void subscribe();
        void unsubscibe();
    }

    interface View extends BaseView<Presenter> {
        void showLoading();
        void hideLoading();
        void onError(int code);
        void onGetNotificationsSuccess(Notification notifications);
        void onGetNotificationEmpty();
        void onGetMoreNotification(Notification data);
        void onGetAllNotification();
    }
}
