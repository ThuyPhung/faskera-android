/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.faskera.common.AppSharePreference;
import com.faskera.common.base.BaseActivity;
import com.faskera.common.view.AnyTextview;
import com.faskera.era.R;
import com.faskera.fcm.RegistrationIntentService;
import com.faskera.model.user.UserInfoResponse;
import com.faskera.model.user.token.OAuthToken;
import com.faskera.utils.ActivityUtils;
import com.faskera.utils.IntentUtils;
import com.faskera.view.activity.topic.TopicActivity;

import java.util.Collections;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginContract.View {
    private static final String LOG_TAG = LoginActivity.class.getSimpleName();
    @BindView(R.id.txt_message)
    AnyTextview mTextMessage;
    private CallbackManager mFacebookCallbackManager;
    private LoginContract.Presenter mPresenter;
    private ProgressDialog mProgressDialog;

    @Override
    protected int getIdLayout() {
        return R.layout.activity_login;
    }

    @Override
    protected void initView() {
        mPresenter = new LoginPresenter(this, this);
        mFacebookCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance()
            .registerCallback(mFacebookCallbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    String accessToken = loginResult.getAccessToken().getToken();
                    Log.d(LOG_TAG, "Token Facebook : " + accessToken);
                    mPresenter.loginWithFacebook(accessToken);
                }

                @Override
                public void onCancel() {
                    Log.d(LOG_TAG, "Cancel facebook login");
                }

                @Override
                public void onError(FacebookException error) {
                    ActivityUtils.showToast(LoginActivity.this,
                        getString(R.string.msg_login_facebook_failed));
                    Log.d(LOG_TAG, "Login facebook failed");
                }
            });
        mProgressDialog = new ProgressDialog(this, R.style.MyTheme);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        mTextMessage.setMovementMethod(LinkMovementMethod.getInstance());
        mTextMessage.setHighlightColor(Color.TRANSPARENT);
    }

    @Override
    protected void initData() {
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    /**
     * VIEW
     */
    @Override
    public void setPresenter(LoginContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showLoading() {
        if (mProgressDialog != null && !mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    @Override
    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    @Override
    public void onAuthTokenSuccess(OAuthToken oAuthToken) {
        AppSharePreference.getInstance(this).storeOAuthToken(oAuthToken);
        mPresenter.getUserInfo();
    }

    @Override
    public void onGetUserInfoSuccess(UserInfoResponse userInfo) {
        // AppSharePreference.getInstance(this).
        AppSharePreference.getInstance(this).storeUserInfo(userInfo);
        // Send instanceID notification to server
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
        IntentUtils.startActivityHome(null, this,
            Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    }

    @Override
    public void onUpdateInterestedTopic() {
        startActivity(new Intent(this, TopicActivity.class));
    }

    @Override
    public void onErrorConnection(int code) {
        handleErrorCode(code);
        ActivityUtils.showToast(this, getString(R.string.error_connection_not_found));
    }

    @Override
    public void onErrorLoginFacebook() {
        ActivityUtils.showToast(this, getString(R.string.msg_login_facebook_failed));
    }

    /**
     * OnClick
     */
    @OnClick({R.id.txt_login, R.id.text_cancel})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_login:
                if (AccessToken.getCurrentAccessToken() != null) {
                    AccessToken tk = AccessToken.getCurrentAccessToken();
                    mPresenter.loginWithFacebook(tk.getToken());
                } else {
                    LoginManager.getInstance().logInWithReadPermissions(this,
                        Collections.singletonList("public_profile"));
                }
                break;
            case R.id.text_cancel:
                finish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(LOG_TAG, "Facebook Sign In Result");
        mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
