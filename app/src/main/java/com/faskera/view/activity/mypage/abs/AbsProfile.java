/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.mypage.abs;

import android.content.Context;

import com.faskera.common.UserDefine;
import com.faskera.common.adapter.AbsUserState;
import com.faskera.view.activity.home.model.ImlHomeRow;

/**
 * @author DANGNGOCDUC
 * @since 1/4/2017
 */

public abstract class AbsProfile implements ImlHomeRow, AbsUserState {


    @UserDefine.TypeUser
    public abstract int getTypeProfile(Context context);

    @UserDefine.StateFollow
    public abstract int getStateFollow();

    public abstract String getName();


    public abstract int getNumberPoll();

    public abstract int getNumberFollower();

    public abstract int getNumberFollowing();

    public abstract String getIntroduction();

    public abstract String getUserId();
    @Override
    public int getType() {
        return ImlHomeRow.TYPE_VIEW_MY_PROFILE;
    }

    public abstract String getUid();

    @UserDefine.StateFollow
    public abstract int getSate();

    public abstract void setSate( @UserDefine.StateFollow int state);


}
