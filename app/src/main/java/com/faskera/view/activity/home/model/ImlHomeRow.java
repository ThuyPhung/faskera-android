/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home.model;

import com.faskera.common.ImlBaseRow;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public interface ImlHomeRow extends ImlBaseRow {

    int TYPE_CREATE_FEED = 2;
    int TYPE_HEADER_FEED = 3;
    int TYPE_CONTENT_FEED = 4;
    int TYPE_CONTENT_FEED_IMAGE = 5;
    int TYPE_ANS = 6;
    int TYPE_FOOTER = 7;
    int TYPE_SUPER_HEADER = 8;
    int TYPE_VIEW_MORE_INFO = 9;
    int TYPE_VIEW_MY_PROFILE = 10;
    int TYPE_VIEW_INFO_USER= 11;
    int TYPE_VIEW_DETAIL = 12;



}
