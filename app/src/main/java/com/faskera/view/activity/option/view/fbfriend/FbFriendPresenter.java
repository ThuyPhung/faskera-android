/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.fbfriend;

import android.content.Context;

import com.faskera.common.base.ObserverAdvance;
import com.faskera.common.model.BaseResponse;
import com.faskera.network.ApiManager;
import com.faskera.view.activity.option.view.fbfriend.model.Friend;
import com.faskera.view.activity.option.view.fbfriend.model.ListFriendBody;
import com.faskera.view.activity.option.view.fbfriend.network.FriendsResponse;

import java.util.ArrayList;
import java.util.List;

import rx.subscriptions.CompositeSubscription;

import static com.faskera.Constant.FriendRelation.FACEBOOK_FRIEND;
import static java.net.HttpURLConnection.HTTP_OK;

/**
 * @author theanh
 * @since 1/28/2017
 */
public class FbFriendPresenter implements FbFriendContract.Presenter {
    private Context mContext;
    private FbFriendContract.View mView;
    private CompositeSubscription mCompositeSubscription;

    public FbFriendPresenter(Context context, FbFriendContract.View view) {
        mContext = context;
        mView = view;
        mView.setPresenter(this);
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getFbFriends() {
        mView.showLoading();
        mCompositeSubscription.add(ApiManager.getInstance()
            .discoveryPeople(mContext, new ObserverAdvance<FriendsResponse>() {
                @Override
                public void onError(int code) {
                    if (mView == null) return;
                    mView.hideLoading();
                    mView.onErrorConnection(code);
                }

                @Override
                public void onSuccess(FriendsResponse data) {
                    if (mView == null) return;
                    mView.hideLoading();
                    List<Friend> fbFriends = getFbFriends(data.listFriends);
                    if (fbFriends.size() == 0) mView.onGetFriendsEmpty();
                    else mView.onGetFriendsSuccess(fbFriends.size(), fbFriends);
                }

                @Override
                public void onCompleted() {
                }
            }));
    }

    //Filter fb friend in friend list
    private List<Friend> getFbFriends(List<Friend> listFriends) {
        ArrayList<Friend> fbFriends = new ArrayList<>();
        for (int i = 0; i < listFriends.size(); i++) {
            Friend friend = listFriends.get(i);
            if (friend.relationship.equals(FACEBOOK_FRIEND)) {
                fbFriends.add(friend);
            }
        }
        return fbFriends;
    }

    @Override
    public void followFriend(String userId) {
        mCompositeSubscription.add(ApiManager.getInstance()
            .followUser(mContext, userId, new ObserverAdvance<BaseResponse>() {
                @Override
                public void onError(int code) {
                    if (mView != null) mView.onFollowFailed();
                }

                @Override
                public void onSuccess(BaseResponse response) {
                    if (mView == null) return;
                    if (response.code == HTTP_OK) mView.onFollowSuccess();
                }

                @Override
                public void onCompleted() {
                }
            }));
    }

    @Override
    public void unfollowFriend(String userId) {
        mCompositeSubscription.add(ApiManager.getInstance()
            .unfollowUser(mContext, userId, new ObserverAdvance<BaseResponse>() {
                @Override
                public void onError(int code) {
                    if (mView != null) mView.onFollowFailed();
                }

                @Override
                public void onSuccess(BaseResponse response) {
                    if (mView == null) return;
                    if (response.code == HTTP_OK) mView.onUnfollowSuccess();
                }

                @Override
                public void onCompleted() {
                }
            }));
    }

    @Override
    public void followAllFriend(ArrayList<String> listUserId) {
        ListFriendBody body = new ListFriendBody(listUserId);
        mCompositeSubscription.add(ApiManager.getInstance()
            .followAllFriend(mContext, body, new ObserverAdvance<BaseResponse>() {
                @Override
                public void onError(int code) {
                    if (mView != null) mView.onFollowFailed();
                }

                @Override
                public void onSuccess(BaseResponse response) {
                    if (mView == null) return;
                    if (response.code == HTTP_OK) mView.onFollowAllSuccess();
                }

                @Override
                public void onCompleted() {
                }
            }));
    }

    @Override
    public void subscribe() {
        getFbFriends();
    }

    @Override
    public void unsubscribe() {
        if (mCompositeSubscription != null && mCompositeSubscription.isUnsubscribed()) {
            mCompositeSubscription.unsubscribe();
            mView = null;
        }
    }
}
