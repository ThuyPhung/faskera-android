/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home.model.abst;

import com.faskera.model.user.Photo;
import com.faskera.view.activity.home.model.ImlHomeRow;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public abstract class AbsFeedContentMedia implements ImlHomeRow {

    @Override
    public int getType() {
        return TYPE_CONTENT_FEED_IMAGE;
    }

    public abstract Photo getPhoto();

}
