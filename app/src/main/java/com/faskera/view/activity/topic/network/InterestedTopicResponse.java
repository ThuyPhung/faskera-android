/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.topic.network;

import com.faskera.view.activity.topic.model.Topic;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author DANGNGOCDUC
 * @since 1/11/2017
 */

public class InterestedTopicResponse implements Serializable {

    @SerializedName("list")
    public ArrayList<Topic> mListTopic;

}
