/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home.model.abst;

import android.content.Context;
import android.text.Spanned;

import com.faskera.view.activity.home.model.ImlHomeRow;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public abstract class AbsFeedSuperHeader implements ImlHomeRow {
    protected int mTypeSuperHeader = -1;

    public static final int TRENDING = 1;
    public static final int FOLLWING = 2;

    public abstract String getStringUid();

    public abstract Spanned getContent(Context context);

    public int getTypeHeader() {
        return mTypeSuperHeader;
    }

    @Override
    public int getType() {
        return TYPE_SUPER_HEADER;
    }
}
