/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.topic;

import com.faskera.mvp.BasePresenter;
import com.faskera.mvp.BaseView;
import com.faskera.view.activity.topic.model.Topic;

import java.util.ArrayList;

/**
 * @author DANGNGOCDUC
 * @since 12/27/2016
 */

public interface TopicContract {

    interface Presenter extends BasePresenter {
        void getTopics();

        void updateInterestedTopics(ArrayList<String> listId);

        void subscribe();

        void unsubscribe();

    }

    interface View extends BaseView<Presenter> {

        void onGetTopicsSuccess(ArrayList<Topic> listCate);

        void onUpdateTopicSuccess();

        void showLoading();

        void hideLoading();

        void onError(int code);

    }
}
