/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.mypage.model;

import com.faskera.view.activity.home.model.ImlHomeRow;

/**
 * @author DANGNGOCDUC
 * @since 1/4/2017
 */

public class ViewMoreInfo implements ImlHomeRow {
    @Override
    public int getType() {
        return ImlHomeRow.TYPE_VIEW_MORE_INFO;
    }
}
