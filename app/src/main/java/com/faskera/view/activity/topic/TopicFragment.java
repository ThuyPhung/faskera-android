/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.topic;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.faskera.common.AppSharePreference;
import com.faskera.common.BasePollActivity;
import com.faskera.common.base.BaseFragment;
import com.faskera.era.R;
import com.faskera.model.createpoll.PollDataModel;
import com.faskera.model.createpoll.TbCreatePoll;
import com.faskera.net.gotev.uploadservice.MultipartUploadRequest;
import com.faskera.net.gotev.uploadservice.ServerResponse;
import com.faskera.net.gotev.uploadservice.UploadInfo;
import com.faskera.net.gotev.uploadservice.UploadNotificationConfig;
import com.faskera.net.gotev.uploadservice.UploadStatusDelegate;
import com.faskera.network.APIConfig;
import com.faskera.utils.AppDialog;
import com.faskera.utils.AppUtils;
import com.faskera.utils.NetworkUtils;
import com.faskera.view.activity.home.model.Feed;
import com.faskera.view.activity.topic.model.Topic;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import butterknife.BindView;

/**
 * @author theanh
 * @since 1/28/2017
 */

public class TopicFragment extends BaseFragment implements TopicContract.View {

    @BindView(R.id.recycle_view)
    public RecyclerView mRecyclerView;
    @BindView(R.id.text_suggest)
    public TextView mTextSuggest;

    public TopicContract.Presenter mPresenter;
    public ProgressDialog mProgressDialog;
    public TopicAdapter mAdapter;
    private PollDataModel mPollDataModel;
    private String mPathImage;

    @Override
    protected int idRoot() {
        return R.layout.fragment_topic;
    }

    @Override
    protected void initView(View root) {
        mAdapter = new TopicAdapter();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setNestedScrollingEnabled(false);
//        mRecyclerView.setItemAnimator(new FadeInDownAnimator());
//        mRecyclerView.getItemAnimator().setAddDuration(400);
        mRecyclerView.setAdapter(mAdapter);

        new TopicPresenter(getContext(), this);
    }

    @Override
    protected void initData() {
        if (getArguments() != null) {
            mPollDataModel = (PollDataModel) getArguments().getSerializable(PollDataModel.class.getName());
            mPathImage = getArguments().getString(AppUtils.BUNDLE_PATH_IMAGE);
        }
    }

    @Override
    public void goToDone() {
        super.goToDone();
        if (mAdapter == null) {
            showDialogChooseTopic();
        } else {
            if (NetworkUtils.isOnline(getActivity())) {
                getBaseActivity().showLoading(R.string.msg_loading);
                if (mPollDataModel != null) {
                    ArrayList<String> mIdPicList = new ArrayList<>();
                    if (mAdapter != null && mAdapter.getTopicList() != null) {
                        ArrayList<Topic> mList = mAdapter.getTopicList();

                        for (Topic category : mList) {
                            if (category.isSelected()) {
                                mIdPicList.add(category.getId());
                            }
                        }
                    }
                    if (mIdPicList.size() > 0) {
                        mPollDataModel.setTopicIds(mIdPicList);
                    } else {
                        getBaseActivity().visibleLoading(false);
                        showDialogChooseTopic();
                        return;
                    }
                    mPollDataModel.setTopicIds(mIdPicList);
                }

            }

        }

        if (mPathImage != null && !mPathImage.trim().isEmpty()) {
            uploadWithImage();
        }
    }

    public void uploadWithImage() {
        MultipartUploadRequest req = null;
        try {
            String authorizationBuilder = AppSharePreference.getTokenType(getActivity()) +
                    " " +
                    AppSharePreference.getToken(getActivity());
            req = new MultipartUploadRequest(getActivity(), APIConfig.BASE_API_URL + "poll")
                    .addHeader("Authorization", authorizationBuilder)
                    .addParameter("data", getJsonObject(mPollDataModel))
                    .setNotificationConfig(getNotificationConfig("Upload"))
                    .setAutoDeleteFilesAfterSuccessfulUpload(true)
                    .setUsesFixedLengthStreamingMode(true)
                    .setMaxRetries(3);
            if (mPathImage != null && !mPathImage.trim().isEmpty()) {
                req.addFileToUpload(mPathImage, "image");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        req.setUtf8Charset();


        req.setDelegate(new UploadStatusDelegate() {
            @Override
            public void onProgress(Context context, UploadInfo uploadInfo) {

                MultipartUploadRequest req = null;
                try {
                    String authorizationBuilder = AppSharePreference.getTokenType(getActivity()) +
                            " " +
                            AppSharePreference.getToken(getActivity());
                    req = new MultipartUploadRequest(getActivity(), APIConfig.BASE_API_URL + "poll")
                            // .addFileToUpload(mPathImage, "image")
                            .addHeader("Authorization", authorizationBuilder)
                            .addParameter("data", getJsonObject(mPollDataModel))
                            .setNotificationConfig(getNotificationConfig("Upload"))
                            .setAutoDeleteFilesAfterSuccessfulUpload(true)
                            .setUsesFixedLengthStreamingMode(true)
                            .setMaxRetries(3);
                } catch (Exception e) {
                    e.printStackTrace();
                    showDialogError();

                }
            }

            @Override
            public void onError(Context context, UploadInfo uploadInfo, Exception exception) {
                showDialogError();

            }

            @Override
            public void onCompleted(Context context, UploadInfo uploadInfo, ServerResponse serverResponse) {
                String s = serverResponse.getBodyAsString();

                Gson gson = new GsonBuilder()
                        .setLenient()
                        .create();

                Feed.Poll poll = gson.fromJson(s, Feed.Poll.class);
                Intent data = new Intent();
                data.putExtra(BasePollActivity.BUNDLE_POLL_CREATE, poll);

                getActivity().setResult(Activity.RESULT_OK, data);

                getBaseActivity().visibleLoading(false);
                getBaseActivity().finish();


            }

            @Override
            public void onCancelled(Context context, UploadInfo uploadInfo) {

            }

        });

    }

    private void showDialogError() {
        getBaseActivity().visibleLoading(false);
        new AppDialog.Builder(this, getString(R.string.msg_content_post_poll_error))
                .setTitle(getString(R.string.msg_title_post_poll_error))
                .setPositiveButton(getString(R.string.msg_ok))
                .setOnClickButtonDialogApp(new AppDialog.OnClickButtonDialogApp() {
                    @Override
                    public void onOk() {
                    }

                    @Override
                    public void onCancel() {

                    }
                })
                .build()
                .show();
    }

    private void showDialogChooseTopic() {
        getBaseActivity().visibleLoading(false);
        new AppDialog.Builder(this, getString(R.string.msg_content_choose_topic))
                .setTitle(getString(R.string.msg_title_choose_topic))
                .setPositiveButton(getString(R.string.msg_ok))
                .setOnClickButtonDialogApp(new AppDialog.OnClickButtonDialogApp() {
                    @Override
                    public void onOk() {
                    }

                    @Override
                    public void onCancel() {

                    }
                })
                .build()
                .show();
    }

    private String getJsonObject(PollDataModel student) {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();

        return gson.toJson(student);
    }

    @Override
    public void onResume() {
        super.onResume();
        getBaseActivity().update(new TbCreatePoll(getString(R.string.msg_title_topic), getString(R.string.msg_create_poll_post)));
    }

    @Override
    public void onRefresh(Object o) {

    }

    public void updateInterestedTopic() {
        mPresenter.updateInterestedTopics(mAdapter.getSelectedTopics());
    }

    @Override
    public void setPresenter(TopicContract.Presenter presenter) {
        mPresenter = presenter;
    }

    //======================================VIEW==============================================//

    @Override
    public void onGetTopicsSuccess(ArrayList<Topic> listTopic) {
        mAdapter.setTopicList(listTopic);
    }

    @Override
    public void onUpdateTopicSuccess() {
        // AppSharePreference.getInstance(getContext()).storeUserInfo(AppSharePreference.getInstance(getContext()).getUserInfoTemp());
        //IntentUtils.startActivityHome(null, getContext());
        getActivity().setResult(Activity.RESULT_OK);
        getBaseActivity().finish();
    }

    @Override
    public void onError(int code) {
        getBaseActivity().handleErrorCode(code);
    }

    @Override
    public void showLoading() {
        if (mProgressDialog != null && !mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    @Override
    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    //====================================LIFE_CYCLE=============================================//
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialog = new ProgressDialog(getContext(), R.style.MyTheme);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        mPresenter.subscribe();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.unsubscribe();
    }


    private UploadNotificationConfig getNotificationConfig(String filename) {

        return new UploadNotificationConfig()
                .setIcon(R.drawable.ic_faskera_notification_icon)
                .setCompletedIcon(R.drawable.ic_faskera_notification_icon)
                .setErrorIcon(R.drawable.ic_faskera_notification_icon)
                .setCancelledIcon(R.drawable.ic_faskera_notification_icon)
                .setIconColor(Color.BLUE)
                .setCompletedIconColor(Color.GREEN)
                .setErrorIconColor(Color.RED)
                .setCancelledIconColor(Color.YELLOW)
                .setTitle(filename)
                .setInProgressMessage(getString(R.string.upload_poll))
                .setCompletedMessage(getString(R.string.upload_poll_success))
                .setErrorMessage(getString(R.string.upload_poll_faile))
                .setCancelledMessage(getString(R.string.upload_poll_faile))
                .setAutoClearOnCancel(true)
                .setAutoClearOnSuccess(true)
                .setClearOnAction(true)
                .setRingToneEnabled(true);
    }
}
