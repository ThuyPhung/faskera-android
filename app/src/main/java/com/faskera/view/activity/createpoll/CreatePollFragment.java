/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.faskera.common.AppSharePreference;
import com.faskera.common.base.BaseFragment;
import com.faskera.era.R;
import com.faskera.model.createpoll.AddOptionModel;
import com.faskera.model.createpoll.PollDataModel;
import com.faskera.model.createpoll.QuestionPollModel;
import com.faskera.model.createpoll.SettingPollModel;
import com.faskera.model.createpoll.TbCreatePoll;
import com.faskera.model.user.UserInfoResponse;
import com.faskera.utils.AppDialog;
import com.faskera.utils.AppUtils;
import com.faskera.utils.EditTextUtil;
import com.faskera.utils.ImageLoader;
import com.faskera.view.activity.createpoll.adapter.AddOptionAdapter;
import com.faskera.view.activity.createpoll.frame.model.FrameModel;
import com.faskera.view.activity.createpoll.frame.task.OnCallBackAsyncTaskListener;
import com.faskera.view.activity.createpoll.frame.task.SavePictureTask;
import com.faskera.view.activity.createpoll.frame.utils.FileUtils;
import com.faskera.view.activity.createpoll.frame.utils.PhotoType;
import com.faskera.view.activity.createpoll.frame.utils.TemplateEntity;
import com.faskera.view.activity.createpoll.frame.utils.TemplateUtils;
import com.faskera.view.activity.createpoll.frame.widget.PhotoModelLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class CreatePollFragment extends BaseFragment {

    @BindView(R.id.id_name)
    TextView mName;

    @BindView(R.id.iv_icon)
    ImageView mAvatar;

    @BindView(R.id.rv_list_topic)
    RecyclerView mListTopic;

    @BindView(R.id.ed_status)
    EditText mEdStatus;

    @BindView(R.id.ed_add_topic)
    EditText mEdAddTopic;

    @BindView(R.id.single_model_area)
    PhotoModelLayout mModelArea;

    @BindView(R.id.model_area_parent)
    RelativeLayout mModelAreaParent;

    private List<AddOptionModel> mListAddOption;
    private AddOptionAdapter mAddOptionAdapter;
    private PhotoType mPhotoType;
    private int mModelAreaParentWidth;
    private int mModelAreaParentHeight;
    private ArrayList<String> mListPath;
    private int mPosition;
    private static final float PHOTO_MODEL_RATIO = 1.0f;
    private FrameModel mFrameModel;
    //dialog
    private String mTitleDialog;
    private String mMessageDialog;
    private SettingPollModel mSettingPollModel;

    @OnClick(R.id.iv_photo)
    public void onClickGallery() {
        Bundle mBundle = new Bundle();
        mBundle.putBoolean(CreatePollFragment.class.getName(), true);
        Intent intent = new Intent(getActivity(), GalleryActivity.class);
        intent.putExtras(mBundle);
        startActivityForResult(intent, 0x04);
    }

    @OnClick(R.id.iv_setting)
    public void goToSetting() {
        new SettingCreatePollDialog(getActivity())
                .setSettingPollModel(mSettingPollModel)
                .setOnDataChangeListener(mSettingPoll -> {
                    mSettingPollModel = mSettingPoll;
                }).show();
    }

    @OnClick(R.id.iv_edit)
    public void goToEditFrame() {
        if (getActivity() instanceof CreatePollActivity && ((CreatePollActivity) getActivity()).getBitmapView() == null) {
            ((CreatePollActivity) getActivity()).setBitmapView(FileUtils.createBitmapFromView(mModelArea));
        }
        if (getActivity() instanceof CreatePollActivity && ((CreatePollActivity) getActivity()).getBitmapView() != null) {
            Bundle mBundle = new Bundle();
            mBundle.putStringArrayList(AppUtils.SELECTED_PATHS, this.mListPath);
            mBundle.putSerializable(AppUtils.TYPE_OF_PHOTO, mPhotoType);
            mBundle.putInt(AppUtils.ID_OF_TEMPLATE, mPosition);
            mBundle.putSerializable(FrameModel.class.getName(), mFrameModel);
            getBaseActivity().goToFragment(new EditImageFragment(), mBundle, true);
        }
    }

    @OnClick(R.id.iv_remove)
    public void goToDeleteFrame() {
        if (this.mListPath != null && mModelArea != null) {
            if (getActivity() instanceof CreatePollActivity) {
                ((CreatePollActivity) getActivity()).setBitmapView(null);
                if (mFrameModel != null) {
                    mFrameModel.removeBitmap();
                    mFrameModel = null;
                }
                this.mListPath.clear();
                setVisibilityEdit(mListPath);
                mModelArea.setBundleList(null);
                mModelArea.setImagePathList(this.mListPath);
                mModelArea.removeAllViews();
                mModelArea.invalidate();
            }
        }
    }

    @Override
    protected int idRoot() {
        return R.layout.fragment_create_poll;
    }

    @Override
    protected void initView(View root) {
        mEdStatus.setOnTouchListener((v, event) -> {
            mEdStatus.setFocusable(true);
            mEdStatus.setFocusableInTouchMode(true);
            return false;
        });

        mEdStatus.setOnEditorActionListener((v, actionId, event) -> {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        if (!TextUtils.isEmpty(v.getText().toString())) {
                            if (mEdStatus.getText().length() < AppUtils.POLL_LENGTH_TEXT_OPTION) {
                                mEdStatus.setFocusable(false);
                                EditTextUtil.hideSoftKeyboard(mEdStatus, getActivity());
                            } //else {
                            //mMessageDialog = getString(R.string.msg_poll_option_choose_text_message);
                            //mTitleDialog = getString(R.string.msg_poll_option_choose_text_title);
                            //showDialog(mEdStatus);
                            // }
                        } //else {
                        //mMessageDialog = getString(R.string.msg_poll_option_next_text_message);
                        //mTitleDialog = getString(R.string.msg_poll_option_next_text_title);
                        //showDialog(mEdStatus);
                        //}
                        return true;
                    }
                    return false;
                }

        );

        mEdAddTopic.setOnTouchListener((v, event) -> {
                    mEdAddTopic.setFocusable(true);
                    mEdAddTopic.setFocusableInTouchMode(true);
                    return false;
                }

        );

        mEdAddTopic.setOnEditorActionListener((v, actionId, event) -> {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        if (!TextUtils.isEmpty(v.getText().toString().trim()) && mListAddOption != null) {
                            if (mListAddOption.size() < AppUtils.POLL_SIZE_OPTION) {
                                String mContent = v.getText().toString();
                                boolean isRepeat = false;
                                if (mContent.length() <= AppUtils.POLL_LENGTH_TEXT_OPTION) {
                                    for (AddOptionModel addOptionModel : mListAddOption) {
                                        if (addOptionModel.text.equals(mContent)) {
                                            isRepeat = true;
                                            break;
                                        }
                                    }
                                    if (!isRepeat) {
                                        isFull = false;
                                        mEdAddTopic.setFilters(new InputFilter[]{new InputFilter.LengthFilter(AppUtils.POLL_LENGTH_TEXT_OPTION * 100)});
                                        mListAddOption.add(new AddOptionModel(mContent));
                                        mAddOptionAdapter.notifyDataSetChanged();
                                        mEdAddTopic.setText(null);
                                        mListTopic.setVisibility(View.VISIBLE);
                                    } else {
                                        mMessageDialog = getString(R.string.msg_poll_option_choose_text_message);
                                        mTitleDialog = getString(R.string.msg_poll_option_choose_text_title);
                                        showDialog(mEdAddTopic);
                                    }
                                } else {
                                    mMessageDialog = getString(R.string.msg_poll_option_length_text_message);
                                    mTitleDialog = getString(R.string.msg_poll_option_length_text_title);
                                    showDialog(mEdAddTopic);
                                    return false;
                                }
                            } else {
                                mMessageDialog = getString(R.string.msg_poll_option_size_message);
                                mTitleDialog = getString(R.string.msg_poll_option_size_title);
                                showDialog(mEdAddTopic);
                            }
                            return true;
                        } else {
                            mMessageDialog = getString(R.string.msg_poll_option_next_text_message);
                            mTitleDialog = getString(R.string.msg_poll_option_next_text_title);
                            showDialog(mEdAddTopic);
                        }
                    }
                    return false;
                }

        );

        mEdAddTopic.addTextChangedListener(new TextWatcher() {
                                               @Override
                                               public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                               }

                                               @Override
                                               public void onTextChanged(CharSequence s, int start, int before, int count) {
                                               }

                                               @Override
                                               public void afterTextChanged(Editable s) {

                                                   if (s.length() > AppUtils.POLL_LENGTH_TEXT_OPTION+1) {
                                                       isFull = true;
                                                   }

                                                   if (s.length() >= AppUtils.POLL_LENGTH_TEXT_OPTION && !isFull) {
                                                       isFull = true;
                                                       s.delete(AppUtils.POLL_LENGTH_TEXT_OPTION, s.length());
                                                       mEdAddTopic.setFilters(new InputFilter[]{new InputFilter.LengthFilter(AppUtils.POLL_LENGTH_TEXT_OPTION)});
                                                       mMessageDialog = getString(R.string.msg_poll_option_length_text_message);
                                                       mTitleDialog = getString(R.string.msg_poll_option_length_text_title);
                                                       showDialog(mEdAddTopic);
                                                   }
                                               }
                                           }

        );

        mModelArea.setEdit(false);
        mEdAddTopic.setFocusable(true);
        mEdAddTopic.setFocusableInTouchMode(true);
    }

    private boolean isFull;

    private void showDialog(EditText mEdit) {
        EditTextUtil.hideSoftKeyboard(getActivity());
        new AppDialog.Builder(this, mMessageDialog)
                .setTitle(mTitleDialog)
                .setPositiveButton(getString(R.string.msg_ok))
                .setOnClickButtonDialogApp(new AppDialog.OnClickButtonDialogApp() {
                    @Override
                    public void onOk() {
                        isFull = false;
                        mEdit.setFocusable(true);
                        mEdit.requestFocus();
                        EditTextUtil.showKeyBoardRun(mEdit, getActivity());
                    }

                    @Override
                    public void onCancel() {

                    }
                })
                .build()
                .show();
    }

    @Override
    protected void initData() {
        mSettingPollModel = new SettingPollModel();
        mListPath = new ArrayList<>();
        if (getArguments() != null) {
            Bundle mBundle = getArguments();
            mListPath = mBundle.getStringArrayList(AppUtils.SELECTED_PATHS);
        }
        setVisibilityEdit(mListPath);
        mListAddOption = new ArrayList<>();
        mAddOptionAdapter = new AddOptionAdapter(mListAddOption, getActivity());
        mAddOptionAdapter.setOnClickItemRecyclerListener(mAddOptionModel -> {
            if (mListAddOption != null && mListAddOption.size() > 0) {
                mListAddOption.remove(mAddOptionModel);
                mAddOptionAdapter.notifyDataSetChanged();
            } else {
                mListTopic.setVisibility(View.GONE);
            }
        });
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mListTopic.setLayoutManager(mLinearLayoutManager);
        mListTopic.setAdapter(mAddOptionAdapter);
        if (AppSharePreference.getInstance(getActivity()).getUserInfo() != null) {

            UserInfoResponse userInfo = AppSharePreference.getInstance(getActivity()).getUserInfo();
            if (userInfo.getPhoto() == null || userInfo.getPhoto().original == null) {
                ImageLoader.loadImageViewWith(getActivity(), R.drawable.ic_avatar_default, mAvatar);
            } else {
                if (userInfo.getPhoto().original.trim().isEmpty()) {
                    ImageLoader.loadImageViewWith(getActivity(), R.drawable.ic_avatar_default, mAvatar);

                } else {
                    ImageLoader.loadImageViewWith(getActivity(), userInfo.getPhoto().original, mAvatar);
                }
            }
            mName.setText(AppSharePreference.getInstance(getActivity()).getUserInfo().fullName);
        }
        if (getArguments() != null) {
            setFrameType(mListPath);
        } else {
            mEdStatus.setFocusableInTouchMode(true);
            mEdStatus.setFocusable(true);
            mEdStatus.requestFocus();
            EditTextUtil.showKeyBoardRun(mEdStatus, getActivity());

        }
    }

    @Override
    public void onRefresh(Object mData) {
        if (mData instanceof ArrayList) {
            mFrameModel = null;
            mListPath = (ArrayList<String>) mData;
            if (mListPath.size() > 0) {
                setFrameType(mListPath);
            }
        } else if (mData instanceof FrameModel) {
            mFrameModel = (FrameModel) mData;
            setFrameType(mListPath);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getBaseActivity().update(new TbCreatePoll(getString(R.string.msg_create_poll), getString(R.string.msg_next)));
    }

    private void setFrameType(ArrayList<String> mListPath) {
        this.mListPath = mListPath;
        setVisibilityEdit(this.mListPath);
        mPhotoType = TemplateUtils.getJigsawType(mListPath.size());
        if (mFrameModel == null) {
            mPosition = mPhotoType.value();
        } else {
            mModelArea.setBundleList(mFrameModel.mListBundles);
            mPosition = mFrameModel.mPositionFrame;
            if (mFrameModel.mListBitmapFilter != null && mFrameModel.mListBitmapFilter.size() > 0) {
                mModelArea.renderSelectedListBitmap(mFrameModel.mListBitmapFilter);
            }
        }
        TemplateEntity entity = TemplateUtils.getEntity(getActivity(), mPhotoType, mPosition);
        mModelArea.setImagePathList(this.mListPath);
        mModelArea.setTemplateEntity(entity);
        mModelArea.invalidate();

        final ViewTreeObserver observer = mModelAreaParent.getViewTreeObserver();
        observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                mModelAreaParent.getViewTreeObserver().removeOnPreDrawListener(this);
                mModelAreaParentWidth = mModelAreaParent.getMeasuredWidth();
                mModelAreaParentHeight = mModelAreaParent.getMeasuredHeight();
                // redraw model area
                reDrawModelArea();
                return true;
            }
        });
    }

    @Override
    public void goToDone() {
        if (isCheckEmptyContent() == 0) {
            if (mListPath != null && mListPath.size() > 0) {
                if (mListPath.size() == 1 || (mFrameModel != null && !mFrameModel.isFilter)) {
                    String mPath = mListPath.get(0);
                    goToFragmentTopic(mPath);
                } else {
                    SavePictureTask savePictureTask = new SavePictureTask(getActivity(), mModelArea);
                    savePictureTask.setOnCallBackAsyncTaskListener(new OnCallBackAsyncTaskListener<String>() {
                        @Override
                        public void onPreExecute() {
                            getBaseActivity().showLoading(R.string.msg_loading);
                        }

                        @Override
                        public void onPostExecute(String mPath) {
                            goToFragmentTopic(mPath);
                        }
                    });
                    savePictureTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            } else {
                goToFragmentTopic(null);
            }
        } else {
            EditTextUtil.hideSoftKeyboard(getActivity());
            new AppDialog.Builder(this, mMessageDialog)
                    .setTitle(mTitleDialog)
                    .setPositiveButton(getString(R.string.msg_ok))
                    .setOnClickButtonDialogApp(new AppDialog.OnClickButtonDialogApp() {
                        @Override
                        public void onOk() {
                            switch (isCheckEmptyContent()) {
                                case 1:
                                    mEdStatus.setFocusableInTouchMode(true);
                                    mEdStatus.requestFocus();
                                    EditTextUtil.showKeyBoardRun(mEdStatus, getActivity());
                                    break;
                                case 2:
                                    mEdAddTopic.setFocusableInTouchMode(true);
                                    mEdAddTopic.requestFocus();
                                    EditTextUtil.showKeyBoardRun(mEdAddTopic, getActivity());
                                    break;
                                default:
                                    break;
                            }
                        }

                        @Override
                        public void onCancel() {

                        }
                    })
                    .build()
                    .show();
        }
    }

    private void goToFragmentTopic(String mPath) {
        getBaseActivity().visibleLoading(false);
        Bundle mBundle = new Bundle();
        PollDataModel mPollDataModel = new PollDataModel();
        mPollDataModel.setQuestion(new QuestionPollModel(mEdStatus.getText().toString().trim()));
        mPollDataModel.setOptions(mListAddOption);
        mPollDataModel.setSettings(mSettingPollModel);
        mBundle.putSerializable(PollDataModel.class.getName(), mPollDataModel);
        mBundle.putString(AppUtils.BUNDLE_PATH_IMAGE, mPath);
        getBaseActivity().goToFragment(new ListTopicFragment(), mBundle, false);
    }

    private int isCheckEmptyContent() {
        if (TextUtils.isEmpty(mEdStatus.getText().toString().trim())) {
            mTitleDialog = getString(R.string.msg_poll_ques_empty_title);
            mMessageDialog = getString(R.string.msg_poll_ques_empty_message);
            return 1;
        } else if (mListAddOption != null && mListAddOption.size() < 2) {
            mTitleDialog = getString(R.string.msg_poll_option_empty_title);
            mMessageDialog = getString(R.string.msg_poll_option_empty_message);
            return 2;
        }
        return 0;
    }

    private void reDrawModelArea() {
        if (0 != mModelAreaParentWidth && 0 != mModelAreaParentHeight && this.mListPath != null && this.mListPath.size() > 0) {
            float ratio = ((float) mModelAreaParentWidth) / mModelAreaParentHeight;

            int modelAreaWidth;
            int modelAreaHeight;

            if (PHOTO_MODEL_RATIO > ratio) {
                modelAreaWidth = mModelAreaParentWidth;
                modelAreaHeight = (int) (mModelAreaParentWidth / PHOTO_MODEL_RATIO);
            } else {
                modelAreaHeight = mModelAreaParentHeight;
                modelAreaWidth = (int) (modelAreaHeight * PHOTO_MODEL_RATIO);
            }

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(modelAreaWidth, modelAreaHeight);
            params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            mModelArea.setLayoutParams(params);

            mModelArea.reDraw(mFrameModel, modelAreaWidth, modelAreaWidth, this.mListPath.size(), 0);
        }
    }

    private void setVisibilityEdit(ArrayList<String> mListPath) {
        if (mModelAreaParent != null && mListPath != null) {
            mModelAreaParent.setVisibility(mListPath.size() != 0 ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (((CreatePollActivity) getActivity()).getBitmapView() != null) {
            ((CreatePollActivity) getActivity()).getBitmapView().recycle();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppCompatActivity.RESULT_OK && requestCode == 0x04) {
            if (data != null) {
                goToDeleteFrame();
                if (mListPath != null) {
                    mListPath.clear();
                }
                Bundle mBundle = data.getExtras();
                mListPath = mBundle.getStringArrayList(AppUtils.SELECTED_PATHS);
                setFrameType(mListPath);
            }
        }
    }
}
