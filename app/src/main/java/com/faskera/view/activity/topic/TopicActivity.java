/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.topic;

import android.content.Intent;
import android.support.v4.content.IntentCompat;
import android.view.View;

import com.facebook.login.LoginManager;
import com.faskera.common.AppSharePreference;
import com.faskera.common.base.BaseActivity;
import com.faskera.era.R;
import com.faskera.view.activity.login.LoginActivity;

import butterknife.OnClick;

/**
 * @author DANGNGOCDUC
 * @since 12/27/2016
 */
public class TopicActivity extends BaseActivity {

    public static final String CATEGORY_FRAGMENT_TAG = TopicFragment.class.getSimpleName();

    @Override
    protected int getIdLayout() {
        return R.layout.activity_topic;
    }

    @Override
    protected void initView() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.root_layout, new TopicFragment(), CATEGORY_FRAGMENT_TAG)
                .commit();
    }

    @Override
    protected void initData() {
    }

    @OnClick({R.id.image_back, R.id.toolbar_text_done})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                LoginManager.getInstance().logOut();
                AppSharePreference.getInstance(this).clearData();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
            case R.id.toolbar_text_done:
                TopicFragment fragment = (TopicFragment) getSupportFragmentManager()
                        .findFragmentByTag(CATEGORY_FRAGMENT_TAG);
                if (fragment != null)
                    fragment.updateInterestedTopic();
                break;
        }
    }

}
