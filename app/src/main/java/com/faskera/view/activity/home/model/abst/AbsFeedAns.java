/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home.model.abst;

import com.faskera.view.activity.home.model.ImlHomeRow;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public abstract class AbsFeedAns implements ImlHomeRow {

    public abstract int getNbSeq();

    public abstract boolean isSelected();

    public abstract void setSelected(boolean selected);

    public abstract String getContent();

    public abstract String getVotes();

    public abstract int getVoteInt();

    public abstract FeedAbs getPoll();

    public abstract String getAnsID();

    @Override
    public int getType() {
        return TYPE_ANS;
    }
}
