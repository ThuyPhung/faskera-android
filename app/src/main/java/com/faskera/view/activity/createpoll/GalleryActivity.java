/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll;

import com.faskera.callback.OnClickItemToolbarListener;
import com.faskera.era.R;
import com.faskera.model.createpoll.TbCreatePoll;

public class GalleryActivity extends CreatePollBaseActivity {

    @Override
    protected int getIdLayout() {
        return R.layout.activity_gallery;
    }

    @Override
    protected void initView() {
        super.initView();
        mToolbarCreatePoll.setOnClickItemToolbarListener(new OnClickItemToolbarListener() {
            @Override
            public void onBack() {
                goToBack();
            }

            @Override
            public void onDone() {
                gotoDone();
            }
        });
    }

    private void goToBack() {
        onBack(null);
    }

    @Override
    protected void initData() {
        super.initData();
        goToFragment(new GalleryFragment(), getIntent().getExtras(), false);
    }

    @Override
    public void update(Object o) {
        super.update(o);
        if (o instanceof TbCreatePoll) {
            TbCreatePoll mTbCreatePoll = (TbCreatePoll) o;
            mToolbarCreatePoll.setToolBarModel(mTbCreatePoll);
        }
    }
}
