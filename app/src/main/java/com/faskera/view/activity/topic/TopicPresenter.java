/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.topic;

import android.content.Context;

import com.faskera.common.base.ObserverAdvance;
import com.faskera.network.ApiManager;
import com.faskera.view.activity.topic.model.UpdateRequestBody;
import com.faskera.view.activity.topic.network.InterestedTopicResponse;
import com.faskera.view.activity.topic.network.UpdateTopicInterestResponse;

import java.util.ArrayList;

import rx.subscriptions.CompositeSubscription;

/**
 * @author DANGNGOCDUC
 * @since 12/27/2016
 */

public class TopicPresenter implements TopicContract.Presenter {

    private TopicContract.View mView;
    private Context mContext;
    private CompositeSubscription mCompositeSubscription;

    public TopicPresenter(Context context, TopicContract.View view) {
        mContext = context;
        mView = view;
        mView.setPresenter(this);
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getTopics() {
        mView.showLoading();
        mCompositeSubscription.add(ApiManager.getInstance()
                .getInterestedTopic(mContext, new ObserverAdvance<InterestedTopicResponse>() {
                    @Override
                    public void onError(int code) {
                        if (mView != null) {
                            mView.hideLoading();
                            mView.onError(code);
                        }

                    }

                    @Override
                    public void onSuccess(InterestedTopicResponse data) {
                        if (mView != null) {
                            mView.hideLoading();
                            mView.onGetTopicsSuccess(data.mListTopic);
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }));


    }

    @Override
    public void updateInterestedTopics(ArrayList<String> listId) {
        mView.showLoading();
        mCompositeSubscription.add(ApiManager.getInstance()
                .updateTopicInterest(mContext, new UpdateRequestBody(listId), new ObserverAdvance<UpdateTopicInterestResponse>() {
                    @Override
                    public void onError(int code) {
                        if (mView != null) {
                            mView.hideLoading();
                            mView.onError(code);
                        }
                    }

                    @Override
                    public void onSuccess(UpdateTopicInterestResponse data) {
                        if (mView != null) {
                            mView.hideLoading();
                            mView.onUpdateTopicSuccess();
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }

                }));
    }

    @Override
    public void subscribe() {
        getTopics();
    }

    @Override
    public void unsubscribe() {
        if (mCompositeSubscription != null && mCompositeSubscription.isUnsubscribed()) {
            mCompositeSubscription.unsubscribe();
            mView = null;
        }
    }
}
