/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.discoverpeople;

import android.content.Context;

import com.faskera.common.base.ObserverAdvance;
import com.faskera.common.model.BaseResponse;
import com.faskera.network.ApiManager;
import com.faskera.view.activity.option.view.fbfriend.network.FriendsResponse;

import rx.subscriptions.CompositeSubscription;

import static java.net.HttpURLConnection.HTTP_OK;

/**
 * @author theanh
 * @since 2/12/2017
 */
public class SearchFriendsPresenter implements SearchFriendsContract.Presenter {
    private Context mContext;
    private SearchFriendsContract.View mView;
    private CompositeSubscription mCompositeSubscription;

    public SearchFriendsPresenter(Context context, SearchFriendsContract.View view) {
        this.mContext = context;
        this.mView = view;
        mView.setPresenter(this);
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getFbFriends() {
        mView.showLoading();
        mCompositeSubscription.add(ApiManager.getInstance()
            .discoveryPeople(mContext, new ObserverAdvance<FriendsResponse>() {
                @Override
                public void onError(int code) {
                    if (mView == null) return;
                    mView.hideLoading();
                    mView.onErrorConnection();
                }

                @Override
                public void onSuccess(FriendsResponse data) {
                    if (mView == null) return;
                    mView.hideLoading();
                    if (data.listFriends.size() == 0) mView.onGetFriendsEmpty();
                    else mView.onGetFriendsSuccess(data.listFriends);
                }

                @Override
                public void onCompleted() {
                }
            }));
    }

    @Override
    public void followFriend(String userId) {
        mCompositeSubscription.add(ApiManager.getInstance()
            .followUser(mContext, userId, new ObserverAdvance<BaseResponse>() {
                @Override
                public void onError(int code) {
                    if (mView != null) mView.onFollowFailed();
                }

                @Override
                public void onSuccess(BaseResponse data) {
                    if (mView != null && data.code == HTTP_OK) mView.onFollowSuccess();
                }

                @Override
                public void onCompleted() {
                }
            }));
    }

    @Override
    public void unfollowFriend(String userId) {
        mCompositeSubscription.add(ApiManager.getInstance()
            .unfollowUser(mContext, userId, new ObserverAdvance<BaseResponse>() {
                @Override
                public void onError(int code) {
                    if (mView != null) mView.onErrorConnection();
                }

                @Override
                public void onSuccess(BaseResponse data) {
                    if (mView != null && data.code == HTTP_OK) mView.onUnfollowSuccess();
                }

                @Override
                public void onCompleted() {
                }
            }));
    }

    @Override
    public void subscribe() {
        getFbFriends();
    }

    @Override
    public void unsubscribe() {
        if (mCompositeSubscription != null && mCompositeSubscription.isUnsubscribed()) {
            mCompositeSubscription.unsubscribe();
            mView = null;
        }
    }
}
