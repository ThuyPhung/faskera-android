/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.mypage.abs;

import android.support.annotation.DrawableRes;

import com.faskera.view.activity.home.model.ImlHomeRow;

/**
 * @author DANGNGOCDUC
 * @since 1/4/2017
 */

public abstract class AbsInfoExtra implements ImlHomeRow {

    public abstract int getIconSource();

    public abstract void setIconSource(@DrawableRes int res);

    public abstract String getTitle();

    @Override
    public int getType() {
        return ImlHomeRow.TYPE_VIEW_INFO_USER;
    }
}
