/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.follow;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.faskera.common.AppSharePreference;
import com.faskera.common.UserDefine;
import com.faskera.common.adapter.AbsUserState;
import com.faskera.common.adapter.ListUserAdapter;
import com.faskera.common.base.BaseActivity;
import com.faskera.common.view.FrameRecycleView;
import com.faskera.era.R;
import com.faskera.utils.ImageLoader;
import com.faskera.utils.IntentUtils;
import com.faskera.view.activity.mypage.ProfileActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Subscription;

/**
 * @author DANGNGOCDUC
 * @since 2/2/2017
 */

public class ShowListUserView extends BaseActivity implements ShowListUserContract.View, ListUserAdapter.CallbackFollow {

    public static final String BUNDLE_USER_ID = "_userId";
    public static final String BUNDLE_TYPE_LIST = "_type_list";

    private ShowListUserContract.Presenter mPresenter;
    private Subscription mSubscription;
    private ListUserAdapter mAdapter;
    private int mPositionUnfollow;
    private LinearLayoutManager mLinearLayoutManager;

    @BindView(R.id.toolbar_title)
    public TextView mTitle;

    @BindView(R.id.toolbar_text_done)
    public TextView mTextDone;

    @BindView(R.id.framerecycleview)
    public FrameRecycleView mRecycleView;

    @OnClick(R.id.image_back)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected int getIdLayout() {
        return R.layout.activity_show_list_user;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getExtras().get(BUNDLE_USER_ID) == null || getIntent().getExtras().get(BUNDLE_TYPE_LIST) == null) {
            finish();
        }
    }

    @Override
    protected void initView() {
        mLinearLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ListUserAdapter(this);
        mRecycleView.getRecycleView().setLayoutManager(mLinearLayoutManager);
        mRecycleView.getRecycleView().setAdapter(mAdapter);
        mRecycleView.getRecycleView().addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                    int totalItemCount = mLinearLayoutManager.getItemCount();
                    int lastVisibleItem = mLinearLayoutManager.findLastVisibleItemPosition();

                    if (!mAdapter.isLoadMore() && totalItemCount <= (lastVisibleItem + 3)) {
                        mPresenter.getUserList(false);
                    }
            }
        });
        mAdapter.setCallbackClick(this);

        mTextDone.setVisibility(View.GONE);
        if (getIntent().getExtras().getInt(BUNDLE_TYPE_LIST) == UserDefine.STATE_FOLLOW) {
            mTitle.setText(getResources().getString(R.string.follows));
        } else {
            mTitle.setText(getResources().getString(R.string.following_caps));
        }
        mRecycleView.setOnSwipeRefreshLayout(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getUserList(true);
            }
        });
    }

    @Override
    protected void initData() {
        mPresenter = new ShowListUserPresenter(this, this, getIntent().getExtras().getString(BUNDLE_USER_ID)
                , getIntent().getExtras().getInt(BUNDLE_TYPE_LIST));

        mSubscription = mPresenter.getUserList(false);
    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public void onLoading() {
        mRecycleView.showLoading();
    }

    @Override
    public void onLoadError(int code) {
        mRecycleView.setRefreshing(false);
        mRecycleView.showContent(R.drawable.ic_notifications, R.string.error_connection );
    }

    @Override
    public void onLoadError(Throwable e) {
        mRecycleView.setRefreshing(false);
        mRecycleView.showContent(R.drawable.ic_notifications, e.getMessage() );
    }

    @Override
    public void onLoadSuccess(ArrayList<AbsUserState> list) {
        mRecycleView.showRecycleView();
        if (mRecycleView.isRefreshing()) {
            mAdapter.setListVoter(list);
            mRecycleView.setRefreshing(false);
        } else {
            mAdapter.addListVoter(list);
            mAdapter.setLoadMore(false);
        }

    }

    @Override
    public void setPresenter(ShowListUserContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }

    @Override
    public void onClickState(int pos) {
        if (AppSharePreference.getInstance(this).isUserLoggedIn()) {
            if (mAdapter.getUserAtPosition(pos).getSate() == UserDefine.STATE_FOLLOW) {
                onFollowUser(mAdapter.getUserAtPosition(pos).getUid());
                mAdapter.getUserAtPosition(pos).setSate(UserDefine.STATE_FOLLWING);
                mAdapter.notifyItemChanged(pos);
            } else if (mAdapter.getUserAtPosition(pos).getSate() == UserDefine.STATE_FOLLWING) {
                mUserUnfollow = mAdapter.getUserAtPosition(pos);
                mDialogConfirmUnfollow.show();
                if ( mUserUnfollow.getAvatar() != null &&  !mUserUnfollow.getAvatar().trim().isEmpty()) {
                    ImageLoader.loadImageViewWith(this, mUserUnfollow.getAvatar(), mAvatarUnfollow);
                } else {
                    ImageLoader.loadImageViewWith(this, R.drawable.ic_avatar_default, mAvatarUnfollow);
                }

                mTextViewUserNameUnfollow.setText(String.format(getResources().getString(R.string.unfollow_user), mUserUnfollow.getName()));
                mPositionUnfollow = pos;
            }
        } else {
            IntentUtils.startActivityLogin(null, this);
        }
    }

    @Override
    public void onClickUser(String id) {
        Bundle bundle = new Bundle();
        if (id.equals(AppSharePreference.getUid(this))) {
            bundle.putInt(ProfileActivity.BUNDLE_TYPE_USER, UserDefine.USER_TYPE_PROFILE);
        } else {
            bundle.putInt(ProfileActivity.BUNDLE_TYPE_USER, UserDefine.USER_TYPE_FRIENDS);
            bundle.putString(ProfileActivity.BUNDLE_USER_ID, id  );
        }
        IntentUtils.startActivityMyPage(bundle, this);
    }

    @Override
    public void onAgreeUnfollow() {
        super.onAgreeUnfollow();
        mAdapter.getUserAtPosition(mPositionUnfollow).setSate( UserDefine.STATE_FOLLOW);
        mAdapter.notifyItemChanged(mPositionUnfollow);
    }
}
