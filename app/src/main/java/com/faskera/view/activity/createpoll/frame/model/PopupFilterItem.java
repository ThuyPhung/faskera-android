/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.model;

import android.graphics.Bitmap;

public class PopupFilterItem {

    private FilterItem filterItem;
    private Bitmap bitmap;
    private String mTitle;
    private int mColor;

    public PopupFilterItem() {
    }

    public PopupFilterItem(FilterItem filterItem, Bitmap bitmap, String title,int mColor) {
        this.filterItem = filterItem;
        this.bitmap = bitmap;
        this.mTitle = title;
        this.mColor = mColor;
    }

    public PopupFilterItem(FilterItem filterItem, Bitmap bitmap, String title) {
        this.filterItem = filterItem;
        this.bitmap = bitmap;
        this.mTitle = title;
    }

    public FilterItem getFilterItem() {
        return filterItem;
    }

    public void setFilterItem(FilterItem filterItem) {
        this.filterItem = filterItem;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getNameTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public void setColor(int mColor) {
        this.mColor = mColor;
    }

    public int getColor() {
        return mColor;
    }
}
