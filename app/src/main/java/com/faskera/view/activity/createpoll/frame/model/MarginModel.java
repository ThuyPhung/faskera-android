/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.model;

public class MarginModel {

    private float top;

    private float bottom;

    private float right;

    private float left;

    public float getTop() {
        return top;
    }

    public void setTop(float top) {
        this.top = top;
    }

    public float getBottom() {
        return bottom;
    }

    public void setBottom(float bottom) {
        this.bottom = bottom;
    }

    public float getLeft() {
        return left;
    }

    public void setLeft(float left) {
        this.left = left;
    }

    public float getRight() {
        return right;
    }

    public void setRight(float right) {
        this.right = right;
    }

    public void setMargin(float margin, int position) {
        switch (position) {
            case 0:
                setTop(margin);
                break;
            case 1:
                setRight(margin);
                break;
            case 2:
                setBottom(margin);
                break;
            case 3:
                setLeft(margin);
                break;
            default:
                break;
        }
    }

    public boolean isMargin() {
        return getRight() + getBottom() + getTop() + getLeft() == 4;
    }
}
