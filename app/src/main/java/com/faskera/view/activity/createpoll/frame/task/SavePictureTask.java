/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.task;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;

import com.faskera.view.activity.createpoll.frame.utils.FileUtils;
import com.faskera.view.activity.createpoll.frame.widget.PhotoModelLayout;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SavePictureTask extends AsyncTask<Void, Void, String> {

    private Context mContext;
    private OnCallBackAsyncTaskListener mOnCallBackAsyncTaskListener;
    private PhotoModelLayout mPhotoModelLayout;

    public SavePictureTask(Context mContext, PhotoModelLayout mPhotoModelLayout) {
        this.mContext = mContext;
        this.mPhotoModelLayout = mPhotoModelLayout;
    }

    public void setOnCallBackAsyncTaskListener(OnCallBackAsyncTaskListener mOnCallBackAsyncTaskListener) {
        this.mOnCallBackAsyncTaskListener = mOnCallBackAsyncTaskListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (mOnCallBackAsyncTaskListener != null) {
            mOnCallBackAsyncTaskListener.onPreExecute();
        }
    }

    @Override
    protected String doInBackground(Void... params) {
        String filePath = null;
        try {

            Bitmap targetBitmap = FileUtils.createBitmapFromView(mPhotoModelLayout);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
            Date curTime = new Date();
            String fileName = "Faskera_" + dateFormat.format(curTime) + ".jpg";

            filePath = saveImage(FileUtils.FASKERA_DIR, fileName, targetBitmap);
        } catch (Exception | OutOfMemoryError e) {
            e.printStackTrace();
        }
        if (isLiveAsyncTask(this, mContext)) {
            return null;
        }
        return filePath;
    }

    @Override
    protected void onPostExecute(String path) {
        super.onPostExecute(path);
        if (mOnCallBackAsyncTaskListener != null) {
            mOnCallBackAsyncTaskListener.onPostExecute(path);
        }
//        if (isLiveAsyncTask(this, mContext)) {
//        }
    }

    private String saveImage(String folderName, String fileName, Bitmap bm) {
        if (null == bm) {
            return null;
        }

        File dir = new File(folderName);
        if (!dir.exists() && !dir.mkdirs()) {
            return null;
        }

        File file = new File(dir, fileName);
        OutputStream out = null;
        try {
            out = new BufferedOutputStream(new FileOutputStream(file));
            if (!bm.compress(Bitmap.CompressFormat.JPEG, 100, out)) {
                return null;
            }

            if (!bm.isRecycled()) {
                bm.recycle();
            }

            Intent mediaScannerIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            final Uri fileContentUri = Uri.fromFile(file);
            mediaScannerIntent.setData(fileContentUri);
            mContext.sendBroadcast(mediaScannerIntent);

            return file.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != out) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    private boolean isLiveAsyncTask(AsyncTask asyncTask, Context mContext) {
        return ((asyncTask != null && asyncTask.isCancelled()) || asyncTask == null || mContext == null);
    }
}