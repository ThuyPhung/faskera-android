/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.notification;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.faskera.common.AppSharePreference;
import com.faskera.common.UserDefine;
import com.faskera.common.base.BaseActivity;
import com.faskera.common.view.AnyTextview;
import com.faskera.era.R;
import com.faskera.utils.IntentUtils;
import com.faskera.view.activity.mypage.ProfileActivity;
import com.faskera.view.activity.notification.model.Notification;
import com.faskera.view.activity.polldetail.PollDetailActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class NotificationActivity extends BaseActivity
    implements NotificationAdapter.OnClickItemNotificationListener, NotificationContract.View,
    SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.image_back)
    ImageView mImageBack;
    @BindView(R.id.toolbar_title)
    AnyTextview mToolbarTitle;
    @BindView(R.id.recycle_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout mSwipeRefresh;
    @BindView(R.id.txt_no_data)
    AnyTextview mTxtNoData;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @OnClick(R.id.image_back)
    public void onClick() {
        onBackPressed();
    }

    private NotificationAdapter mAdapter;
    private NotificationContract.Presenter mPresenter;
    private boolean mIsDataAvailable = true;

    @Override
    protected int getIdLayout() {
        return R.layout.activity_notification;
    }

    @Override
    protected void initView() {
        mAdapter = new NotificationAdapter(this);
        mAdapter.setOnClickItemNotificationListener(this);
        mToolbarTitle.setText(getResources().getString(R.string.tb_notification));
        mSwipeRefresh.setOnRefreshListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int visibleItemCount = recyclerView.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                if ((totalItemCount - visibleItemCount) <= (firstVisibleItem + 3) &&
                    mIsDataAvailable) {
                    mAdapter.showLoadingMore();
                    mPresenter.subscribe();
                }
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        new NotificationPresenter(this, this);
        mPresenter.subscribe();
    }

    @Override
    protected void initData() {
        AppSharePreference.getInstance(this).setNotificationCounter(0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.unsubscibe();
    }

    @Override
    public void goToUserPoll(String pollID) {
        Bundle bundle = new Bundle();
        bundle.putString(PollDetailActivity.BUNDLE_POLL_ID, pollID);
        IntentUtils.startActivityDetail(this, bundle);
    }

    @Override
    public void goToYourPoll(String pollID) {
        Bundle bundle = new Bundle();
        bundle.putString(PollDetailActivity.BUNDLE_POLL_ID, pollID);
        IntentUtils.startActivityDetail(this, bundle);
    }

    @Override
    public void goToUserPage(String userID) {
        Bundle bundle = new Bundle();
        if (userID.equals(AppSharePreference.getUid(this))) {
            bundle.putInt(ProfileActivity.BUNDLE_TYPE_USER, UserDefine.USER_TYPE_PROFILE);
        } else {
            bundle.putInt(ProfileActivity.BUNDLE_TYPE_USER, UserDefine.USER_TYPE_FRIENDS);
            bundle.putString(ProfileActivity.BUNDLE_USER_ID, userID);
        }
        IntentUtils.startActivityMyPage(bundle, this);
    }

    @Override
    protected void onAccessTokenChange() {
        super.onAccessTokenChange();
    }

    @Override
    public void onRefresh() {
        mPresenter.refresh();
        mAdapter.setMoreDataAvailable(true);
        mIsDataAvailable = true;
    }

    @Override
    public void showLoading() {
        if (!mSwipeRefresh.isRefreshing()) {
            mProgressBar.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
        }
        mTxtNoData.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        if (mSwipeRefresh.isRefreshing()) mSwipeRefresh.setRefreshing(false);
        mRecyclerView.setVisibility(View.VISIBLE);
        mTxtNoData.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onError(int code) {
        handleErrorCode(code);
    }

    @Override
    public void onGetNotificationsSuccess(Notification notifications) {
        mAdapter.updateNotificationList(notifications.list);
    }

    @Override
    public void onGetNotificationEmpty() {
        mRecyclerView.setVisibility(View.GONE);
        mTxtNoData.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        mAdapter.setMoreDataAvailable(false);
    }

    @Override
    public void onGetMoreNotification(Notification data) {
        mAdapter.hideLoadingMore();
        mAdapter.updateMoreNotification(data);
    }

    @Override
    public void onGetAllNotification() {
        mAdapter.hideLoadingMore();
        mAdapter.setMoreDataAvailable(false);
        mIsDataAvailable = false;
    }

    @Override
    public void setPresenter(NotificationContract.Presenter presenter) {
        mPresenter = presenter;
    }
}
