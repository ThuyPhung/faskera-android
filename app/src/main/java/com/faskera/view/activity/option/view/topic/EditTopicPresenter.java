/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.topic;

import android.content.Context;

import com.faskera.common.base.ObserverAdvance;
import com.faskera.network.ApiManager;
import com.faskera.view.activity.topic.model.UpdateRequestBody;
import com.faskera.view.activity.topic.network.InterestedTopicResponse;
import com.faskera.view.activity.topic.network.UpdateTopicInterestResponse;

import java.util.ArrayList;

import rx.subscriptions.CompositeSubscription;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;

/**
 * @author theanh
 * @since 2/4/2017
 */
public class EditTopicPresenter implements EditTopicContract.Presenter {
    private Context mContext;
    private EditTopicContract.View mView;
    private CompositeSubscription mCompositeSubscription;

    public EditTopicPresenter(Context context, EditTopicContract.View view) {
        mContext = context;
        mView = view;
        mView.setPresenter(this);
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getTopics() {
        mView.showLoading();
        mCompositeSubscription.add(ApiManager.getInstance()
            .getInterestedTopic(mContext, new ObserverAdvance<InterestedTopicResponse>() {
                @Override
                public void onError(int code) {
                    if (mView == null) return;
                    mView.hideLoading();
                    mView.onErrorConnection(code);
                }

                @Override
                public void onSuccess(InterestedTopicResponse data) {
                    if (mView == null) return;
                    mView.hideLoading();
                    mView.onGetTopicsSuccess(data.mListTopic);
                }

                @Override
                public void onCompleted() {
                }
            }));
    }

    @Override
    public void updateInterestedTopics(ArrayList<String> listId) {
        mView.showLoading();
        mCompositeSubscription.add(ApiManager.getInstance()
            .updateTopicInterest(mContext, new UpdateRequestBody(listId),
                new ObserverAdvance<UpdateTopicInterestResponse>() {
                    @Override
                    public void onError(int code) {
                        if (mView == null) return;
                        mView.hideLoading();
                        if (code == HTTP_BAD_REQUEST) mView.onErrorUpdate();
                        else mView.onErrorConnection(code);
                    }

                    @Override
                    public void onSuccess(UpdateTopicInterestResponse data) {
                        if (mView == null) return;
                        mView.hideLoading();
                        mView.onUpdateTopicSuccess();
                    }

                    @Override
                    public void onCompleted() {
                    }
                }));
    }

    @Override
    public void subscribe() {
        getTopics();
    }

    @Override
    public void unsubscribe() {
        if (mCompositeSubscription != null && mCompositeSubscription.isUnsubscribed()) {
            mCompositeSubscription.unsubscribe();
            mView = null;
        }
    }
}
