/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.faskera.common.AppSharePreference;
import com.faskera.common.BasePollActivity;
import com.faskera.common.UserDefine;
import com.faskera.common.asyncdata.CursorAdapterOptimize;
import com.faskera.common.asyncdata.FooterType;
import com.faskera.common.asyncdata.ImlFeedCallback;
import com.faskera.common.debug.DebugUtil;
import com.faskera.common.feedflow.ActionCreator;
import com.faskera.common.feedflow.Dispatcher;
import com.faskera.common.feedflow.FeedAction;
import com.faskera.common.feedflow.FeedListener;
import com.faskera.common.view.AnyTextview;
import com.faskera.common.view.FrameRecycleView;
import com.faskera.database.FaskeraProvider;
import com.faskera.database.table.FeedTable;
import com.faskera.era.R;
import com.faskera.utils.IntentUtils;
import com.faskera.view.activity.discoverpeople.SearchFriendsActivity;
import com.faskera.view.activity.home.model.Feed;
import com.faskera.view.activity.home.model.ImlHomeRow;
import com.faskera.view.activity.home.model.abst.AbsFeedAns;
import com.faskera.view.activity.home.model.abst.FeedAbs;
import com.faskera.view.activity.mypage.ProfileActivity;
import com.faskera.view.activity.notification.NotificationActivity;
import com.faskera.view.widget.FlashDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author DANGNGOCDUC
 * @since 2/20/2017
 */
public class HomeActivityOptimize extends BasePollActivity implements HomeContract.View, ImlFeedCallback
        , SharedPreferences.OnSharedPreferenceChangeListener, FeedListener {

    private HomeContract.Presenter mPresenter;
    private long mLastTime;
    private LinearLayoutManager mLinearLayoutManager;
    private SharedPreferences mSharedPreferences;

    @BindView(R.id.txt_counter)
    AnyTextview mTxtCounter;

    @BindView(R.id.framerecycleview)
    public FrameRecycleView mRecycleView;

    @OnClick({R.id.app_icon, R.id.app_name})
    public void onClickTop(View view) {
        if (mRecycleView.isRefreshing()) return;

        if (!mRecycleView.scrollToTop()) {
            mRecycleView.setRefreshing(true);
            mPresenter.getNewFeed(true);
        }
    }

    @OnClick({R.id.action_search, R.id.action_see_notifications})
    public void onClick(View view) {

        if (AppSharePreference.getInstance(this).isUserLoggedIn()) {
            switch (view.getId()) {
                case R.id.action_search:
                    startActivity(new Intent(this, SearchFriendsActivity.class));
                    break;
                case R.id.action_see_notifications:
                    startActivity(new Intent(this, NotificationActivity.class));
                    break;
            }
        } else {
            IntentUtils.startActivityLogin(null, this);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Dispatcher.register(this);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mSharedPreferences.registerOnSharedPreferenceChangeListener(this);
        if (AppSharePreference.getInstance(this).isFirstTimeUsing()) {
            new FlashDialog(this).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        int counterNotification = AppSharePreference.getInstance(this).getNotificationCounter();
        if (counterNotification == 0) mTxtCounter.setVisibility(View.GONE);
        else {
            mTxtCounter.setVisibility(View.VISIBLE);
            mTxtCounter.setText(String.valueOf(counterNotification));
        }
    }

    @OnClick(R.id.btn_my_page)
    public void gotoMyPage() {
        if (AppSharePreference.getInstance(this).isUserLoggedIn()) {
            Bundle bundle = new Bundle();
            bundle.putInt(ProfileActivity.BUNDLE_TYPE_USER, UserDefine.USER_TYPE_PROFILE);
            IntentUtils.startActivityMyPage(bundle, this);
        } else {
            IntentUtils.startActivityLogin(null, this);
        }

    }

    private CursorAdapterOptimize mAdapterHome;

    @Override
    protected int getIdLayout() {
        return R.layout.activity_home;
    }

    @Override
    protected void initView() {

    }

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            mPresenter.getNewFeed(true);
        }
    };

    @Override
    protected void initData() {
        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecycleView.getRecycleView().setLayoutManager(mLinearLayoutManager);
        mRecycleView.setOnSwipeRefreshLayout(onRefreshListener);
        mAdapterHome = new CursorAdapterOptimize(this);
        mRecycleView.getRecycleView().setAdapter(mAdapterHome);
        mRecycleView.getRecycleView().setItemViewCacheSize(10);
        mRecycleView.getRecycleView().setHasFixedSize(true);
        mRecycleView.getRecycleView().setDrawingCacheEnabled(true);
        mRecycleView.getRecycleView().setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mAdapterHome.setCallbackImageFeed(this);
        mRecycleView.getRecycleView().addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int totalItemCount = mLinearLayoutManager.getItemCount();
                    int lastVisibleItem = mLinearLayoutManager.findLastVisibleItemPosition();

                    if (mAdapterHome.getTypeLoadMore() == FooterType.TYPE_LOADMORE_NONE && totalItemCount <= (lastVisibleItem + 3)) {
                        mPresenter.getNewFeed(false);
                    }

                }
            }
        });
        new HomePresenter(this, this);
        mRecycleView.showRecycleView();
        mRecycleView.enableScrollToTop();
        ArrayList<Feed> list = mPresenter.getFeedFromDB();
        ArrayList<ImlHomeRow> listRow = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            listRow.addAll(list.get(i).genListItem(AppSharePreference.getInstance(this).isUserLoggedIn()));
        }
        mAdapterHome.insertListPoll(listRow);
        mPresenter.getNewFeed(false);
    }

    @Override
    public void onLoading() {
        if (mAdapterHome.getItemCount() > 0) {
            mAdapterHome.setTypeLoadMore(FooterType.TYPE_LOADMORE);
            mRecycleView.showRecycleView();
        } else {
            mRecycleView.showLoading();
        }
    }

    @Override
    public void onLoadMore() {
        mAdapterHome.setTypeLoadMore(FooterType.TYPE_LOADMORE);
    }

    @Override
    public void onLoadError(int code) {
        handleErrorCode(code);
    }

    @Override
    public void onLoadError(Throwable e) {
        if (DebugUtil.IS_DEBUG) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
        }
        if (mRecycleView.isRefreshing()) {
            mRecycleView.setRefreshing(false);
        }
    }

    @Override
    public void onGetFeedSuccess(ArrayList<Feed> list, String nmaxID) {
        ContentResolver contentResolver = getContentResolver();
        if (nmaxID == null || nmaxID.trim().isEmpty()) {
            // Firt load : remove database
            mRecycleView.showRecycleView();
            if (mRecycleView.isRefreshing()) {
                mRecycleView.setRefreshing(false);
            }
            contentResolver.delete(FaskeraProvider.CONTENT_URI_FEED, null, null);
            mAdapterHome.clearData();
        }

        ArrayList<ImlHomeRow> listRow = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            mLastTime = System.currentTimeMillis() + i;
            ContentValues contentValues = list.get(i).toContentValue(true);
            contentValues.put(FeedTable.TIME_MODIFIED, mLastTime + i);
            String id = list.get(i).mObjectId;
            Cursor cursor = contentResolver.query(FaskeraProvider.CONTENT_URI_FEED, null, FeedTable._ID + "= '" + id + "'", null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                contentResolver.update(FaskeraProvider.CONTENT_URI_FEED, contentValues, FeedTable._ID + "= '" + id + "'", null);
            } else {
                contentResolver.insert(FaskeraProvider.CONTENT_URI_FEED, contentValues);
            }
            if (cursor != null)
                cursor.close();
            listRow.addAll(list.get(i).genListItem(AppSharePreference.getInstance(this).isUserLoggedIn()));
        }
        mAdapterHome.setTypeLoadMore(FooterType.TYPE_LOADMORE_NONE);

        mAdapterHome.insertListPoll(listRow);

    }

    @Override
    public void setPresenter(HomeContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onClickAsw(AbsFeedAns absFeedAns) {
        super.onClickAsw(absFeedAns);
        if (mAdapterHome.getIndex(absFeedAns) > 0) {
            if (absFeedAns.getNbSeq() == 0) {
                mAdapterHome.notifyItemRangeChanged(mAdapterHome.getIndex(absFeedAns), absFeedAns.getPoll().getFeedAns().size() + 2);
            } else {
                mAdapterHome.notifyItemRangeChanged(mAdapterHome.getIndex(absFeedAns) - absFeedAns.getNbSeq(), absFeedAns.getPoll().getFeedAns().size() + 2);
            }
        } else {
            mAdapterHome.notifyDataSetChanged();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Dispatcher.unRegister(this);
        mPresenter.onDestroy();
        mSharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
    }


    @Override
    public void onCreatePollSuccess(Feed.Poll poll) {
        mAdapterHome.insertListPoll(poll.genListItem(), 3);
    }

    @Override
    protected void onDelPollResult(String pollID, boolean result) {
        super.onDelPollResult(pollID, result);
        if (result) {
            mAdapterHome.removePoll(pollID);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        if (key.equals(AppSharePreference.NOTIFICATION_COUNTER)) {
            int counterNotification = AppSharePreference.getInstance(this).getNotificationCounter();
            if (counterNotification == 0) mTxtCounter.setVisibility(View.GONE);
            else {
                mTxtCounter.setVisibility(View.VISIBLE);
                mTxtCounter.setText(String.valueOf(counterNotification));
            }
        }
    }

    @Override
    public void onSateChange(FeedAction action) {
        switch (action.getType()) {
            case ActionCreator.ADD:
                if (action.getData() != null && action.getData().getPoll() != null) {
                    onCreatePollSuccess(action.getData().getPoll());
                }
                break;
            case ActionCreator.REMOVE:
                break;
            case ActionCreator.CHANGE:
                AbsFeedAns item = mAdapterHome.findFeedAns(action.getData().getPoll().getFeedAns().get(0).getAnsID());
                if (item != null) {
                    FeedAbs abs = item.getPoll();
                    abs.getPoll().cloneSateObject(action.getData());
                    mAdapterHome.notifyDataSetChanged();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UPDATE_TOPIC_REQUEST_CODE && resultCode == RESULT_OK) {
            mPresenter.getNewFeed(true);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == CODE_CREATE_POLL) {
            Feed.Poll poll = (Feed.Poll) data.getSerializableExtra(BUNDLE_POLL_CREATE);
            onCreatePollSuccess(poll);
        }

    }

    @Override
    protected void onAccessTokenChange() {
        super.onAccessTokenChange();
        if (mRecycleView.isRefreshing()) {
            mPresenter.getNewFeed(true);
        } else {
            mPresenter.getNewFeed(false);
        }
    }

}
