/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home;

import com.faskera.mvp.BasePresenter;
import com.faskera.mvp.BaseView;
import com.faskera.view.activity.home.model.Feed;

import java.util.ArrayList;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public interface HomeContract {

    interface Presenter extends BasePresenter {

        ArrayList<Feed>  getFeedFromDB();

        void getNewFeed(boolean isRefresh);

        String getNextMaxId();

        void resetNextMaxId();

        void onDestroy();

    }

    interface View extends BaseView<Presenter> {
        void onLoading();

        void onLoadMore();

        void onLoadError(int code);

        void onLoadError(Throwable e);

        void onGetFeedSuccess(ArrayList<Feed> list, String nmaxID);
    }
}
