/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.discoverpeople;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.faskera.callback.OnClickItemToolbarListener;
import com.faskera.common.AppSharePreference;
import com.faskera.common.UserDefine;
import com.faskera.common.base.BaseActivity;
import com.faskera.common.view.AnyTextview;
import com.faskera.era.R;
import com.faskera.model.option.TbOption;
import com.faskera.utils.ImageLoader;
import com.faskera.utils.IntentUtils;
import com.faskera.view.activity.mypage.ProfileActivity;
import com.faskera.view.activity.option.view.fbfriend.model.Friend;
import com.faskera.view.widget.ToolbarOptions;

import java.util.ArrayList;

import butterknife.BindView;

public class SearchFriendsActivity extends BaseActivity
    implements SearchFriendsContract.View, SearchFriendsAdapter.SearchFriendsAdapterListener {
    private static final String LOG_TAG = SearchFriendsActivity.class.getSimpleName();
    @BindView(R.id.recycle_view)
    RecyclerView mRecycleView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout mSwipeRefresh;
    @BindView(R.id.txt_no_data)
    AnyTextview mTextNoData;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.toolbar)
    ToolbarOptions mToolbar;
    @BindView(R.id.text_error_connection)
    AnyTextview mTextErrorConnection;
    private SearchFriendsAdapter mAdapter;
    private SearchFriendsContract.Presenter mPresenter;
    //position of friend we might want unfollow
    public int friendPosition;

    @Override
    protected int getIdLayout() {
        return R.layout.activity_search_friends;
    }

    @Override
    protected void initView() {
        mToolbar.setToolbarModel(new TbOption(getString(R.string.tb_discover_people), false));
        mToolbar.setOnClickItemToolbarListener(new OnClickItemToolbarListener() {
            @Override
            public void onBack() {
                finish();
            }

            @Override
            public void onDone() {
            }
        });
        mAdapter = new SearchFriendsAdapter(this, new ArrayList<>());
        mAdapter.setSearchFriendsAdapterListener(this);
        mRecycleView.setLayoutManager(new LinearLayoutManager(this));
        mRecycleView.setAdapter(mAdapter);
        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefresh.setRefreshing(true);
                mPresenter.subscribe();
            }
        });
        new SearchFriendsPresenter(this, this);
        mPresenter.subscribe();
    }

    @Override
    protected void initData() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.unsubscribe();
    }

    /**
     * View
     */
    @Override
    public void onGetFriendsSuccess(ArrayList<Friend> listFriends) {
        mRecycleView.setVisibility(View.VISIBLE);
        mTextNoData.setVisibility(View.GONE);
        mAdapter.updateListFriends(listFriends);
    }

    @Override
    public void onGetFriendsEmpty() {
        mRecycleView.setVisibility(View.GONE);
        mTextNoData.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFollowSuccess() {
        Log.d(LOG_TAG, "Follow Success");
    }

    @Override
    public void onUnfollowSuccess() {
        Log.d(LOG_TAG, "Unfollow");
    }

    @Override
    public void showLoading() {
        if (!mSwipeRefresh.isRefreshing()) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoading() {
        if (mSwipeRefresh.isRefreshing())
            mSwipeRefresh.setRefreshing(false);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onErrorConnection() {
        mTextNoData.setVisibility(View.GONE);
        mRecycleView.setVisibility(View.GONE);
        mTextErrorConnection.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFollowFailed() {
        Toast.makeText(this, getString(R.string.error_connection_not_found), Toast.LENGTH_SHORT)
            .show();
    }

    @Override
    public void setPresenter(SearchFriendsContract.Presenter presenter) {
        mPresenter = presenter;
    }

    /**
     * Adapter callback
     */
    @Override
    public void onClickBtnFollow(String userId, int position) {
        mPresenter.followFriend(userId);
        mAdapter.updateFollowUser(position);
    }

    @Override
    public void onClickBtnUnfollow(Friend friend, int position) {
        mUserUnfollow = friend;
        mDialogConfirmUnfollow.show();
        ImageLoader.loadImageViewWith(this, mUserUnfollow.getAvatar(), mAvatarUnfollow);
        mTextViewUserNameUnfollow.setText(String
            .format(getResources().getString(R.string.unfollow_user), mUserUnfollow.getName()));
        friendPosition = position;
    }

    @Override
    public void onAgreeUnfollow() {
        super.onAgreeUnfollow();
        mAdapter.updateUnfollowUser(friendPosition);
    }

    @Override
    public void onClickProfile(String userId) {
        Bundle bundle = new Bundle();
        if (userId.equals(AppSharePreference.getUid(this))) {
            bundle.putInt(ProfileActivity.BUNDLE_TYPE_USER, UserDefine.USER_TYPE_PROFILE);
        } else {
            bundle.putInt(ProfileActivity.BUNDLE_TYPE_USER, UserDefine.USER_TYPE_FRIENDS);
            bundle.putString(ProfileActivity.BUNDLE_USER_ID, userId);
        }
        IntentUtils.startActivityMyPage(bundle, this);
    }
}
