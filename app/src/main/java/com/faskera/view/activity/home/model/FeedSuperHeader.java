/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home.model;

import android.content.Context;
import android.text.Spanned;

import com.faskera.era.R;
import com.faskera.utils.TextViewUtils;
import com.faskera.view.activity.home.model.abst.AbsFeedSuperHeader;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public class FeedSuperHeader extends AbsFeedSuperHeader {


    public Feed.LastActivity mActivity;

    public FeedSuperHeader(Feed.LastActivity activity) {
        mTypeSuperHeader = FOLLWING;
        mActivity = activity;
    }

    public FeedSuperHeader() {
        mTypeSuperHeader = TRENDING;
    }

    @Override
    public String getStringUid() {
        if (mActivity.mUser != null) {
            return mActivity.mUser.id;
        } else {
            return null;
        }
    }

    @Override
    public Spanned getContent(Context context) {
        if (mTypeSuperHeader == FOLLWING) {
            if (mActivity.mUser != null) {
                // StringBuffer mContent = new StringBuffer(mActivity.mUser.fullName);
                if (mActivity.mActionType.equals("answer")) {
                  //  return String.format(context.getResources().getString(R.string.feed_header_answer), mActivity.mUser.fullName);
                    return TextViewUtils.buildHtmlForm(
                            "<b>" + mActivity.mUser.fullName + " </b>"
                                    + context.getResources().getString(R.string.feed_header_answer));
                } else if (mActivity.mActionType.equals("create")) {
                   // return String.format(context.getResources().getString(R.string.feed_header_create), mActivity.mUser.fullName);
                    return TextViewUtils.buildHtmlForm(
                            "<b>" + mActivity.mUser.fullName + " </b>"
                                    + context.getResources().getString(R.string.feed_header_create));
                } else {
                    return TextViewUtils.buildHtmlForm("");
                }
            }
        } else {
            return TextViewUtils.buildHtmlForm(context.getResources().getString(R.string.trending));
        }

        return TextViewUtils.buildHtmlForm("");
    }
}
