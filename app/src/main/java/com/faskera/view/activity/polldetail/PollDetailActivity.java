/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.polldetail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.TextView;

import com.faskera.common.AppSharePreference;
import com.faskera.common.BasePollActivity;
import com.faskera.common.asyncdata.CursorAdapterOptimize;
import com.faskera.common.feedflow.ActionCreator;
import com.faskera.common.feedflow.Dispatcher;
import com.faskera.common.feedflow.FeedAction;
import com.faskera.common.feedflow.FeedListener;
import com.faskera.common.view.FrameRecycleView;
import com.faskera.era.R;
import com.faskera.view.activity.home.model.Feed;
import com.faskera.view.activity.home.model.ImlHomeRow;
import com.faskera.view.activity.home.model.abst.AbsFeedAns;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author DANGNGOCDUC
 * @since 1/4/2017
 */
public class PollDetailActivity extends BasePollActivity
    implements PollDetailContract.View, FeedListener {
    public static String BUNDLE_POLL_ID = "_id_poll";
    public static String BUNDLE_POLL = "_poll";
    public static final String BUNDLE_GO_THROUGH_NOTIFICATION = "BUNDLE_GO_THROUGH_NOTIFICATION";
    private PollDetailContract.Presenter mPresenter;
    @BindView(R.id.framerecycleview)
    public FrameRecycleView mRecycleView;

    @OnClick(R.id.image_back)
    public void onClickBack() {
        finish();
    }

    @BindView(R.id.toolbar_text_done)
    public TextView mTextDone;
    @BindView(R.id.toolbar_title)
    public TextView mTextViewTitle;
    private CursorAdapterOptimize mAdapterHome;

    @Override
    protected int getIdLayout() {
        return R.layout.activity_poll_detail;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Dispatcher.register(this);
    }

    @Override
    protected void initView() {
    }

    @Override
    protected void initData() {
        mTextDone.setVisibility(View.GONE);
        mTextViewTitle.setText(getResources().getString(R.string.tb_poll));
        new PollDetailPresenter(this, this);
        LinearLayoutManager linearLayoutManager =
            new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecycleView.getRecycleView().setLayoutManager(linearLayoutManager);
        mAdapterHome = new CursorAdapterOptimize(this);
        mAdapterHome.setCallbackImageFeed(this);
        mRecycleView.getRecycleView().setAdapter(mAdapterHome);
        if (getIntent().getExtras().getSerializable(BUNDLE_POLL) != null) {
            mAdapterHome.insertListPoll(((Feed.Poll)getIntent().getExtras().getSerializable(BUNDLE_POLL)).genListItem());
            mRecycleView.showRecycleView();
        }
        mPresenter.getPollDetail(getIntent().getExtras().getString(BUNDLE_POLL_ID));
        if (getIntent().getExtras().getBoolean(BUNDLE_GO_THROUGH_NOTIFICATION, false)) {
            AppSharePreference.getInstance(this).setNotificationCounter(-1);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Dispatcher.unRegister(this);
        mPresenter.onDestroy();
    }

    @Override
    public void onLoading() {
        if (mAdapterHome.getItemCount() <= 0)
        mRecycleView.showLoading();
    }

    @Override
    public void onLoadError(int e) {
        if (mAdapterHome.getItemCount() <=0 )
        mRecycleView.showContent(R.drawable.ic_cloud_off, R.string.error_connection);
    }

    @Override
    public void onSuccess(ArrayList<ImlHomeRow> list) {
        mAdapterHome.clearData();
        mAdapterHome.insertListPoll(list);
        mRecycleView.showRecycleView();
    }

    @Override
    public void onCreatePollSuccess(Feed.Poll poll) {
    }

    @Override
    public void onClickAsw(AbsFeedAns absFeedAns) {
        super.onClickAsw(absFeedAns);
        mAdapterHome.notifyDataSetChanged();
        if (AppSharePreference.getInstance(this).isUserLoggedIn()) {
            if (mAdapterHome.getIndex(absFeedAns) > 0) {
                if (absFeedAns.getNbSeq() == 0) {
                    mAdapterHome.notifyItemRangeChanged(mAdapterHome.getIndex(absFeedAns),
                        absFeedAns.getPoll().getFeedAns().size() + 2);
                } else {
                    mAdapterHome.notifyItemRangeChanged(
                        mAdapterHome.getIndex(absFeedAns) - absFeedAns.getNbSeq(),
                        absFeedAns.getPoll().getFeedAns().size() + 2);
                }
            } else {
                mAdapterHome.notifyDataSetChanged();
            }
            Dispatcher.sendEvent(this,
                ActionCreator.createActionFeed(ActionCreator.CHANGE, absFeedAns.getPoll()));
        }
    }

    @Override
    public void setPresenter(PollDetailContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onSateChange(FeedAction action) {
    }

    @Override
    protected void onDelPollResult(String pollID, boolean result) {
        super.onDelPollResult(pollID, result);
        if (result) finish();
    }
}
