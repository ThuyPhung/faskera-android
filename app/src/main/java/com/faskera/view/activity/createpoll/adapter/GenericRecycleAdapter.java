/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.faskera.era.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class GenericRecycleAdapter<T, K extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter {

    public enum RecyclerViewType {
        VIEW_TYPE_ITEM(0),
        VIEW_TYPE_LOADING(1);

        int mValue;

        RecyclerViewType(int value) {
            mValue = value;
        }

        public int value() {
            return mValue;
        }
    }

    protected List<T> mList;
    protected Context mContext;
    protected boolean isMore;

    public GenericRecycleAdapter(List<T> mList, Context context) {
        this.mList = mList;
        this.mContext = context;
    }

    public abstract int getLayout();

    protected int getLayoutLoadMore() {
        return R.layout.layout_loading_item;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == RecyclerViewType.VIEW_TYPE_LOADING.value()) {
            View view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(getLayoutLoadMore(), parent, false);
            return new LoadingViewHolder(view);
        }
        View v = LayoutInflater.from(parent.getContext()).inflate(getLayout(), parent, false);
        return getHolderViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof LoadingViewHolder) {
            Glide.with(mContext)
                    .load(R.drawable.ic_faskera_notification_icon)
                    .into(((LoadingViewHolder) holder).mLoadMore);
        } else {
            onSet(mList.get(position), (K) holder, position);
        }
    }

    protected abstract RecyclerView.ViewHolder getHolderViewHolder(View v);

    protected abstract void onItem(T item, int position);

    protected abstract void onSet(T item, K holder, int position);

    public void setDataSource(List<T> item) {
        mList = item;
        notifyDataSetChanged();
    }

    public void addDataSource(List<T> item) {
        if (mList == null) {
            mList = new ArrayList<>();
        }

        if (item != null) {
            mList.addAll(item);
            notifyDataSetChanged();
        }
    }

    public boolean isMoreData() {
        return isMore;
    }

    public void setMoreData(boolean isMoreData) {
        this.isMore = isMoreData;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (isMoreData()) {
            return mList == null ? 1 : mList.size() + 1;
        }

        return mList == null ? 0 : mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == getItemCount() - 1 && isMoreData())
                ? RecyclerViewType.VIEW_TYPE_LOADING.value()
                : RecyclerViewType.VIEW_TYPE_ITEM.value();
    }

    public static class LoadingViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_loading)
        ImageView mLoadMore;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}