/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.home.model.abst;

import com.faskera.view.activity.home.model.ImlHomeRow;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public abstract class AbsFeedFooter implements ImlHomeRow {

    public abstract FeedAbs getPoll();

    public abstract String getPollID();

    public abstract String getVote();

    public abstract int getVoteInt();

    /**
     *
     * @return link share facebook
     */
    public abstract String getLink();

    @Override
    public int getType() {
        return TYPE_FOOTER;
    }
}
