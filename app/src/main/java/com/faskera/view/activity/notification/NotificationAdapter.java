/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.notification;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.faskera.common.view.AnyTextview;
import com.faskera.era.R;
import com.faskera.utils.HumanTime;
import com.faskera.utils.ImageLoader;
import com.faskera.view.activity.notification.model.List;
import com.faskera.view.activity.notification.model.Notification;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.faskera.Constant.Notification.ACTION_ANSWER;
import static com.faskera.Constant.Notification.ACTION_CREATE;
import static com.faskera.Constant.Notification.ACTION_FOLLOW;
import static com.faskera.Constant.Notification.ACTION_SHARE;
import static com.faskera.Constant.Notification.ACTION_START;

/**
 * @author framgia
 * @since 14/02/2017
 */
public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<List> mListNotifications;
    private Context mContext;
    private OnClickItemNotificationListener mListener;
    private boolean isShowingLoadMore = false, isMoreDataAvailable = true;
    private LayoutInflater mInflater;
    private static final int TYPE_LOAD = 3;
    private static final int TYPE_NOTIFICATION = 4;

    public interface OnClickItemNotificationListener {
        void goToUserPoll(String pollID);
        void goToYourPoll(String pollID);
        void goToUserPage(String userID);
    }

    public NotificationAdapter(Context context) {
        mContext = context;
        mListNotifications = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mInflater == null) mInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case TYPE_NOTIFICATION:
                return new NotificationHolder(
                    mInflater.inflate(R.layout.item_view_notification, parent, false));
            case TYPE_LOAD:
                return new LoadingHolder(
                    mInflater.inflate(R.layout.item_view_loading, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_NOTIFICATION)
            ((NotificationHolder) holder).bind(position);
    }

    @Override
    public int getItemCount() {
        return mListNotifications != null ? mListNotifications.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return mListNotifications.get(position) == null ? TYPE_LOAD : TYPE_NOTIFICATION;
    }

    public void updateNotificationList(java.util.List<List> notifications) {
        mListNotifications.clear();
        mListNotifications.addAll(notifications);
        notifyDataSetChanged();
    }

    public void hideLoadingMore() {
        //if loading more is enable
        if (isShowingLoadMore) {
            mListNotifications.remove(getItemCount() - 1);
            notifyDataSetChanged();
            isShowingLoadMore = false;
        }
    }

    public void showLoadingMore() {
        // If loading more is not enable and there still data available
        if (!isShowingLoadMore && isMoreDataAvailable) {
            //enable loading more;
            isShowingLoadMore = true;
            //add loading to bottom
            mListNotifications.add(null);
            notifyItemInserted(mListNotifications.size() - 1);
        }
    }

    public void updateMoreNotification(Notification notifications) {
        int currentListSize = mListNotifications.size();
        mListNotifications.addAll(notifications.list);
        notifyItemRangeChanged(currentListSize, mListNotifications.size());
    }

    public void setOnClickItemNotificationListener(OnClickItemNotificationListener listener) {
        mListener = listener;
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    public class LoadingHolder extends RecyclerView.ViewHolder {
        public LoadingHolder(View itemView) {
            super(itemView);
        }
    }

    public class NotificationHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_avatar)
        ImageView mImageAvatar;
        @BindView(R.id.text_notification_content)
        AnyTextview mTextNotificationContent;
        @BindView(R.id.text_notification_time)
        AnyTextview mTextNotificationTime;
        @BindView(R.id.linear_notification)
        LinearLayout mLinearNotification;

        public NotificationHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(int position) {
            List list = mListNotifications.get(position);
            if (list == null) return;
            mTextNotificationContent.setText(getActionNotificationContent(list.action, list));
            mTextNotificationTime.setText(new HumanTime().timeAgo(list.time));
            if (list.activities.get(0).actorUser.profilePhoto != null) {
                ImageLoader.loadImageViewWith
                    (mContext, list.activities.get(0).actorUser.profilePhoto.original,
                        mImageAvatar);
            } else {
                ImageLoader.loadImageViewWith
                    (mContext, R.drawable.ic_avatar_default, mImageAvatar);
            }
            mLinearNotification.setOnClickListener(v -> {
                if (mListener != null) {
                    switch (list.action) {
                        case ACTION_ANSWER:
                            mListener.goToYourPoll(list.targetPoll.id);
                            break;
                        case ACTION_CREATE:
                            mListener.goToUserPoll(list.targetPoll.id);
                            break;
                        case ACTION_SHARE:
                            mListener.goToUserPoll(list.targetPoll.id);
                            break;
                        case ACTION_FOLLOW:
                            mListener.goToUserPage(list.activities.get(0).actorUser.id);
                            break;
                        case ACTION_START:
                            mListener.goToUserPage(list.targetUser.getUid());
                            break;
                    }
                }
            });
        }

        private CharSequence getActionNotificationContent(String action, List list) {
            String firstReactor = list.activities.get(0).actorUser.fullname;
            String pollQuestion = list.targetPoll != null ? list.targetPoll.quote : "";
            int numberActivities = list.activities.size();
            String secondReactor =
                numberActivities > 1 ? list.activities.get(1).actorUser.fullname : "";
            switch (action) {
                case ACTION_ANSWER:
                    switch (numberActivities) {
                        case 1:
                            return NotificationContentUtils
                                .oneVotePoll(mContext, firstReactor, pollQuestion);
                        case 2:
                            return NotificationContentUtils
                                .twoVotePoll(mContext, firstReactor, secondReactor, pollQuestion);
                        default:
                            return NotificationContentUtils
                                .manyVotePoll(mContext, firstReactor, numberActivities,
                                    pollQuestion);
                    }
                case ACTION_SHARE:
                    switch (numberActivities) {
                        case 1:
                            return NotificationContentUtils
                                .oneSharePoll(mContext, firstReactor, pollQuestion);
                        case 2:
                            return NotificationContentUtils
                                .twoSharePoll(mContext, firstReactor, secondReactor, pollQuestion);
                        default:
                            return NotificationContentUtils
                                .manySharePoll(mContext, firstReactor, numberActivities,
                                    pollQuestion);
                    }
                case ACTION_CREATE:
                    return NotificationContentUtils
                        .createPoll(mContext, firstReactor, pollQuestion);
                case ACTION_FOLLOW:
                    return NotificationContentUtils.following(mContext, firstReactor);
                case ACTION_START:
                    return NotificationContentUtils.newFbFriendUsing(mContext, firstReactor);
                default:
                    return null;
            }
        }
    }
}
