/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.option.view.profile;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.faskera.common.view.AnyTextview;
import com.faskera.era.R;
import com.faskera.view.activity.option.OptionActivity;
import com.faskera.view.activity.profile.EditProfileActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author framgia
 * @since 10/02/2017
 */
public class EditPhotoDialog extends BottomSheetDialogFragment {
    @BindView(R.id.text_change_photo)
    AnyTextview mTextChangePhoto;
    @BindView(R.id.text_remove_photo)
    AnyTextview mTextRemovePhoto;
    @BindView(R.id.text_cancel)
    AnyTextview mTextCancel;
    private EditPhotoBottomSheetCallback mCallback;

    public interface EditPhotoBottomSheetCallback {
        void onChangePhotoClick();
        void onRemovePhotoClick();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OptionActivity || context instanceof EditProfileActivity) {
            mCallback = (EditPhotoBottomSheetCallback) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = LayoutInflater.from(getContext())
            .inflate(R.layout.dialog_edit_profile_photo, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @OnClick({R.id.text_change_photo, R.id.text_remove_photo, R.id.text_cancel})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_change_photo:
                mCallback.onChangePhotoClick();
                break;
            case R.id.text_remove_photo:
                mCallback.onRemovePhotoClick();
                break;
            case R.id.text_cancel:
                break;
        }
        dismiss();
    }
}
