/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.mypage;

import com.faskera.mvp.BasePresenter;
import com.faskera.mvp.BaseView;
import com.faskera.view.activity.home.model.Feed;
import com.faskera.view.activity.mypage.abs.AbsProfile;

import java.util.ArrayList;

/**
 * @author DANGNGOCDUC
 * @since 1/4/2017
 */

public class ProfileContract {

    interface Presenter extends BasePresenter {
        AbsProfile getAbsProfile();

        ArrayList<Feed.Poll> getPollOfUserDB();

        void getUserInfo();

        void getPollOfUser();

        String getUid();

        void onUpdateInfo();
    }

    interface View extends BaseView<Presenter> {

        void loadViewForUserFriends(boolean isFriends);

        void onLoadInfo();

        void onLoadInfoSuccess(AbsProfile profile, boolean isOnline);

        void onLoadInfoError(int code);

        void onLoadPoll();

        void onLoadPollError(int e);

        void onLoadPollSuccess(ArrayList<Feed.Poll> list, boolean isOnline);

        void onUpdateInfoSuccess(AbsProfile profile);

    }


}
