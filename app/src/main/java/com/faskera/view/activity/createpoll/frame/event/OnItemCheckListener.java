/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.event;

import com.faskera.view.activity.createpoll.frame.model.Photo;

public interface OnItemCheckListener {
    /***
     * @param position          The position of the selected image
     * @param path              The selected image
     * @param isCheck           the current state
     * @param selectedItemCount The selected number
     * @return enable check
     */
    boolean OnItemCheck(int position, Photo path, boolean isCheck, int selectedItemCount);

}
