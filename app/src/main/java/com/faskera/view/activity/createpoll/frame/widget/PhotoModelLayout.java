/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.activity.createpoll.frame.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.faskera.era.R;
import com.faskera.view.activity.createpoll.frame.event.OnFilterItemClickListener;
import com.faskera.view.activity.createpoll.frame.event.OnItemClickListener;
import com.faskera.view.activity.createpoll.frame.model.FrameModel;
import com.faskera.view.activity.createpoll.frame.model.MarginModel;
import com.faskera.view.activity.createpoll.frame.utils.TemplateEntity;
import com.faskera.view.activity.createpoll.frame.utils.ViewUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;

public class PhotoModelLayout extends RelativeLayout {
    private static final float DRAG_SCALE = 1.05f;

    private Context context;
    private Rect curRect;
    private PhotoModelView curModelView;
    private int curSelection;
    private boolean showSelected = false;
    private PhotoPopupWindow popupWindow;

    private List<String> imagePaths;
    private List<String> positionList;
    private List<Bundle> bundleList;
    private List<Rect> boundaryList;
    private List<PhotoModelView> modelViewList;
    private List<String> marginList;

    private int downX;
    private int downY;
    private int windowX;
    private int windowY;
    private int winViewX;
    private int winViewY;
    private boolean isMoving = false;

    private ImageView dragImageView;
    private WindowManager windowManager;
    private WindowManager.LayoutParams windowParams;
    private boolean isEdit;
    private ArrayList<MarginModel> marginModelList;

    public PhotoModelLayout(Context context) {
        super(context);
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.touch_slot_layout, this);

        init(false);
    }

    public PhotoModelLayout(Context context, boolean showPopup) {
        super(context);
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.touch_slot_layout, this);

        init(showPopup);
    }

    public PhotoModelLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.touch_slot_layout, this);

        init(false);
    }

    public PhotoModelLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.touch_slot_layout, this);

        init(false);
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(state);

        for (int i = bundleList.size() - 1; i >= 0; i--) {
            modelViewList.get(i).restoreInstanceState(bundleList.get(i));
        }
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        if (null == bundleList) {
            bundleList = new ArrayList<>();
        } else {
            bundleList.clear();
        }

        for (PhotoModelView slotView : modelViewList) {
            bundleList.add(slotView.saveInstanceState());
        }

        return super.onSaveInstanceState();
    }

    private void init(Boolean showPopup) {
        if (showPopup) {
            popupWindow = new PhotoPopupWindow(context);
        }
        boundaryList = new ArrayList<>();
        modelViewList = new ArrayList<>();
        bundleList = new ArrayList<>();
    }

    /**
     *
     */
    public void dismissPopup() {
        if (null != popupWindow && popupWindow.isShowing()) {
            popupWindow.dismiss();
        }
    }

    /**
     * @param onFilterItemClickListener
     */
    public void setOnPopupFilterItemClickListener(final OnFilterItemClickListener onFilterItemClickListener) {
        if (null == popupWindow) {
            return;
        }

        popupWindow.setOnFilterItemClickListener(new OnFilterItemClickListener() {
            @Override
            public void onItemClick(View view, int position, GPUImageFilter filter) {
                curModelView.setCurFilterPosition(position);

                if (null != onFilterItemClickListener) {
                    onFilterItemClickListener.onItemClick(view, position, filter);
                }
            }
        });
    }

    public void setCurFilterPosition(int position) {
        curModelView = modelViewList.get(0);
        curModelView.setIsDrawBoundary(true);
        curModelView.setCurFilterPosition(position);
    }

    /**
     * @param onItemClickListener
     */
    public void setOnPopupSelectListener(final OnItemClickListener onItemClickListener) {
        if (null == popupWindow) {
            return;
        }

        popupWindow.setSelectListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onItemClickListener) {
                    onItemClickListener.onItemClick(v, curSelection);
                }
            }
        });
    }

    /**
     * @param paths
     */
    public void setImagePathList(List<String> paths) {
        if (null != paths) {
            imagePaths = paths;
        }
    }

    /**
     * @param entity
     */
    public void setTemplateEntity(TemplateEntity entity) {
        if (null != entity) {
            positionList = str2Coor(entity);//entity.getPoints(), entity.getPolygons()
        }
    }

    public void setMarginList(List<String> marginList) {
        this.marginList = marginList;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        System.out.println("onInterceptTouchEvent" + isEdit());
        if (isEdit()) {
            if (MotionEvent.ACTION_DOWN == ev.getAction()) {
                downX = (int) ev.getX();
                downY = (int) ev.getY();
                windowX = (int) ev.getRawX();
                windowY = (int) ev.getRawY();


                if (null == curRect || !curRect.contains(downX, downY) || (null != popupWindow && !popupWindow.isShowing())) {
                    int position = pointToPosition(downX, downY);
                    if (position >= 0) {
                        curSelection = position;
                        curRect = boundaryList.get(position);
                        curModelView = modelViewList.get(position);
                        curModelView.setIsDrawBoundary(true);
                    }
                }
                return isMoving;
            }
            return false;
        } else {
            return true;
        }

    }

    /**
     * @param x
     * @param y
     * @return
     */
    private int pointToPosition(int x, int y) {
        int targetPosition = -1;

        for (int i = boundaryList.size() - 1; i >= 0; i--) {
            Rect rect = boundaryList.get(i);
            if (rect.contains(x, y)) {
                targetPosition = i;

                break;
            }
        }

        return targetPosition;
    }

    /**
     * @param showSelected
     */
    public void setShowSelectedState(boolean showSelected) {
        this.showSelected = showSelected;

        if (!showSelected) {
            // 隐藏悬浮窗
            dismissPopup();

            for (PhotoModelView photoModelView : modelViewList) {
                photoModelView.setIsDrawBoundary(false);
                photoModelView.invalidate();
            }
        }
    }

    /**
     * @return
     */
    public boolean getShowSelectedState() {
        return showSelected;
    }

    /**
     * @param path
     */
    public void replaceSelectedBitmap(String path) {
        if (null != curModelView) {
            curModelView.resetOriginBitmap();
            curModelView.setCurFilterPosition(0);
            Glide.with(context).load(path).asBitmap().into(curModelView);
        }
    }

    /**
     * @param bitmap
     */
    public void renderSelectedBitmap(Bitmap bitmap) {
        curModelView.setImageBitmap(bitmap);
    }

    public void renderSelectedListBitmap(ArrayList<Bitmap> bitmap) {
        int i = 0;
        for (PhotoModelView photoModelView : modelViewList) {
            photoModelView.onRestoreInstanceState(bundleList.get(i));
            photoModelView.setImageBitmap(bitmap.get(i));
            i++;
        }
    }

    /**
     * @return
     */
    public Bitmap getSelectedBitmap() {
        return curModelView.getOriginBitmap();
    }

    public ArrayList<Bitmap> getListBitmap() {
        ArrayList<Bitmap> mListBitmaps = new ArrayList<>();
        for (PhotoModelView photoModelView : modelViewList) {
            curModelView = photoModelView;
            mListBitmaps.add(curModelView.getOriginBitmap());
        }
        return mListBitmaps;
    }


    /**
     *
     */
    public void reDraw(FrameModel mFrameModel, int width, int height, int size, int border) {
        if (null == positionList || null == imagePaths || 0 == width || 0 == height) {
            return;
        }

        if (null == boundaryList) {
            boundaryList = new ArrayList<>();
        } else {
            boundaryList.clear();
        }

        if (null == modelViewList) {
            modelViewList = new ArrayList<>();
        } else {
            modelViewList.clear();
        }

        // remove all views before add
        this.removeAllViews();

        int left, top, right, bottom;
        for (int i = 0; i < imagePaths.size(); i++) {
            String pathImage = imagePaths.get(i);
            PhotoModelView singleTouchView = new PhotoModelView(context);
            singleTouchView.setBackgroundColor(Color.GRAY);
            View view = new View(context);
            left = (int) ((Float.parseFloat(positionList.get(i * 4).split(":")[0])) * width) + (int) (marginModelList == null ? 0 : marginModelList.get(i).getLeft() * border);
            top = (int) ((Float.parseFloat(positionList.get(i * 4).split(":")[1])) * height) + (int) (marginModelList == null ? 0 : marginModelList.get(i).getTop() * border);
            right = (int) ((Float.parseFloat(positionList.get(i * 4 + 2).split(":")[0])) * width) - (int) (marginModelList == null ? 0 : marginModelList.get(i).getRight() * border);
            bottom = (int) ((Float.parseFloat(positionList.get(i * 4 + 2).split(":")[1])) * height) - (int) (marginModelList == null ? 0 : marginModelList.get(i).getBottom() * border);

            int childWidth = Math.abs(right - left);
            int childHeight = Math.abs(bottom - top);

            if (size != 1) {
                LayoutParams layoutParams = new LayoutParams(childWidth, childHeight);
                layoutParams.leftMargin = left;
                layoutParams.topMargin = top;

                singleTouchView.setLayoutParams(layoutParams);

                if (marginModelList != null && marginModelList.get(i).isMargin()) {
                    left = (int) ((Float.parseFloat(positionList.get(i * 4).split(":")[0])) * width);
                    top = (int) ((Float.parseFloat(positionList.get(i * 4).split(":")[1])) * height);
                    right = (int) ((Float.parseFloat(positionList.get(i * 4 + 2).split(":")[0])) * width);
                    bottom = (int) ((Float.parseFloat(positionList.get(i * 4 + 2).split(":")[1])) * height);

                    int childWidthView = Math.abs(right - left);
                    int childHeightView = Math.abs(bottom - top);
                    LayoutParams layoutParamsView = new LayoutParams(childWidthView, childHeightView);
                    layoutParamsView.leftMargin = left;
                    layoutParamsView.topMargin = top;
                    //view.setBackgroundResource(Color.GRAY);
                    view.setLayoutParams(layoutParamsView);
                }
            }

            // add move image
            singleTouchView.setOnClickListener(itemOnClickListener);
            singleTouchView.setOnLongClickListener(itemOnLongClickListener);

            if (marginModelList != null && marginModelList.get(i).isMargin()) {
                this.addView(view);
            }

            this.addView(singleTouchView);
            if (size != 1) {
                Glide.with(context).load(pathImage)
                        .asBitmap()
                        .into(singleTouchView);
            } else {
                Glide.with(context).load(pathImage)
                        .asBitmap()
                        .into(new BitmapImageViewTarget(singleTouchView) {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                super.onResourceReady(resource, glideAnimation);
                                LayoutParams layoutParams = new LayoutParams(width, height);
                                int left = (int) (marginModelList == null ? 0 : marginModelList.get(0).getLeft() * border);
                                int top = (int) (marginModelList == null ? 0 : marginModelList.get(0).getTop() * border);
                                int right = (int) (marginModelList == null ? 0 : marginModelList.get(0).getRight() * border);
                                int bottom = (int) (marginModelList == null ? 0 : marginModelList.get(0).getBottom() * border);
                                layoutParams.leftMargin = left;
                                layoutParams.topMargin = top;
                                layoutParams.rightMargin = right;
                                layoutParams.bottomMargin = bottom;
                                singleTouchView.setLayoutParams(layoutParams);
                                singleTouchView.invalidate();
                            }
                        });
            }
            boundaryList.add(new Rect(left, top, right, bottom));

            modelViewList.add(singleTouchView);
            if (bundleList != null && bundleList.size() > 0) {
                if (mFrameModel != null && mFrameModel.mListBitmapFilter != null && mFrameModel.mListBitmapFilter.size() > 0) {
                    renderSelectedListBitmap(mFrameModel.mListBitmapFilter);
                } else {
                    modelViewList.get(i).restoreInstanceState(bundleList.get(i));
                }
                //modelViewList.get(i).restoreInstanceState(bundleList.get(i));
                //modelViewList.get(i).requestLayout();
            }
        }
    }

    private OnClickListener itemOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (null != popupWindow && isEdit()) {
                popupWindow.showAsDropDown(curModelView);
                popupWindow.setSelectedFilterPosition(curModelView.getCurFilterPosition());
                showSelected = true;
            }
        }
    };

    private OnLongClickListener itemOnLongClickListener = new OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            if (isEdit()) {
                Bitmap bm = curModelView.getDrawingCacheBitmap();
                winViewX = downX - v.getLeft();
                winViewY = downY - v.getTop();
                startDrag(bm, windowX, windowY);
                ViewUtils.viewFadeOut(context, curModelView, 0);
                isMoving = true;
            }
            return true;
        }
    };

    /**
     * @param bm
     * @param x
     * @param y
     */
    private void startDrag(Bitmap bm, int x, int y) {
        stopDrag();

        windowParams = new WindowManager.LayoutParams();
        windowParams.gravity = Gravity.TOP | Gravity.LEFT;

        windowParams.x = x - winViewX;
        windowParams.y = y - winViewY;

        windowParams.width = (int) (DRAG_SCALE * bm.getWidth());
        windowParams.height = (int) (DRAG_SCALE * bm.getHeight());
        windowParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
        windowParams.format = PixelFormat.TRANSLUCENT;
        windowParams.windowAnimations = 0;
        ImageView imageView = new ImageView(context);
        imageView.setImageBitmap(bm);
        windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.addView(imageView, windowParams);

        dragImageView = imageView;
    }

    private void stopDrag() {
        if (null != dragImageView) {
            windowManager.removeView(dragImageView);
            dragImageView = null;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (null != dragImageView && isEdit()) {
            int x = (int) ev.getX();
            int y = (int) ev.getY();

            switch (ev.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    downX = (int) ev.getX();
                    downY = (int) ev.getY();
                    windowX = (int) ev.getRawX();
                    windowY = (int) ev.getRawY();
                    return true;

                case MotionEvent.ACTION_MOVE:
                    onDrag((int) ev.getRawX(), (int) ev.getRawY());
                    return true;

                case MotionEvent.ACTION_UP:
                    stopDrag();
                    onDrop(x, y);
                    return true;

                default:
                    break;
            }
            return super.onTouchEvent(ev);
        }
        return false;
    }

    /**
     * @param rawX
     * @param rawY
     */
    private void onDrag(int rawX, int rawY) {
        if (null != dragImageView) {
            windowParams.alpha = 0.6f;
            windowParams.x = rawX - winViewX;
            windowParams.y = rawY - winViewY;

            windowManager.updateViewLayout(dragImageView, windowParams);
        }
    }

    /**
     * @param x
     * @param y
     */
    private void onDrop(int x, int y) {
        int position = pointToPosition(x, y);

        if (position >= 0 && position != curSelection) {
            exchangeViews(position, curSelection);
        }

        ViewUtils.viewFadeIn(context, curModelView, 0);
        isMoving = false;
    }

    /**
     * @param i
     * @param j
     */
    private void exchangeViews(int i, int j) {

        Collections.swap(imagePaths, i, j);

        PhotoModelView photoModelView;

        photoModelView = modelViewList.get(i);
        photoModelView.resetOriginBitmap();
        photoModelView.setCurFilterPosition(0);
        Glide.with(context).load(imagePaths.get(i)).asBitmap().into(photoModelView);

        photoModelView = modelViewList.get(j);
        photoModelView.resetOriginBitmap();
        photoModelView.setCurFilterPosition(0);
        Glide.with(context).load(imagePaths.get(j)).asBitmap().into(photoModelView);
    }

    /**
     * @param entity
     * @return
     */
    private List<String> str2Coor(TemplateEntity entity) {
        String pointsString = entity.getPoints();
        String polygonsString = entity.getPolygons();
        String marginString = entity.getMargin();
        List<String> coordinateStringList = new ArrayList<>();
        // 0:0 0.5:0 1:0 0:1 0.5:1 1:1
        String[] pointsStr = pointsString.split(",");
        // 0,1,4,3 1,2,5,4
        String[] polygonsStr = polygonsString.split("/");
        // 0,1,4,3 1,2,5,4

        for (String pStr : polygonsStr) {
            // "0" "1" "4" "3"
            String[] cStr = pStr.split(",");
            // 0 1 4 3
            int[] cInt = new int[cStr.length];
            for (int i = 0; i < cInt.length; i++) {
                cInt[i] = Integer.parseInt(cStr[i]);
            }
            for (int pInt : cInt) {
                // "0:0" "0.5:0" "0.5:1" "0:1"
                coordinateStringList.add(pointsStr[pInt]);
            }
        }

        if (!TextUtils.isEmpty(marginString)) {
            String[] mMargin = marginString.split("/");
            marginModelList = new ArrayList<>();
            for (String margin : mMargin) {
                // "0" "1" "4" "3"
                String[] cStr = margin.split(",");
                // 0 1 4 3
                float[] cInt = new float[cStr.length];
                MarginModel marginModel = new MarginModel();
                for (int i = 0; i < cInt.length; i++) {
                    cInt[i] = Float.parseFloat(cStr[i]);
                    marginModel.setMargin(cInt[i], i);
                }
                marginModelList.add(marginModel);
            }


        }
        return coordinateStringList;
    }

    public boolean isEdit() {
        return isEdit;
    }

    public void setEdit(boolean edit) {
        isEdit = edit;
    }

    @Override
    public void addOnLayoutChangeListener(OnLayoutChangeListener listener) {
        super.addOnLayoutChangeListener(listener);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    public void setStrokeWidth(boolean strokeWidth) {
        this.strokeWidth = strokeWidth;
    }

    private boolean strokeWidth;

    public void saveImage() {
        onSaveInstanceState();
    }

    public List<Bundle> getBundleList() {
        return bundleList;
    }

    public void setBundleList(List<Bundle> bundleList) {
        this.bundleList = bundleList;
    }
}
