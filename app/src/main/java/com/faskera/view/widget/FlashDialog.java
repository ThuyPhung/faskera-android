/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.Window;

import com.faskera.common.AppSharePreference;
import com.faskera.common.view.AnyTextview;
import com.faskera.era.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author theanh
 * @since 3/4/2017
 */

public class FlashDialog {
    @BindView(R.id.text_policy)
    AnyTextview mTextPolicy;
    @BindView(R.id.text_continue)
    AnyTextview mTextContinue;
    private Dialog mDialog;

    public FlashDialog(Context context) {
        mDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        View view = View.inflate(context, R.layout.flash_dialog, null);
        ButterKnife.bind(this, view);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(view);
        mDialog.setCancelable(false);
        //    Window window = mDialog.getWindow();
        //   if (window != null)
        //      window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        mTextPolicy.setMovementMethod(LinkMovementMethod.getInstance());
        mTextPolicy.setHighlightColor(Color.TRANSPARENT);

        mTextContinue.setOnClickListener(view1 -> {
            AppSharePreference.getInstance(context).setFirstTimeUsing(false);
            dismiss();
        });
    }

    public void show() {
        if (mDialog != null && !mDialog.isShowing())
            mDialog.show();
    }

    public void dismiss() {
        if (mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
    }
}
