/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.faskera.era.R;

/**
 * @author theanh
 * @since 2/8/2017
 */

public class CustomAlertDialog {
    private Dialog mDialog;
    private TextView mBtYes, mBtNo, mMessage;

    public CustomAlertDialog(Context context) {
        mDialog = new Dialog(context);

        View view = LayoutInflater.from(context).inflate(R.layout.custom_alert_dialog, null);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(view);
        mBtYes = (TextView) view.findViewById(R.id.btn_yes);
        mBtNo = (TextView) view.findViewById(R.id.btn_no);
        mMessage = (TextView) view.findViewById(R.id.text_message);
    }

    public void setMessage(String message) {
        mMessage.setText(message);
    }

    public void setYesButton(String mess, View.OnClickListener listener) {
        mBtYes.setText(mess);
        mBtYes.setOnClickListener(listener);
    }

    public void setNoButton(String mess, View.OnClickListener listener) {
        mBtNo.setText(mess);
        mBtNo.setOnClickListener(listener);
    }

    public void show() {
        if (mDialog != null && !mDialog.isShowing())
            mDialog.show();
    }

    public void dismiss() {
        if (mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
    }

}
