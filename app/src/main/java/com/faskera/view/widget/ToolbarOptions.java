/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.widget;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.faskera.callback.OnClickItemToolbarListener;
import com.faskera.era.R;
import com.faskera.model.option.TbOption;

/**
 * @author theanh
 * @since 1/16/2017
 */

public class ToolbarOptions extends LinearLayout implements View.OnClickListener {

    private OnClickItemToolbarListener mOnClickItemToolbarListener;

    public ToolbarOptions(Context context) {
        super(context);
        init(context);
    }

    public ToolbarOptions(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ToolbarOptions(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ToolbarOptions(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_toolbar_option, this);
        findViewById(R.id.image_back).setOnClickListener(this);
        findViewById(R.id.image_done).setOnClickListener(this);
    }

    private void setTextTitle(String title) {
        if (findViewById(R.id.toolbar_title) != null) {
            findViewById(R.id.toolbar_title).setVisibility(VISIBLE);
            ((TextView) findViewById(R.id.toolbar_title)).setText(title);
        }
    }

    private void setEditMode(boolean isDoneEnable) {
        ImageView ivDone = (ImageView) findViewById(R.id.image_done);
        ImageView ivBack = (ImageView) findViewById(R.id.image_back);
        if (ivDone != null) {
            if (isDoneEnable) {
                ivDone.setVisibility(VISIBLE);
                ivBack.setImageResource(R.drawable.ic_clear);
            } else {
                ivDone.setVisibility(INVISIBLE);
                ivBack.setImageResource(R.drawable.ic_arrow_back);
            }
        }
    }

    public void disableAction() {
        ImageView ivDone = (ImageView) findViewById(R.id.image_done);
        ivDone.setVisibility(GONE);
    }

    public void setToolbarModel(TbOption tbOption) {
        setTextTitle(tbOption.mTitle);
        setEditMode(tbOption.isEditMode);
    }

    public void setOnClickItemToolbarListener(OnClickItemToolbarListener mOnClickItemToolbarListener) {
        this.mOnClickItemToolbarListener = mOnClickItemToolbarListener;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                if (mOnClickItemToolbarListener != null) {
                    mOnClickItemToolbarListener.onBack();
                }
                break;
            case R.id.image_done:
                if (mOnClickItemToolbarListener != null) {
                    mOnClickItemToolbarListener.onDone();
                }
                break;
        }
    }
}
