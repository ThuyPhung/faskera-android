/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import butterknife.ButterKnife;

public abstract class BaseRelativeLayout extends RelativeLayout implements View.OnClickListener {

    public BaseRelativeLayout(Context context) {
        super(context);
        init(context);
    }

    public BaseRelativeLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setStyle(attrs);
        init(context);
    }

    public BaseRelativeLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setStyle(attrs);
        init(context);
    }

    protected abstract void setStyle(AttributeSet attrs);

    public void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(getIdLayoutRes(), this);
        ButterKnife.bind(this, view);
        initView();
    }

    protected abstract int getIdLayoutRes();

    protected abstract void initView();
}
