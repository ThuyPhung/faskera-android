/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.faskera.callback.OnClickItemToolbarListener;
import com.faskera.era.R;
import com.faskera.model.createpoll.TbCreatePoll;

public class ToolbarCreatePoll extends LinearLayout implements View.OnClickListener {

    private OnClickItemToolbarListener mOnClickItemToolbarListener;

    public ToolbarCreatePoll(Context context) {
        super(context);
        init(context);
    }

    public ToolbarCreatePoll(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ToolbarCreatePoll(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.tool_bar_create_poll, this);
        findViewById(R.id.iv_back).setOnClickListener(this);
        findViewById(R.id.tv_done).setOnClickListener(this);
    }

    public void setTextTitle(String title) {
        if (findViewById(R.id.tv_title) != null) {
            findViewById(R.id.tv_title).setVisibility(VISIBLE);
            ((TextView) findViewById(R.id.tv_title)).setText(title);
        }
    }

    public void setTextDone(String done) {
        if (findViewById(R.id.tv_done) != null) {
            findViewById(R.id.tv_done).setVisibility(VISIBLE);
            ((TextView) findViewById(R.id.tv_done)).setText(done);
        }
    }

    public void setToolBarModel(TbCreatePoll mTbCreatePoll){
        setTextTitle(mTbCreatePoll.mTitle);
        setTextDone(mTbCreatePoll.mDone);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                if (mOnClickItemToolbarListener != null) {
                    mOnClickItemToolbarListener.onBack();
                }
                break;
            case R.id.tv_done:
                if (mOnClickItemToolbarListener != null) {
                    mOnClickItemToolbarListener.onDone();
                }
                break;
            default:
                break;
        }
    }

    public void setOnClickItemToolbarListener(OnClickItemToolbarListener mOnClickItemToolbarListener) {
        this.mOnClickItemToolbarListener = mOnClickItemToolbarListener;
    }
}
