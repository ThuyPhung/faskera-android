/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.view.widget;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.faskera.era.R;
import com.faskera.utils.ImageLoader;

/**
 * @author theanh
 * @since 3/1/2017
 */

public class ConfirmUnfollowDialog {
    private Context mContext;
    private AlertDialog mDialog;
    private TextView mMessage;
    private ImageView mImageAvatar;

    View.OnClickListener onCLickYes, OnCLickNo;


    public ConfirmUnfollowDialog(Context context) {
        mContext = context;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_confirm_unfollow, null, false);
        mImageAvatar = (ImageView) view.findViewById(R.id.avatar);
        mMessage = (TextView) view.findViewById(R.id.text_name);
        builder.setView(view);
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                OnCLickNo.onClick(null);
            }
        }).setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onCLickYes.onClick(null);

            }
        });
        mDialog = builder.create();

    }

    public void setMessage(String message) {
        mMessage.setText(message);
    }

    public void setYesButton(String mess, View.OnClickListener listener) {
        onCLickYes = listener;
    }

    public void setNoButton(String mess, View.OnClickListener listener) {
        OnCLickNo = listener;
    }

    public void setImageAvatar(String imageUrl) {
        if (imageUrl != null && !imageUrl.trim().isEmpty()) {
            ImageLoader.loadImageViewWith(mContext, imageUrl, mImageAvatar);
        } else {
            ImageLoader.loadImageViewWith(mContext, R.drawable.ic_avatar_default, mImageAvatar);
        }
    }

    public void show() {
        if (mDialog != null && !mDialog.isShowing())
            mDialog.show();
    }

    public void dismiss() {
        if (mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
    }

}
