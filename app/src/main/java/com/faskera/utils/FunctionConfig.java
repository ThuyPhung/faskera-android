/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.utils;

import java.io.Serializable;

public class FunctionConfig implements Serializable {

    public final static int REQUEST_STORAGE = 122;
    public final static int REQUEST_CAMERA = 123;
}
