/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.view.View;

import com.faskera.Constant;
import com.faskera.common.AppSharePreference;
import com.faskera.era.R;

/**
 * @author framgia
 * @since 20/02/2017
 */

public class TextViewUtils {

    public static Spannable getLoginMessage(Context context) {
        ClickableSpan termOfUserLink = buildLink(context, "https://www.faskera.com/terms-of-use");
        ClickableSpan privacyLink = buildLink(context, "https://www.faskera.com/privacy");
        ClickableSpan aboutLink = buildLink(context, "https://www.faskera.com/about");

        SpannableString loginMessage = new SpannableString(context.getResources().getString(R.string.text_notice_policy_login));
        if (AppSharePreference.getInstance(context).getMyLanguage().equals(Constant.Language.EN_US_CODE)) {
            loginMessage.setSpan(termOfUserLink, 39, 57, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            loginMessage.setSpan(privacyLink, 61, 86, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            loginMessage.setSpan(aboutLink, 142, 162, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        return loginMessage;
    }

    private static ClickableSpan buildLink(Context context, String url) {
        return new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                context.startActivity(browserIntent);
            }
        };
    }

    public static String getTopics(Context context, String topic) {
        switch (topic) {
            case "Music":
                return context.getResources().getString(R.string.topic_music);
            case "Sport":
                return context.getResources().getString(R.string.topic_sport);
            case "Food":
                return context.getResources().getString(R.string.topic_food);
            case "Sex-Love":
                return context.getResources().getString(R.string.topic_sex_love);
            case "Game":
                return context.getResources().getString(R.string.topic_game);
            case "Showbiz":
                return context.getResources().getString(R.string.topic_showbiz);
            case "Celebrities":
                return context.getResources().getString(R.string.topic_celebrities);
            case "Fashion":
                return context.getResources().getString(R.string.topic_fashion);
            case "Travel":
                return context.getResources().getString(R.string.topic_travel);
            case "Idols":
                return context.getResources().getString(R.string.topic_idols);
            case "Love":
                return context.getResources().getString(R.string.topic_love);
            case "Family":
                return context.getResources().getString(R.string.topic_family);
            case "Tech":
                return context.getResources().getString(R.string.topic_tech);
            case "Film":
                return context.getResources().getString(R.string.topic_film);
            case "Education":
                return context.getResources().getString(R.string.topic_education);
            default:
                return null;
        }
    }

    public static Spanned buildHtmlForm(String form) {
        if (Build.VERSION.SDK_INT >= 24)
            return Html.fromHtml(form, Html.FROM_HTML_MODE_LEGACY);
        else
            return Html.fromHtml(form);
    }

}
