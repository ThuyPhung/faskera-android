/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class EditTextUtil {

    public static void fixFontPasswordType(EditText edt) {
        edt.setTypeface(Typeface.DEFAULT);
    }

    public static void hideSoftKeyboard(EditText edt, Context ctx) {
        if (edt == null || ctx == null) {
            return;
        }

        InputMethodManager imm = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
    }

    public static void showKeyboard(EditText edt, Context ctx) {
        if (edt == null || ctx == null) {
            return;
        }

        InputMethodManager imm = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edt, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void showKeyBoardRun(final EditText edt, final Activity ctx) {
        edt.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e("", "show Key");
                InputMethodManager keyboard = (InputMethodManager) ctx
                        .getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(edt, InputMethodManager.SHOW_IMPLICIT);

            }
        }, 200);
    }

    public static void hideKeyboard(Activity activity) {
        if (activity == null) {
            return;
        }
        activity.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }// end method

    public static void hideSoftKeyboard(Activity activity) {
        if (activity == null) {
            return;
        }
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }

    }

    public interface EnterListener {
         void onEnterPress();
    }

    public static void setOnEnterListener(EditText edt,
                                          final EnterListener enterListener) {
        edt.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    if (enterListener != null) {
                        enterListener.onEnterPress();
                    }
                }
                return false;
            }
        });
    }

    public static boolean isCheckShow(Context context) {
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        return imm.isAcceptingText();
    }
}