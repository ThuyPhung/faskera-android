/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.faskera.common.base.BaseActivity;
import com.faskera.view.activity.follow.ShowListUserView;
import com.faskera.view.activity.home.HomeActivityOptimize;
import com.faskera.view.activity.login.LoginActivity;
import com.faskera.view.activity.mypage.ProfileActivity;
import com.faskera.view.activity.polldetail.PollDetailActivity;
import com.faskera.view.activity.topic.TopicActivity;
import com.faskera.view.activity.viewoption.ViewOptionActivity;
import com.faskera.view.activity.viewvote.ViewVoteActivity;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public class IntentUtils {

    public static void startActivityHome(Bundle bundle, Context context) {
        Intent intent = new Intent(context, HomeActivityOptimize.class);
        context.startActivity(intent);
    }

    public static void startActivityHome(Bundle bundle, Context context, int flag) {
        Intent intent = new Intent(context, HomeActivityOptimize.class);
        intent.addFlags(flag);
        context.startActivity(intent);
    }

    public static void startActivityViewVote(Bundle bundle, Context context) {
        Intent intent = new Intent(context, ViewVoteActivity.class);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    public static void startActivityMyPage(@NonNull Bundle bundle, Context context) {
        Intent intent = new Intent(context, ProfileActivity.class);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    public static void startActivityDetail(Context context, @NonNull Bundle bundle) {
        Intent intent = new Intent(context, PollDetailActivity.class);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    public static void startActivityLogin(Bundle bundle, Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void startActivityViewListUser(Bundle bundle, Context context) {
        Intent intent = new Intent(context, ShowListUserView.class);
        intent.putExtras(bundle);
        context.startActivity(intent);

    }

    public static void startActivityTopic(Context context, Bundle bundle, int flags) {
        Intent intent = new Intent(context, TopicActivity.class);
        intent.addFlags(flags);
        context.startActivity(intent);
    }

    public static void startActivityViewVoteOption(@NonNull Bundle bundle, Context context) {
        Intent intent = new Intent(context, ViewOptionActivity.class);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    public static void startActivityUpdateTopic(Bundle bundle, Activity context) {
        context.startActivityForResult(new Intent(context, TopicActivity.class), BaseActivity.UPDATE_TOPIC_REQUEST_CODE);
    }
}
