/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.utils;

import android.content.Context;

import com.faskera.era.R;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author framgia
 * @since 14/02/2017
 */

public class DateUtils {

    public static final long MINUTE = 60000;
    public static final long HOUR = MINUTE * 60;
    public static final long DAY = HOUR * 24;
    public static final long WEEK = DAY * 7;
    public static final long MONTH = WEEK * 4;

    static DateTimeFormatter hourMinuteFmt =
            DateTimeFormat.forPattern("hh:mm a").withLocale(Locale.getDefault());
    static DateTimeFormatter dayOfWeekFmt =
            DateTimeFormat.forPattern("EEE").withLocale(Locale.getDefault());
    static DateTimeFormatter dateMonthFmt =
            DateTimeFormat.forPattern("MMM dd").withLocale(Locale.getDefault());
    static DateTimeFormatter minuteFmt =
            DateTimeFormat.forPattern("mm").withLocale(Locale.getDefault());
    static DateTimeFormatter dayFmt =
            DateTimeFormat.forPattern("dd").withLocale(Locale.getDefault());
    static DateTimeFormatter hourFmt =
            DateTimeFormat.forPattern("hh").withLocale(Locale.getDefault());
    static DateTimeFormatter weekFmt =
            DateTimeFormat.forPattern("hh").withLocale(Locale.getDefault());


    // public static String getTimeAgo(long time) {
    //    return new PrettyTime().format(new Date(time));
    //}

    private static long getTimeRange(long time) {
        DateTime today = new DateTime();
        DateTime activityTime = new DateTime(time);
        return today.getMillis() - activityTime.getMillis();
    }

    public static String getTimeNotification(long time) {

        long timeRange = getTimeRange(time);
//        if (timeRange < MINUTE)
//            return "Just now";
//        if (timeRange < HOUR) {
//            return minuteFmt.print(new DateTime(timeRange)) + " minutes";
//        }
//        if (timeRange < DAY) {
//            return hourFmt.print(new DateTime(timeRange)) + " hour";
//        }
//        if (timeRange < WEEK) {
//
//            if (TimeUnit.MILLISECONDS.toDays(timeRange) <= 7)
//                return dayFmt.print(new DateTime(timeRange)) + " day";
//        }
        return DateTimeFormat.forPattern("DD/MM hh : mm a").print(new DateTime(timeRange));
    }

    public static String getRelativeTimeSpanString(Context context, long from) {

        Date date = new Date(from);

        StringBuffer dateStr = new StringBuffer();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        Calendar now = Calendar.getInstance();

        int days = daysBetween(calendar.getTime(), now.getTime());
        int weeks = days / 7;
        int minutes = hoursBetween(calendar.getTime(), now.getTime());
        int hours = minutes / 60;
        if (days == 0) {

            int second = minuteBetween(calendar.getTime(), now.getTime());
            if (minutes > 60) {
                if (hours >= 1 && hours <= 24) {
                    dateStr.append(String.format("%s %s %s",
                            hours,
                            context.getString(R.string.hour)
                            , context.getString(R.string.ago)));
                }
            } else {
                if (second <= 10) {
                    dateStr.append(context.getString(R.string.now));
                } else if (second > 10 && second <= 30) {
                    dateStr.append(context.getString(R.string.few_seconds_ago));
                } else if (second > 30 && second <= 60) {
                    dateStr.append(String.format("%s %s %s",
                            second,
                            context.getString(R.string.seconds)
                            , context.getString(R.string.ago)));
                } else if (second >= 60 && minutes <= 60) {
                    dateStr.append(String.format("%s %s %s",
                            minutes,
                            context.getString(R.string.minutes)
                            , context.getString(R.string.ago)));
                }
            }
        } else if (hours > 24 && days < 7) {
            dateStr.append(String.format("%s %s %s",
                    days,
                    context.getString(R.string.days)
                    , context.getString(R.string.ago)));
        } else if (weeks == 1) {
            dateStr.append(String.format("%s %s %s",
                    weeks,
                    context.getString(R.string.weeks)
                    , context.getString(R.string.ago)));
        } else {
            return SimpleDateFormat.getDateInstance(SimpleDateFormat.LONG).format(date).toUpperCase();
        }

        return dateStr.toString();
    }

    public static int minuteBetween(Date d1, Date d2) {
        return (int) ((d2.getTime() - d1.getTime()) / android.text.format.DateUtils.SECOND_IN_MILLIS);
    }

    public static int hoursBetween(Date d1, Date d2) {
        return (int) ((d2.getTime() - d1.getTime()) / android.text.format.DateUtils.MINUTE_IN_MILLIS);
    }

    public static int daysBetween(Date d1, Date d2) {
        return (int) ((d2.getTime() - d1.getTime()) / android.text.format.DateUtils.DAY_IN_MILLIS);
    }
}

