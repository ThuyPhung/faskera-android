/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.utils;

/**
 * @author jacky
 * @since 1/9/17
 */

public class AppUtils {
    public static final int MAX_NUMBER_IMAGE_PICKER = 6;
    public final static String SELECTED_PATHS = "selected_paths";
    public final static String TYPE_OF_PHOTO = "type_of_photo";
    public final static String ID_OF_TEMPLATE = "id_of_template";
    public final static String BUNDLE_PATH_IMAGE = "_BUNDLE_PATH_IMAGE";
    public static final int POLL_LENGTH_TEXT_OPTION = 140;
    public static final int POLL_SIZE_OPTION = 8;
    public static final String PATH_NAME = "Faskera_";
}
