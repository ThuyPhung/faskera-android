/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.utils;

import android.content.Context;
import android.content.res.Resources;

import com.faskera.Constant;
import com.faskera.common.LocaleHelper;
import com.faskera.era.R;
import com.faskera.model.option.LanguageModel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author theanh
 * @since 1/24/2017
 */

public class LanguageUtils {
    public static List<LanguageModel> initLanguageList(Context context) {
        List<LanguageModel> lists = new ArrayList<>();
        Resources resources = context.getResources();
        lists.add(new LanguageModel(Constant.Language.EN_US_CODE, resources.getString(R.string.language_english), false));
        lists.add(new LanguageModel(Constant.Language.VN_CODE, resources.getString(R.string.language_vietnamese), false));

        String currentLan = LocaleHelper.getLanguage(context);
        for (int i = 0; i < lists.size(); i++) {
            if (lists.get(i).getCode().equals(currentLan))
                lists.get(i).setUsed(true);
        }
        return lists;
    }
}
