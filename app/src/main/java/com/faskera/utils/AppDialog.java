/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

public class AppDialog {

    private AlertDialog mAlertDialog;

    private AppDialog(@NonNull final Object activityOrFragment,
                      @NonNull final Context context,
                      @NonNull String rationale,
                      @Nullable String title,
                      @Nullable String positiveButton,
                      @Nullable String negativeButton,
                      @Nullable OnClickButtonDialogApp mOnClickButtonDialogApp) {
        // Create empty builder
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // Set rationale
        dialogBuilder.setMessage(rationale);

        // Set title
        dialogBuilder.setTitle(title);

        // Positive button text, or default
        // Positive click listener, launches App screen
        if (!TextUtils.isEmpty(positiveButton)) {
            dialogBuilder.setPositiveButton(positiveButton, (dialog, which) -> {
                // Create App settings intent
                if (mOnClickButtonDialogApp != null) {
                    mOnClickButtonDialogApp.onOk();
                }
            });
        }

        // Negative button text, or default
            if (!TextUtils.isEmpty(negativeButton)) {
            // Negative click listener, dismisses dialog
            dialogBuilder.setNegativeButton(negativeButton, (dialog, which) -> {
                if (mOnClickButtonDialogApp != null) {
                    mOnClickButtonDialogApp.onCancel();
                }
            });
        }

        // Build dialog
        mAlertDialog = dialogBuilder.create();
        mAlertDialog.setCanceledOnTouchOutside(false);
    }

    /**
     * Display the built dialog.
     */
    public void show() {
        mAlertDialog.show();
    }

    /**
     * Builder for an {@link AppDialog}.
     */
    public static class Builder {

        private Object mActivityOrFragment;
        private Context mContext;
        private String mRationale;
        private String mTitle;
        private String mPositiveButton;
        private String mNegativeButton;
        private OnClickButtonDialogApp mOnClickButtonDialogApp;

        public Builder(@NonNull Activity activity, @NonNull String rationale) {
            mActivityOrFragment = activity;
            mContext = activity;
            mRationale = rationale;
        }

        public Builder(@NonNull Fragment fragment, @NonNull String rationale) {
            mActivityOrFragment = fragment;
            mContext = fragment.getContext();
            mRationale = rationale;
        }

        @TargetApi(11)
        public Builder(@NonNull android.app.Fragment fragment, @NonNull String rationale) {
            mActivityOrFragment = fragment;
            mContext = fragment.getActivity();
            mRationale = rationale;
        }

        public AppDialog.Builder setTitle(String title) {
            mTitle = title;
            return this;
        }

        public AppDialog.Builder setPositiveButton(String positiveButton) {
            mPositiveButton = positiveButton;
            return this;
        }

        public AppDialog.Builder setNegativeButton(String negativeButton) {
            mNegativeButton = negativeButton;
            return this;
        }

        public AppDialog.Builder setOnClickButtonDialogApp(OnClickButtonDialogApp onClickButtonDialogApp) {
            mOnClickButtonDialogApp = onClickButtonDialogApp;
            return this;
        }

        public AppDialog build() {
            return new AppDialog(mActivityOrFragment, mContext, mRationale, mTitle,
                    mPositiveButton, mNegativeButton, mOnClickButtonDialogApp);
        }

    }

    public interface OnClickButtonDialogApp {
        void onOk();

        void onCancel();
    }
}
