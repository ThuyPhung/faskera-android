/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.utils;

import android.content.Context;
import android.content.res.Resources;

import com.faskera.era.R;

import java.text.MessageFormat;
import java.util.Date;

/**
 * @author DANGNGOCDUC
 * @since 1/11/2017
 */

public class HumanTime {

    public static void initLanguageApp(Context context) {
        Resources resource  = context.getResources();
        suffixAgo = resource.getString(R.string.f_suffixAgo);
        suffixFromNow = resource.getString(R.string.f_suffixFromNow);
        seconds = resource.getString(R.string.f_seconds);
        minute = resource.getString(R.string.f_minute);
        minutes = resource.getString(R.string.f_minutes);
        hour = resource.getString(R.string.f_hour);
        hours = resource.getString(R.string.f_hours);
        day = resource.getString(R.string.f_day);
        days = resource.getString(R.string.f_days);
        month = resource.getString(R.string.f_month);
        months = resource.getString(R.string.f_months);
        year = resource.getString(R.string.f_year);
        years = resource.getString(R.string.f_years);
    }

    private static String prefixAgo = null;

    private static String prefixFromNow = null;

    private static String suffixAgo = "ago"; //$NON-NLS-1$

    private static String suffixFromNow = "from now"; //$NON-NLS-1$

    private static String seconds = "less than a minute"; //$NON-NLS-1$

    private static String minute = "about a minute"; //$NON-NLS-1$

    private static String minutes = "{0} minutes"; //$NON-NLS-1$

    private static String hour = "about an hour"; //$NON-NLS-1$

    private static String hours = "{0} hours"; //$NON-NLS-1$

    private static String day = "a day"; //$NON-NLS-1$

    private static String days = "{0} days"; //$NON-NLS-1$

    private static String month = "about a month"; //$NON-NLS-1$

    private static String months = "{0} months"; //$NON-NLS-1$

    private static String year = "about a year"; //$NON-NLS-1$

    private static String years = "{0} years"; //$NON-NLS-1$

    /**
     * Get time until specified date
     *
     * @param date
     * @return time string
     */
    public String timeUntil(final Date date) {
        return timeUntil(date.getTime());
    }

    /**
     * Get time ago that date occurred
     *
     * @param date
     * @return time string
     */
    public String timeAgo(final Date date) {
        return timeAgo(date.getTime());
    }

    /**
     * Get time until specified milliseconds date
     *
     * @param millis
     * @return time string
     */
    public String timeUntil(final long millis) {
        return time(millis - System.currentTimeMillis(), true);
    }

    /**
     * Get time ago that milliseconds date occurred
     *
     * @param millis
     * @return time string
     */
    public String timeAgo(final long millis) {
        return time(System.currentTimeMillis() - millis, false);
    }

    /**
     * Get time string for milliseconds distance
     *
     * @param distanceMillis
     * @param allowFuture
     * @return time string
     */
    public String time(long distanceMillis, final boolean allowFuture) {
        final String prefix;
        final String suffix;
        if (allowFuture && distanceMillis < 0) {
            distanceMillis = Math.abs(distanceMillis);
            prefix = prefixFromNow;
            suffix = suffixFromNow;
        } else {
            prefix = prefixAgo;
            suffix = suffixAgo;
        }

        final double seconds = distanceMillis / 1000;
        final double minutes = seconds / 60;
        final double hours = minutes / 60;
        final double days = hours / 24;
        final double years = days / 365;

        final String time;
        if (seconds < 45)
            time = HumanTime.seconds;
        else if (seconds < 90)
            time = minute;
        else if (minutes < 45)
            time = MessageFormat.format(HumanTime.minutes, Math.round(minutes));
        else if (minutes < 90)
            time = hour;
        else if (hours < 24)
            time = MessageFormat.format(HumanTime.hours, Math.round(hours));
        else if (hours < 48)
            time = day;
        else if (days < 30)
            time = MessageFormat.format(HumanTime.days, Math.floor(days));
        else if (days < 60)
            time = month;
        else if (days < 365)
            time = MessageFormat.format(months, Math.floor(days / 30));
        else if (years < 2)
            time = year;
        else
            time = MessageFormat.format(HumanTime.years, Math.floor(years));

        //hot fix for language vietnam . stupid
        if (time.equals("bây giờ")) {
            return join(prefix, time, "");
        } else {
            return join(prefix, time, suffix);
        }
    }

    /**
     * Join time string with prefix and suffix. The prefix and suffix are only
     * joined with the time if they are non-null and non-empty
     *
     * @param prefix
     * @param time
     * @param suffix
     * @return non-null joined string
     */
    public String join(final String prefix, final String time,
                       final String suffix) {
        StringBuilder joined = new StringBuilder();
        if (prefix != null && prefix.length() > 0)
            joined.append(prefix).append(' ');
        joined.append(time);
        if (suffix != null && suffix.length() > 0)
            joined.append(' ').append(suffix);
        return joined.toString();
    }

    /**
     * @return prefixAgo
     */
    public String getPrefixAgo() {
        return prefixAgo;
    }

    /**
     * @param prefixAgo
     *            prefixAgo value
     * @return this instance
     */
    public HumanTime setPrefixAgo(final String prefixAgo) {
        HumanTime.prefixAgo = prefixAgo;
        return this;
    }

    /**
     * @return prefixFromNow
     */
    public String getPrefixFromNow() {
        return prefixFromNow;
    }

    /**
     * @param prefixFromNow
     *            prefixFromNow value
     * @return this instance
     */
    public HumanTime setPrefixFromNow(final String prefixFromNow) {
        HumanTime.prefixFromNow = prefixFromNow;
        return this;
    }

    /**
     * @return suffixAgo
     */
    public String getSuffixAgo() {
        return suffixAgo;
    }

    /**
     * @param suffixAgo
     *            suffixAgo value
     * @return this instance
     */
    public HumanTime setSuffixAgo(final String suffixAgo) {
        HumanTime.suffixAgo = suffixAgo;
        return this;
    }

    /**
     * @return suffixFromNow
     */
    public String getSuffixFromNow() {
        return suffixFromNow;
    }

    /**
     * @param suffixFromNow
     *            suffixFromNow value
     * @return this instance
     */
    public HumanTime setSuffixFromNow(final String suffixFromNow) {
        HumanTime.suffixFromNow = suffixFromNow;
        return this;
    }

    /**
     * @return seconds
     */
    public String getSeconds() {
        return seconds;
    }

    /**
     * @param seconds
     *            seconds value
     * @return this instance
     */
    public HumanTime setSeconds(final String seconds) {
        HumanTime.seconds = seconds;
        return this;
    }

    /**
     * @return minute
     */
    public String getMinute() {
        return minute;
    }

    /**
     * @param minute
     *            minute value
     * @return this instance
     */
    public HumanTime setMinute(final String minute) {
        HumanTime.minute = minute;
        return this;
    }

    /**
     * @return minutes
     */
    public String getMinutes() {
        return minutes;
    }

    /**
     * @param minutes
     *            minutes value
     * @return this instance
     */
    public HumanTime setMinutes(final String minutes) {
        HumanTime.minutes = minutes;
        return this;
    }

    /**
     * @return hour
     */
    public String getHour() {
        return hour;
    }

    /**
     * @param hour
     *            hour value
     * @return this instance
     */
    public HumanTime setHour(final String hour) {
        HumanTime.hour = hour;
        return this;
    }

    /**
     * @return hours
     */
    public String getHours() {
        return hours;
    }

    /**
     * @param hours
     *            hours value
     * @return this instance
     */
    public HumanTime setHours(final String hours) {
        HumanTime.hours = hours;
        return this;
    }

    /**
     * @return day
     */
    public String getDay() {
        return day;
    }

    /**
     * @param day
     *            day value
     * @return this instance
     */
    public HumanTime setDay(final String day) {
        HumanTime.day = day;
        return this;
    }

    /**
     * @return days
     */
    public String getDays() {
        return days;
    }

    /**
     * @param days
     *            days value
     * @return this instance
     */
    public HumanTime setDays(final String days) {
        HumanTime.days = days;
        return this;
    }

    /**
     * @return month
     */
    public String getMonth() {
        return month;
    }

    /**
     * @param month
     *            month value
     * @return this instance
     */
    public HumanTime setMonth(final String month) {
        HumanTime.month = month;
        return this;
    }

    /**
     * @return months
     */
    public String getMonths() {
        return months;
    }

    /**
     * @param months
     *            months value
     * @return this instance
     */
    public HumanTime setMonths(String months) {
        HumanTime.months = months;
        return this;
    }

    /**
     * @return year
     */
    public String getYear() {
        return year;
    }

    /**
     * @param year
     *            year value
     * @return this instance
     */
    public HumanTime setYear(String year) {
        HumanTime.year = year;
        return this;
    }

    /**
     * @return years
     */
    public String getYears() {
        return years;
    }

    /**
     * @param years
     *            years value
     * @return this instance
     */
    public HumanTime setYears(String years) {
        HumanTime.years = years;
        return this;
    }
}
