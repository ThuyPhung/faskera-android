/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public class ImageLoader {

    public static void loadImageViewWith(Context context, String link, ImageView imageView) {
        Glide.with(context).load(link).crossFade().crossFade(200).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
    }

    public static void loadImageViewWith(Context context, int res, ImageView imageView) {
        Glide.with(context).load(res).asBitmap().diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
    }

}
