/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.task;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class AsyncTaskExecutor {

    private static final int CORE_POOL_SIZE;
    private static final int MAXIMUM_POOL_SIZE;
    private static final int KEEP_ALIVE;
    private static final TimeUnit TIME_UNIT;

    private static final BlockingQueue<Runnable> mConcurrentPoolWorkQueue;
    private static final ThreadFactory mConcurrentThreadFactory;
    private static final ThreadPoolExecutor mConcurrentExecutor;

    private AsyncTaskExecutor() {}

    static {
        CORE_POOL_SIZE    = 5;
        MAXIMUM_POOL_SIZE = 128;
        KEEP_ALIVE        = 1;
        TIME_UNIT         = TimeUnit.SECONDS;

        mConcurrentPoolWorkQueue = new LinkedBlockingQueue<Runnable>(10);
        mConcurrentThreadFactory = new AsyncTaskThreadFactory();
        mConcurrentExecutor      = new ThreadPoolExecutor(
                CORE_POOL_SIZE,
                MAXIMUM_POOL_SIZE,
                KEEP_ALIVE,
                TIME_UNIT,
                mConcurrentPoolWorkQueue,
                mConcurrentThreadFactory
        );
    }

    /**
     * Concurrently executes AsyncTask on any Android version
     * @param task to execute
     * @param params for task
     * @return executing AsyncTask
     */
    @SuppressLint("NewApi")
    public static <Params, Progress, Result> AsyncTask<Params, Progress, Result>
    executeConcurrently(AsyncTask<Params, Progress, Result> task, Params... params) {
            task.executeOnExecutor(mConcurrentExecutor, params);

        return task;
    }

    /**
     * Thread factory for AsyncTaskExecutor
     * @author Artem Zinnatullin
     */
    private static class AsyncTaskThreadFactory implements ThreadFactory {
        private final AtomicInteger count = new AtomicInteger(1);

        @Override
        public Thread newThread(@NonNull Runnable r) {
            return new Thread(r, "AsyncTask #" + count.getAndIncrement());
        }
    }
}