/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common;

import android.view.View;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public class DividerViewHolder extends ViewHolder {
    public DividerViewHolder(View itemView) {
        super(itemView);
    }
}
