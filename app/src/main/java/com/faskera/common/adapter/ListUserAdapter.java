/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.faskera.common.AppSharePreference;
import com.faskera.common.ImlBaseRow;
import com.faskera.common.RowLoadMore;
import com.faskera.common.UserDefine;
import com.faskera.common.ViewHolder;
import com.faskera.common.debug.DebugUtil;
import com.faskera.era.R;
import com.faskera.utils.ImageLoader;
import com.faskera.view.activity.follow.ImlListUser;
import com.faskera.view.activity.home.HomeViewHolder;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * @author DANGNGOCDUC
 * @since 1/4/2017
 */

public class ListUserAdapter extends RecyclerView.Adapter {

    private ArrayList<ImlListUser> mListVoter = new ArrayList<>();
    private LayoutInflater mLayoutInflater;
    private CallbackFollow mCallbackClick;
    private Context mContext;
    private String mFollow, mFollowing;

    private View.OnClickListener onClickUser = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mCallbackClick != null) {
                mCallbackClick.onClickUser((String) v.getTag());
            }
        }
    };

    public synchronized void setLoadMore(boolean loadMore) {
        try {
            if (loadMore) {
                if (mListVoter.get(mListVoter.size() - 1).getType() != 0) {
                    mListVoter.add(new RowLoadMore());
                    notifyItemInserted(mListVoter.size() - 1);
                }
            } else {
                if (mListVoter.get(mListVoter.size() - 1).getType() == 0) {
                    mListVoter.remove(mListVoter.size() - 1);
                    notifyDataSetChanged();
                }
            }
        } catch (IndexOutOfBoundsException e) {

            DebugUtil.d("===========================ListUserAdapter-setLoadMore", e.toString());
            
        }
    }

    private View.OnClickListener onClickState = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mCallbackClick != null) {
                mCallbackClick.onClickState((int) v.getTag());
            }
        }
    };

    synchronized public boolean isLoadMore() {
        if (mListVoter.size() == 0) {
            return false;
        } else {
            return mListVoter.get(mListVoter.size() - 1).getType() == ImlBaseRow.TYPE_LOAD_MORE;

        }
    }

    public ListUserAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
        mFollow = context.getResources().getString(R.string.follow);
        mFollowing = context.getResources().getString(R.string.following_caps);
    }

    public void setCallbackClick(CallbackFollow mCallbackClick) {
        this.mCallbackClick = mCallbackClick;
    }

    public AbsUserState getUserAtPosition(int pos) {
        if (mListVoter.get(pos) instanceof AbsUserState) {
            return (AbsUserState) mListVoter.get(pos);
        } else {
            return null;
        }
    }

    public void setListVoter(ArrayList<AbsUserState> list) {
        mListVoter.clear();
        mListVoter.addAll(list);
        notifyDataSetChanged();
    }

    public void addListVoter(ArrayList<AbsUserState> list) {
        mListVoter.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return mListVoter.get(position).getType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case 0:
                view = mLayoutInflater.inflate(R.layout.item_loading, parent, false);
                return new  HomeViewHolder.ItemLoadingViewHolder(view);
            default:
                view = mLayoutInflater.inflate(R.layout.row_view_vote, parent, false);
                view.setOnClickListener(onClickUser);
                return new FollowViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
       int type = getItemViewType(position);
        switch (type) {
            case 0 :
                break;
            default:
                ((FollowViewHolder) holder).mTextViewFollow.setTag(position);
                ((FollowViewHolder) holder).mTextViewFollow.setOnClickListener(onClickState);
                ((FollowViewHolder) holder).bindData(mContext, (AbsUserState) mListVoter.get(position));
                break;

        }

    }

    @Override
    public int getItemCount() {
        return mListVoter.size();
    }


    public  class FollowViewHolder extends ViewHolder {

        @BindView(R.id.imageview)
        public ImageView mImageAvatar;

        @BindView(R.id.textview_name)
        public TextView mTextName;

        @BindView(R.id.textview_follow)
        public TextView mTextViewFollow;

        public FollowViewHolder(View itemView) {
            super(itemView);
        }

        public void bindData(Context context, AbsUserState data) {
            itemView.setTag(data.getUid());
            ImageLoader.loadImageViewWith(context, data.getAvatar(), mImageAvatar);

            if (data.getAvatar() == null || data.getAvatar().trim().isEmpty()) {
                ImageLoader.loadImageViewWith(context, R.drawable.ic_avatar_default, mImageAvatar);
            } else {

                    ImageLoader.loadImageViewWith(context, data.getAvatar(), mImageAvatar);
            }
            mTextName.setText(data.getName());
            if (data.getSate() == UserDefine.STATE_FOLLOW) {

                if (data.getUid().equals(AppSharePreference.getUid(context))) {
                    mTextViewFollow.setVisibility(View.GONE);
                } else {
                    mTextViewFollow.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT < 23) {
                        mTextViewFollow.setTextAppearance(context, R.style.Follow);
                    } else {
                        mTextViewFollow.setTextAppearance(R.style.Follow);
                    }
                    mTextViewFollow.setText(mFollow);
                    mTextViewFollow.setBackgroundResource(R.drawable.bg_follow);
                }

            } else if (data.getSate() == UserDefine.STATE_FOLLWING) {
                mTextViewFollow.setVisibility(View.VISIBLE);

                if (Build.VERSION.SDK_INT < 23) {
                    mTextViewFollow.setTextAppearance(context, R.style.Following);
                } else {
                    mTextViewFollow.setTextAppearance(R.style.Following);
                }
                mTextViewFollow.setText(mFollowing);
                mTextViewFollow.setBackgroundResource(R.drawable.bg_following);

            } else {
                mTextViewFollow.setVisibility(View.GONE);
            }
        }
    }

    public interface CallbackFollow {

        void onClickState(int pos);

        void onClickUser(String userID);
    }
}
