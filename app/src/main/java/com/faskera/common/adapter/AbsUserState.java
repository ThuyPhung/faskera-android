/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.adapter;

import com.faskera.common.UserDefine;
import com.faskera.view.activity.follow.ImlListUser;

/**
 * @author DANGNGOCDUC
 * @since 1/3/2017
 */

public interface AbsUserState extends ImlListUser {

    String getAvatar();

    String getUid();

    String getName();

    @UserDefine.StateFollow
    int getSate();

    void setSate( @UserDefine.StateFollow int state);

}
