/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.adapter;

import com.faskera.model.user.Profile;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author DANGNGOCDUC
 * @since 1/25/2017
 */

public class ListPeople implements Serializable {

    @SerializedName("list")
    public ArrayList<Profile> mlistUser;

    @SerializedName("nextMaxId")
    public String mNextMaxId;

    @SerializedName("size")
    public int mSize;
}
