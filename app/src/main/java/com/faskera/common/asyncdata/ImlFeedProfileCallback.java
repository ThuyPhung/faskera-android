/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.asyncdata;

/**
 * @author DANGNGOCDUC
 * @since 2/2/2017
 */

public interface ImlFeedProfileCallback extends ImlFeedCallback {

    void onEditProfile();

    void onFollowClick();

    void onFollowingClick();

    void onViewListFollower(String uid);

    void onViewListFollowing(String uid);

}
