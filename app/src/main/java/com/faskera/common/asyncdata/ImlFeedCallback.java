/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.asyncdata;

import android.view.View;

import com.faskera.view.activity.home.model.Feed;
import com.faskera.view.activity.home.model.abst.AbsFeedAns;
import com.faskera.view.activity.home.model.abst.FeedAbs;

/**
 * @author DANGNGOCDUC
 * @since 2/2/2017
 */

public interface ImlFeedCallback {

    void onHeaderClick(String userId);

    void onViewPeopleVoted(String mPollId);

    void onClickCreatePoll(View v);

    void onFeedMenuClick(FeedAbs feed, boolean isMyFeed);

    void onGotoDetailClick(Feed.Poll pollID);

    void onClickAsw(AbsFeedAns absFeedAns);

    void onClickShare(FeedAbs feedAbs);

    void onViewOptionClick(String optionID);

}
