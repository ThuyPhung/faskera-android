/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.asyncdata;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.faskera.common.AppSharePreference;
import com.faskera.common.Divider;
import com.faskera.common.DividerViewHolder;
import com.faskera.common.RowLoadMore;
import com.faskera.common.UserDefine;
import com.faskera.era.R;
import com.faskera.model.user.Profile;
import com.faskera.utils.IntentUtils;
import com.faskera.utils.NetworkUtils;
import com.faskera.view.activity.home.HomeActivityOptimize;
import com.faskera.view.activity.home.HomeViewHolder;
import com.faskera.view.activity.home.model.Feed;
import com.faskera.view.activity.home.model.FeedCreateNew;
import com.faskera.view.activity.home.model.FeedViewDetail;
import com.faskera.view.activity.home.model.ImlHomeRow;
import com.faskera.view.activity.home.model.abst.AbsFeedAns;
import com.faskera.view.activity.home.model.abst.AbsFeedContentMedia;
import com.faskera.view.activity.home.model.abst.AbsFeedContentText;
import com.faskera.view.activity.home.model.abst.AbsFeedFooter;
import com.faskera.view.activity.home.model.abst.AbsFeedHeader;
import com.faskera.view.activity.home.model.abst.AbsFeedSuperHeader;
import com.faskera.view.activity.home.model.abst.FeedAbs;
import com.faskera.view.activity.mypage.abs.AbsInfoExtra;
import com.faskera.view.activity.mypage.abs.AbsProfile;
import com.faskera.view.activity.viewvote.ViewVoteActivity;

import java.util.ArrayList;

import static com.faskera.common.ImlBaseRow.TYPE_LOAD_MORE;

/**
 * @author DANGNGOCDUC
 * @since 2/20/2017
 */
public class CursorAdapterOptimize extends RecyclerView.Adapter {

    private Context mContext;
    private Profile mProfile;
    private ArrayList<ImlHomeRow> listInfo = new ArrayList<>();
    private ImlFeedCallback mCallback;
    private ArrayList<ImlHomeRow> mListItem = new ArrayList<>();
    private LayoutInflater mLayoutInflater;
    public boolean mViewMore = false;
    private long timeClick;

    public int getIndex(ImlHomeRow object) {
        return mListItem.indexOf(object);
    }

    public CursorAdapterOptimize(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
        if (AppSharePreference.getInstance(context).isUserLoggedIn()) {
            if (mContext instanceof HomeActivityOptimize) {
                mListItem.add(new Divider());
            mListItem.add(new FeedCreateNew());
            mListItem.add(new Divider());
        }

        }
    }

    public void setCallbackImageFeed(ImlFeedCallback callback) {
        mCallback = callback;
    }

    public void clearData() {
        mListItem.clear();
        if (AppSharePreference.getInstance(mContext).isUserLoggedIn()) {
            if (mContext instanceof HomeActivityOptimize) {
                mListItem.add(new Divider());
                mListItem.add(new FeedCreateNew());
                mListItem.add(new Divider());
            }
        }
        notifyDataSetChanged();
    }

    /**
     *
     * @param pollID Identify of Poll to Del.
     *
     */
    public void removePoll(String pollID) {
       for (int i = 0; i < mListItem.size(); i++) {
           if (mListItem.get(i) instanceof AbsFeedHeader) {
               if (((AbsFeedHeader) mListItem.get(i)).getPoll().getPollId().equals(pollID)) {

                   ArrayList<ImlHomeRow> listDelete = new ArrayList<>();
                   if (i > 0) {
                       // khong phai feed dau tien
                       if (mListItem.get(i -1) instanceof AbsFeedSuperHeader) {
                           listDelete.add(mListItem.get(i - 1));
                        }
                   }
                   listDelete.add(mListItem.get(i));
                   int j = i + 1;
                   do {
                       if (mListItem.get(j) instanceof AbsFeedHeader || mListItem.get(j) instanceof AbsFeedSuperHeader) {
                           break;
                       } else {
                           listDelete.add(mListItem.get(j));
                           j++;
                       }

                   } while (j < mListItem.size());

                   mListItem.removeAll(listDelete);
                   if (listDelete.size() > 0) {
                       if (listDelete.get(0) instanceof AbsFeedSuperHeader) {
                           notifyItemRangeRemoved(i -1, listDelete.size());
                       } else {
                           notifyItemRangeRemoved(i , listDelete.size());

                       }
                   }

                   break;
               }
            }
       }
    }

    public int findOffsetInsertNewFeedProfile() {
        if (mViewMore) {
            return 2 + listInfo.size()  + 3;
        } else {
            return  5;
        }
    }

    public void insertListPoll(ArrayList<ImlHomeRow> list) {
        mListItem.addAll(list);
        notifyItemRangeInserted(mListItem.size() - list.size(), list.size());
    }

    public AbsFeedAns findFeedAns(String idAsw) {
        for (ImlHomeRow item : mListItem) {
            if (item instanceof AbsFeedAns) {
                if (((AbsFeedAns) item).getAnsID().equals(idAsw)) {
                    return (AbsFeedAns)item;
                }
            }
        }
        return null; // not Found Asw
    }

    public void insertListPoll(ArrayList<ImlHomeRow> list, int pos) {
        mListItem.addAll(pos, list);
        notifyItemRangeInserted(pos, list.size());
    }

    @FooterType.FooterTypeIml
    private int mTypeLoadMore = 0;

    public void setProfile(AbsProfile profile) {
        mProfile = (Profile) profile;
        listInfo = new ArrayList<>(mProfile.genListInfoExtra());
    }

    public void updateUserInfo(AbsProfile profile) {
        mProfile =(Profile) profile;
        if (!mViewMore) {
            listInfo.clear();
            listInfo.addAll(mProfile.genListInfoExtra());
        } else {
            mListItem.removeAll(listInfo);
            listInfo.clear();
            listInfo.addAll(mProfile.genListInfoExtra());
            mListItem.addAll(1, listInfo);
        }

        notifyDataSetChanged();
    }

    public int getTypeLoadMore() {
        return mTypeLoadMore;
    }

    public void setTypeLoadMore( @FooterType.FooterTypeIml int typeLoadMore) {
        if (mTypeLoadMore != typeLoadMore) {
            if (typeLoadMore == FooterType.TYPE_LOADMORE) {
                if (mTypeLoadMore ==  FooterType.TYPE_LOADMORE_ERROR) {
                    notifyItemChanged(getItemCount() - 1);
                } else {
                    mListItem.add(new RowLoadMore());
                    notifyItemInserted(mListItem.size() );
                }
                mTypeLoadMore = typeLoadMore;

            } else  if (typeLoadMore == FooterType.TYPE_LOADMORE_ERROR) {
                if (mTypeLoadMore ==  FooterType.TYPE_LOADMORE) {
                    notifyItemChanged(getItemCount() - 1);
                } else {
                    mListItem.add(new RowLoadMore());
                    notifyItemInserted(mListItem.size() );
                }
                mTypeLoadMore = typeLoadMore;

            } else {
                mTypeLoadMore = FooterType.TYPE_LOADMORE_NONE;
                int size = mListItem.size();
                int removeIndex = size - 1;
                if (size > 0 && mListItem.get(removeIndex).getType() == TYPE_LOAD_MORE) {
                    mListItem.remove(removeIndex);
                    notifyItemRemoved(removeIndex);
                }
            }
        }

    }

    private View.OnClickListener mOnClickViewDetail = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mCallback != null && v.getTag() != null && v.getTag() instanceof Feed.Poll) {
                mCallback.onGotoDetailClick( (Feed.Poll)v.getTag());
            }

        }
    };

    private View.OnClickListener getOnClickVote = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (System.currentTimeMillis() - timeClick > 300) {
                if (v.getTag() != null)
                {
                    timeClick = System.currentTimeMillis();
                    mCallback.onClickAsw((AbsFeedAns)v.getTag());
                }
            }
        }
    };

    private View.OnClickListener mOnClickShare = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            FeedAbs id = (FeedAbs)v.getTag();

            if (mCallback != null) {
                mCallback.onClickShare(id);
            }

        }
    };

    private View.OnClickListener mOnViewPeopleVoted = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (NetworkUtils.isOnline(mContext)) {
                if (AppSharePreference.getInstance(mContext).isUserLoggedIn()) {
                    String id = (String) v.getTag();

                    Bundle bundle = new Bundle();
                    bundle.putString(ViewVoteActivity.BUNDLE_POLL_ID, id);
                    IntentUtils.startActivityViewVote(bundle, mContext);
                } else {
                    IntentUtils.startActivityLogin(null, mContext);
                }
            } else {
                Toast.makeText(mContext, R.string.error_connection, Toast.LENGTH_SHORT).show();
            }
        }
    };

    private View.OnClickListener mOnGotoProfile = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (mCallback != null && v.getTag() != null) {
                String id =  (String) v.getTag();
                if (!id.isEmpty()) {
                    mCallback.onHeaderClick((String) v.getTag());

                }
            }
        }
    };

    private View.OnClickListener onClickCreatePoll = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if (mCallback != null) {
              mCallback.onClickCreatePoll(v);
          }
        }
    };

    private View.OnClickListener onClickMenuFeed = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Feed.Poll feed = (Feed.Poll) v.getTag();
            if (mCallback != null) {
                if (feed.mCreatedUser != null) {
                    if (feed.mCreatedUser.id.equals(AppSharePreference.getUid(mContext))) {
                        mCallback.onFeedMenuClick(feed, true);
                    } else {
                        mCallback.onFeedMenuClick(feed, false);
                    }
                } else {
                    mCallback.onFeedMenuClick(feed, feed.mMine);
                }
            }

        }
    };

    private View.OnClickListener onClickViewMore = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (!mViewMore) {
                mViewMore = true;
                notifyItemChanged(1);
                 mListItem.addAll(1, listInfo);
                notifyItemRangeInserted(1, listInfo.size());

            } else {
                mViewMore = false;
                notifyItemChanged(1 + listInfo.size());
                mListItem.removeAll(listInfo);
                notifyItemRangeRemoved(1, listInfo.size());
            }
        }
    };

    private View.OnClickListener mOnClickEditProfile = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag() != null && mCallback != null) {
                if (mProfile.getUserId().equals(AppSharePreference.getUid(mContext))) {
                    ((ImlFeedProfileCallback) mCallback).onEditProfile();
                } else {
                    if (mProfile.getStateFollow() == UserDefine.STATE_FOLLOW) {
                        ((ImlFeedProfileCallback) mCallback).onFollowClick();
                    } else {
                        ((ImlFeedProfileCallback) mCallback).onFollowingClick();
                    }
                }
            }

        }
    };

    private View.OnClickListener mOnClickShowListFollower = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag() != null) {
                if (mCallback != null ) {
                    ((ImlFeedProfileCallback) mCallback).onViewListFollower((String)v.getTag());
                }
            }
        }
    };

    private View.OnClickListener mOnClickShowListFollowing = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag() != null) {
                if (mCallback != null )  {
                    ((ImlFeedProfileCallback) mCallback).onViewListFollowing((String)v.getTag());
                }
            }
        }
    };

    private View.OnClickListener onClickOption = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String  id = (String) v.getTag();
            if (mCallback != null) {
                mCallback.onViewOptionClick(id);
            }
        }
    };

    @Override
    public int getItemViewType(int position) {

        return mListItem.get(position).getType();
    }

    protected ImlHomeRow getItem(int pos) {
        return mListItem.get(pos);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case ImlHomeRow.TYPE_CREATE_FEED:
                view = mLayoutInflater.inflate(R.layout.item_feed_create, parent, false);
                view.findViewById(R.id.tv_ask_something).setOnClickListener(onClickCreatePoll);
                view.findViewById(R.id.iv_camera).setOnClickListener(onClickCreatePoll);
                return new HomeViewHolder.FeedViewHolderCreate(view);

            case ImlHomeRow.TYPE_HEADER_FEED:
                view = mLayoutInflater.inflate(R.layout.item_feed_header, parent, false);
                HomeViewHolder.FeedViewHolderHeader headerHolder =  new HomeViewHolder.FeedViewHolderHeader(view);
                headerHolder.itemView.setOnClickListener(mOnGotoProfile);
                headerHolder.mImageMore.setOnClickListener(onClickMenuFeed);
                return headerHolder;

            case ImlHomeRow.TYPE_CONTENT_FEED:
                view = mLayoutInflater.inflate(R.layout.item_feed_content, parent, false);
                return new HomeViewHolder.FeedViewHolderContent(view);

            case ImlHomeRow.TYPE_CONTENT_FEED_IMAGE:
                view = mLayoutInflater.inflate(R.layout.item_feed_content_image, parent, false);
                return new HomeViewHolder.FeedViewHolderContentImage(view);

            case ImlHomeRow.TYPE_ANS:
                view = mLayoutInflater.inflate(R.layout.item_feed_asw, parent, false);
                return new HomeViewHolder.FeedViewHolderAns(view);

            case ImlHomeRow.TYPE_FOOTER:
                view = mLayoutInflater.inflate(R.layout.item_feed_footer, parent, false);
                view.setOnClickListener(mOnViewPeopleVoted);
                return new HomeViewHolder.FeedViewHolderFooter(view);

            case ImlHomeRow.TYPE_SUPER_HEADER:
                view = mLayoutInflater.inflate(R.layout.item_super_header, parent, false);
                return new HomeViewHolder.FeedViewHolderSuperHeader(view);

            case ImlHomeRow.TYPE_DIVIDER:
                view = mLayoutInflater.inflate(R.layout.item_big_divider, parent, false);
                return new DividerViewHolder(view);

            case ImlHomeRow.TYPE_VIEW_MY_PROFILE:
                view = mLayoutInflater.inflate(R.layout.item_myprofile, parent, false);
                return new HomeViewHolder.ProfileUser(view);

            case ImlHomeRow.TYPE_VIEW_MORE_INFO:
                view = mLayoutInflater.inflate(R.layout.item_view_more_profile, parent, false);
                view.setOnClickListener(onClickViewMore);
                return new HomeViewHolder.ViewMoreViewHolder(view);

            case ImlHomeRow.TYPE_VIEW_INFO_USER:
                view = mLayoutInflater.inflate(R.layout.item_user_info, parent, false);
                return new HomeViewHolder.InfoExtraViewHolder(view);

            case ImlHomeRow.TYPE_VIEW_DETAIL:
                view = mLayoutInflater.inflate(R.layout.item_feed_asw, parent, false);
                view.setOnClickListener(mOnClickViewDetail);
                return new HomeViewHolder.ItemViewDetailViewHolder(view);

            case TYPE_LOAD_MORE:
                view = mLayoutInflater.inflate(R.layout.item_loading, parent, false);
                return new HomeViewHolder.ItemLoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        int viewType = getItemViewType(position);
        switch (viewType) {
            case ImlHomeRow.TYPE_CREATE_FEED: // Row Create Poll

                break;
            case ImlHomeRow.TYPE_HEADER_FEED:
                    if ( ((AbsFeedHeader) getItem(position)).getPoll().mCreatedUser != null) {
                        holder.itemView.setTag( ((AbsFeedHeader) getItem(position)).getPoll().mCreatedUser.id);
                    } else {
                        holder.itemView.setTag("");
                    }
                    ((HomeViewHolder.FeedViewHolderHeader) holder).mImageMore.setTag( ((AbsFeedHeader) getItem(position)).getPoll());
                    ((HomeViewHolder.FeedViewHolderHeader) holder).bindData(mContext, ((AbsFeedHeader) getItem(position)), mProfile);


                break;
            case ImlHomeRow.TYPE_CONTENT_FEED:

                    ((HomeViewHolder.FeedViewHolderContent) holder).bindData(mContext,  ((AbsFeedContentText) getItem(position)));
                break;
            case ImlHomeRow.TYPE_CONTENT_FEED_IMAGE:
                    ((HomeViewHolder.FeedViewHolderContentImage) holder).bindData(mContext,  ((AbsFeedContentMedia) getItem(position)));
                break;
            case ImlHomeRow.TYPE_ANS:
                ((HomeViewHolder.FeedViewHolderAns) holder).bindData(mContext,( ((AbsFeedAns) getItem(position))));
                ((HomeViewHolder.FeedViewHolderAns) holder).itemView.setTag(getItem(position));
                ((HomeViewHolder.FeedViewHolderAns) holder).itemView.setOnClickListener(getOnClickVote);
                ((HomeViewHolder.FeedViewHolderAns) holder).mTxtVotes.setTag(((AbsFeedAns) getItem(position)).getAnsID());
                if (((AbsFeedAns) getItem(position)).getVoteInt() > 0) {
                    ((HomeViewHolder.FeedViewHolderAns) holder).mTxtVotes.setOnClickListener(onClickOption);
                } else {
                    ((HomeViewHolder.FeedViewHolderAns) holder).mTxtVotes.setOnClickListener(null);
                }

                break;
            case ImlHomeRow.TYPE_FOOTER:
                ((HomeViewHolder.FeedViewHolderFooter) holder).bindData(mContext, ((AbsFeedFooter) getItem(position)));
                ((HomeViewHolder.FeedViewHolderFooter) holder).itemView.setTag( ((AbsFeedFooter) getItem(position)).getPollID());

                if (((AbsFeedFooter) getItem(position)).getVoteInt() > 0) {
                    ((HomeViewHolder.FeedViewHolderFooter) holder).itemView.setOnClickListener(mOnViewPeopleVoted);
                } else {
                    ((HomeViewHolder.FeedViewHolderFooter) holder).itemView.setOnClickListener(null);

                }
                ((HomeViewHolder.FeedViewHolderFooter) holder).mTextShare.setTag( ((AbsFeedFooter) getItem(position)).getPoll());
                ((HomeViewHolder.FeedViewHolderFooter) holder).mTextShare.setOnClickListener(mOnClickShare);
                break;
            case ImlHomeRow.TYPE_SUPER_HEADER:
                ((HomeViewHolder.FeedViewHolderSuperHeader) holder).bindData(mContext,((AbsFeedSuperHeader)getItem(position)));
                if (((AbsFeedSuperHeader)getItem(position)).getTypeHeader() == AbsFeedSuperHeader.FOLLWING) {
                    ((HomeViewHolder.FeedViewHolderSuperHeader) holder).itemView.setTag(((AbsFeedSuperHeader)getItem(position)).getStringUid());
                    ((HomeViewHolder.FeedViewHolderSuperHeader) holder).itemView.setOnClickListener(mOnGotoProfile);
                } else {
                    ((HomeViewHolder.FeedViewHolderSuperHeader) holder).itemView.setOnClickListener(null);
                }
                break;

            case ImlHomeRow.TYPE_VIEW_DETAIL:
                ((HomeViewHolder.ItemViewDetailViewHolder) holder).itemView.setTag( ((FeedViewDetail)getItem(position)).getPoll());
                ((HomeViewHolder.ItemViewDetailViewHolder) holder).bindData(mContext, ((FeedViewDetail)getItem(position)));
                break;

            case TYPE_LOAD_MORE:
                break;

            case ImlHomeRow.TYPE_VIEW_MY_PROFILE:
                ((HomeViewHolder.ProfileUser) holder).bindData(mContext, mProfile);
                ((HomeViewHolder.ProfileUser) holder).mEditProfile.setTag(mListItem.get(position));
                ((HomeViewHolder.ProfileUser) holder).mFollowingView.setTag(((AbsProfile) mListItem.get(position)).getUserId());
                ((HomeViewHolder.ProfileUser) holder).mFollowView.setTag(((AbsProfile) mListItem.get(position)).getUserId());

                ((HomeViewHolder.ProfileUser) holder).mEditProfile.setOnClickListener(mOnClickEditProfile);
                if (((AbsProfile) mListItem.get(position)).getNumberFollower() > 0) {
                    ((HomeViewHolder.ProfileUser) holder).mFollowView.setOnClickListener(mOnClickShowListFollower);
                } else {
                    ((HomeViewHolder.ProfileUser) holder).mFollowView.setOnClickListener(null);
                }

                if (((AbsProfile) mListItem.get(position)).getNumberFollowing() > 0) {
                    ((HomeViewHolder.ProfileUser) holder).mFollowingView.setOnClickListener(mOnClickShowListFollowing);
                } else {
                    ((HomeViewHolder.ProfileUser) holder).mFollowingView.setOnClickListener(null);
                }
                break;

            case ImlHomeRow.TYPE_VIEW_INFO_USER:
             ((HomeViewHolder.InfoExtraViewHolder) holder).bindData(mContext, (AbsInfoExtra) mListItem.get(position));
                break;
            case ImlHomeRow.TYPE_VIEW_MORE_INFO:
                if (listInfo.size() == 0) {
                    ((HomeViewHolder.ViewMoreViewHolder) holder).mRoot.setVisibility(View.GONE);
                } else {
                    ((HomeViewHolder.ViewMoreViewHolder) holder).mRoot.setVisibility(View.VISIBLE);
                }
                ((HomeViewHolder.ViewMoreViewHolder) holder).bindData(mContext, mViewMore);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mListItem.size();
    }
}
