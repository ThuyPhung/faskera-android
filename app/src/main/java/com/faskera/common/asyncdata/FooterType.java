/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.asyncdata;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author DANGNGOCDUC
 * @since 1/14/2017
 */


public class FooterType {
    public static final int TYPE_LOADMORE_NONE = 0;
    public static final int TYPE_LOADMORE = 1;
    public static final int TYPE_LOADMORE_ERROR = 2;


    @Retention(RetentionPolicy.SOURCE)
    @IntDef({TYPE_LOADMORE, TYPE_LOADMORE_ERROR, TYPE_LOADMORE_NONE})
    public @interface FooterTypeIml {}



}
