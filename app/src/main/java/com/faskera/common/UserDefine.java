/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/*
  Created by DANGNGOCDUC on 1/6/2017.
 */

/**
 *
 * <p>
 *     ??nh ngh?a nh?ng h?ng s? cho các thông tin dùng chung trong app nh? lo?i User , Sate Follow
 * </p>
 */
public class UserDefine {

    public static final int STATE_FOLLOW = 1;
    public static final int STATE_FOLLWING = 2;
    public static final int STATE_NONE = 3;

    public static final int USER_TYPE_PROFILE = 1;
    public static final int USER_TYPE_FRIENDS = 2;



    @Retention(RetentionPolicy.SOURCE)
    @IntDef({USER_TYPE_FRIENDS, USER_TYPE_PROFILE})
    public @interface TypeUser {}


    @Retention(RetentionPolicy.SOURCE)
    @IntDef({STATE_FOLLOW, STATE_FOLLWING, STATE_NONE})
    public @interface StateFollow{
    }


}
