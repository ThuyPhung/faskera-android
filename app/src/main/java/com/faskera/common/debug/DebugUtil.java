/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.debug;

import android.util.Log;

import com.faskera.era.BuildConfig;

/**
 * @author ANDROID
 * @since 5/25/2016
 */
public class DebugUtil {

    public static final boolean IS_DEBUG = BuildConfig.DEBUG;

    public static void d(String TAG , String info){
        if(IS_DEBUG) Log.d(TAG,info);
    }

    public static void e(String TAG , String info){
        if(IS_DEBUG) Log.e(TAG,info);
    }

    public static void i(String TAG , String info){
        if(IS_DEBUG) Log.i(TAG,info);
    }

}
