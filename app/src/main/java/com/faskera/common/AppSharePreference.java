/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.faskera.model.user.MyProfile;
import com.faskera.model.user.UserInfoResponse;
import com.faskera.model.user.token.OAuthToken;
import com.google.gson.Gson;

/**
 * @author DANGNGOCDUC
 * @since 1/3/2017
 */

public class AppSharePreference {

    public static final String OAUTH_TOKEN = "OAUTH_TOKEN";
    public static final String USER_INFO = "USER_INFO";
    public static final String MY_PROFILE = "MY_PROFILE";
    public static final String SELECTED_LANGUAGE = "Locale.Helper.Selected.Language";
    public static final String NOTIFICATION_COUNTER = "NOTIFICATION_COUNTER";
    public static final String REF_FIRST_USING = "REF_FIRST_USING";
    public static final String REF_TOKEN_FACE = "REF_TOKEN_FACE";
    public static final String REF_TOKEN_NOTIFICATION = "REF_TOKEN_NOTIFICATION";

    private static AppSharePreference instance;

    private SharedPreferences privateSharedPreferences;

    private AppSharePreference(Context context) {
        this.privateSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static AppSharePreference getInstance(Context context) {
        synchronized (AppSharePreference.class) {
            if (instance == null) {
                instance = new AppSharePreference(context);
            }
            return instance;
        }
    }

    public String getStringFromSharedPreferences(String key) {
        return privateSharedPreferences.getString(key, "");
    }

    public void clearData() {
        SharedPreferences.Editor editor = privateSharedPreferences.edit();
        editor.putString(OAUTH_TOKEN, "");
        editor.putString(USER_INFO, "");
        editor.putString(MY_PROFILE, "");
        editor.putInt(NOTIFICATION_COUNTER, 0);
        editor.apply();
    }

    public static String getToken(Context context) {
        //// TODO: 1/11/2017 Test fake token
        if (AppSharePreference.getInstance(context).getOAuthToken() != null)
            return AppSharePreference.getInstance(context).getOAuthToken().accessToken;
        else return "";
    }

    public static String getRefreshToken(Context context) {
        if (AppSharePreference.getInstance(context).getOAuthToken() != null)
            return AppSharePreference.getInstance(context).getOAuthToken().mRefreshToken;
        else return "";
    }

    public static String getTokenType(Context context) {
        if (AppSharePreference.getInstance(context).getOAuthToken() != null)
            return AppSharePreference.getInstance(context).getOAuthToken().getTokenType();
        else
            return "";
    }

    public static String getUid(Context context) {
        OAuthToken oAuthToken = AppSharePreference.getInstance(context).getOAuthToken();
        UserInfoResponse userInfo = AppSharePreference.getInstance(context).getUserInfo();

        if (oAuthToken != null && userInfo != null)
            return userInfo.id;
        else return "";
    }

    public void storeFaceToken(String content) {
        SharedPreferences.Editor editor = privateSharedPreferences.edit();
        editor.putString(REF_TOKEN_FACE, content).apply();
    }

    public String getFaceToken() {
        return getStringFromSharedPreferences(REF_TOKEN_FACE);
    }

    //<editor-fold desc="OAUTH_TOKEN">
    public void storeOAuthToken(OAuthToken oAuthToken) {
        SharedPreferences.Editor editor = privateSharedPreferences.edit();
        editor.putString(OAUTH_TOKEN, OAuthToken.getOAuthTokenString(oAuthToken)).apply();
    }

    public OAuthToken getOAuthToken() {
        return OAuthToken.getOAuthToken(getStringFromSharedPreferences(OAUTH_TOKEN));
    }
    //</editor-fold>

    //<editor-fold desc="USER_INFO">
    public void storeUserInfo(UserInfoResponse userInfoResponse) {
        SharedPreferences.Editor editor = privateSharedPreferences.edit();
        editor.putString(USER_INFO, UserInfoResponse.getUserInfoString(userInfoResponse));
        editor.apply();
    }

    public boolean isUserLoggedIn() {
        return !getStringFromSharedPreferences(USER_INFO).equals("");
    }

    public UserInfoResponse getUserInfo() {
        return UserInfoResponse.getUserInfo(getStringFromSharedPreferences(USER_INFO));
    }

    //</editor-fold>

    //<editor-fold desc="MY_PROFILE">
    public void storeMyProfile(MyProfile myProfile) {
        SharedPreferences.Editor editor = privateSharedPreferences.edit();
        editor.putString(MY_PROFILE, getMyProfileJson(myProfile));
        editor.apply();
    }

    public MyProfile getMyProfileObj() {
        String myProfileJson = privateSharedPreferences.getString(MY_PROFILE, "");
        if (!myProfileJson.equals(""))
            return new Gson().fromJson(myProfileJson, MyProfile.class);
        return null;
    }

    private String getMyProfileJson(MyProfile myProfile) {
        return new Gson().toJson(myProfile);
    }
    //</editor-fold>

    //<editor-fold desc="LANGUAGE">
    public void setMyLanguage(String key) {
        SharedPreferences.Editor editor = privateSharedPreferences.edit();
        editor.putString(SELECTED_LANGUAGE, key);
        editor.apply();
    }

    public String getMyLanguage() {
        return privateSharedPreferences.getString(SELECTED_LANGUAGE, "");
    }
    //</editor-fold>

    //<editor-fold desc="NOTIFICATION">
    public void setNotificationCounter(int number) {
        SharedPreferences.Editor editor = privateSharedPreferences.edit();
        if (number == 0)     //Reset counter when go to notification activity
            editor.putInt(NOTIFICATION_COUNTER, 0);
        else // Increase counter received
            editor.putInt(NOTIFICATION_COUNTER, getNotificationCounter() + number);
        editor.apply();

    }

    public int getNotificationCounter() {
        return privateSharedPreferences.getInt(NOTIFICATION_COUNTER, 0);
    }
    //</editor-fold>

    //<editor-fold desc="FIRST_USING">
    public void setFirstTimeUsing(boolean firstUsing) {
        SharedPreferences.Editor editor = privateSharedPreferences.edit();
        editor.putBoolean(REF_FIRST_USING, firstUsing).apply();
    }

    public boolean isFirstTimeUsing() {
        return privateSharedPreferences.getBoolean(REF_FIRST_USING, true);
    }
    //</editor-fold>

    //<editor-fold desc="TOKEN_NOTIFICATION">
    public void storeTokenNotification(String token) {
        SharedPreferences.Editor editor = privateSharedPreferences.edit();
        editor.putString(REF_TOKEN_NOTIFICATION, token).apply();
    }

    public String getTokenNotification() {
        return privateSharedPreferences.getString(REF_TOKEN_NOTIFICATION, "");
    }
    //</editor-fold
}
