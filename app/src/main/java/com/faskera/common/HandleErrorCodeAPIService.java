/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common;

import android.support.annotation.IntDef;

import com.faskera.common.base.BaseActivity;
import com.faskera.utils.IntentUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author DANGNGOCDUC
 * @since 1/23/2017
 */

public class HandleErrorCodeAPIService {
    private static HandleErrorCodeAPIService mInstance;

    public static HandleErrorCodeAPIService getInstance() {
        if (mInstance == null) {
            mInstance = new HandleErrorCodeAPIService();
        }
        return mInstance;

    }

    public  void handleErrorCode(BaseActivity context, int error) {
        switch (error) {
            case -1 :
                break;
            case ERROR_TOKEN:
                context.reNewtoken();
                break;
            case ERROR_TOPIC_NOT_ENOUGH:
                IntentUtils.startActivityUpdateTopic(null, context);
                break;
            default:
        }

    }
    public static final int ERROR_UNKNOWN_ERROR = -1;
    public static final int ERROR_NETWORK = 0;
    public static final int ERROR_TOKEN = 1;
    public static final int ERROR_TOPIC_NOT_ENOUGH = 2;
    public static final int ERROR_INVALID_GRANT = 3;

    public static int parseErrorFromErrorBody(String errorBody) throws UnknownError {
        JSONObject json ;
        try {
            json = new JSONObject(errorBody);
            if (json.has("error")) {
                switch (json.getString("error")) {
                    case "invalid_token" :
                        return ERROR_TOKEN;
                    case "topic.interested.not.enough" :
                       return ERROR_TOPIC_NOT_ENOUGH;
                    case "invalid_grant" :
                        return ERROR_INVALID_GRANT;
                    default:
                        throw  new UnknownError(json.getString("error_description"));
                }
            } else if (json.has("code")) {
                if (json.has("error_message")) {
                    switch (json.getString("error_message")) {
                        case "topic.interested.not.enough" :
                            return ERROR_TOPIC_NOT_ENOUGH;
                        default:
                            throw new UnknownError(json.getString("error_message"));
                    }

                } else {
                    throw new UnknownError("Unknown error");
                }

            } else {
                throw new UnknownError("Unknown error");
            }
        } catch (JSONException e) {
           throw new UnknownError("Parse Json Error body");
        }

    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ERROR_TOKEN})
    @interface ErrorCode{}

}
