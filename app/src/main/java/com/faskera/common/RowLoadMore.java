/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common;

import com.faskera.view.activity.follow.ImlListUser;
import com.faskera.view.activity.home.model.ImlHomeRow;

/**
 * @author DANGNGOCDUC
 * @since 2/21/2017
 */

public class RowLoadMore implements ImlBaseRow, ImlListUser, ImlHomeRow {

    @Override
    public int getType() {
        return TYPE_LOAD_MORE;
    }
}
