/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.apihandle;

import java.util.HashMap;

/**
 * @author DANGNGOCDUC
 * @since 3/15/2017
 */

public class Action {

    public static final String ACTION_LOAD_FEED = "feed";
    public static final String ACTION_VOTE = "vote";
    public static final String ACTION_GETINTERESTTOPIC = "getInterestedTopic";
    private String mActionType;
    private HashMap<String, String> params;
    public String mTag;

}
