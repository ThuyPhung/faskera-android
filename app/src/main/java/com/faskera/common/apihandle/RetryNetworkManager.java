/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.apihandle;

/**
 * @author DANGNGOCDUC
 * @since 3/15/2017
 */

public interface RetryNetworkManager {

    void addAction(Action action);

    void removeActionByTag(String tag);

    void onRetryActions();

}
