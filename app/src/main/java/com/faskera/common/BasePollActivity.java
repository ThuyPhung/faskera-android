/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.faskera.common.apihandle.Action;
import com.faskera.common.asyncdata.ImlFeedProfileCallback;
import com.faskera.common.base.BaseActivity;
import com.faskera.common.base.ObserverAdvance;
import com.faskera.database.FaskeraProvider;
import com.faskera.database.table.FeedTable;
import com.faskera.database.table.UserInfoTable;
import com.faskera.era.R;
import com.faskera.model.user.Profile;
import com.faskera.network.ApiManager;
import com.faskera.network.PutResponse;
import com.faskera.utils.ImageLoader;
import com.faskera.utils.IntentUtils;
import com.faskera.utils.NetworkUtils;
import com.faskera.view.activity.createpoll.CreatePollActivity;
import com.faskera.view.activity.createpoll.GalleryActivity;
import com.faskera.view.activity.follow.ShowListUserView;
import com.faskera.view.activity.home.model.Feed;
import com.faskera.view.activity.home.model.abst.AbsFeedAns;
import com.faskera.view.activity.home.model.abst.FeedAbs;
import com.faskera.view.activity.login.LoginActivity;
import com.faskera.view.activity.mypage.ProfileActivity;
import com.faskera.view.activity.mypage.abs.AbsProfile;
import com.faskera.view.activity.polldetail.PollDetailActivity;
import com.faskera.view.activity.profile.EditProfileActivity;
import com.faskera.view.activity.topic.network.UpdateTopicInterestResponse;
import com.faskera.view.activity.viewoption.ViewOptionActivity;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.Holder;

import java.util.HashMap;

import rx.subscriptions.CompositeSubscription;

import static com.faskera.Constant.REQUEST_EDIT_PROFILE;

/**
 * @author DANGNGOCDUC
 * @since 1/7/2017
 */

public abstract class BasePollActivity extends BaseActivity implements ImlFeedProfileCallback {
    public static HashMap<String, Action> mListAction = new HashMap<>();
    public static final int CODE_CREATE_POLL = 1107;
    public static final String BUNDLE_POLL_CREATE = "poll_create";
    public static final String BUNDLE_TYPE_USER = "BUNDLE_TYPE_USER"; // My Profile or Friend
    public static final String BUNDLE_USER_ID = "BUNDLE_USER_ID"; // My Profile or Friend

    protected AbsProfile mProfile;
    protected CallbackManager callbackManager;
    protected ShareDialog shareDialog;
    protected CompositeSubscription mCompositeSubscription = new CompositeSubscription();
    protected DialogPlus mDialogMenu;
    private View.OnClickListener onClickMenuPoll = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.close_copy_link:

                    if (v.getTag() == null) return;
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Link", (String) v.getTag());
                    clipboard.setPrimaryClip(clip);
                    break;
                case R.id.cancel_action:
                    if (mDialogMenu != null && mDialogMenu.isShowing()) {
                        mDialogMenu.dismiss();
                    }
                    break;
                case R.id.action_report:
                    String id = (String) v.getTag();
                    reportPoll(id);
                    break;
                case R.id.hide_action:
                    Feed.Poll poll = (Feed.Poll) v.getTag();
                    onHidePoll(poll, !poll.mHidden);
                    break;

                case R.id.close_action:
                    poll = (Feed.Poll) v.getTag();
                    onDelPoll(poll.getPollId());
                    break;
            }

            if (mDialogMenu != null && mDialogMenu.isShowing()) {
                mDialogMenu.dismiss();
            }
        }
    };

    protected void showDialogHideOrlClosePolls(boolean isMyPoll, FeedAbs data) {
        Holder holder = new com.orhanobut.dialogplus.ViewHolder(com.faskera.era.R.layout.dialog_setup_poll);
        mDialogMenu = DialogPlus.newDialog(this).setContentHolder(holder).setGravity(Gravity.BOTTOM).setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT).create();
        if (isMyPoll) {
            mDialogMenu.findViewById(R.id.action_report).setVisibility(View.GONE);
            if (data.getPoll().mHidden) {
                ((TextView) mDialogMenu.findViewById(R.id.hide_action)).setText(R.string.action_unhide_poll);
            } else {
                ((TextView) mDialogMenu.findViewById(R.id.hide_action)).setText(R.string.action_hide_poll);

            }
        } else {
            mDialogMenu.findViewById(R.id.hide_action).setVisibility(View.GONE);
            mDialogMenu.findViewById(R.id.close_action).setVisibility(View.GONE);
        }
        mDialogMenu.findViewById(R.id.close_action).setTag(data.getPoll());
        mDialogMenu.findViewById(R.id.close_action).setOnClickListener(onClickMenuPoll);

        mDialogMenu.findViewById(R.id.action_report).setTag(data.getPollId());
        mDialogMenu.findViewById(R.id.action_report).setOnClickListener(onClickMenuPoll);

        mDialogMenu.findViewById(R.id.hide_action).setOnClickListener(onClickMenuPoll);
        mDialogMenu.findViewById(R.id.hide_action).setTag(data.getPoll());

        mDialogMenu.findViewById(R.id.close_copy_link).setTag(data.getPollLink());
        mDialogMenu.findViewById(R.id.close_copy_link).setOnClickListener(onClickMenuPoll);

        mDialogMenu.findViewById(R.id.cancel_action).setOnClickListener(onClickMenuPoll);

        mDialogMenu.show();
    }

    protected void reportPoll(String pollID) {
        if (NetworkUtils.isOnline(this)) {
            if (AppSharePreference.getInstance(this).isUserLoggedIn()) {
                if (mDialogMenu != null && mDialogMenu.isShowing()) {
                    mDialogMenu.dismiss();
                }
                mCompositeSubscription.add(ApiManager.getInstance().reportPoll(this, pollID, new ObserverAdvance<UpdateTopicInterestResponse>() {
                    @Override
                    public void onError(int code) {

                    }

                    @Override
                    public void onSuccess(UpdateTopicInterestResponse data) {

                        Toast.makeText(BasePollActivity.this, getResources().getString(R.string.reportsuccess), Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onCompleted() {

                    }
                }));
            } else {
                IntentUtils.startActivityLogin(null, this);
            }
        } else {
            Toast.makeText(this, R.string.error_connection, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCompositeSubscription.unsubscribe();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        // this part is optional
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK && requestCode == CODE_CREATE_POLL) {
//            Feed.Poll poll = (Feed.Poll) data.getSerializableExtra(BUNDLE_POLL_CREATE);
//            onCreatePollSuccess(poll);
//        }
        if (resultCode == RESULT_OK && requestCode == REQUEST_EDIT_PROFILE) {
            onChangeUserInfo();
        }

    }

    protected void onChangeUserInfo() {

    }

    public abstract void onCreatePollSuccess(Feed.Poll poll);

    public void sharePoll(FeedAbs feed) {
        if (NetworkUtils.isOnline(this)) {
            if (ShareDialog.canShow(ShareLinkContent.class)) {
                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse(feed.getPollLink()))
                        .build();

                shareDialog.show(linkContent);
            }
        } else {
            Toast.makeText(this, R.string.error_connection, Toast.LENGTH_SHORT).show();
        }

    }

    public boolean votePoll(String pollID, String optionID, boolean selected) {

        if (AppSharePreference.getInstance(this).isUserLoggedIn()) {
            if (NetworkUtils.isOnline(this)) {
                mCompositeSubscription.add(ApiManager.getInstance().votePoll(this, pollID, optionID, selected, new ObserverAdvance<UpdateTopicInterestResponse>() {
                    @Override
                    public void onError(int code) {
                       Toast.makeText(getBaseContext(), R.string.data_invalid, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(UpdateTopicInterestResponse data) {

                    }

                    @Override
                    public void onCompleted() {

                    }
                }));
                return true;
            } else {
                return false;
            }
        } else {
            IntentUtils.startActivityLogin(null, this);
            return false;
        }
    }

    //<editor-fold desc="action click">
    @Override
    public void onEditProfile() {
        if (NetworkUtils.isOnline(this)) {
            startActivityForResult(new Intent(this, EditProfileActivity.class), REQUEST_EDIT_PROFILE);
        } else {
            Toast.makeText(this, R.string.error_connection, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFollowClick() {

        if (AppSharePreference.getInstance(this).isUserLoggedIn()) {
            if (onFollowUser(mProfile.getUserId())) {
                ((Profile) mProfile).mIsFollowing = true;
                ContentResolver contentResolver = getContentResolver();
                Cursor cursor = contentResolver.query(FaskeraProvider.CONTENT_URI_USER, null, UserInfoTable._ID + "='" + mProfile.getUserId() + "'", null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    contentResolver.update(FaskeraProvider.CONTENT_URI_USER, ((Profile) mProfile).toContentValue(), UserInfoTable._ID + "='" + ((Profile) mProfile).mUid + "'", null);
                } else {
                    contentResolver.insert(FaskeraProvider.CONTENT_URI_USER, ((Profile) mProfile).toContentValue());
                }
                if (cursor != null && !cursor.isClosed())
                cursor.close();
                contentResolver.notifyChange(FaskeraProvider.CONTENT_URI_USER, null);
            }
        } else {
            IntentUtils.startActivityLogin(null, this);
        }
    }

    @Override
    public void onAgreeUnfollow() {
        super.onAgreeUnfollow();
        ((Profile) mProfile).mIsFollowing = false;
        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(FaskeraProvider.CONTENT_URI_USER, null, UserInfoTable._ID + "='" + mProfile.getUserId() + "'", null, null);
        if (cursor != null && cursor.moveToFirst()) {
            contentResolver.update(FaskeraProvider.CONTENT_URI_USER, ((Profile) mProfile).toContentValue(), UserInfoTable._ID + "='" + ((Profile) mProfile).mUid + "'", null);
        } else {
            contentResolver.insert(FaskeraProvider.CONTENT_URI_USER, ((Profile) mProfile).toContentValue());
        }
        if (cursor != null && !cursor.isClosed())
            cursor.close();
        contentResolver.notifyChange(FaskeraProvider.CONTENT_URI_USER, null);

    }

    @Override
    public void onFollowingClick() {
        if (AppSharePreference.getInstance(this).isUserLoggedIn()) {
            mUserUnfollow = mProfile;
            mDialogConfirmUnfollow.show();
            if (mUserUnfollow.getAvatar() == null || mUserUnfollow.getAvatar().trim().isEmpty()) {
                ImageLoader.loadImageViewWith(this, R.drawable.ic_avatar_default, mAvatarUnfollow);

            } else {
                ImageLoader.loadImageViewWith(this, mUserUnfollow.getAvatar(), mAvatarUnfollow);
            }
            mTextViewUserNameUnfollow.setText(String.format(getResources().getString(R.string.unfollow_user), mUserUnfollow.getName()));
        } else {
            IntentUtils.startActivityLogin(null, this);
        }
    }

    @Override
    public void onViewListFollower(String uid) {

        if (AppSharePreference.getInstance(this).isUserLoggedIn()) {
            Bundle bundle = new Bundle();
            bundle.putInt(ShowListUserView.BUNDLE_TYPE_LIST, UserDefine.STATE_FOLLOW);
            bundle.putString(ShowListUserView.BUNDLE_USER_ID, uid);
            IntentUtils.startActivityViewListUser(bundle, this);
        } else {
            IntentUtils.startActivityLogin(null, this);
        }

    }

    @Override
    public void onViewListFollowing(String uid) {
        if (AppSharePreference.getInstance(this).isUserLoggedIn()) {
            Bundle bundle = new Bundle();
            bundle.putInt(ShowListUserView.BUNDLE_TYPE_LIST, UserDefine.STATE_FOLLWING);
            bundle.putString(ShowListUserView.BUNDLE_USER_ID, uid);
            IntentUtils.startActivityViewListUser(bundle, this);
        } else {
            IntentUtils.startActivityLogin(null, this);
        }

    }

    @Override
    public void onHeaderClick(String id) {

        Bundle bundle = new Bundle();
        if (id.equals(AppSharePreference.getUid(this))) {
            bundle.putInt(ProfileActivity.BUNDLE_TYPE_USER, UserDefine.USER_TYPE_PROFILE);
        } else {
            bundle.putInt(ProfileActivity.BUNDLE_TYPE_USER, UserDefine.USER_TYPE_FRIENDS);
            bundle.putString(BUNDLE_USER_ID, id);
        }

        IntentUtils.startActivityMyPage(bundle, this);
    }

    @Override
    public void onViewPeopleVoted(String mPollId) {

    }

    @Override
    public void onClickCreatePoll(View v) {
        if (!AppSharePreference.getInstance(this).isUserLoggedIn()) {
            startActivity(new Intent(this, LoginActivity.class));
        } else {
            if (v instanceof TextView) {
                Intent intent = new Intent(this, CreatePollActivity.class);
                startActivityForResult(intent, CODE_CREATE_POLL);
            } else {
                Intent intent = new Intent(this, GalleryActivity.class);
                startActivityForResult(intent, CODE_CREATE_POLL);
            }
        }
    }

    @Override
    public void onFeedMenuClick(FeedAbs feed, boolean isMyFeed) {

        showDialogHideOrlClosePolls(isMyFeed, feed);
    }

    @Override
    public void onGotoDetailClick(Feed.Poll pollID) {
        Bundle bundle = new Bundle();
        bundle.putString(PollDetailActivity.BUNDLE_POLL_ID, pollID.mPollID);
        bundle.putSerializable(PollDetailActivity.BUNDLE_POLL, pollID);

        IntentUtils.startActivityDetail(this, bundle);
    }

    @Override
    public void onClickAsw(AbsFeedAns absFeedAns) {
        if (NetworkUtils.isOnline(this)) {

            if (AppSharePreference.getInstance(this).isUserLoggedIn()) {

                boolean multiChoose;
                if (absFeedAns.getPoll() instanceof Feed) {
                    multiChoose = ((Feed) absFeedAns.getPoll()).mPoll.mSetting.mMultiChoice;
                    if (multiChoose) {

                        if (absFeedAns.isSelected()) {
                            votePoll(absFeedAns.getPoll().getPollId(), absFeedAns.getAnsID(), false);
                            ((Feed) absFeedAns.getPoll()).mPoll.mListOptions.get(absFeedAns.getNbSeq()).setSelected(false);
                            ((Feed) absFeedAns.getPoll()).mPoll.mListOptions.get(absFeedAns.getNbSeq()).mResult--;

                            boolean remove = true;
                            for (Feed.Options op : ((Feed) absFeedAns.getPoll()).mPoll.mListOptions) {
                                if (op.isSelected()) {
                                    remove = false;
                                    break;
                                }
                            }
                            if (remove) {
                                ((Feed) absFeedAns.getPoll()).mPoll.mNbUsersAnswered--;
                                ((Feed) absFeedAns.getPoll()).mPoll.votedByMe = false;
                            }

                        } else {
                            votePoll(absFeedAns.getPoll().getPollId(), absFeedAns.getAnsID(), true);
                            ((Feed) absFeedAns.getPoll()).mPoll.mListOptions.get(absFeedAns.getNbSeq()).setSelected(true);
                            ((Feed) absFeedAns.getPoll()).mPoll.mListOptions.get(absFeedAns.getNbSeq()).mResult++;
                            if (!((Feed) absFeedAns.getPoll()).mPoll.votedByMe) {
                                ((Feed) absFeedAns.getPoll()).mPoll.mNbUsersAnswered++;
                                ((Feed) absFeedAns.getPoll()).mPoll.votedByMe = true;
                            }
                        }
                    } else {

                        if (absFeedAns.isSelected()) {
                            votePoll(absFeedAns.getPoll().getPollId(), absFeedAns.getAnsID(), false);
                            ((Feed) absFeedAns.getPoll()).mPoll.mListOptions.get(absFeedAns.getNbSeq()).setSelected(false);
                            ((Feed) absFeedAns.getPoll()).mPoll.mListOptions.get(absFeedAns.getNbSeq()).mResult--;
                            ((Feed) absFeedAns.getPoll()).mPoll.mNbUsersAnswered--;
                            ((Feed) absFeedAns.getPoll()).mPoll.votedByMe = false;

                        } else {
                            votePoll(absFeedAns.getPoll().getPollId(), absFeedAns.getAnsID(), true);

                            for (Feed.Options op : ((Feed) absFeedAns.getPoll()).mPoll.mListOptions) {
                                if (op.isSelected()) {
                                    op.mResult--;
                                    op.setSelected(false);
                                    ((Feed) absFeedAns.getPoll()).mPoll.mNbUsersAnswered--;
                                    break;
                                }
                            }
                            ((Feed) absFeedAns.getPoll()).mPoll.mListOptions.get(absFeedAns.getNbSeq()).setSelected(true);
                            ((Feed) absFeedAns.getPoll()).mPoll.mListOptions.get(absFeedAns.getNbSeq()).mResult++;
                            ((Feed) absFeedAns.getPoll()).mPoll.mNbUsersAnswered++;
                            ((Feed) absFeedAns.getPoll()).mPoll.votedByMe = true;

                        }
                    }

                    ContentResolver contentResolver = getContentResolver();
                    contentResolver.update(FaskeraProvider.CONTENT_URI_FEED, (absFeedAns.getPoll()).toContentValue(true)
                            , FeedTable._ID + "='" + (absFeedAns.getPoll()).getPollId() + "'", null);
                    contentResolver.notifyChange(FaskeraProvider.CONTENT_URI_FEED, null);

                } else if (absFeedAns.getPoll() instanceof Feed.Poll) {
                    multiChoose = ((Feed.Poll) absFeedAns.getPoll()).mSetting.mMultiChoice;
                    if (multiChoose) {

                        if (absFeedAns.isSelected()) {
                            votePoll(absFeedAns.getPoll().getPollId(), absFeedAns.getAnsID(), false);

                            ((Feed.Poll) absFeedAns.getPoll()).mListOptions.get(absFeedAns.getNbSeq()).setSelected(false);
                            ((Feed.Poll) absFeedAns.getPoll()).mListOptions.get(absFeedAns.getNbSeq()).mResult--;
                            boolean remove = true;
                            for (Feed.Options op : ((Feed.Poll) absFeedAns.getPoll()).mListOptions) {
                                if (op.isSelected()) {
                                    remove = false;
                                    break;
                                }
                            }
                            if (remove) {
                                ((Feed.Poll) absFeedAns.getPoll()).mNbUsersAnswered--;
                                ((Feed.Poll) absFeedAns.getPoll()).votedByMe = false;
                            }
                        } else {
                            votePoll(absFeedAns.getPoll().getPollId(), absFeedAns.getAnsID(), true);

                            ((Feed.Poll) absFeedAns.getPoll()).mListOptions.get(absFeedAns.getNbSeq()).setSelected(true);
                            ((Feed.Poll) absFeedAns.getPoll()).mListOptions.get(absFeedAns.getNbSeq()).mResult++;
                            if (!((Feed.Poll) absFeedAns.getPoll()).votedByMe) {
                                ((Feed.Poll) absFeedAns.getPoll()).votedByMe = true;
                                ((Feed.Poll) absFeedAns.getPoll()).mNbUsersAnswered++;
                            }
                        }
                    } else {

                        if (absFeedAns.isSelected()) {
                            votePoll(absFeedAns.getPoll().getPollId(), absFeedAns.getAnsID(), false);

                            ((Feed.Poll) absFeedAns.getPoll()).mListOptions.get(absFeedAns.getNbSeq()).setSelected(false);
                            ((Feed.Poll) absFeedAns.getPoll()).mListOptions.get(absFeedAns.getNbSeq()).mResult--;
                            ((Feed.Poll) absFeedAns.getPoll()).mNbUsersAnswered--;
                            ((Feed.Poll) absFeedAns.getPoll()).votedByMe = false;
                        } else {
                            votePoll(absFeedAns.getPoll().getPollId(), absFeedAns.getAnsID(), true);

                            for (Feed.Options op : ((Feed.Poll) absFeedAns.getPoll()).mListOptions) {
                                if (op.isSelected()) {
                                    op.mResult--;
                                    op.setSelected(false);
                                    ((Feed.Poll) absFeedAns.getPoll()).mNbUsersAnswered--;
                                    break;
                                }
                            }
                            ((Feed.Poll) absFeedAns.getPoll()).mListOptions.get(absFeedAns.getNbSeq()).setSelected(true);
                            ((Feed.Poll) absFeedAns.getPoll()).mListOptions.get(absFeedAns.getNbSeq()).mResult++;
                            ((Feed.Poll) absFeedAns.getPoll()).mNbUsersAnswered++;
                            ((Feed.Poll) absFeedAns.getPoll()).votedByMe = true;
                        }

                    }
                    ContentResolver contentResolver = getContentResolver();
                    contentResolver.update(FaskeraProvider.CONTENT_URI_POLL, (absFeedAns.getPoll()).toContentValue(true)
                            , FeedTable._ID + "='" + (absFeedAns.getPoll()).getPollId() + "'", null);
                    contentResolver.notifyChange(FaskeraProvider.CONTENT_URI_POLL, null);
                }

            } else {
                IntentUtils.startActivityLogin(null, this);
            }
        } else {
            Toast.makeText(this, R.string.error_connection, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClickShare(FeedAbs feedAbs) {
        sharePoll(feedAbs);
    }

    @Override
    public void onViewOptionClick(String optionID) {
        if (AppSharePreference.getInstance(this).isUserLoggedIn()) {

            IntentUtils.startActivityViewVoteOption(ViewOptionActivity.getBundle(optionID), this);
        } else {
            IntentUtils.startActivityLogin(null, this);
        }
    }

    void onHidePoll(Feed.Poll poll, boolean isHide) {
        if (NetworkUtils.isOnline(this)) {
            String stateHide;
            if (isHide) {
                stateHide = "hide";
            } else {
                stateHide = "unhide";
            }

            mCompositeSubscription.add(ApiManager.getInstance().hideAndUnhidePoll(this, poll.getPollId(), stateHide, new ObserverAdvance<PutResponse>() {
                @Override
                public void onError(int code) {
                }

                @Override
                public void onSuccess(PutResponse data) {
                    if (isHide) {
                        Toast.makeText(BasePollActivity.this, R.string.hide_msg_sucees, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(BasePollActivity.this, R.string.unhide_msg_sucees, Toast.LENGTH_SHORT).show();
                    }
                    poll.mHidden = !poll.mHidden;

                }

                @Override
                public void onCompleted() {

                }
            }));
        } else {
            Toast.makeText(this, R.string.error_connection, Toast.LENGTH_SHORT).show();
        }

    }

    void onDelPoll(String pollID) {
        if (NetworkUtils.isOnline(this)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.dialog_warning)
                    .setMessage(R.string.dialog_msg_confirm_del)
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton(R.string.msg_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mCompositeSubscription.add(ApiManager.getInstance().delPoll(BasePollActivity.this, pollID, new ObserverAdvance<PutResponse>() {
                                @Override
                                public void onError(int code) {
                                    onDelPollResult(pollID, false);
                                    Toast.makeText(BasePollActivity.this, R.string.msg_del_faile, Toast.LENGTH_SHORT).show();

                                }

                                @Override
                                public void onSuccess(PutResponse data) {
                                    Toast.makeText(BasePollActivity.this, R.string.msg_del_success, Toast.LENGTH_SHORT).show();
                                    onDelPollResult(pollID, true);
                                }

                                @Override
                                public void onCompleted() {

                                    dialog.dismiss();

                                }
                            }));
                        }
                    }).show();
        } else {
            Toast.makeText(this, R.string.error_connection, Toast.LENGTH_SHORT).show();
        }
    }

    protected void onDelPollResult(String pollID, boolean result) {

    }




}