/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.view;

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * @author DANGNGOCDUC
 * @since 1/6/2017
 */

public class ImageViewAvatar extends SquartImageView {


    public ImageViewAvatar(Context context) {
        super(context);
        initView();
    }

    private void initView() {
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN: {

                                getDrawable().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                                invalidate();
                                break;
                            }
                            case MotionEvent.ACTION_UP:
                            case MotionEvent.ACTION_CANCEL: {

                               getDrawable().clearColorFilter();
                               invalidate();
                                break;
                            }
                        }

                        return false;
                    }
                });



            }

    public ImageViewAvatar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();

    }

    public ImageViewAvatar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();

    }

}
