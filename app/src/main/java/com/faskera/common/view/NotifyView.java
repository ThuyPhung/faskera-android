/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.view;

import android.content.Context;
import android.util.AttributeSet;

/**
 * @author DANGNGOCDUC
 * @since 3/9/2017
 */

public class NotifyView extends AnyTextview {
    public NotifyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public NotifyView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NotifyView(Context context) {
        super(context);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int max = getMeasuredHeight() > getMeasuredWidth() ? getMeasuredHeight() : getMeasuredWidth();

        setMeasuredDimension(max, max);
    }
}
