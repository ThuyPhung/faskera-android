/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.faskera.era.R;

/**
 * @author DANGNGOCDUC
 * @since 8/8/2016
 */
public class AnyTextview extends AppCompatTextView {


    public AnyTextview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);

    }

    public AnyTextview(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context, attrs);
    }

    public AnyTextview(Context context) {
        super(context);
    }

    public void setFonts(String name) {
        Typeface t = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + name);
        setTypeface(t);
    }

    public void init(Context context, AttributeSet attrs) {
        TypedArray typedArray;
        typedArray = context.obtainStyledAttributes(attrs, R.styleable.font);
        String name_font = typedArray.getString(R.styleable.font_fonts);
        typedArray.recycle();
        if (name_font != null) {
            Typeface t = Typeface.createFromAsset(context.getAssets(), "fonts/" + name_font);
            setTypeface(t);
        }
    }
}

