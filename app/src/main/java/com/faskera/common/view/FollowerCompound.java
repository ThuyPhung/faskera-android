/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.faskera.era.R;

/**
 * @author DANGNGOCDUC
 * @since 1/4/2017
 */

public class FollowerCompound extends LinearLayout {

    private TextView mTitle;
    private TextView mSubContent;
    public FollowerCompound(Context context) {
        super(context);
        initView(context);

    }

    public FollowerCompound(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public FollowerCompound(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        LayoutInflater mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mLayoutInflater.inflate(R.layout.view_follower, this);
        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER);
        mTitle = (TextView) findViewById(R.id.title);
        mSubContent = (TextView) findViewById(R.id.sub_content);
    }

    public void setTitle(String title) {
        mTitle.setText(title);
    }

    public void setSubContentRes(int res) {
        mSubContent.setText(getResources().getString(res));
    }

    public void setSubContent(String subContent) {
        mSubContent.setText(subContent);
    }
}
