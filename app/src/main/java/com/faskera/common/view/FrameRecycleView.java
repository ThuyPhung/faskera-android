/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.view;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.faskera.era.R;

/**
 * @author DANGNGOCDUC
 * @since 1/3/2017
 */

public class FrameRecycleView extends FrameLayout {
    protected int mPosX;

    protected boolean isEnableScrollToTop = false;

    protected RecyclerView mRecycleView;

    protected LinearLayout mLoadingView,mRootRecycleView;

    protected LinearLayout mRootContent;

    protected ImageView mImageContent;

    protected SwipeRefreshLayout mSwipeRefreshLayout;

    protected TextView mTextViewContent, mTextAsync;

    public FrameRecycleView(Context context) {
        super(context);
        init(context);
    }

    public FrameRecycleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FrameRecycleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.frame_recycleview, this);
        mRecycleView = (RecyclerView) findViewById(R.id.recycleview);
        mLoadingView = (LinearLayout) findViewById(R.id.view_loading);
        mRootContent = (LinearLayout) findViewById(R.id.frame_recycle_view_content);
        mImageContent = (ImageView) findViewById(R.id.frame_recycle_image_content);
        mRootRecycleView = (LinearLayout) findViewById(R.id.view_recycleview);
        mTextViewContent = (TextView) findViewById(R.id.frame_recycle_text_content);
        mTextAsync = (TextView) findViewById(R.id.txt_async);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        mSwipeRefreshLayout.setEnabled(false);
    }

    public boolean scrollToTop() {
        if (isEnableScrollToTop) {
            if (mPosX == 0) {
                return false;
            } else {
                mRecycleView.smoothScrollBy(0, -mPosX);
                return true;
            }
        } else {
            throw new ExceptionInInitializerError("You have to Enable ScrollToTop");
        }
    }

    public void setRefreshing(boolean refreshing){
        mSwipeRefreshLayout.setRefreshing(refreshing);
    }

    public void setOnSwipeRefreshLayout(SwipeRefreshLayout.OnRefreshListener listener) {
        if (listener != null) {
            mSwipeRefreshLayout.setEnabled(true);
            mSwipeRefreshLayout.setOnRefreshListener(listener);

        } else {
            mSwipeRefreshLayout.setEnabled(false);
        }
    }

    /**
     * this method called after init {@link android.support.v7.widget.RecyclerView.LayoutManager} for {@link RecyclerView}
     */
    public void enableScrollToTop(){
        isEnableScrollToTop = true;
        mRecycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mPosX = mPosX + dy;
            }
        });

    }

    public boolean isRefreshing() {
        return  mSwipeRefreshLayout.isRefreshing();
    }

    public void showRecycleView() {
        mRootRecycleView.setVisibility(View.VISIBLE);
        mRootContent.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.GONE);
    }

    public void showLoading() {
        mRootRecycleView.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.VISIBLE);
        mRootContent.setVisibility(View.GONE);
    }

    public void showContent(int resource, int content) {
        mRootRecycleView.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.GONE);
        mRootContent.setVisibility(View.VISIBLE);
        mImageContent.setImageResource(resource);
        mTextViewContent.setText(getContext().getResources().getString(content));
    }

    public void showContent(int resource, String content) {
        mRootRecycleView.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.GONE);
        mRootContent.setVisibility(View.VISIBLE);
        mImageContent.setImageResource(resource);
        mTextViewContent.setText(content);
    }

    public ImageView getImageContent() {
        return mImageContent;
    }

    public LinearLayout getLoadingView() {
        return mLoadingView;
    }

    public RecyclerView getRecycleView() {
        return mRecycleView;
    }

    public LinearLayout getRootContent() {
        return mRootContent;
    }

    public TextView getTextViewContent() {
        return mTextViewContent;
    }
}
