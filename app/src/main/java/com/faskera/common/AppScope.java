/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * @author DANGNGOCDUC
 * @since 1/3/2017
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)

public @interface AppScope {
}
