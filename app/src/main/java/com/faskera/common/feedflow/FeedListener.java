/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.feedflow;

/**
 * @author DANGNGOCDUC
 * @since 3/3/2017
 */

public interface FeedListener {

    void onSateChange(FeedAction action);

}
