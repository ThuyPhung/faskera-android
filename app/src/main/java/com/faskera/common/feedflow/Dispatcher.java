/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.feedflow;

import java.util.ArrayList;

/**
 * @author DANGNGOCDUC
 * @since 3/2/2017
 */

public class Dispatcher {

    private static ArrayList<FeedListener> mArrayListFeedListeners = new ArrayList<>();

    public static void register(FeedListener target) {
        if (mArrayListFeedListeners.indexOf(target) < 0) {
            mArrayListFeedListeners.add(target);
        }
    }

    public static void unRegister(FeedListener target) {
        if (mArrayListFeedListeners.indexOf(target) >= 0) {
            mArrayListFeedListeners.remove(target);
        }
    }

    public static void sendEvent(FeedListener from, FeedAction action) {
        for (FeedListener subscribe : mArrayListFeedListeners) {
            if (!subscribe.equals(from)) {
                subscribe.onSateChange(action);
            }
        }
    }

}
