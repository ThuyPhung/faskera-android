/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.feedflow;

import android.support.annotation.IntDef;

import com.faskera.view.activity.home.model.abst.FeedAbs;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author DANGNGOCDUC
 * @since 3/2/2017
 */

public class ActionCreator {
    public static final int ADD = 1;
    public static final int REMOVE = 2;
    public static final int CHANGE = 3;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ADD, REMOVE, CHANGE })
    public @interface ActionType {}


    public static FeedAction createActionFeed(@ActionType int type, FeedAbs objetc) {
        return new FeedAction(type, objetc);
    }
}
