/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.feedflow;

/**
 * @author DANGNGOCDUC
 * @since 3/2/2017
 */

public interface Action<T> {

    @ActionCreator.ActionType int getType();

    T getData();
}
