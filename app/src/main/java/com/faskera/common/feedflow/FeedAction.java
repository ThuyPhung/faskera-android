/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.feedflow;

import com.faskera.view.activity.home.model.abst.FeedAbs;

/**
 * @author DANGNGOCDUC
 * @since 3/2/2017
 */

public class FeedAction implements Action<FeedAbs> {
    private FeedAbs mFeed;
    @ActionCreator.ActionType
    private int mTypeAction;

    public FeedAction(@ActionCreator.ActionType int type, FeedAbs feedAbs) {
        mTypeAction = type;
        mFeed = feedAbs;
    }

    @Override
    public int getType() {
        return mTypeAction;
    }

    @Override
    public FeedAbs getData() {
        return mFeed;
    }
}
