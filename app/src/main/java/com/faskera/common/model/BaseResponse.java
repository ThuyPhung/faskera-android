/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author framgia
 * @since 21/02/2017
 */
public class BaseResponse {
    @SerializedName("code")
    public int code;
}
