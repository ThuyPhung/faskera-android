/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common;

import android.os.Handler;
import android.os.HandlerThread;

/**
 * @author DANGNGOCDUC
 * @since 1/10/2017
 */

public class FaskeraContract {

    public static final String CONTENT_AUTHORITY = "com.faskera.database.FaskeraProvider";
    public static final String PATH_FEED = "feed";
    public static final String PATH_USER = "_user";
    public static final String PATH_POLL = "poll";
    public static final String PATH_NOTIFICATION = "notification";


    public static final HandlerThread sWorkerThread = new HandlerThread("faskera-worker");

    static {
        sWorkerThread.start();
    }

    private static final Handler sWorker = new Handler(sWorkerThread.getLooper());


    public static void runOnWorkerThread(Runnable r) {
        if (sWorkerThread.getThreadId() == android.os.Process.myTid()) {
            r.run();
        } else {
            sWorker.post(r);
        }

    }


}
