/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common;

import com.faskera.view.activity.home.model.ImlHomeRow;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public class Divider implements ImlBaseRow, ImlHomeRow {

    @Override
    public int getType() {
        return TYPE_DIVIDER;
    }
}
