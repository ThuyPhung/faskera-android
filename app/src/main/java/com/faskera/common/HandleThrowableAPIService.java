/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common;

import android.content.Context;
import android.widget.Toast;

import java.net.ConnectException;

/**
 * @author DANGNGOCDUC
 * @since 1/3/2017
 */

public class HandleThrowableAPIService {

    public static void handleError(Context context, Throwable e) {
        if (e != null ) {
            if (e instanceof ConnectException) {
                Toast.makeText(context, "Mất kết nối, kiểm tra kết nối của bạn.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Xảy ra lỗi trong quá trình xử lý.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
