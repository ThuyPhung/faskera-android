/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import butterknife.ButterKnife;

public abstract class BaseFragment<T> extends Fragment {

    protected ProgressBar mLoading;
    protected boolean isDestroyView;
    protected Fragment target;
    protected OnCallBackData mOnCallBackData;
    protected FragmentManager mFrgManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View root = inflater.inflate(idRoot(), container, false);
        ButterKnife.bind(this, root);
        initView(root);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData();
    }

    protected abstract int idRoot();

    protected abstract void initView(View root);

    protected abstract void initData();

    public abstract void onRefresh(T t);

    public BaseActivity getBaseActivity() {
        if (getActivity() instanceof BaseActivity) {
            return (BaseActivity) getActivity();
        }
        return null;
    }

    public void goToDone()  {

    }

    public void onBack(){
    }

    public interface OnCallBackData<K>{
        void onCallBack(K t);
    }
}