/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.base;

import com.faskera.common.HandleErrorCodeAPIService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observer;


/**
 * @author DANGNGOCDUC
 * @since 1/11/2017
 */

public abstract class ObserverAdvance<T> implements Observer<JsonObject> {
    private Class<T> persistentClass;

    public Class<T> getPersistentClass() {
        if (persistentClass == null) {
            this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        }
        return persistentClass;
    }

    @Override
    public void onNext(JsonObject jsonObject) {

        if (jsonObject.has("error")) {
            onError(1);
        } else {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            onSuccess(gson.fromJson(jsonObject.toString(), getPersistentClass()));
        }

    }

    @Override
    public void onError(Throwable e) {
        if (e instanceof HttpException) {
            HttpException exception = (HttpException) e;
            JSONObject Json ;
            try {
                Json = new JSONObject(exception.response().errorBody().string());
                if (Json.has("error")) {

                    switch (Json.getString("error")) {
                        case "invalid_token" :
                            onError(HandleErrorCodeAPIService.ERROR_TOKEN);
                            break;
                        case "topic.interested.not.enough" :
                            onError(HandleErrorCodeAPIService.ERROR_TOPIC_NOT_ENOUGH);
                            break;
                        case "invalid_grant" :
                            onError(HandleErrorCodeAPIService.ERROR_INVALID_GRANT);
                            break;
                        default:
                            onError(exception.code());

                    }


                } else {
                    if (exception.code() == 400) {
                        if (Json.has("error_message")) {
                            if (Json.getString("error_message").equals("topic.interested.not.enough")) {
                                onError(HandleErrorCodeAPIService.ERROR_TOPIC_NOT_ENOUGH);
                            } else {
                                onError(exception.code());
                            }

                        } else {
                            onError(exception.code());
                        }
                    } else
                    onError(exception.code());
                }
            } catch (JSONException e1) {
                e1.printStackTrace();
                onError(exception.code());
            } catch (IOException e1) {
                e1.printStackTrace();
                onError(exception.code());
            }

//            HttpException exception = (HttpException) e;
//            if (exception.code() == 401) {
//                try {
//
//                } catch (Exception e1) {
//                    onError(-1);
//                }
//            } else if (exception.code() == 400) {
//                onError(400);
//            } else {
//                onError(-1);
//            }
        } else {
            onError(-1);
        }
        onCompleted();
    }

    public abstract void onError(int code);

    public abstract void onSuccess(T data);
}
