/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.faskera.common.AppSharePreference;
import com.faskera.common.LocaleHelper;
import com.faskera.common.adapter.AbsUserState;
import com.faskera.common.model.BaseResponse;
import com.faskera.era.R;
import com.faskera.model.user.token.OAuthToken;
import com.faskera.network.ApiManager;
import com.faskera.utils.EditTextUtil;
import com.faskera.utils.IntentUtils;
import com.faskera.utils.NetworkUtils;
import com.faskera.view.activity.login.LoginPresenter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.util.Stack;

import butterknife.ButterKnife;
import retrofit2.adapter.rxjava.Result;
import rx.Observer;
import rx.subscriptions.CompositeSubscription;

import static com.faskera.common.HandleErrorCodeAPIService.ERROR_NETWORK;
import static com.faskera.common.HandleErrorCodeAPIService.ERROR_TOKEN;
import static com.faskera.common.HandleErrorCodeAPIService.ERROR_TOPIC_NOT_ENOUGH;

/**
 * @author ANDROID
 * @since 5/25/2016
 */
public abstract class BaseActivity<T> extends AppCompatActivity {

    public static final int UPDATE_TOPIC_REQUEST_CODE = 1004;
    protected FragmentManager mFrgManager;
    protected Stack<BaseFragment> mFragStack;
    protected ProgressDialog mProgressDialog;
    protected String TAG_FRAGMENT;
    protected CompositeSubscription mCompositeSubscription = new CompositeSubscription();
    protected AlertDialog mDialogConfirmUnfollow;
    protected ImageView mAvatarUnfollow;
    protected TextView mTextViewUserNameUnfollow;
    protected AbsUserState mUserUnfollow;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getIdLayout());
        ButterKnife.bind(this);
        initView();
        initData();
        initDialog();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        // overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);

    }

    @Override
    public void finish() {
        super.finish();
        //  overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);

    }

    @Override
    public void onBackPressed() {
        if (!isPopBackStack()) {
            finish();
        } else {
            onBack(null);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
        }
    }

    protected abstract int getIdLayout();

    protected abstract void initView();

    protected abstract void initData();

    public void openActivity(Class<?> pClass) {
        openActivity(pClass, null);
    }

    public void openActivity(Class<?> pClass, Bundle pBundle) {
        Intent intent = new Intent(this, pClass);
        if (pBundle != null) {
            intent.putExtras(pBundle);
        }
        startActivity(intent);
    }

    public void goToFragment(BaseFragment mBaseFragment, Bundle mBundle) {
        if (mBaseFragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            if (!mBaseFragment.getClass().getName().equals(TAG_FRAGMENT)) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                if (mBundle != null) {
                    mBaseFragment.setArguments(mBundle);
                }
                TAG_FRAGMENT = mBaseFragment.getClass().getName();
                fragmentTransaction.replace(R.id.container_frame_edit, mBaseFragment, mBaseFragment.getClass().getName());
                fragmentTransaction.commit();
            }
        }
    }

    public void goToFragment(@NonNull BaseFragment mBaseFragment, Bundle mBundle, boolean isAnimation) {
        hideKeyboard();
        FragmentTransaction trans = mFrgManager.beginTransaction();
        if (mBundle != null) {
            mBaseFragment.setArguments(mBundle);
        }
        if (mFragStack != null && mFragStack.size() >= 1) {
            trans.hide(mFragStack.lastElement());
        }
        mFragStack.push(mBaseFragment);
        trans.add(R.id.container_frame, mBaseFragment,
                mBaseFragment.getClass().getSimpleName());
        trans.commit();
        mFrgManager.executePendingTransactions();
    }

    public void onBack(T t) {
        hideKeyboard();
        FragmentTransaction trans = mFrgManager.beginTransaction();
        if (mFragStack.size() > 1) {
            //trans.setCustomAnimations(R.anim.anim_in_from_left, R.anim.anim_out_to_right);
            trans.remove(mFragStack.pop());
            trans.show(mFragStack.lastElement());
            if (t != null) {
                mFragStack.lastElement().onRefresh(t);
            }
            mFragStack.lastElement().onResume();
            trans.commit();
        } else {
            finish();
            //overridePendingTransition(R.anim.anim_in_from_left, R.anim.anim_out_to_right);
        }
    }

    public void showLoading(String mMessage) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog = new ProgressDialog(this, R.style.MyTheme);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        visibleLoading(true);
    }

    public void showLoading(int id) {
        showLoading(getString(id));
    }

    public void visibleLoading(boolean isShow) {
        if (mProgressDialog != null) {
            if (isShow) {
                mProgressDialog.show();
            } else if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }
    }

    private void hideKeyboard() {
        EditTextUtil.hideKeyboard(this);
        EditTextUtil.hideSoftKeyboard(this);
    }

    private boolean isPopBackStack() {
        return (mFragStack != null && mFragStack.size() >= 1);
    }

    public void update(T t) {
    }

    public void clearBackStackInclusive() {
        TAG_FRAGMENT = null;
        getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.container_frame_edit)).commit();
    }

    protected boolean onFollowUser(String userID) {
        if (AppSharePreference.getInstance(this).isUserLoggedIn()) {
            if (NetworkUtils.isOnline(this)) {
                mCompositeSubscription.add(ApiManager.getInstance().followUser(this, userID, new ObserverAdvance<BaseResponse>() {
                            @Override
                            public void onError(int code) {

                            }

                            @Override
                            public void onSuccess(BaseResponse data) {

                            }

                            @Override
                            public void onCompleted() {

                            }
                        }
                ));
                return true;
            } else {
                return false;
            }
        } else {
            IntentUtils.startActivityLogin(null, this);
            return false;
        }
    }

    protected boolean onUnFollowUser(String userID) {
        if (AppSharePreference.getInstance(this).isUserLoggedIn()) {
            if (NetworkUtils.isOnline(this)) {
                mCompositeSubscription.add(ApiManager.getInstance().unFollowFriend(this, userID, new ObserverAdvance<BaseResponse>() {
                            @Override
                            public void onError(int code) {

                            }

                            @Override
                            public void onSuccess(BaseResponse data) {

                            }

                            @Override
                            public void onCompleted() {

                            }
                        }
                ));
                return true;
            } else {
                return false;
            }
        } else {
            IntentUtils.startActivityLogin(null, this);
            return false;
        }
    }

    public void reNewtoken() {
        ApiManager.getInstance().refreshTokent(LoginPresenter.CLIENT_ID, LoginPresenter.CLIENT_SECRET, "refresh_token"
                , AppSharePreference.getRefreshToken(this), new Observer<Result<JsonObject>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Result<JsonObject> jsonObjectResult) {
                        if (jsonObjectResult.response().body() != null) {
                            try {
                                Gson gson = new GsonBuilder()
                                        .setLenient()
                                        .create();

                                OAuthToken au = gson.fromJson(jsonObjectResult.response().body().toString(), (OAuthToken.class));
                                AppSharePreference.getInstance(BaseActivity.this).storeOAuthToken(au);
                                onAccessTokenChange();

                            } catch (Exception e) {
                                reNewTokenWithFacebook();
                            }

                        } else {
                            reNewTokenWithFacebook();
                        }

                    }
                });
    }

    public void reNewTokenWithFacebook() {
        ApiManager.getInstance().oauthFaceRefresh(LoginPresenter.CLIENT_ID, LoginPresenter.CLIENT_SECRET, LoginPresenter.GRANT_TYPE
                , AppSharePreference.getInstance(this).getFaceToken()
                , LoginPresenter.PROVIDER_ID, new Observer<Result<OAuthToken>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Result<OAuthToken> jsonObjectResult) {
                        if (jsonObjectResult.isError()) {
                            AppSharePreference.getInstance(BaseActivity.this).clearData();
                            IntentUtils.startActivityLogin(null, BaseActivity.this);
                        } else {
                            AppSharePreference.getInstance(BaseActivity.this).storeOAuthToken(jsonObjectResult.response().body());
                            onAccessTokenChange();
                        }


                    }
                });
    }

    protected void onAccessTokenChange() {
    }

    public void handleErrorCode(int error) {
        switch (error) {
            case -1:
                Toast.makeText(this, R.string.error_connection, Toast.LENGTH_SHORT).show();
                break;
            case ERROR_TOKEN:
                    reNewtoken();

                break;
            case ERROR_TOPIC_NOT_ENOUGH:
                IntentUtils.startActivityUpdateTopic(null, this);
                break;

            case ERROR_NETWORK:
                Toast.makeText(this, R.string.error_connection, Toast.LENGTH_SHORT).show();
                break;
            default:
        }

    }

    private void initDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_confirm_unfollow, null, false);
        mAvatarUnfollow = (ImageView) view.findViewById(R.id.avatar);
        mTextViewUserNameUnfollow = (TextView) view.findViewById(R.id.text_name);
        builder.setView(view);
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (onUnFollowUser(mUserUnfollow.getUid())) {
                    onAgreeUnfollow();
                }
                dialog.dismiss();

            }
        });
        mDialogConfirmUnfollow = builder.create();
    }

    public void onAgreeUnfollow() {

    }


}
