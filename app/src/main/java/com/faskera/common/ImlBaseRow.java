/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.common;

/**
 * @author DANGNGOCDUC
 * @since 12/28/2016
 */

public interface ImlBaseRow {

    int TYPE_DIVIDER = 1;

    int TYPE_LOAD_MORE = 0;

    int getType();

}
