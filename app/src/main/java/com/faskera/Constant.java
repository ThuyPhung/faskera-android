/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera;

/**
 * @author theanh
 * @since 3/2/2017
 */
public class Constant {
    public static final String URL_TERM_OF_USE = "https://www.faskera.com/terms-of-use";
    public static final String URL_PRIVACY_POLICY = "https://www.faskera.com/privacy";
    public static final String URL_COMMUNITY_GUIDELINES = "https://www.faskera.com/about";
    public static final int REQUEST_EDIT_PROFILE = 6969;

    public static class Language {
        public static final String EN_US_CODE = "en";
        public static final String VN_CODE = "vi";
    }

    public static class FriendRelation {
        public static final String FACEBOOK_FRIEND = "facebook_friend";
    }

    public static class Notification {
        public static final String ACTION_CREATE = "create";
        public static final String ACTION_SHARE = "share_fb";
        public static final String ACTION_ANSWER = "answer";
        public static final String ACTION_START = "start_using";
        public static final String ACTION_FOLLOW = "follow";
    }
}
