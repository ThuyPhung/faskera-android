/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.database.table;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

/**
 * @author framgia
 * @since 16/02/2017
 */

public class NotificationTable implements BaseColumns {

    public static final String TABLE_NAME = "notification";
    public static final String ACTION = "action";
    public static final String TARGET_TYPE = "target_type";
    public static final String TARGET_POLL = "target_poll";
    public static final String TARGET_USER = "target_user";
    public static final String TIME = "time";
    public static final String DATA = "data";

    public static void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
                + _ID + " TEXT  PRIMARY KEY ,"
                + ACTION + " TEXT,"
                + TARGET_TYPE + " TEXT,"
                + TARGET_POLL + " TEXT,"
                + TARGET_USER + " TEXT,"
                + TIME + " TEXT,"
                + DATA + " TEXT);"
        );

    }
    public static void onUpdate(SQLiteDatabase db, int old, int newv) {
        if (newv > old) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }

    }
}
