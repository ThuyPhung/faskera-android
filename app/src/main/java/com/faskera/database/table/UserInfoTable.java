/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.database.table;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

/**
 * @author DANGNGOCDUC
 * @since 1/13/2017
 */

public class UserInfoTable implements BaseColumns {

    public static final String NAME  = "_user";

    public static final String FOLLOWING  = "following";

    public static final String DATA = "data";

    public static void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + NAME + "("
                + _ID + " TEXT  PRIMARY KEY ,"
                + FOLLOWING + " INTEGER,"
                + DATA + " TEXT);"

        );

    }
    public static void onUpdate(SQLiteDatabase db, int old, int newv) {
        if (newv > old) {
            db.execSQL("DROP TABLE IF EXISTS " + NAME);
            onCreate(db);
        }

    }
}
