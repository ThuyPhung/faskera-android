/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.database.table;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

/**
 * @author DANGNGOCDUC
 * @since 1/21/2017
 */

public class PollTable implements BaseColumns {

    public static final String NAME  = "poll";

    public static final String TIME_CREATE  = "time_create";

    public static final String USER_ID  = "user_id";

    public static final String DATA = "data";

    public static final String OFFLINE = "offline";


    public static final String NUMBER_ITEM = "numberitem";


    public static void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + NAME + "("
                + _ID+ " TEXT  PRIMARY KEY ,"
                + TIME_CREATE + " TEXT,"
                + USER_ID + " TEXT,"
                + NUMBER_ITEM + " INTEGER,"
                + OFFLINE + " INTEGER,"
                + DATA + " TEXT);"
        );

    }

    public static void onUpdate(SQLiteDatabase db, int old, int newv) {
        if (newv > old) {
            db.execSQL("DROP TABLE IF EXISTS " + NAME);
            onCreate(db);
        }

    }
}
