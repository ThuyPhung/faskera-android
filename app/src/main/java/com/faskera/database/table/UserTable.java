/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.database.table;

import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;

import com.faskera.database.FaskeraContract;

/**
 * @author theanh
 * @since 1/12/2017
 */

public class UserTable implements BaseColumns {


    public static final String TABLE_NAME = "user";
    public static final String USER_ID = "id";
    public static final String FULL_NAME = "full_name";
    public static final String PROFILE_PHOTO = "profile_photo";
    public static final String DATA = "data";

    public static final Uri CONTENT_URI =
            FaskeraContract.BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

    public static Uri buildUserUri(String userId) {
        return FaskeraContract.BASE_CONTENT_URI.buildUpon().appendPath(userId).build();
    }

    public static void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
                + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + USER_ID + " TEXT,"
                + FULL_NAME + " TEXT,"
                + PROFILE_PHOTO + " TEXT,"
                + DATA + " TEXT);"
        );
    }

    public static void onUpdate(SQLiteDatabase db, int old, int newv) {
        if (newv > old) {

            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }

    }
}
