/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

/**
 * @author DANGNGOCDUC
 * @since 1/10/2017
 */

public class FaskeraProvider extends ContentProvider {

    private FaskeraDbHelper mOpenHelper;

    public static final Uri CONTENT_URI_FEED = Uri.parse("content://"
            + com.faskera.common.FaskeraContract.CONTENT_AUTHORITY + "/" + com.faskera.common.FaskeraContract.PATH_FEED);

    public static final Uri CONTENT_URI_USER = Uri.parse("content://"
            + com.faskera.common.FaskeraContract.CONTENT_AUTHORITY + "/" + com.faskera.common.FaskeraContract.PATH_USER);

    public static final Uri CONTENT_URI_POLL = Uri.parse("content://"
            + com.faskera.common.FaskeraContract.CONTENT_AUTHORITY + "/" + com.faskera.common.FaskeraContract.PATH_POLL);

    public static final Uri CONTENT_URI_NOTIFICATION = Uri.parse("content://"
            + FaskeraContract.CONTENT_AUTHORITY + "/" + com.faskera.common.FaskeraContract.PATH_NOTIFICATION);


    @Override
    public boolean onCreate() {
        mOpenHelper = new FaskeraDbHelper(getContext());
        return true;
    }


    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SqlArguments args = new SqlArguments(uri, selection, selectionArgs);
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(args.table);
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        return qb.query(db, projection, args.where, args.args, null, null, sortOrder);
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        SqlArguments args = new SqlArguments(uri, null, null);
        if (TextUtils.isEmpty(args.where)) {
            return "vnd.android.cursor.dir/" + args.table;
        } else {
            return "vnd.android.cursor.item/" + args.table;
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {

        SqlArguments args = new SqlArguments(uri);
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final long rowId = dbInsertAndCheck(db, args.table, null, values);
        if (rowId <= 0) return null;
        uri = ContentUris.withAppendedId(uri, rowId);
        // getContext().getContentResolver().notifyChange(uri, null);
        return uri;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        SqlArguments args = new SqlArguments(uri, selection, selectionArgs);
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        return db.delete(args.table, args.where, args.args);

    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SqlArguments args = new SqlArguments(uri, selection, selectionArgs);
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count = db.update(args.table, values, args.where, args.args);
        if (count > 0 && getContext() != null && getContext().getContentResolver()!= null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    private static long dbInsertAndCheck(SQLiteDatabase db, String table, String nullColumnHack, ContentValues values) {
        if (!values.containsKey(BaseColumns._ID)) {
            throw new RuntimeException("Error: attempting to add item without specifying an id");
        }
        return db.insert(table, nullColumnHack, values);
    }


    static class SqlArguments {
        public final String table;
        public final String where;
        public final String[] args;

        SqlArguments(Uri url, String where, String[] args) {
            if (url.getPathSegments().size() == 1) {
                this.table = url.getPathSegments().get(0);
                this.where = where;
                this.args = args;
            } else if (url.getPathSegments().size() != 2) {
                throw new IllegalArgumentException("Invalid URI: " + url);
            } else if (!TextUtils.isEmpty(where)) {
                throw new UnsupportedOperationException("WHERE clause not supported: " + url);
            } else {
                this.table = url.getPathSegments().get(0);
                this.where = "_id=" + ContentUris.parseId(url);
                this.args = null;
            }
        }

        SqlArguments(Uri url) {
            if (url.getPathSegments().size() == 1) {
                table = url.getPathSegments().get(0);
                where = null;
                args = null;
            } else {
                throw new IllegalArgumentException("Invalid URI: " + url);
            }
        }
    }
}
