/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.faskera.database.table.FeedTable;
import com.faskera.database.table.NotificationTable;
import com.faskera.database.table.PollTable;
import com.faskera.database.table.UserInfoTable;

/**
 * @author DANGNGOCDUC
 * @since 1/10/2017
 */

public class FaskeraDbHelper extends SQLiteOpenHelper {

    static final String DATABASE_NAME = "faskera.db";
    private static final int DATABASE_VERSION = 3;


    public FaskeraDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        FeedTable.onCreate(db);
        UserInfoTable.onCreate(db);
        PollTable.onCreate(db);
        NotificationTable.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        FeedTable.onUpdate(db, oldVersion, newVersion);
        UserInfoTable.onUpdate(db, oldVersion, newVersion);
        PollTable.onUpdate(db, oldVersion, newVersion);
        NotificationTable.onUpdate(db, oldVersion, newVersion);
    }
}
