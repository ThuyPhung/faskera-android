/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera.database;

import android.net.Uri;

/**
 * @author theanh
 * @since 1/12/2017
 */

public class FaskeraContract {

    public static final String CONTENT_AUTHORITY = "com.faskera";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

}
