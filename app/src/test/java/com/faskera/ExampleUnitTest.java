/*
 * Copyright (C) Faskera, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.faskera;

import android.text.format.DateUtils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    public void testDate() {
        long MINUTE = 60000;
        long HOUR = MINUTE * 60;
        long DAY = HOUR * 24;
        long WEEK = DAY * 7;
        long MONTH = WEEK * 4;

        //     PrettyTime p = new PrettyTime();

        DateTime today = new DateTime();

        DateTime dateTime = new DateTime();
        System.out.println(today);
        System.out.println(dateTime.plusHours(-70));
        long dif = today.getMillis() - dateTime.plusHours(-500).getMillis();

        DateTimeFormatter HourMinuteFmt = DateTimeFormat.forPattern("hh:mm a").withLocale(Locale.getDefault());
        DateTimeFormatter DayOfWeekFmt = DateTimeFormat.forPattern("EEE").withLocale(Locale.getDefault());
        DateTimeFormatter DateMonthFmt = DateTimeFormat.forPattern("MMM dd").withLocale(Locale.getDefault());

        if (dif < MINUTE)
            System.out.println("Just now");
        else if (dif < HOUR) {
            System.out.println(DateTimeFormat.forPattern("mm").print(new DateTime(dif)) + " minutes ago");
        } else if (dif < DAY) {
            System.out.println(DateTimeFormat.forPattern("HH").print(new DateTime(dif)) + " hour ");
        } else if (dif < WEEK) {
            if (TimeUnit.MILLISECONDS.toDays(dif) == 1)
                System.out.println("Yesterday at " + HourMinuteFmt.print(new DateTime(dif)));
            else if (TimeUnit.MILLISECONDS.toDays(dif) <= 7)
                System.out.println(DayOfWeekFmt.print(new DateTime(dif)) + " at " + HourMinuteFmt.print(new DateTime(dif)));
        } else
            System.out.println(DateMonthFmt.print(new DateTime(dif)) + " at " + HourMinuteFmt.print(new DateTime(dif)));

    }

    @Test
    public void testRelativeTime() {
        System.out.println(
                DateUtils.getRelativeTimeSpanString(Long.valueOf("1486458758481"), System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS) + "");

    }
}